export const environment = {
  production: true,
  SERVER_ERROR_MSG: "Internal Server Error. Please Contact IT Support Team.",
  docUrl : 'http://103.213.115.187:9010/api/v1/files/',  // Document Url


  baseUrl: "http://103.213.115.187:9010/api/v1/",    // Production IP

  //SYSTEM TYPES DROPDOWN CODES
  ASSET_TYPE: "ATD",
  BUSINESS_TYPE: "BTD",
  BASE_STATION_TYPE: "BSTD",
  CATEGORY_TYPE: "CYTD",
  CHANNEL_TYPE: "CTD",
  CONTAINER_TYPE: "CND",
  CITY_TYPE: "CTTD",
  CONTRACT_TYPE: "CRTD",
  COMPANY_DOCUMENT_TYPE: "CDD",
  CUSTOMER_TYPE: "CUTD",
  DEPARTMENT_TYPE: "DTD",
  EMPLOYEE_DOCUMENT_TYPE: "EDD",
  JOB_STATUS_TYPE: "JSD",
  LEASE_TYPE: "LTD",
  MAKE_TYPE: "MTD",
  POSITION_TYPE: "PTD",
  PAYMENT_MODE_TYPE: "PMD",
  RWB_STATUS_TYPE: "RSD",
  STOP_TYPE: "SPD",
  USER_PRIVILEGE_TYPE: "UPD",
  TRAILER_TYPE: "TTD",
  TRAILER_SIZE_TYPE: "TSD",
  FORMATION_TYPE: "FTD",
  FUEL_TANK_TYPE: "FTT",
  SUPPLIER_TYPE: "STD",
  EMPLOYEE_TYPE: "ETD",
  VEHICLE_OWNERSHIP_TYPE: "VOT",
  INCOME_TYPE: "ITD",
  EXPENSE_TYPE: "XTD",
  COMPANY_TYPE: "COTD",
  STATE_TYPE: "STTD",
  REIMBURSEMENT_STATUS_TYPE: "RITD",
  INVOICE_STATUS_TYPE : "INTD",
  AREA_TYPE : "ARTD",
  PAYMENT_STATUS_TYPE : "PSTD",
  ITEM_TYPE : "ITMD",
	ITEM_CATEGORY : "ITCT",
  BRAND : "BRD",
  ITEM_UNIT : "ITUT",
  PURCHASE_ORDER_STATUS : "POS",
  WORK_ORDER_STATUS : "WOS",
	WORK_ORDER_CATEGORY : "WOC",
	WORK_ORDER_SUB_CATEGORY : "WOSC",
	PRIORITY : "PRTD",

  //SYSTEM TYPES DROPDOWN VARIABLES
  SystemTypes_SUPPLIER: "Supplier",
  SystemTypes_DRIVER: "Driver",
  SystemTypes_OWNED: "Owned",
  SystemTypes_RENTAL: "Rental",
  SystemTypes_CLOSE: "Close",
  SystemTypes_INPROGRESS: "In Progress",
  SystemTypes_OPEN: "Open",
  SystemTypes_CONTRACTUAL: "Contractual",
  SystemTypes_PURCHASED: "Purchased",
  SystemTypes_DISPATCHED: "Dispatched",
  SystemTypes_EMPTY: "Empty",
  SystemTypes_MAINTENANCE: "Maintenance",
	SystemTypes_FUEL: "Fuel",
  SystemTypes_COMPANY: "Company",
  SystemTypes_INDIVIDUAL: "Individual",
  SystemTypes_DEDICATED: "Dedicated",
  SystemTypes_PERTON: "Per Ton",
  SystemTypes_BOOKED: "Booked",
  SystemTypes_FREIGHTRATE: "Freight Rate",
  SystemTypes_REIMBURSEMENT_SENT: "Sent",
  SystemTypes_REIMBURSEMENT_REIMBURSED: "Reimbursed",
  SystemTypes_REIMBURSEMENT_PENDING: "Pending",
  SystemTypes_Trailer: "trailers",
  SystemTypes_Tractor: "tractors",
  SystemTypes_PURCHASE_RECEIVED: "Received",

  SystemTypes_Date_Format: "yyyy-MM-dd",
};
