import { Component, OnInit, Optional, Inject, Type } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BackEnd2Service } from 'app/services/back-end2.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { environment } from 'environments/environment';
import { data } from 'jquery';
import { ErrorModalComponent } from '../error-modal/error-modal.component';
import { SuccessModalComponent } from '../success-modal/success-modal.component';
import { InfoModalComponent } from '../info-modal/info-modal.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-category-brand',
  templateUrl: './add-category-brand.component.html',
  styleUrls: ['./add-category-brand.component.scss']
})
export class AddCategoryBrandComponent implements OnInit {

  addOptionForm: FormGroup;
  displayData = null;
  brandCode = environment.BRAND;
  categoryCode = environment.ITEM_CATEGORY;


  constructor(public dialog: MatDialog,
		public backendService: BackEnd2Service,
		public errorMsgService: ErrorMessageService,
		public dialogRef: MatDialogRef<AddCategoryBrandComponent>,
		@Optional() @Inject(MAT_DIALOG_DATA) public data,) {
      this.displayData = data;
   } 

  ngOnInit() {
    this.addOptionForm = new FormGroup({
      name: new FormControl(null, Validators.required)
    });
  }

  add() {
    if(this.addOptionForm.invalid){
      return;
    }
    let data = {
      id: null,
      value: this.addOptionForm.value.name,
      status: "Active",
      code: null,
      description: null
    }
    if(this.displayData == "Brand"){
      data.code = this.brandCode;
      data.description =" Brand Type";
      this.backendService.addBrand(data).subscribe(res =>{
        if (res.success) {
          this.dialog.closeAll();
					this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("ITEM DELETE RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});
    }
    if(this.displayData == "Item Category"){
      data.code = this.categoryCode;
      data.description =" Item Category Type";
      this.backendService.addItemCategories(data).subscribe(res =>{
        if (res.success) {
          this.dialog.closeAll();
					this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("ITEM DELETE RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});
    }
  }

  	/* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger info dialog
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}
}
