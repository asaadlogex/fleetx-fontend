import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ErrorModalComponent } from '../error-modal/error-modal.component';
import { SuccessModalComponent } from '../success-modal/success-modal.component';
import { InfoModalComponent } from '../info-modal/info-modal.component';

@Component({
  selector: 'app-add-area-modal',
  templateUrl: './add-area-modal.component.html',
  styleUrls: ['./add-area-modal.component.scss']
})
export class AddAreaModalComponent implements OnInit {

  addAreaForm: FormGroup;
  citiesDropDown: any;
  
  constructor(public backendService: BackEndService,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {
    this.addAreaForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      code: new FormControl(null, Validators.required),
      city: new FormControl(null, Validators.required),
    })
   }

   ngOnInit() {
    // this.getStateDropdown(this.stateTypeDropdownCode)
    if(this.backendService.citiesData != null){
      this.citiesDropDown = this.backendService.citiesData;
    }
  }

  add() {
    if (this.addAreaForm.invalid) {
      return;
    }
    let data = {
      id: null,
      code: this.addAreaForm.value.code,
      value: this.addAreaForm.value.name,
      state: this.addAreaForm.value.city.state,
      description: "Area Type",
      status: "Active"
    }
    let params = {
      city: this.addAreaForm.value.city.code,
      area : true,
    }
    this.backendService.addCityDropdownService(data, params).subscribe(res => {
      if(res.success){
        sessionStorage.setItem("code", params.city);
        this.dialog.closeAll();
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      // console.log("Area ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
