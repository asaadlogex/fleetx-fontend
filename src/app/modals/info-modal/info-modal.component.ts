import { Component, OnInit } from '@angular/core';
import { ErrorMessageService } from 'app/services/error-message.service';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.scss']
})
export class InfoModalComponent implements OnInit {

  infoMsg : string = '';

  constructor(public errMsgService : ErrorMessageService) { }

  ngOnInit() {
    this.errMsgService.infoMessage.subscribe(message => {
      this.infoMsg = message;
    })
  }
}
