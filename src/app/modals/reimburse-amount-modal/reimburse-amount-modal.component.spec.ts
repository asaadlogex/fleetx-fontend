import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReimburseAmountModalComponent } from './reimburse-amount-modal.component';

describe('ReimburseAmountModalComponent', () => {
  let component: ReimburseAmountModalComponent;
  let fixture: ComponentFixture<ReimburseAmountModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReimburseAmountModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReimburseAmountModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
