import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { DatePipe } from "@angular/common";
import { BackEndService } from "app/services/back-end.service";
import { ErrorMessageService } from "app/services/error-message.service";
import { HttpErrorResponse } from "@angular/common/http";
import { environment } from "environments/environment";
import { InfoModalComponent } from "../info-modal/info-modal.component";
import { ErrorModalComponent } from "../error-modal/error-modal.component";
import { SuccessModalComponent } from "../success-modal/success-modal.component";

@Component({
	selector: "app-reimburse-amount-modal",
	templateUrl: "./reimburse-amount-modal.component.html",
	styleUrls: ["./reimburse-amount-modal.component.scss"],
})
export class ReimburseAmountModalComponent implements OnInit {
	reimburseAmountForm: FormGroup;
	billIds: any = [];
	amount: any;

	constructor(public dialog: MatDialog,
		public datePipe: DatePipe,
		public backendService: BackEndService,
		public errorMsgService: ErrorMessageService) {
	}

	ngOnInit() { 
		this.billIds = this.backendService.reimbursedIds;
		this.amount = this.backendService.reimbursedData;
		this.backendService.reimbursedData = null;
		this.backendService.reimbursedIds = null;

		this.reimburseAmountForm = new FormGroup({
			paymentDate: new FormControl(null, Validators.required),
			refNo: new FormControl(null, Validators.required),
			totalAmount: new FormControl(this.amount, Validators.required),
			verifiedBy: new FormControl(null, Validators.required),
		});

	}

	reimburse() {
		// console.log(this.reimburseAmountForm.value);
		if (this.reimburseAmountForm.invalid) {
			return;
		}
		let data = {
			reimbursementInfoId: null,
			referenceNumber: this.reimburseAmountForm.value.refNo,
			reimbursementDate: this.datePipe.transform(this.reimburseAmountForm.value.paymentDate, environment.SystemTypes_Date_Format),
			reimbursementAmount: this.reimburseAmountForm.value.totalAmount,
			reimbursementNote: this.reimburseAmountForm.value.verifiedBy,
			jobIds: this.billIds
		}
		this.sendJobForApproval(data);
	}

	sendJobForApproval(val) {
		this.backendService.sendJobApprovalService(val).subscribe(
			(res) => {
				if (res.success) {
					sessionStorage.setItem("amount", "paid");
					this.backendService.reimbursedIds = val.jobIds;
					this.dialog.closeAll();
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("SEND REIMBURSEMNT APPROVAL RESPONSE", res);
			},
			(err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
					this.openInfoDialog();
				}
			});

	}

	/************************ function to trigger info dialog ****************************/
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}
}
