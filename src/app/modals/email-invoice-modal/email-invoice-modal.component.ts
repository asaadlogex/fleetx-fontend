import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
	selector: "app-email-invoice-modal",
	templateUrl: "./email-invoice-modal.component.html",
	styleUrls: ["./email-invoice-modal.component.scss"],
})
export class EmailInvoiceModalComponent implements OnInit {
	emailInvoiceForm: FormGroup;
	demo: any;

	constructor() {
		this.emailInvoiceForm = new FormGroup({
			to: new FormControl(null),
			cc: new FormControl(null),
			bcc: new FormControl(null),
			subject: new FormControl(null),
			body: new FormControl(null),
		});
	}

	ngOnInit() {}

	send() {
		console.log(this.emailInvoiceForm.value, this.demo);
	}
}
