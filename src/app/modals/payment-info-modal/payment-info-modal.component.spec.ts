import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentInfoModalComponent } from './payment-info-modal.component';

describe('PaymentInfoModalComponent', () => {
  let component: PaymentInfoModalComponent;
  let fixture: ComponentFixture<PaymentInfoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentInfoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
