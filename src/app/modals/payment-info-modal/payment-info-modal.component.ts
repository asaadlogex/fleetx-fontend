import { Component, OnInit, ViewChild, Optional, Inject } from "@angular/core";
import { MatTableDataSource, MatPaginator, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-payment-info-modal",
  templateUrl: "./payment-info-modal.component.html",
  styleUrls: ["./payment-info-modal.component.scss"],
})
export class PaymentInfoModalComponent implements OnInit {
  displayedColumns: string[] = ["date", "refNo", "amount", "verifiedBy"];
  dataSource : MatTableDataSource<any>;
  invData : any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
		public dialogRef: MatDialogRef<PaymentInfoModalComponent>,
		@Optional() @Inject(MAT_DIALOG_DATA) public data, ) {
      this.invData = data;
      this.dataSource = this.invData;
    }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}
