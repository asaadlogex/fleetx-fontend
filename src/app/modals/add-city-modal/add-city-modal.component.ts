import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from 'environments/environment';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from '../error-modal/error-modal.component';
import { SuccessModalComponent } from '../success-modal/success-modal.component';
import { InfoModalComponent } from '../info-modal/info-modal.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-city-modal',
  templateUrl: './add-city-modal.component.html',
  styleUrls: ['./add-city-modal.component.scss']
})
export class AddCityModalComponent implements OnInit {

  addCityForm: FormGroup;
  stateDropDown: any

  constructor(public backendService: BackEndService,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {
    this.addCityForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      code: new FormControl(null, Validators.required),
      state: new FormControl(null, Validators.required),
    })
  }

  ngOnInit() {
    // this.getStateDropdown(this.stateTypeDropdownCode)
    if(this.backendService.stateData != null){
      this.stateDropDown = this.backendService.stateData;
    }
  }

  add() {
    if (this.addCityForm.invalid) {
      return;
    }
    let data = {
      id: null,
      code: this.addCityForm.value.code,
      value: this.addCityForm.value.name,
      state: this.addCityForm.value.state,
      description: "City Type",
      status: "Active"
    }
    let params = {
      area : false,
    }
    this.backendService.addCityDropdownService(data, params).subscribe(res => {
      if(res.success){
        this.dialog.closeAll();
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      // console.log("CITY ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}

