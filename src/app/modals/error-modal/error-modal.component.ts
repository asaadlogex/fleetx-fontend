import { Component, OnInit } from '@angular/core';
import { ErrorMessageService } from 'app/services/error-message.service';

@Component({
  selector: 'app-error-modal',
  templateUrl: './error-modal.component.html',
  styleUrls: ['./error-modal.component.scss']
})
export class ErrorModalComponent implements OnInit {

  errorMsg : string = '';

  constructor(public errMsgService : ErrorMessageService) { }

  ngOnInit() {
    this.errMsgService.errorMessage.subscribe(message => {
      this.errorMsg = message;
    })
  }

}
