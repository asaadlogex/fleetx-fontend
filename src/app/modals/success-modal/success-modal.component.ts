import { Component, OnInit } from '@angular/core';
import { ErrorMessageService } from 'app/services/error-message.service';

@Component({
  selector: 'app-success-modal',
  templateUrl: './success-modal.component.html',
  styleUrls: ['./success-modal.component.scss']
})
export class SuccessModalComponent implements OnInit {

  successMsg : string = '';

  constructor(public errMsgService : ErrorMessageService) { }

  ngOnInit() {
    this.errMsgService.successMessage.subscribe(message => {
      this.successMsg = message;
    })
  }

}
