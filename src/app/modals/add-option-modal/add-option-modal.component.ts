import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-option-modal',
  templateUrl: './add-option-modal.component.html',
  styleUrls: ['./add-option-modal.component.scss']
})
export class AddOptionModalComponent implements OnInit {
  addOptionForm: FormGroup;


  constructor() {
    this.addOptionForm = new FormGroup({
      name: new FormControl(null),
      code: new FormControl(null)
    });
   }

  ngOnInit() {
  }

  add() {
    console.log(this.addOptionForm.value);
  }

}
