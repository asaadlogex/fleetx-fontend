import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckPendingModalComponent } from './check-pending-modal.component';

describe('CheckPendingModalComponent', () => {
  let component: CheckPendingModalComponent;
  let fixture: ComponentFixture<CheckPendingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckPendingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckPendingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
