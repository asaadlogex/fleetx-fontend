import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material";
import { BackEndService } from "app/services/back-end.service";

@Component({
	selector: "app-check-pending-modal",
	templateUrl: "./check-pending-modal.component.html",
	styleUrls: ["./check-pending-modal.component.scss"],
})
export class CheckPendingModalComponent implements OnInit {
	displayedColumns: string[] = ["customer", "perTrip", "perTon", "dedicated"];
	dataSource: MatTableDataSource<any>;
	objectKeys = Object.keys;

	constructor(private backEndService: BackEndService) {
		// let data = this.backEndService.pendingData;
		// if(data != null)
		// this.dataSource = data;
		// console.log(data);
	}

	ngOnInit() {
		let data = this.backEndService.pendingData;
		if (data != null) {
			this.dataSource = data;
		}
	}
}

export interface InvoicesTableData {
	customer: string;
	invoices: string;
}

const INVOICES_DATA: InvoicesTableData[] = [
	{ customer: "Edgeon", invoices: "Hydrogen" },
	{ customer: "Edgeon", invoices: "Helium" },
	{ customer: "Edgeon", invoices: "Lithium" },
	{ customer: "Edgeon", invoices: "Beryllium" },
	{ customer: "Edgeon", invoices: "Boron" },
	{ customer: "Edgeon", invoices: "Carbon" },
	{ customer: "Edgeon", invoices: "Nitrogen" },
	{ customer: "Edgeon", invoices: "Oxygen" },
	{ customer: "Edgeon", invoices: "Fluorine" },
	{ customer: "Edgeon", invoices: "Neon" },
];
