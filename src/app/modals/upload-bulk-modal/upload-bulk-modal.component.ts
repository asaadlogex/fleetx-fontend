import { Component, OnInit, Optional, Inject } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { BackEndService } from "app/services/back-end.service";
import { environment } from "environments/environment";
import { ErrorModalComponent } from "../error-modal/error-modal.component";
import { SuccessModalComponent } from "../success-modal/success-modal.component";
import { InfoModalComponent } from "../info-modal/info-modal.component";
import { ErrorMessageService } from "app/services/error-message.service";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
	selector: "app-upload-bulk-modal",
	templateUrl: "./upload-bulk-modal.component.html",
	styleUrls: ["./upload-bulk-modal.component.scss"],
})
export class UploadBulkModalComponent implements OnInit {
	bulkUploadForm: FormGroup;
	tyresUploadForm: FormGroup;
	checkAsset: string;
	showTyres = true;

	constructor(public dialogRef: MatDialogRef<UploadBulkModalComponent>,
		@Optional() @Inject(MAT_DIALOG_DATA) public data,
		public backendService: BackEndService,
		public dialog: MatDialog,
		public errorMsgService: ErrorMessageService, ) {

		this.bulkUploadForm = new FormGroup({ docs: new FormControl(null) });
		this.tyresUploadForm = new FormGroup({ docsTyre: new FormControl(null) });
		this.checkAsset = data;
	}

	ngOnInit() {
		if (this.checkAsset == "Employees") {
			this.showTyres = false;
		} else {
			this.showTyres = true;
		}
	}

	add() {
		const formdata = new FormData();
		formdata.append('file', this.bulkUploadForm.value.docs._files[0]);

		if (this.checkAsset == "Tractor") {
			this.backendService.uploadBulkFileService(formdata, environment.SystemTypes_Tractor).subscribe(res => {
				console.log(res);
				if (res.success) {
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("EMPLOYEE ADD RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});
		}

		if (this.checkAsset == "Trailer") {
			this.backendService.uploadBulkFileService(formdata, environment.SystemTypes_Trailer).subscribe(res => {
				console.log(res);
				if (res.success) {
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("EMPLOYEE ADD RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});
		}

		if (this.checkAsset == "Employees") {
			this.backendService.uploadEmpBulkFileService(formdata).subscribe(res => {
				console.log(res);
				if (res.success) {
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("EMPLOYEE ADD RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});
		}
	}

	addTyre() {
		const formdata = new FormData();
		formdata.append('file', this.tyresUploadForm.value.docsTyre._files[0]);
		this.backendService.uploadBulkFileService(formdata, "tyres").subscribe(res => {
			console.log(res);
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("EMPLOYEE ADD RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});

	}

	/* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger info dialog
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}
}
