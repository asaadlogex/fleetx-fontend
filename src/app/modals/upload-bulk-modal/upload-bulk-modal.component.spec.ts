import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadBulkModalComponent } from './upload-bulk-modal.component';

describe('UploadBulkModalComponent', () => {
  let component: UploadBulkModalComponent;
  let fixture: ComponentFixture<UploadBulkModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadBulkModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadBulkModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
