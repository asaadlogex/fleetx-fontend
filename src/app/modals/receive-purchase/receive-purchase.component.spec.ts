import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivePurchaseComponent } from './receive-purchase.component';

describe('ReceivePurchaseComponent', () => {
  let component: ReceivePurchaseComponent;
  let fixture: ComponentFixture<ReceivePurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceivePurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivePurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
