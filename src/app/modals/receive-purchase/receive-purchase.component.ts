import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { BackEnd2Service } from 'app/services/back-end2.service';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ErrorModalComponent } from '../error-modal/error-modal.component';
import { SuccessModalComponent } from '../success-modal/success-modal.component';
import { InfoModalComponent } from '../info-modal/info-modal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-receive-purchase',
  templateUrl: './receive-purchase.component.html',
  styleUrls: ['./receive-purchase.component.scss']
})
export class ReceivePurchaseComponent implements OnInit {

  receiveOrderForm: FormGroup;
  receiveData;
  Items: FormArray;
  temp: any;

  constructor(private formBuilder: FormBuilder,
    public dialog: MatDialog,
    public backend2Service: BackEnd2Service,
    public errorMsgService: ErrorMessageService,
    public datePipe: DatePipe,
    private router: Router,
    public dialogRef: MatDialogRef<ReceivePurchaseComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data, ) {

    this.receiveData = data;
  }

  ngOnInit() {

    this.receiveOrderForm = new FormGroup({
      orderId: new FormControl(null),
      orderDate: new FormControl(null, Validators.required),
      itemList: new FormArray([]),
    });

    this.receiveOrderForm.get("orderId").setValue(this.receiveData.purchaseOrderId);

    if (this.receiveData.itemPurchaseDetail !== null) {
      for (let i = 0; i < this.receiveData.itemPurchaseDetail.length; i++) {
        this.Items = this.receiveOrderForm.get('itemList') as FormArray;
        this.Items.push(this.formBuilder.group({
          itemPurchaseDetailId: [this.receiveData.itemPurchaseDetail[i].itemPurchaseDetailId],
          item: [this.receiveData.itemPurchaseDetail[i].item],
          qtyReceived: [this.receiveData.itemPurchaseDetail[i].qtyReceived, Validators.required],
        }));
      }
    }
    // console.log("item", this.receiveOrderForm)
  }

  submit() {
    if (this.receiveOrderForm.invalid) {
      return;
    }

    let orderData = {
      purchaseOrderId: this.receiveOrderForm.value.orderId,
      date: this.datePipe.transform(this.receiveOrderForm.value.orderDate, environment.SystemTypes_Date_Format),
      itemReceived: this.receiveOrderForm.value.itemList,
    }

    this.receiveOrder(orderData)

  }

  receiveOrder(data) {
    this.backend2Service.receivePurchaseOrderService(data, data.purchaseOrderId).subscribe(res => {
      if (res.success) {
        this.dialog.closeAll();
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.router.navigate(['purchaseOrder']);
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("ORDER UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }


}
