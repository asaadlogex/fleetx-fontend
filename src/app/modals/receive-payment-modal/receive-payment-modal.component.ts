import { Component, OnInit, Optional, Inject } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { BackEndService } from "app/services/back-end.service";
import { ErrorMessageService } from "app/services/error-message.service";
import { DatePipe } from "@angular/common";
import { HttpErrorResponse } from "@angular/common/http";
import { environment } from "environments/environment";
import { ErrorModalComponent } from "../error-modal/error-modal.component";
import { SuccessModalComponent } from "../success-modal/success-modal.component";
import { InfoModalComponent } from "../info-modal/info-modal.component";
import { CheckPendingModalComponent } from "../check-pending-modal/check-pending-modal.component";

@Component({
	selector: "app-receive-payment-modal",
	templateUrl: "./receive-payment-modal.component.html",
	styleUrls: ["./receive-payment-modal.component.scss"],
})
export class ReceivePaymentModalComponent implements OnInit {
	receivePaymentForm: FormGroup;
	invoiceData;

	constructor(public dialog: MatDialog,
		public backendService: BackEndService,
		public errorMsgService: ErrorMessageService,
		public datePipe: DatePipe,
		public dialogRef: MatDialogRef<ReceivePaymentModalComponent>,
		@Optional() @Inject(MAT_DIALOG_DATA) public data, ) {

		this.invoiceData = data;
		this.receivePaymentForm = new FormGroup({
			paymentDate: new FormControl(null, Validators.required),
			refNo: new FormControl(null),
			totalAmount: new FormControl(this.invoiceData.totalAmount, Validators.required),
			verifiedBy: new FormControl(null, Validators.required),
		});
	}

	ngOnInit() {
		this.receivePaymentForm = new FormGroup({
			paymentDate: new FormControl(null, Validators.required),
			refNo: new FormControl(null),
			totalAmount: new FormControl(this.invoiceData.remainingAmount, Validators.required),
			verifiedBy: new FormControl(null, Validators.required),
		});
	}

	receive() {
		// console.log(this.receivePaymentForm.value);
		let data = {
			invoicePaymentId: null,
			referenceNumber: this.receivePaymentForm.value.refNo,
			paymentDate: this.datePipe.transform(this.receivePaymentForm.value.paymentDate, environment.SystemTypes_Date_Format),
			paymentAmount: this.receivePaymentForm.value.totalAmount,
			paymentNote: this.receivePaymentForm.value.verifiedBy,
			invoiceId: null,
			dhInvoiceId: null
		}
		if (this.invoiceData.type == "Normal") {
			data.invoiceId = this.invoiceData.invoiceId;
			this.backendService.InvoicePaymentService(data).subscribe(res => {
				if (res.success) {
					this.receivePaymentForm.reset();
					this.dialog.closeAll();
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
				} else {
					this.dialog.closeAll();
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
			},
				(err: HttpErrorResponse) => {
					if (err.status == 500) {
						this.dialog.closeAll();
						this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
						this.openInfoDialog();
					}
				});
		}

		if (this.invoiceData.type == "DH") {
			data.dhInvoiceId = this.invoiceData.invoiceId;
			this.backendService.DHInvoicePaymentService(data).subscribe(res => {
				if (res.success) {
					this.receivePaymentForm.reset();
					this.dialog.closeAll();
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
				} else {
					this.dialog.closeAll();
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
			},
				(err: HttpErrorResponse) => {
					if (err.status == 500) {
						this.dialog.closeAll();
						this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
						this.openInfoDialog();
					}
				});
		}
	}

	/* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to trigger info dialog
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to open check pending dialog
	openCheckPendingDialog() {
		const dialogRef = this.dialog.open(CheckPendingModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => {
		});
	}

}
