import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { BackEndService } from "app/services/back-end.service";
import { Router } from "@angular/router";
import { UploadBulkModalComponent } from "app/modals/upload-bulk-modal/upload-bulk-modal.component";
import { MatDialog, PageEvent, MatPaginator } from "@angular/material";
import { FormGroup, FormControl } from "@angular/forms";

export interface Tractors {
  name: string;
  businessType: string;
  address: string;
  verified: string;
  status: string;
}

// const ELEMENT_DATA: Companies[] = [
//   { name: 'Dakota Rice', businessType: 'Supplier', address: 'Niger', verified: 'Yes' },
//   { name: 'Minerva Hooper', businessType: 'Broker', address: 'Chicago', verified: 'Yes' },
//   { name: 'Sage Rodriguez', businessType: 'Career', address: 'Netherlands', verified: 'No' },
//   { name: 'Shavais Bhai', businessType: 'Company', address: 'Johar', verified: 'Yes' },
//   { name: 'Muzammil Bhai', businessType: 'Supplier', address: 'Landhi', verified: 'Yes' },
//   { name: 'Mustafa Bhai', businessType: 'Supplier', address: 'Korangi', verified: 'Yes' },
//   { name: 'Ayaz Bhai', businessType: 'Supplier', address: 'Lalu khet', verified: 'Yes' },
//   { name: 'Hamza Bhai', businessType: 'Supplier', address: 'Niger', verified: 'Yes' },
// ];

@Component({
  selector: "app-vehicles",
  templateUrl: "./vehicles.component.html",
  styleUrls: ["./vehicles.component.scss"],
})
export class TractorsComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = [
    "name",
    "businessType",
    "address",
    "verified",
    "status",
  ];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;
  pageEvent: PageEvent;
  page = 1;
  pageSize = 50;
  length: number;
  pageSizeOptions = [20, 50, 100];

  

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    public backEnd: BackEndService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.form = new FormGroup({
      vin: new FormControl(null),
      enginenum: new FormControl(null),
      num: new FormControl(null),
    });
  }

  ngOnInit() {
    this.getAllTractors(this.page, this.pageSize);
  }

  getAllTractors(pg, pgSz) {
    let params = {
      page: pg,
      size: pgSz
    }
    this.backEnd.getAllTractorService(params).subscribe((res) => {
      if (res.data != null) {
        const ELEMENT_DATA: Tractors[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.length = res.totalRecords;
      }
    });
  }

  findTractorById(data: any) {
    this.backEnd.getTractorById(data.tractorId).subscribe((res) => {
      if (res.data != null || res.data != "") {
        sessionStorage.setItem("getTractor", JSON.stringify(res.data));
        this.router.navigate(["tractors/edit"]);
      }
    });
  }

  getServerData(val) {
    this.getAllTractors(val.pageIndex + 1, val.pageSize)
  }

  exportTractorList() {
    this.backEnd.getTractorExportFileService().subscribe(res => {
      const newBlob = new Blob([res.toString()], {
        type: "application/excel",
      });
      var downloadURL = window.URL.createObjectURL(res);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = "Tractor_List" + ".xlsx";
      link.click();
    });
  }

  search() {
    let data = {
      vin: this.form.value.vin,
      eng: this.form.value.enginenum,
      num: this.form.value.num,
    }

    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
    this.backEnd.getSearchTractorService(queryString).subscribe(res => {
      if (res.success) {
        this.dataSource = new MatTableDataSource(res.data);
      }
    })
  }

  ClearForm() {
    this.form.reset();
    this.getAllTractors(1, 50);
  }
  // function to trigger upload bulk dialog
  openBulkUploadDialog(): void {
    const dialogRef = this.dialog.open(UploadBulkModalComponent, {
      width: "600px",
      data: "Tractor",
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }
}
