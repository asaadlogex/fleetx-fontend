import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { BackEndService } from "app/services/back-end.service";
import { Router } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";

export interface Trackers {
  name: string;
  businessType: string;
  address: string;
  verified: string;
}

// const ELEMENT_DATA: Companies[] = [
//   { name: 'Dakota Rice', businessType: 'Supplier', address: 'Niger', verified: 'Yes' },
//   { name: 'Minerva Hooper', businessType: 'Broker', address: 'Chicago', verified: 'Yes' },
//   { name: 'Sage Rodriguez', businessType: 'Career', address: 'Netherlands', verified: 'No' },
//   { name: 'Shavais Bhai', businessType: 'Company', address: 'Johar', verified: 'Yes' },
//   { name: 'Muzammil Bhai', businessType: 'Supplier', address: 'Landhi', verified: 'Yes' },
//   { name: 'Mustafa Bhai', businessType: 'Supplier', address: 'Korangi', verified: 'Yes' },
//   { name: 'Ayaz Bhai', businessType: 'Supplier', address: 'Lalu khet', verified: 'Yes' },
//   { name: 'Hamza Bhai', businessType: 'Supplier', address: 'Niger', verified: 'Yes' },
// ];

@Component({
  selector: "app-tracker",
  templateUrl: "./tracker.component.html",
  styleUrls: ["./tracker.component.scss"],
})
export class TrackerComponent implements OnInit {
  displayedColumns: string[] = ["name", "businessType", "address", "verified"];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;

  search() {}

  ClearForm() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public backEnd: BackEndService, private router: Router) {
    this.form = new FormGroup({
      jobId: new FormControl(null),
      rwbId: new FormControl(null),
      lic: new FormControl(null),
      start: new FormControl(null),
      end: new FormControl(null),
      jobStatus: new FormControl(null),
      ownerShipStatus: new FormControl(null),
    });
  }

  ngOnInit() {
    this.getAllTrackers();
  }

  getAllTrackers() {
    this.backEnd.getAllTrackerService().subscribe((res) => {
      if (res.data != null) {
        res.data.forEach((element) => {
          element.installationDate = element.installationDate.substring(0, 10);
        });
        const ELEMENT_DATA: Trackers[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      }
    });
  }

  findTrackerById(data: any) {
    this.backEnd.getTrackerById(data.trackerId).subscribe((res) => {
      if (res.data != null || res.data != "") {
        sessionStorage.setItem("getTracker", JSON.stringify(res.data));
        this.router.navigate(["tracker/edit"]);
      }
    });
  }
}
