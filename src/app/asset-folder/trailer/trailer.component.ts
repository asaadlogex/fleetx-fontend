import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { BackEndService } from "app/services/back-end.service";
import { Router } from "@angular/router";
import { UploadBulkModalComponent } from "app/modals/upload-bulk-modal/upload-bulk-modal.component";
import { MatDialog, MatPaginator, PageEvent } from "@angular/material";
import { FormControl, FormGroup } from "@angular/forms";

export interface Trailers {
  name: string;
  businessType: string;
  address: string;
  verified: string;
  status: string;
}

// const ELEMENT_DATA: Companies[] = [
//   { name: 'Dakota Rice', businessType: 'Supplier', address: 'Niger', verified: 'Yes' },
//   { name: 'Minerva Hooper', businessType: 'Broker', address: 'Chicago', verified: 'Yes' },
//   { name: 'Sage Rodriguez', businessType: 'Career', address: 'Netherlands', verified: 'No' },
//   { name: 'Shavais Bhai', businessType: 'Company', address: 'Johar', verified: 'Yes' },
//   { name: 'Muzammil Bhai', businessType: 'Supplier', address: 'Landhi', verified: 'Yes' },
//   { name: 'Mustafa Bhai', businessType: 'Supplier', address: 'Korangi', verified: 'Yes' },
//   { name: 'Ayaz Bhai', businessType: 'Supplier', address: 'Lalu khet', verified: 'Yes' },
//   { name: 'Hamza Bhai', businessType: 'Supplier', address: 'Niger', verified: 'Yes' },
// ];

@Component({
  selector: "app-trailer",
  templateUrl: "./trailer.component.html",
  styleUrls: ["./trailer.component.scss"],
})
export class TrailerComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  
  displayedColumns: string[] = [
    "name",
    "businessType",
    "address",
    "verified",
    "status",
  ];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;
  pageEvent: PageEvent;
  page = 1;
  pageSize = 50;
  length: number;
  pageSizeOptions = [20, 50, 100];


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    public backEnd: BackEndService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.form = new FormGroup({
      vin: new FormControl(null),
    });
  }

  ngOnInit() {
    this.getAllTrailers(this.page, this.pageSize);
  }

  getAllTrailers(pg, pgSz) {
    let params = {
      page: pg,
      size: pgSz
    }
    this.backEnd.getAllTrailerService(params).subscribe((res) => {
      if (res.data != null) {
        const ELEMENT_DATA: Trailers[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.length = res.totalRecords;
      }
    });
  }

  findTrailerById(data: any) {
    this.backEnd.getTrailerById(data.trailerId).subscribe((res) => {
      if (res.data != null || res.data != "") {
        sessionStorage.setItem("getTrailer", JSON.stringify(res.data));
        this.router.navigate(["trailer/edit"]);
      }
    });
  }

  getServerData(val) {
    this.getAllTrailers(val.pageIndex + 1, val.pageSize)
  }

  exportTrailerList() {
    this.backEnd.getTrailerExportFileService().subscribe(res => {
      const newBlob = new Blob([res.toString()], {
        type: "application/excel",
      });
      var downloadURL = window.URL.createObjectURL(res);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = "Trailer_List" + ".xlsx";
      link.click();
    });
  }

  // function to trigger upload bulk dialog
  openBulkUploadDialog(): void {
    const dialogRef = this.dialog.open(UploadBulkModalComponent, {
      width: "600px",
      data: "Trailer",
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  search() {
    let data = {
      vin: this.form.value.vin,
    }

    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
    this.backEnd.getSearchTrailerService(queryString).subscribe(res => {
      if (res.success) {
        this.dataSource = new MatTableDataSource(res.data);
      }
    })
  }

  ClearForm() {
    this.form.reset();
    this.getAllTrailers(1, 50);
  }
}
