import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';

@Component({
  selector: 'app-add-trailer',
  templateUrl: './add-trailer.component.html',
  styleUrls: ['./add-trailer.component.scss']
})
export class AddTrailerComponent implements OnInit {

  /* =============================== ENVIRONMENT VARIABLE FOR DROPDOWNS ================================= */
  LeaseTypeDropdown = environment.LEASE_TYPE;
  makeDropdown = environment.MAKE_TYPE;
  trailerTypeDropdown = environment.TRAILER_TYPE;
  trailerSizeDropdown = environment.TRAILER_SIZE_TYPE;

  /* =============================== VARIABLES ================================= */
  assetType: any;
  trailerForm: FormGroup;
  tyre: FormArray;
  leaseTypeDropDown: any;
  supplierDropDown: any;
  companiesDropDown: any;
  trailerSizeDropDown: any;
  trailerTypeDropDown: any;
  makeDropDown: any;
  showSuppField = false;
  trailers: any;
  Tyres: FormArray;


  constructor(private formBuilder: FormBuilder,
    public backendService: BackEndService,
    private router: Router,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {

    this.trailerForm = this.formBuilder.group({
      trailId: [''],
      owner: [''],
      trailerType: ['', Validators.required],
      trailerSize: ['', Validators.required],
      assetId: [''],
      induct: ['', Validators.required],
      leasetype: ['', Validators.required],
      suppId: [null],
      vinNum: ['', Validators.required],
      make: [''],
      model: [''],
      year: [''],
      colour: [''],
      status: ['Active', Validators.required],
      tyresize: [''],
      tyreNum: ['', Validators.required],
      tyres: this.formBuilder.array([]),
      gross: [''],
      weightUnit: [''],
      tyreSerNum: [''],
      tyreMake: [''],
      tyreStartKm: [''],
      tyreTotalKm: [''],
      tyreStatus: [''],
    });

  }

  /* =============================== ONINIT COMPONENT FUNCTIONS ================================= */
  ngOnInit() {

    this.getLeaseType(this.LeaseTypeDropdown);
    this.getSupplierDropdown();
    this.getMakeDropdown(this.makeDropdown)
    this.getCompaniesDropdown();
    this.getTrailerSizeDropdown(this.trailerSizeDropdown);
    this.getTrailerTypeDropdown(this.trailerTypeDropdown);

    let getTrailer = JSON.parse(sessionStorage.getItem("getTrailer"));
    sessionStorage.removeItem("getTrailer");
    if (getTrailer != null) {
      this.trailers = getTrailer;
      this.trailerForm = this.formBuilder.group({
        trailId: [this.trailers.trailerId],
        owner: [''],
        trailerType: [this.trailers.trailerType, Validators.required],
        trailerSize: [this.trailers.trailerSize, Validators.required],
        assetId: [this.trailers.asset.assetId],
        induct: [this.trailers.asset.induction, Validators.required],
        leasetype: [this.trailers.asset.leaseType, Validators.required],
        suppId: [this.trailers.asset.supplierId],
        vinNum: [this.trailers.asset.vin],
        make: [this.trailers.asset.assetMake],
        model: [this.trailers.asset.model],
        year: [this.trailers.asset.year],
        colour: [this.trailers.asset.color],
        status: [this.trailers.asset.status, Validators.required],
        tyresize: [this.trailers.asset.tyreSize],
        tyreNum: [this.trailers.asset.numberOfTyres, Validators.required],
        tyres: this.formBuilder.array([]),
        gross: [this.trailers.asset.grossWeight],
        weightUnit: [this.trailers.asset.weightUnit],
        tyreSerNum: [''],
        tyreMake: [''],
        tyreStartKm: [''],
        tyreTotalKm: [''],
        tyreStatus: [''],
      });

      if (this.trailers.asset.tyres != null) {
        for (let i = 0; i < this.trailers.asset.tyres.length; i++) {
          this.Tyres = this.trailerForm.get('tyres') as FormArray;
          this.Tyres.push(this.formBuilder.group({
            tyreId: [this.trailers.asset.tyres[i].tyreId],
            tyreSerialNumber: [this.trailers.asset.tyres[i].tyreSerialNumber],
            make: [this.trailers.asset.tyres[i].make],
            startingKm: [this.trailers.asset.tyres[i].startingKm],
            totalKm: [this.trailers.asset.tyres[i].totalKm],
            status: [this.trailers.asset.tyres[i].status],
            leaseType: [this.trailers.asset.tyres[i].leaseType],
            adminCompanyId: [this.trailers.asset.tyres[i].adminCompanyId],
            supplierBusinessId: [null],
            editable: [false]
          }));
        }
      }
    }
  }

  /* =============================== ADD DELETE UPDATE TYRES FUNCTIONS ================================= */
  addTyres() {
    return this.formBuilder.group({
      tyreId: [""],
      tyreSerialNumber: [this.trailerForm.value.tyreSerNum],
      make: [this.trailerForm.value.tyreMake],
      startingKm: [this.trailerForm.value.tyreStartKm],
      totalKm: [this.trailerForm.value.tyreTotalKm],
      status: ["Active"],
      leaseType: [null],
      adminCompanyId: [this.trailerForm.getRawValue().owner.companyId],
      supplierBusinessId: [null],
      editable: [false]
    });
  }

  pushTyresInArray() {
    this.Tyres = <FormArray>this.trailerForm.get('tyres');
    this.Tyres.push(this.addTyres());
    this.trailerForm.get("tyreSerNum").reset();
    this.trailerForm.get("tyreMake").reset();
    this.trailerForm.get("tyreStartKm").reset();
    this.trailerForm.get("tyreTotalKm").reset();
    this.trailerForm.get("tyreStatus").reset();
  }

  removeTyre(id: any) {
    this.Tyres = <FormArray>this.trailerForm.get('tyres');
    this.Tyres.removeAt(id);
  }

  makeEditable(id: any) {
    this.trailerForm.value.tyres[id].editable = true;
    this.trailerForm.get('tyres').get([id]).get('editable').setValue(this.trailerForm.value.tyres[id].editable);
    this.trailerForm.get('tyres').get([id]).get('tyreSerialNumber').setValue(this.trailerForm.value.tyres[id].tyreSerialNumber);
    this.trailerForm.get('tyres').get([id]).get('status').setValue(this.trailerForm.value.tyres[id].status);
    this.trailerForm.get('tyres').get([id]).get('make').setValue(this.trailerForm.value.tyres[id].make);
    this.trailerForm.get('tyres').get([id]).get('startingKm').setValue(this.trailerForm.value.tyres[id].startingKm);
    this.trailerForm.get('tyres').get([id]).get('totalKm').setValue(this.trailerForm.value.tyres[id].totalKm);
  }

  saveEditTyre(id: any) {
    this.Tyres = <FormArray>this.trailerForm.get('tyres');
    this.Tyres.value[id].editable = false;
  }

  /* =============================== HIDE AND SHOW SUPPLIER FUNCTION ================================= */

  showSupplier(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_CONTRACTUAL) {
        this.showSuppField = true;
      } else {
        this.showSuppField = false;
      }
    }
  }

  /* =============================== ADD UPDATE DELETE TRAILER FUNCTIONS ================================= */

  submitTrailer() {
    if (this.trailerForm.invalid) {
      return;
    }
    if (this.trailerForm.value.tyres != []) {
      for (let i = 0; i < this.trailerForm.value.tyres.length; i++) {
        delete this.trailerForm.value.tyres[i].editable;
      }
    }

    this.trailers = {
      trailerId: this.trailerForm.value.trailId,
      trailerType: this.trailerForm.value.trailerType,
      trailerSize: this.trailerForm.value.trailerSize,
      adminCompanyId: this.trailerForm.getRawValue().owner.companyId,
      asset: {
        assetId: this.trailerForm.value.assetId,
        induction: this.datePipe.transform(this.trailerForm.value.induct, environment.SystemTypes_Date_Format),
        leaseType: this.trailerForm.value.leasetype,
        supplierId: null,
        vin: this.trailerForm.value.vinNum,
        assetMake: this.makeDropDown[0],
        model: this.trailerForm.value.model,
        year: this.trailerForm.value.year,
        color: this.trailerForm.value.colour,
        status: this.trailerForm.value.status,
        tyreSize: this.trailerForm.value.tyresize,
        numberOfTyres: this.trailerForm.value.tyreNum,
        tyres: this.trailerForm.value.tyres,
        grossWeight: this.trailerForm.value.gross,
        weightUnit: this.trailerForm.value.weightUnit
      }
    }
    if (this.trailers.asset.leaseType.value == environment.SystemTypes_CONTRACTUAL) {
      if (this.trailerForm.value.suppId != null)
        this.trailers.asset.supplierId = this.trailerForm.value.suppId.companyId;
    }

    if (this.trailers.asset.tyres != []) {
      for (let i = 0; i < this.trailers.asset.tyres.length; i++) {
        delete this.trailers.asset.tyres[i].editable;
        for (let j = 0; j < this.leaseTypeDropDown.length; j++) {
          if (this.leaseTypeDropDown[j].value == environment.SystemTypes_PURCHASED) {
            this.trailers.asset.tyres[i].leaseType = this.leaseTypeDropDown[j]
          }
        }
      }
    }

    if (this.trailerForm.value.trailId == "") {
      this.addTrailer();
    } else {
      this.updateTrailer();
    }
  }

  addTrailer() {
    this.backendService.addTrailerService(this.trailers).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.trailerForm.get("trailId").setValue(res.data.trailerId)
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("TRAILER ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateTrailer() {
    this.backendService.updateTrailerService(this.trailers).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("TRAILER UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteTrailer() {
    if (this.trailers.trailerId != null || this.trailers.trailerId != "") {
      this.backendService.deleteTrailerService(this.trailers.trailerId).subscribe(res => {
        if (res.success) {
          this.router.navigate(['trailer'])
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("TRAILER DELETE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    }
  }

  /* =============================== DROPDOWNS FUNCTIONS ================================= */

  getLeaseType(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.leaseTypeDropDown = res.data;
    })
  }

  getSupplierDropdown() {
    this.backendService.getAllFilterTrailerSupplier().subscribe(res => {
      this.supplierDropDown = res.data;
      if (this.trailerForm.value.trailId != "") {
        if (this.trailers.asset.leaseType.value == environment.SystemTypes_CONTRACTUAL) {
          this.showSuppField = true;
          for (let i = 0; i < this.supplierDropDown.length; i++) {
            if (this.supplierDropDown[i].companyId == this.trailers.supplierId) {
              this.trailerForm.get('suppId').setValue(this.supplierDropDown[i]);
            }
          }
        }
      }
    })
  }


  getCompaniesDropdown() {
    this.backendService.getAdminCompanies().subscribe(res => {
      this.companiesDropDown = res.data;
      this.trailerForm.get('owner').setValue(this.companiesDropDown[0])
      this.trailerForm.get('owner').disable();
    })
  }

  getMakeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.makeDropDown = res.data;

    })
  }

  getTrailerTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.trailerTypeDropDown = res.data;

    })
  }

  getTrailerSizeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.trailerSizeDropDown = res.data;

    })
  }
  /* =============================== COMPARE DROPDOWNS FUNCTIONS ================================= */

  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

  compareCompany(o1, o2): boolean {
    return o1.companyId === o2.companyId;
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
