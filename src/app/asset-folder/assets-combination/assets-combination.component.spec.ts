import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetsCombinationComponent } from './assets-combination.component';

describe('AssetsCombinationComponent', () => {
  let component: AssetsCombinationComponent;
  let fixture: ComponentFixture<AssetsCombinationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetsCombinationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetsCombinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
