import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';
import { ErrorMessageService } from 'app/services/error-message.service';
import { MatDialog } from '@angular/material';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-assets-combination',
  templateUrl: './assets-combination.component.html',
  styleUrls: ['./assets-combination.component.scss']
})
export class AssetsCombinationComponent implements OnInit {

  /* =============================== VARIABLES ================================= */

  assetCombinationForm: FormGroup;
  assetCombinations: any;
  tractorData: any = [];
  filteredTractors: any = [];
  trailerData: any = [];
  filteredTrailers: any = [];
  trackerData: any = [];
  filteredTrackers: any = [];
  containerData: any = [];
  filteredContainers: any = [];
  driver1Data: any = [];
  driver2Data: any = [];
  // Autocomplete
  filteredDriver1: any = []
  filteredDriver2: any = []

  constructor(private formBuilder: FormBuilder,
    public backendService: BackEndService,
    private router: Router,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {

    this.assetCombinationForm = this.formBuilder.group({
      assetCombinationID: [''],
      status: ['Active'],
      tractor: [null],
      trailer: [null],
      driver1: [null],
      driver2: [null],
      tracker: [null],
      container: [null],
      notes: [null],
    });

  }

  /* =============================== ONINIT COMPONENT FUNCTIONS ================================= */

  ngOnInit() {
    this.onChanges();

    let getCombination = JSON.parse(sessionStorage.getItem("getCombination"));
    sessionStorage.removeItem("getCombination");
    if (getCombination != null) {
      this.getAllContainers();
      this.getAllDrivers();
      this.getAllTrackers();
      this.getAllTractors();
      this.getAllTrailers();
      this.assetCombinations = getCombination;
      this.assetCombinationForm = this.formBuilder.group({
        assetCombinationID: [this.assetCombinations.assetCombinationId],
        status: this.assetCombinations.status,
        notes: this.assetCombinations.note,
        tractor: null,
        trailer: null,
        driver1: null,
        driver2: null,
        tracker: null,
        container: null
      })
      if (this.assetCombinations.tractor != null) {
        this.tractorData.push(this.assetCombinations.tractor);
        this.filteredTractors.push(this.assetCombinations.tractor);
      }
      if (this.assetCombinations.trailer != null) {
        this.trailerData.push(this.assetCombinations.trailer);
        this.filteredTrailers.push(this.assetCombinations.trailer);
      }
      if (this.assetCombinations.tracker != null) {
        this.trackerData.push(this.assetCombinations.tracker);
        this.filteredTrackers.push(this.assetCombinations.tracker);
      }
      if (this.assetCombinations.container != null) {
        this.containerData.push(this.assetCombinations.container);
        this.filteredContainers.push(this.assetCombinations.container);
      }
      if (this.assetCombinations.driver1 != null) {
        this.driver1Data.push(this.assetCombinations.driver1);
        this.filteredDriver1.push(this.assetCombinations.driver1);
      }
      if (this.assetCombinations.driver2 != null) {
        this.driver2Data.push(this.assetCombinations.driver2);
        this.filteredDriver2.push(this.assetCombinations.driver2);
      }

      this.assetCombinationForm.get('tractor').setValue(this.assetCombinations.tractor)
      this.assetCombinationForm.get('driver1').setValue(this.assetCombinations.driver1)
      this.assetCombinationForm.get('driver2').setValue(this.assetCombinations.driver2)
      this.assetCombinationForm.get('trailer').setValue(this.assetCombinations.trailer)
      this.assetCombinationForm.get('tracker').setValue(this.assetCombinations.tracker)
      this.assetCombinationForm.get('container').setValue(this.assetCombinations.container)
    }
  }

  /* =============================== ADD UPDATE DELETE ASSET COMBINATION FUNCTIONS ================================= */

  submitCombination() {
    this.assetCombinations = {
      assetCombinationId: this.assetCombinationForm.value.assetCombinationID,
      tractor: this.assetCombinationForm.value.tractor,
      trailer: this.assetCombinationForm.value.trailer,
      tracker: this.assetCombinationForm.value.tracker,
      container: this.assetCombinationForm.value.container,
      driver1: this.assetCombinationForm.value.driver1,
      driver2: this.assetCombinationForm.value.driver2,
      status: this.assetCombinationForm.value.status,
      note: this.assetCombinationForm.value.notes
    }
    if (this.assetCombinationForm.value.assetCombinationID == "") {
      this.addCombination();
    } else {
      this.updateCombination();
    }
  }

  addCombination() {
    this.backendService.addCombinationsService(this.assetCombinations).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.assetCombinationForm.get("assetCombinationID").setValue(res.data.assetCombinationId)
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("ASSET COMBINATION ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateCombination() {
    this.backendService.updateCombinationsService(this.assetCombinations).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("ASSET COMBINATION UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteCombination() {
    if (this.assetCombinations.assetCombinationId != null || this.assetCombinations.assetCombinationId != "") {
      this.backendService.deleteCombinationsService(this.assetCombinations.assetCombinationId).subscribe(res => {
        if (res.success) {
          this.router.navigate(['assetsCombination'])
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("ASSET COMBINATION DELETE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    }
  }

  /* =============================== DROPDOWNS FUNCTIONS ================================= */

  getAllTractors() {
    this.backendService.getAllFilterTractor().subscribe(res => {
      if (res.success) {
        for (let i = 0; i < res.data.length; i++) {
          this.tractorData.push(res.data[i]);
          this.filteredTractors.push(res.data[i]);
        }
      }
    })
  }

  getAllDrivers() {
    this.backendService.getAllDrivers().subscribe(res => {
      if (res.success) {
        for (let i = 0; i < res.data.length; i++) {
          this.driver1Data.push(res.data[i]);
          this.driver2Data.push(res.data[i]);
          // Autcomplete
          this.filteredDriver1.push(res.data[i]);
          this.filteredDriver2.push(res.data[i]);
        }
      }
    })
  }

  getAllTrailers() {
    this.backendService.getAllFilterTrailer().subscribe(res => {
      if (res.success) {
        for (let i = 0; i < res.data.length; i++) {
          this.trailerData.push(res.data[i]);
          this.filteredTrailers.push(res.data[i]);
        }
      }
    })
  }

  getAllTrackers() {
    this.backendService.getAllFilterTracker().subscribe(res => {
      if (res.success) {
        for (let i = 0; i < res.data.length; i++) {
          this.trackerData.push(res.data[i]);
          this.filteredTrackers.push(res.data[i]);
        }
      }
    })
  }

  getAllContainers() {
    this.backendService.getAllFilterContainer().subscribe(res => {
      if (res.success) {
        for (let i = 0; i < res.data.length; i++) {
          this.containerData.push(res.data[i]);
          this.filteredContainers.push(res.data[i]);
        }
      }
    })
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // Autocomplete

  onChanges() {
    this.assetCombinationForm.controls['driver1'].valueChanges.subscribe(val => {
      if (val.length > 0) {
        const filterValue = val.toLowerCase();
        this.filteredDriver1 = this.driver1Data.filter(option => {
          return option.employeeName.toLowerCase().includes(filterValue)

        })
      } else {
        this.filteredDriver1 = this.driver1Data;
      }
    })

    this.assetCombinationForm.controls['tractor'].valueChanges.subscribe(val => {
      if (val.length > 0) {
        const filterValue = val.toLowerCase();
        this.filteredTractors = this.tractorData.filter(option => {
          return option.numberPlate.toLowerCase().includes(filterValue)

        })
      } else {
        this.filteredTractors = this.tractorData;
      }
    })

    this.assetCombinationForm.controls['trailer'].valueChanges.subscribe(val => {
      if (val.length > 0) {
        const filterValue = val.toLowerCase();
        this.filteredTrailers = this.trailerData.filter(option => {
          return option.vin.toLowerCase().includes(filterValue)
        })
      } else {
        this.filteredTrailers = this.trailerData;
      }
    })

    this.assetCombinationForm.controls['container'].valueChanges.subscribe(val => {
      if (val.length > 0) {
        const filterValue = val.toLowerCase();
        this.filteredContainers = this.containerData.filter(option => {
          return option.containerNumber.toLowerCase().includes(filterValue)

        })
      } else {
        this.filteredContainers = this.containerData;
      }
    })

    this.assetCombinationForm.controls['tracker'].valueChanges.subscribe(val => {
      if (val.length > 0) {
        const filterValue = val.toLowerCase();
        this.filteredTrackers = this.trackerData.filter(option => {
          return option.trackerNumber.toLowerCase().includes(filterValue)

        })
      } else {
        this.filteredTrackers = this.trackerData;
      }
    })

    this.assetCombinationForm.controls['driver2'].valueChanges.subscribe(val => {
      if (val.length > 0) {
        const filterValue = val.toLowerCase();
        this.filteredDriver2 = this.driver2Data.filter(option => {
          return option.employeeName.toLowerCase().includes(filterValue)

        })
      } else {
        this.filteredDriver2 = this.driver2Data;
      }
    })

  }

  displayDriver(driver): string {
    return driver && driver.employeeNumber ? driver.employeeNumber + '-' + driver.employeeName : '';
  }

  displayTractor(tractor): string {
    return tractor && tractor.numberPlate;
  }

  displayTrailer(trailer): string {
    return trailer && trailer.vin;
  }

  displayTracker(tracker): string {
    return tracker && tracker.trackerNumber;
  }

  displayContainer(container): string {
    return container && container.containerNumber;
  }

  focusedDriver() {
    if (this.driver1Data.length == 0)
      this.getAllDrivers()
  }

  focusedTractor() {
    if (this.tractorData.length == 0)
      this.getAllTractors()
  }

  focusedTrailer() {
    if (this.trailerData.length == 0)
      this.getAllTrailers()
  }

  focusedContainer() {
    if (this.containerData.length == 0)
      this.getAllContainers()
  }

  focusedTracker() {
    if (this.trackerData.length == 0)
      this.getAllTrackers()
  }
}
