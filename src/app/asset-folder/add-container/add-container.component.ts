import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { ErrorMessageService } from 'app/services/error-message.service';
import { MatDialog } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';

@Component({
  selector: 'app-add-container',
  templateUrl: './add-container.component.html',
  styleUrls: ['./add-container.component.scss']
})
export class AddContainerComponent implements OnInit {

  /* ========================= ADD UPDATE DELETE CONTAINER FUNCTIONS  ==================================== */
  LeaseTypeDropdown = environment.LEASE_TYPE;
  supplierTypeDropdown = environment.SUPPLIER_TYPE;
  trailerTypeDropdown = environment.TRAILER_TYPE;
  containerSizeDropdown = environment.TRAILER_SIZE_TYPE;
  containerTypeDropdown = environment.CONTAINER_TYPE;

  /* ========================= VARIABLES ==================================== */
  containers: any;
  containerForm: FormGroup;
  leaseTypeDropDown: any;
  supplierDropDown: any;
  companiesDropDown: any;
  containerSizeDropDown: any;
  containerTypeDropDown: any;
  showSuppField = false;

  constructor(private formBuilder: FormBuilder,
    public backendService: BackEndService,
    private router: Router,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {

    this.containerForm = this.formBuilder.group({
      contId: [''],
      status: ['Active', Validators.required],
      containerSize: [null, Validators.required],
      containerType: [null, Validators.required],
      leasetype: [null, Validators.required],
      suppId: [null],
      owner: [''],
      containerNumber: ['', Validators.required],
      weight: ['', Validators.required],
      weightUnit: ['', Validators.required],
      make: [''],
      year: [''],
      purchaseDate: ['', Validators.required],
    });

  }

  /* ========================= ONINIT COMPONENT FUNCTION ==================================== */
  ngOnInit() {

    let getContainer = JSON.parse(sessionStorage.getItem("getContainer"));
    sessionStorage.removeItem("getContainer");
    if (getContainer != null) {
      this.containers = getContainer;
      this.containerForm = this.formBuilder.group({
        contId: [this.containers.containerId],
        status: [this.containers.status, Validators.required],
        containerSize: [this.containers.containerSize, Validators.required],
        containerType: [this.containers.containerType, Validators.required],
        leasetype: [this.containers.leaseType, Validators.required],
        suppId: [this.containers.supplierId],
        owner: [''],
        containerNumber: [this.containers.containerNumber, Validators.required],
        weight: [this.containers.containerWeight, Validators.required],
        weightUnit: [this.containers.containerWeightUnit, Validators.required],
        make: [this.containers.containerMake],
        year: [this.containers.containerYear],
        purchaseDate: [this.containers.purchaseDate, Validators.required],
      })
    }

    this.getLeaseType(this.LeaseTypeDropdown);
    this.getContainerSupplierDropdown();
    this.getCompaniesDropdown();
    this.getTrailerSizeDropdown(this.containerSizeDropdown);
    this.getContainerTypeDropdown(this.containerTypeDropdown);

  }

  /* ========================= ADD UPDATE DELETE CONTAINER FUNCTIONS  ==================================== */
  submitContainer() {
    if(this.containerForm.invalid){
      return;
    }
    this.containers = {
      containerId: this.containerForm.value.contId,
      containerNumber: this.containerForm.value.containerNumber,
      containerSize: this.containerForm.value.containerSize,
      containerType: this.containerForm.value.containerType,
      containerWeight: this.containerForm.value.weight,
      containerWeightUnit: this.containerForm.value.weightUnit,
      adminCompanyId: this.containerForm.getRawValue().owner.companyId,
      status: this.containerForm.value.status,
      containerMake: this.containerForm.value.make,
      containerYear: this.containerForm.value.year,
      purchaseDate: this.datePipe.transform(this.containerForm.value.purchaseDate, environment.SystemTypes_Date_Format),
      leaseType: this.containerForm.value.leasetype,
      supplierId: null,
    }

    if (this.containers.leaseType.value == environment.SystemTypes_CONTRACTUAL) {
      if (this.containerForm.value.suppId != null) {
        this.containers.supplierId = this.containerForm.value.suppId.companyId;
      }
    }

    if (this.containerForm.value.contId == "") {
      this.addContainer();
    } else {
      this.updateContainer();
    }
  }

  addContainer() {
    this.backendService.addContainerService(this.containers).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.containerForm.get("contId").setValue(res.data.containerId);
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("CONTAINER ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateContainer() {
    this.backendService.updateContainerService(this.containers).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("CONTAINER UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteContainer() {
    if (this.containers.containerId != null || this.containers.containerId != "") {
      this.backendService.deleteContainerService(this.containers.containerId).subscribe(res => {
        if (res.success) {
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
          this.router.navigate(['container'])
        }else{
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("CONTAINER DELETE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    }
  }

  /* ====================================== SHOW CONTRACTUAL SUPPLIER =============================================== */
  showSupplier(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_CONTRACTUAL) {
        this.showSuppField = true;
      } else {
        this.showSuppField = false;
      }
    }
  }

  /* ====================================== DROPDOWNS FUNCTIONS =============================================== */
  getLeaseType(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.leaseTypeDropDown = res.data;
    })
  }

  getContainerSupplierDropdown() {
    this.backendService.getAllFilterContainerSupplier().subscribe(res => {
      this.supplierDropDown = res.data;
      if (this.containerForm.value.contId != "") {
        if (this.containers.leaseType.value == environment.SystemTypes_CONTRACTUAL) {
          if (this.containers.supplierBusinessId != null) {
            for (let i = 0; i < this.supplierDropDown.length; i++) {
              if (this.supplierDropDown[i].companyId == this.containers.supplierId) {
                this.showSuppField = true;
                this.containerForm.get('suppId').setValue(this.supplierDropDown[i]);
              }
            }
          }
        }
      }
    })
  }


  getCompaniesDropdown() {
    this.backendService.getAdminCompanies().subscribe(res => {
      this.companiesDropDown = res.data;
      this.containerForm.get('owner').setValue(this.companiesDropDown[0])
      this.containerForm.get('owner').disable();
    })
  }

  getTrailerSizeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.containerSizeDropDown = res.data;

    })
  }

  getContainerTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.containerTypeDropDown = res.data;

    })
  }

  /* ====================== COMPARE DROPDOWN FUNCTIONS ============================ */
  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

  compareCompany(o1, o2): boolean {
    return o1.companyId === o2.companyId;
  }

  
  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
