import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { BackEndService } from "app/services/back-end.service";
import { Router } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";

export interface Containers {
  name: string;
  businessType: string;
  address: string;
  verified: string;
}

// const ELEMENT_DATA: Companies[] = [
//   { name: 'Dakota Rice', businessType: 'Supplier', address: 'Niger', verified: 'Yes' },
//   { name: 'Minerva Hooper', businessType: 'Broker', address: 'Chicago', verified: 'Yes' },
//   { name: 'Sage Rodriguez', businessType: 'Career', address: 'Netherlands', verified: 'No' },
//   { name: 'Shavais Bhai', businessType: 'Company', address: 'Johar', verified: 'Yes' },
//   { name: 'Muzammil Bhai', businessType: 'Supplier', address: 'Landhi', verified: 'Yes' },
//   { name: 'Mustafa Bhai', businessType: 'Supplier', address: 'Korangi', verified: 'Yes' },
//   { name: 'Ayaz Bhai', businessType: 'Supplier', address: 'Lalu khet', verified: 'Yes' },
//   { name: 'Hamza Bhai', businessType: 'Supplier', address: 'Niger', verified: 'Yes' },
// ];

@Component({
  selector: "app-container",
  templateUrl: "./container.component.html",
  styleUrls: ["./container.component.scss"],
})
export class ContainerComponent implements OnInit {
  displayedColumns: string[] = ["name", "businessType", "address", "verified"];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;

  search() {}

  ClearForm() {}
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public backEnd: BackEndService, private router: Router) {
    this.form = new FormGroup({
      jobId: new FormControl(null),
      rwbId: new FormControl(null),
      lic: new FormControl(null),
      start: new FormControl(null),
      end: new FormControl(null),
      jobStatus: new FormControl(null),
      ownerShipStatus: new FormControl(null),
    });
  }

  ngOnInit() {
    this.getAllContainers();
  }

  getAllContainers() {
    this.backEnd.getAllContainerService().subscribe((res) => {
      if (res.data != null) {
        const ELEMENT_DATA: Containers[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      }
    });
  }

  findContainerById(data: any) {
    this.backEnd.getContainerById(data.containerId).subscribe((res) => {
      if (res.data != null || res.data != "") {
        sessionStorage.setItem("getContainer", JSON.stringify(res.data));
        this.router.navigate(["container/edit"]);
      }
    });
  }
}
