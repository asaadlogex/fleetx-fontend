import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { DatePipe } from '@angular/common';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'app-add-vehicle',
	templateUrl: './add-vehicle.component.html',
	styleUrls: ['./add-vehicle.component.scss']
})
export class AddTractorComponent implements OnInit {

	/* =============================== ENVIRONMENT VARIABLE FOR DROPDOWNS ================================= */

	LeaseTypeDropdownCode = environment.LEASE_TYPE;
	makeDropdownCode = environment.MAKE_TYPE;

	/* =============================== VARIABLES ================================= */

	assetType: any;
	tractorForm: FormGroup;
	tyre: FormArray;
	leaseTypeDropDown: any;
	supplierDropDown: any;
	companiesDropDown: any;
	makeDropDown: any;
	showSuppField = false;
	tractors: any;
	Tyres: FormArray;



	constructor(private formBuilder: FormBuilder,
		public backendService: BackEndService,
		private router: Router,
		public datePipe: DatePipe,
		public dialog: MatDialog,
		public errorMsgService: ErrorMessageService) {

		this.tractorForm = this.formBuilder.group({
			tracId: [''],
			owner: [''],
			engType: [''],
			engNum: [null, Validators.required],
			trans: [null],
			numPlate: [null, Validators.required],
			numGear: [null],
			numFuel: [null],
			fuelCap: [null],
			startMeter: [null],
			currentMeter: [null],
			odoUnit: [null],
			assetId: [''],
			induct: ['', Validators.required],
			leasetype: ['', Validators.required],
			suppId: [''],
			vinNum: ['', Validators.required],
			make: [''],
			model: [''],
			year: [''],
			colour: [''],
			tracStatus: ['Active', Validators.required],
			tyresize: [''],
			tyreNum: ['', Validators.required],
			tyres: this.formBuilder.array([]),
			gross: [''],
			weightUnit: [''],
			tyreSerNum: [''],
			tyreMake: [''],
			tyreStartKm: [''],
			tyreTotalKm: [''],
			tyreStatus: [''],
		});
	}

	/* =============================== ONINIT COMPONENT FUNCTIONS ================================= */

	ngOnInit() {
		this.getLeaseType(this.LeaseTypeDropdownCode);
		this.getSupplierDropdown();
		this.getMakeDropdown(this.makeDropdownCode)
		this.getCompaniesDropdown();

		let getTractor = JSON.parse(sessionStorage.getItem("getTractor"));
		sessionStorage.removeItem("getTractor");
		if (getTractor != null) {
			this.tractors = getTractor;
			this.tractorForm = this.formBuilder.group({
				tracId: [this.tractors.tractorId],
				owner: [''],
				engType: [this.tractors.engineType],
				engNum: [this.tractors.engineNumber, Validators.required],
				trans: [this.tractors.transmission],
				numPlate: [this.tractors.numberPlate, Validators.required],
				numGear: [this.tractors.numberOfGears],
				numFuel: [this.tractors.numberOfFuelTanks],
				fuelCap: [this.tractors.fuelCapacity],
				startMeter: [this.tractors.startingOdometer],
				currentMeter: [this.tractors.currentOdometer],
				odoUnit: [this.tractors.odometerUnit],
				assetId: [this.tractors.asset.assetId],
				induct: [this.tractors.asset.induction, Validators.required],
				leasetype: [this.tractors.asset.leaseType, Validators.required],
				suppId: [this.tractors.asset.supplierId],
				vinNum: [this.tractors.asset.vin, Validators.required],
				make: [this.tractors.asset.assetMake],
				model: [this.tractors.asset.model],
				year: [this.tractors.asset.year],
				colour: [this.tractors.asset.color],
				tracStatus: [this.tractors.asset.status, Validators.required],
				tyresize: [this.tractors.asset.tyreSize],
				tyreNum: [this.tractors.asset.numberOfTyres, Validators.required],
				tyres: this.formBuilder.array([]),
				gross: [this.tractors.asset.grossWeight],
				weightUnit: [this.tractors.asset.weightUnit],
				tyreSerNum: [''],
				tyreMake: [''],
				tyreStartKm: [''],
				tyreTotalKm: [''],
				tyreStatus: [''],
			});

			if (this.tractors.asset.tyres != null) {
				for (let i = 0; i < this.tractors.asset.tyres.length; i++) {
					this.Tyres = this.tractorForm.get('tyres') as FormArray;
					this.Tyres.push(this.formBuilder.group({
						tyreId: [this.tractors.asset.tyres[i].tyreId],
						tyreSerialNumber: [this.tractors.asset.tyres[i].tyreSerialNumber],
						make: [this.tractors.asset.tyres[i].make],
						startingKm: [this.tractors.asset.tyres[i].startingKm],
						totalKm: [this.tractors.asset.tyres[i].totalKm],
						status: [this.tractors.asset.tyres[i].status],
						leaseType: [this.tractors.asset.tyres[i].leaseType],
						adminCompanyId: [this.tractors.asset.tyres[i].adminCompanyId],
						supplierBusinessId: [this.tractors.asset.tyres[i].supplierId],
						editable: [false]
					}));
				}
			}
		}
	}

	/* =============================== ADD DELETE UPDATE TYRES FUNCTIONS ================================= */

	addTyres() {
		return this.formBuilder.group({
			tyreId: [""],
			tyreSerialNumber: [this.tractorForm.value.tyreSerNum],
			make: [this.tractorForm.value.tyreMake],
			startingKm: [this.tractorForm.value.tyreStartKm],
			totalKm: [this.tractorForm.value.tyreTotalKm],
			status: ["Active"],
			leaseType: [null],
			adminCompanyId: [this.tractorForm.getRawValue().owner.companyId],
			supplierBusinessId: [null],
			editable: [false]
		});
	}

	pushTyresInArray() {
		this.Tyres = <FormArray>this.tractorForm.get('tyres');
		this.Tyres.push(this.addTyres());
		this.tractorForm.get("tyreSerNum").reset();
		this.tractorForm.get("tyreMake").reset();
		this.tractorForm.get("tyreStartKm").reset();
		this.tractorForm.get("tyreTotalKm").reset();
		this.tractorForm.get("tyreStatus").reset();
	}

	removeTyre(id: any) {
		this.Tyres = <FormArray>this.tractorForm.get('tyres');
		this.Tyres.removeAt(id);
	}

	makeEditable(id: any) {
		this.tractorForm.value.tyres[id].editable = true;
		this.tractorForm.get('tyres').get([id]).get('editable').setValue(this.tractorForm.value.tyres[id].editable);
		this.tractorForm.get('tyres').get([id]).get('tyreSerialNumber').setValue(this.tractorForm.value.tyres[id].tyreSerialNumber);
		this.tractorForm.get('tyres').get([id]).get('status').setValue(this.tractorForm.value.tyres[id].status);
		this.tractorForm.get('tyres').get([id]).get('make').setValue(this.tractorForm.value.tyres[id].make);
		this.tractorForm.get('tyres').get([id]).get('startingKm').setValue(this.tractorForm.value.tyres[id].startingKm);
		this.tractorForm.get('tyres').get([id]).get('totalKm').setValue(this.tractorForm.value.tyres[id].totalKm);
	}

	saveEditTyre(id: any) {
		this.Tyres = <FormArray>this.tractorForm.get('tyres');
		this.Tyres.value[id].editable = false;
	}

	/* =============================== HIDE AND SHOW SUPPLIER FUNCTION ================================= */

	showSupplier(e) {
		if (e != undefined) {
			if (e.value == environment.SystemTypes_CONTRACTUAL) {
				this.showSuppField = true;
			} else {
				this.showSuppField = false;
			}
		}
	}

	/* =============================== ADD UPDATE DELETE TRACTOR FUNCTIONS ================================= */

	submitTractor() {
		if (this.tractorForm.invalid) {
			return;
		}

		this.tractors = {
			tractorId: this.tractorForm.value.tracId,
			engineType: this.tractorForm.value.engType,
			engineNumber: this.tractorForm.value.engNum,
			transmission: this.tractorForm.value.trans,
			numberPlate: this.tractorForm.value.numPlate,
			numberOfGears: this.tractorForm.value.numGear,
			numberOfFuelTanks: this.tractorForm.value.numFuel,
			fuelCapacity: this.tractorForm.value.fuelCap,
			startingOdometer: this.tractorForm.value.startMeter,
			currentOdometer: this.tractorForm.value.currentMeter,
			odometerUnit: this.tractorForm.value.odoUnit,
			adminCompanyId: this.tractorForm.getRawValue().owner.companyId,
			asset: {
				assetId: this.tractorForm.value.assetId,
				induction: this.datePipe.transform(this.tractorForm.value.induct, environment.SystemTypes_Date_Format),
				leaseType: this.tractorForm.value.leasetype,
				supplierId: null,
				vin: this.tractorForm.value.vinNum,
				assetMake: this.tractorForm.value.make,
				model: this.tractorForm.value.model,
				year: this.tractorForm.value.year,
				color: this.tractorForm.value.colour,
				status: this.tractorForm.value.tracStatus,
				tyreSize: this.tractorForm.value.tyresize,
				numberOfTyres: this.tractorForm.value.tyreNum,
				tyres: this.tractorForm.value.tyres,
				grossWeight: this.tractorForm.value.gross,
				weightUnit: this.tractorForm.value.weightUnit
			}
		}

		if (this.tractors.asset.leaseType.value == environment.SystemTypes_CONTRACTUAL) {
			if (this.tractorForm.value.suppId != null)
				this.tractors.asset.supplierId = this.tractorForm.value.suppId.companyId;
		}

		if (this.tractors.asset.tyres != []) {
			for (let i = 0; i < this.tractors.asset.tyres.length; i++) {
				delete this.tractors.asset.tyres[i].editable;
				for (let j = 0; j < this.leaseTypeDropDown.length; j++) {
					if (this.leaseTypeDropDown[j].value == environment.SystemTypes_PURCHASED) {
						this.tractors.asset.tyres[i].leaseType = this.leaseTypeDropDown[j]
					}
				}
			}
		}

		if (this.tractorForm.value.tracId == "") {
			this.addTractor();
		} else {
			this.updateTractor();
		}
	}

	addTractor() {
		this.backendService.addTractorService(this.tractors).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
				this.tractorForm.get("tracId").setValue(res.data.tractorId);
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("TRACTOR ADD RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	updateTractor() {
		this.backendService.updateTractorService(this.tractors).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("TRACTOR UPDATE RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	deleteTractor() {
		if (this.tractors.tractorId != null || this.tractors.tractorId != "") {
			this.backendService.deleteTractorService(this.tractors.tractorId).subscribe(res => {
				if (res.success) {
					this.router.navigate(['tractors'])
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("TRACTOR DELETE RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});

		}
	}

	/* =============================== DROPDOWNS FUNCTIONS ================================= */

	getLeaseType(type: string) {
		this.backendService.getDropdowns(type).subscribe(res => {
			this.leaseTypeDropDown = res.data;
		})
	}

	getSupplierDropdown() {
		this.backendService.getAllFilterTruckSupplier().subscribe(res => {
			this.supplierDropDown = res.data;
			if (this.tractorForm.value.tracId != "") {
				if (this.tractors.asset.leaseType.value == environment.SystemTypes_CONTRACTUAL) {
					this.showSuppField = true;
					for (let i = 0; i < this.supplierDropDown.length; i++) {
						if (this.supplierDropDown[i].companyId == this.tractors.supplierId) {
							this.tractorForm.get('suppId').setValue(this.supplierDropDown[i]);
						}
					}
				}
			}
		})
	}

	getCompaniesDropdown() {
		this.backendService.getAdminCompanies().subscribe(res => {
			this.companiesDropDown = res.data;
			this.tractorForm.get('owner').setValue(this.companiesDropDown[0])
			this.tractorForm.get('owner').disable();
		})
	}

	getMakeDropdown(type: string) {
		this.backendService.getDropdowns(type).subscribe(res => {
			this.makeDropDown = res.data;

		})
	}

	/* =============================== COMPARE DROPDOWNS FUNCTIONS ================================= */

	compareDropdownType(o1, o2): boolean {
		if (o2 != null)
			return o1.value === o2.value;
	}

	compareCompany(o1, o2): boolean {
		return o1.companyId === o2.companyId;
	}

	/* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger info dialog
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}
}
