import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { BackEndService } from "app/services/back-end.service";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormControl } from "@angular/forms";

export interface Combinations {
  tractor: string;
  trailer: string;
  container: string;
  driver1: string;
  driver2: string;
}

@Component({
  selector: "app-combination-list",
  templateUrl: "./combination-list.component.html",
  styleUrls: ["./combination-list.component.scss"],
})
export class CombinationListComponent implements OnInit {
  displayedColumns: string[] = [
    "tractor",
    "trailer",
    "container",
    "driver1",
    "driver2",
  ];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;

  search() {}

  ClearForm() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public backEnd: BackEndService, private router: Router) {
    this.form = new FormGroup({
      jobId: new FormControl(null),
      rwbId: new FormControl(null),
      lic: new FormControl(null),
      start: new FormControl(null),
      end: new FormControl(null),
      jobStatus: new FormControl(null),
      ownerShipStatus: new FormControl(null),
    });
  }

  ngOnInit() {
    this.getAllAssetCombanitions();
  }

  getAllAssetCombanitions() {
    this.backEnd.getAllCombinationsService().subscribe((res) => {
      const ELEMENT_DATA: Combinations[] = res.data;
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    });
  }

  findAssetCombanitionsById(data: any) {
    this.backEnd
      .getCombinationsById(data.assetCombinationId)
      .subscribe((res) => {
        if (res.data != null || res.data != "") {
          sessionStorage.setItem("getCombination", JSON.stringify(res.data));
          this.router.navigate(["assetsCombination/edit"]);
        }
      });
  }
}
