import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';

@Component({
  selector: 'app-add-tracker',
  templateUrl: './add-tracker.component.html',
  styleUrls: ['./add-tracker.component.scss']
})
export class AddTrackerComponent implements OnInit {

   /* =============================== ENVIRONMENT VARIABLE FOR DROPDOWNS ================================= */

  LeaseTypeDropdown = environment.LEASE_TYPE;

   /* =============================== VARIABLES ================================= */

  trackers: any;
  trackerForm: FormGroup;
  leaseTypeDropDown: any;
  supplierDropDown: any;
  companiesDropDown: any;
  showSuppField = false;

  constructor(private formBuilder: FormBuilder,
    public backendService: BackEndService,
    private router: Router,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {
      
    this.trackerForm = this.formBuilder.group({
      trackerId: [''],
      status: ['Active', Validators.required],
      contTerm: [''],
      insDate: ['', Validators.required],
      trackName: ['', Validators.required],
      trackNum: ['', Validators.required],
      retDate: [''],
      leasetype: ['', Validators.required],
      suppId: [''],
      owner: [''],
    });
  }

   /* =============================== ONINIT COMPONENT FUNCTIONS ================================= */

  ngOnInit() {

    this.getLeaseType(this.LeaseTypeDropdown);
    this.getSupplierDropdown();
    this.getCompaniesDropdown();

    let getTracker = JSON.parse(sessionStorage.getItem("getTracker"));
    sessionStorage.removeItem("getTracker");
    if (getTracker != null) {
      this.trackers = getTracker;
      this.trackerForm = this.formBuilder.group({
        trackerId: [this.trackers.trackerId],
        status: [this.trackers.status, Validators.required],
        insDate: [this.trackers.installationDate, Validators.required],
        retDate: [this.trackers.returnDate],
        trackName: [this.trackers.trackerName, Validators.required],
        trackNum: [this.trackers.trackerNumber, Validators.required],
        contTerm: [this.trackers.contractTerms],
        leasetype: [this.trackers.leaseType, Validators.required],
        suppId: [this.trackers.supplierId],
        owner: ['']
      })
    }
  }

   /* =============================== ADD UPDATE DELETE TRACKER FUNCTIONS ================================= */

  submitTracker() {
    if(this.trackerForm.invalid){
      return;
    }
    
    this.trackers = {
      trackerId: this.trackerForm.value.trackerId,
      status: this.trackerForm.value.status,
      trackerName: this.trackerForm.value.trackName,
      trackerNumber: this.trackerForm.value.trackNum,
      adminCompanyId: this.trackerForm.getRawValue().owner.companyId,
      leaseType: this.trackerForm.value.leasetype,
      supplierId: null,
      installationDate: this.datePipe.transform(this.trackerForm.value.insDate, environment.SystemTypes_Date_Format),
      returnDate: this.datePipe.transform(this.trackerForm.value.retDate, environment.SystemTypes_Date_Format),
      contractTerms: this.trackerForm.value.contTerm,
    }
    if (this.trackers.leaseType.value == environment.SystemTypes_CONTRACTUAL) {
      if (this.trackerForm.value.suppId != null) {
        this.trackers.supplierId = this.trackerForm.value.suppId.companyId;
      }
    }

    if (this.trackerForm.value.trackerId == "") {
      this.addTracker();
    } else {
      this.updateTracker();
    }
  }

  addTracker() {
    this.backendService.addTrackerService(this.trackers).subscribe(res => {
      if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.trackerForm.get("trackerId").setValue(res.data.trackerId)
			  }else{
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			  }
			  //console.log("TRACKER ADD RESPONSE", res);
			}, (err: HttpErrorResponse) => {
			  if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			  }
			});
  }

  updateTracker() {
    this.backendService.updateTrackerService(this.trackers).subscribe(res => {
      if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
			  }else{
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			  }
			  //console.log("TRACKER UPDATE RESPONSE", res);
			}, (err: HttpErrorResponse) => {
			  if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			  }
			});
  }

  deleteTracker() {
    if (this.trackers.trackerId != null || this.trackers.trackerId != "") {
      this.backendService.deleteTrackerService(this.trackers.trackerId).subscribe(res => {
        if (res.success) {
          this.router.navigate(['tracker'])
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
          }else{
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
          }
          //console.log("TRACKER DELETE RESPONSE", res);
        }, (err: HttpErrorResponse) => {
          if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
          }
        });
    }
  }

   /* =============================== SHOW SUPPLIER FUNCTION ================================= */

  showSupplier(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_CONTRACTUAL) {
        this.showSuppField = true;
      } else {
        this.showSuppField = false;
      }
    }
  }

   /* =============================== DROPDOWNS FUNCTIONS ================================= */

  getLeaseType(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.leaseTypeDropDown = res.data;
    })
  }

  getSupplierDropdown() {
    this.backendService.getAllFilterTrackerSupplier().subscribe(res => {
      this.supplierDropDown = res.data;
      if (this.trackerForm.value.trackerId != "") {
				if (this.trackers.leaseType == environment.SystemTypes_CONTRACTUAL) {
					this.showSuppField = true;
					for (let i = 0; i < this.supplierDropDown.length; i++) {
						if (this.supplierDropDown[i].companyId == this.trackers.supplierId) {
							this.trackerForm.get('suppId').setValue(this.supplierDropDown[i]);
						}
					}
				}
			}
    })
  }


  getCompaniesDropdown() {
    this.backendService.getAdminCompanies().subscribe(res => {
      this.companiesDropDown = res.data;
        this.trackerForm.get('owner').setValue(this.companiesDropDown[0])
        this.trackerForm.get('owner').disable();
    })
  }

   /* =============================== COMPARE DROPDOWNS FUNCTIONS ================================= */

  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

  compareCompany(o1, o2): boolean {
    return o1.companyId === o2.companyId;
  }

   /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
