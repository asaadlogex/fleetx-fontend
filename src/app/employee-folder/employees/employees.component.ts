import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { BackEndService } from "app/services/back-end.service";
import { Router } from "@angular/router";
import { UploadBulkModalComponent } from "app/modals/upload-bulk-modal/upload-bulk-modal.component";
import { MatDialog, MatPaginator, PageEvent } from "@angular/material";
import { FormControl, FormGroup } from "@angular/forms";

export interface Employees {
  empNo: string;
  name: string;
  dob: string;
  businessEntity: string;
  position: string;
  licenseExpiry: string;
  medicalExpiry: string;
  status: string;
}

@Component({
  selector: "app-employees",
  templateUrl: "./employees.component.html",
  styleUrls: ["./employees.component.scss"],
})
export class EmployeesComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = [
    "empNo",
    "name",
    "dob",
    "businessEntity",
    "position",
    "licenseExpiry",
    "medicalExpiry",
    "status",
  ];
  
  dataSource: MatTableDataSource<any>;
  form: FormGroup;
  pageEvent: PageEvent;
  page = 1;
  pageSize = 50;
  length: number;
  pageSizeOptions = [20, 50, 100];


  ClearForm() {
    this.form.reset();
    this.getAllEmployees(1, 50);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    public backEnd: BackEndService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.form = new FormGroup({
      empNo: new FormControl(null),
      fname: new FormControl(null),
      lname: new FormControl(null),
      licence: new FormControl(null),
      cnic: new FormControl(null),
    });


  }

  ngOnInit() {
    this.getAllEmployees(this.page, this.pageSize);
  }

  getAllEmployees(pg, pgSz) {
    let params = {
      page: pg,
      size: pgSz
    }
    this.backEnd.getAllEmployeeService(params).subscribe((res) => {
      if (res.data != null) {
        res.data.forEach((element) => {
          element.dateOfBirth = element.dateOfBirth.substring(0, 10);
        });
        const ELEMENT_DATA: Employees[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.length = res.totalRecords;
      }
    });
  }

  findEmployeeById(data: any) {
    this.backEnd.getEmployeeById(data.employeeId).subscribe((res) => {
      if (res.data != null || res.data != "") {
        sessionStorage.setItem("getEmployee", JSON.stringify(res.data));
        this.router.navigate(["employees/edit"]);
      }
    });
  }

  exportDriverList() {
    this.backEnd.getEmployeeExportFileService().subscribe(res => {
      const newBlob = new Blob([res.toString()], {
        type: "application/excel",
      });
      var downloadURL = window.URL.createObjectURL(res);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = "Driver_List" + ".xlsx";
      link.click();
    });
  }

  getServerData(val) {
    this.getAllEmployees(val.pageIndex + 1, val.pageSize)
  }

  searchEmployee() {
    let data = {
      enum: this.form.value.empNo,
      fname: this.form.value.fname,
      lname: this.form.value.lname,
      cnic: this.form.value.cnic,
      lic: this.form.value.licence
    }

    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
    this.backEnd.getSearchEmployeeService(queryString).subscribe(res => {
      if (res.success) {
        this.dataSource = new MatTableDataSource(res.data);
      }
    })
  }



  // function to trigger upload bulk dialog
  openBulkUploadDialog(): void {
    const dialogRef = this.dialog.open(UploadBulkModalComponent, {
      width: "600px",
      data: "Employees",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }
}
