import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { ChangeDetectionStrategy } from '@angular/compiler/src/core';
import { fileURLToPath } from 'url';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss'],
})
export class AddEmployeeComponent implements OnInit {

  channelTypeDropdownCode = environment.CHANNEL_TYPE;
  departmentTypeDropdownCode = environment.DEPARTMENT_TYPE;
  positionTypeDropdownCode = environment.POSITION_TYPE;
  employeeTypeDropdownCode = environment.EMPLOYEE_TYPE;
  cityTypeDropdownCode = environment.CITY_TYPE;
  stateTypeDropdownCode = environment.STATE_TYPE;

  employees: any;
  addEmployeeForm: FormGroup;
  addresses: FormArray;
  contacts: FormArray;
  contracts: FormArray;
  trainingProfiles: FormArray;
  medicalTestReports: FormArray;
  employeeReferences: FormArray;
  heavyVehicles: FormArray;
  getHeavyVehicle: any = [];
  companiesDropDown: any;
  channelTypeDropDown: any;
  departmentDropDown: any;
  employeeTypeDropDown: any;
  supplierCompanyDropDown: any;
  positionDropDown: any;
  stateDropDown: any;
  cityDropDown: any;
  companyAddress: [];
  showLicenseField = false;
  showSupplierField = false;
  companyLocations: any;
  showTrainingProfileTab: boolean = false;
  showHeavyVehicleTab: boolean = false;
  showMedicalTestTab: boolean = false;
  showReferenceTab: boolean = false;

  medicalTestURL: string;
  CnicURL: string;
  refFormURL: string;

  constructor(private formBuilder: FormBuilder,
    public backendService: BackEndService,
    private router: Router,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService,
    public cdRef: ChangeDetectorRef) {

    this.addEmployeeForm = this.formBuilder.group({
      empId: [''],
      empCompany: [''],
      empPicture: [''],
      empStatus: ['Active', Validators.required],
      empType: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      businessAddress: ['', Validators.required],
      empNo: ['', Validators.required],
      empDob: ['', Validators.required],
      empCnic: ['', Validators.required],
      cnicExpiry: [''],
      empDepartment: ['', Validators.required],
      empPosition: ['', Validators.required],
      empKinName: [''],
      empSupplier: [null],
      empNtn: [],
      empKinRelation: [''],
      empLicenseNo: [''],
      empLicenseExpiry: [''],
      empLicenseIssue: [''],
      empLicenceClass: [''],
      empHVstart: [''],
      empHVend: [''],
      empHVtruck: [''],
      empHVcompany: [''],
      empAddress: this.formBuilder.array([this.createAddress()]),
      empContact: this.formBuilder.array([]),
      empContract: this.formBuilder.array([this.createContract()]),
      empTrainingProfile: this.formBuilder.array([this.createTrainingProfile()]),
      empMedicalReport: this.formBuilder.array([this.createMedicalReport()]),
      empReferences: this.formBuilder.array([this.createEmployeeReference()])
    });

  }

  ngOnInit() {

    this.getCompaniesDropdown();
    this.getSupplierCompaniesDropdown();
    this.getEmployeeTypeDropdown(this.employeeTypeDropdownCode);
    this.getContactChannelDropdown(this.channelTypeDropdownCode);
    this.getDepartmentDropdown(this.departmentTypeDropdownCode)
    this.getPositionDropdown(this.positionTypeDropdownCode);
    this.getStateDropdown(this.stateTypeDropdownCode);
    this.getCityDropdown(this.cityTypeDropdownCode);

    //FETCH EMPLOYEE DATA FOR UPDATING PROCESS
    let getEmployee = JSON.parse(sessionStorage.getItem("getEmployee"));
    sessionStorage.removeItem("getEmployee");
    if (getEmployee != null) {
      this.employees = getEmployee;
      this.addEmployeeForm = this.formBuilder.group({
        empId: [this.employees.employeeId],
        empCompany: [''],
        empPicture: [''],
        empType: [this.employees.employeeType, Validators.required],
        empStatus: [this.employees.status, Validators.required],
        firstName: [this.employees.firstName, Validators.required],
        lastName: [this.employees.lastName, Validators.required],
        businessAddress: [this.employees.businessAddress, Validators.required],
        empNo: [this.employees.employeeNumber, Validators.required],
        empDob: [this.employees.dateOfBirth],
        empCnic: [this.employees.cnic, Validators.required],
        cnicExpiry: [this.employees.cnicExpiry],
        empDepartment: [this.employees.department, Validators.required],
        empPosition: [this.employees.position, Validators.required],
        empKinName: [this.employees.nextOfKin],
        empSupplier: [this.employees.supplierId],
        empNtn: [this.employees.ntnNumber],
        empKinRelation: [this.employees.nextOfKinRelation],
        empLicenseNo: [''],
        empLicenseExpiry: [''],
        empLicenseIssue: [''],
        empLicenceClass: [''],
        empHVstart: [''],
        empHVend: [''],
        empHVtruck: [''],
        empHVcompany: [''],
        empAddress: this.formBuilder.array([]),
        empContact: this.formBuilder.array([]),
        empContract: this.formBuilder.array([]),
        empTrainingProfile: this.formBuilder.array([]),
        empMedicalReport: this.formBuilder.array([]),
        empReferences: this.formBuilder.array([]),
      });

      // empPicture: "https://api.rayafinancing.com:8012/api/v1/comapny/logo/Hyundai",

      this.showSupplier(this.employees.employeeType);

      // if(this.employees.pictureUrl != ""){
      //   this.addEmployeeForm.controls['empPicture'].setValue([
      //     File environment.docUrl+this.employees.pictureUrl])
      // }

      if (this.employees.position.value == environment.SystemTypes_DRIVER) {
        this.showLicenseField = true;
        this.showTrainingProfileTab = true;
        this.showHeavyVehicleTab = true;
        this.showMedicalTestTab = true;
        this.showReferenceTab = true;
        this.getTrainingProfiles(this.employees.employeeId);
        this.getMedicalReports(this.employees.employeeId);
        this.addEmployeeForm.get('empLicenseNo').setValue(this.employees.licenceNumber);
        this.addEmployeeForm.get('empLicenceClass').setValue(this.employees.licenceClass);
        this.addEmployeeForm.get('empLicenseExpiry').setValue(this.employees.licenceExpiry);
        this.addEmployeeForm.get('empLicenseIssue').setValue(this.employees.licenceIssue);
      }
      else {
        this.showTrainingProfileTab = false;
        this.showHeavyVehicleTab = false;
        this.showMedicalTestTab = false;
        this.showReferenceTab = false;
      }

      for (let i = 0; i < this.employees.employeeAddresses.length; i++) {
        this.addresses = this.addEmployeeForm.get('empAddress') as FormArray;
        this.addresses.push(this.formBuilder.group({
          addressId: [this.employees.employeeAddresses[i].addressId],
          city: [this.employees.employeeAddresses[i].city],
          street1: [this.employees.employeeAddresses[i].street1],
          street2: [this.employees.employeeAddresses[i].street2],
          country: [this.employees.employeeAddresses[i].country],
          zipCode: [this.employees.employeeAddresses[i].zipCode],
          state: [this.employees.employeeAddresses[i].state]
        }));
        // this.addresses.disable();
      }

      for (let i = 0; i < this.employees.employeeContacts.length; i++) {
        this.contacts = this.addEmployeeForm.get('empContact') as FormArray;
        this.contacts.push(this.formBuilder.group({
          contactId: [this.employees.employeeContacts[i].contactId],
          channel: [this.employees.employeeContacts[i].channel],
          data: [this.employees.employeeContacts[i].data]
        }));

      }

      for (let i = 0; i < this.employees.employeeContracts.length; i++) {
        this.contracts = this.addEmployeeForm.get('empContract') as FormArray;
        this.contracts.push(this.formBuilder.group({
          startDate: [this.employees.employeeContracts[i].startDate],
          endDate: [this.employees.employeeContracts[i].endDate],
          salary: [this.employees.employeeContracts[i].salary],
          contractStatus: [this.employees.employeeContracts[i].contractStatus],
          separationReason: [this.employees.employeeContracts[i].separationReason],
          workingExperience: [this.employees.employeeContracts[i].workingExperience],
          previousEmployer1: [this.employees.employeeContracts[i].previousEmployer1],
          previousEmployer2: [this.employees.employeeContracts[i].previousEmployer2]
        }));
      }

    }

  }

  /* ================ EMPLOYEE DETAILS, ADDRESS AND CONTACT FUNCTIONS ======================= */

  createAddress() {
    return this.formBuilder.group({
      addressId: [null],
      city: ['', Validators.required],
      street1: ['', Validators.required],
      street2: [''],
      country: ['Pakistan', Validators.required],
      zipCode: ['', Validators.required],
      state: ['', Validators.required]
    });
  }

  createContact() {
    return this.formBuilder.group({
      contactId: [null],
      channel: ['', Validators.required],
      data: ['', Validators.required]
    });
  }

  createContract() {
    return this.formBuilder.group({
      startDate: [''],
      endDate: [''],
      salary: [''],
      contractStatus: ['Active'],
      separationReason: [''],
      workingExperience: [''],
      previousEmployer1: [''],
      previousEmployer2: [''],
    });
  }

  addNewAddress() {
    this.addresses = this.addEmployeeForm.get('empAddress') as FormArray;
    this.addresses.push(this.createAddress());
  }

  addNewContact() {
    this.contacts = this.addEmployeeForm.get('empContact') as FormArray;
    this.contacts.push(this.createContact());
  }

  addNewContract() {
    this.contracts = this.addEmployeeForm.get('empContract') as FormArray;
    this.contracts.push(this.createContract());
  }

  //Function to Delete a Contact
  deleteContact(data, index) {
    if (index > 0) {
      this.contacts = this.addEmployeeForm.get('empContact') as FormArray;
      this.contacts.removeAt(index);
    }
  }

  //Function to Delete a Contact
  deleteAddress(data, index) {
    // if (index > 0) {
    this.addresses = this.addEmployeeForm.get('empAddress') as FormArray;
    this.addresses.removeAt(index);
    // }
  }

  //Function to Delete a Contract
  deleteContracts(data, index) {
    if (index > 0) {
      this.contracts = this.addEmployeeForm.get('empContract') as FormArray;
      this.contracts.removeAt(index);
    }
  }

  showLicenseDetail(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_DRIVER) {
        this.showLicenseField = true;
      } else {
        this.showLicenseField = false;
      }
    }
  }

  showSupplier(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_CONTRACTUAL) {
        this.showSupplierField = true;
      } else {
        this.showSupplierField = false;
      }
    }
  }

  submitEmployee() {

    if (this.addEmployeeForm.invalid) {
      return;
    }

    for (let i = 0; i < this.addEmployeeForm.value.empContract.length; i++) {
      let startDate = this.datePipe.transform(this.addEmployeeForm.value.empContract[i].startDate, environment.SystemTypes_Date_Format);
      let endDate = this.datePipe.transform(this.addEmployeeForm.value.empContract[i].endDate, environment.SystemTypes_Date_Format);
      this.addEmployeeForm.value.empContract[i].startDate = startDate;
      this.addEmployeeForm.value.empContract[i].endDate = endDate;
    }

    this.employees = {
      employeeId: this.addEmployeeForm.value.empId,
      employeeNumber: this.addEmployeeForm.value.empNo,
      firstName: this.addEmployeeForm.value.firstName,
      lastName: this.addEmployeeForm.value.lastName,
      status: this.addEmployeeForm.value.empStatus,
      department: this.addEmployeeForm.value.empDepartment,
      position: this.addEmployeeForm.value.empPosition,
      companyId: this.addEmployeeForm.getRawValue().empCompany.companyId,
      businessAddress: this.addEmployeeForm.value.businessAddress,
      cnic: this.addEmployeeForm.value.empCnic,
      cnicExpiry: this.datePipe.transform(this.addEmployeeForm.value.cnicExpiry, environment.SystemTypes_Date_Format),
      ntnNumber: this.addEmployeeForm.value.empNtn,
      dateOfBirth: this.datePipe.transform(this.addEmployeeForm.value.empDob, environment.SystemTypes_Date_Format),
      nextOfKin: this.addEmployeeForm.value.empKinName,
      nextOfKinRelation: this.addEmployeeForm.value.empKinRelation,
      employeeAddresses: this.addEmployeeForm.getRawValue().empAddress,
      employeeContacts: this.addEmployeeForm.value.empContact,
      employeeContracts: this.addEmployeeForm.value.empContract,
      employeeType: this.addEmployeeForm.value.empType,
      licenceExpiry: null,
      licenceIssue: null,
      licenceNumber: null,
      licenceClass: null,
      supplierId: null,
      pictureUrl: null,
    }

    if (this.addEmployeeForm.value.empType.value == environment.SystemTypes_CONTRACTUAL) {
      if (this.addEmployeeForm.value.empSupplier != null) {
        this.employees.supplierId = this.addEmployeeForm.value.empSupplier.companyId;
      }
    }

    if (this.employees.position.value == environment.SystemTypes_DRIVER) {
      this.employees.licenceNumber = this.addEmployeeForm.value.empLicenseNo.toString();
      this.employees.licenceExpiry = this.datePipe.transform(this.addEmployeeForm.value.empLicenseExpiry, environment.SystemTypes_Date_Format);
      this.employees.licenceIssue = this.datePipe.transform(this.addEmployeeForm.value.empLicenseIssue, environment.SystemTypes_Date_Format);
      this.employees.licenceClass = this.addEmployeeForm.value.empLicenceClass
    }

    if (this.addEmployeeForm.value.empId == '') {
      this.addEmployee();
    } else {
      this.updateEmployee();
    }
  }

  //FUNCTION TO ADD EMPLOYEE
  addEmployee() {
    this.backendService.addEmployeeService(this.employees).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.addEmployeeForm.get("empId").setValue(res.data.employeeId);
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("EMPLOYEE ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  //FUNCTION TO Update EMPLOYEE
  updateEmployee() {
    this.backendService.updateEmployeeService(this.employees).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("EMPLOYEE UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  //FUNCTION TO Delete EMPLOYEE
  deleteEmployee() {
    if (this.employees.employeeId != null || this.employees.employeeId != "") {
      this.backendService.deleteEmployeeService(this.employees.employeeId).subscribe(res => {
        if (res.success) {
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
          this.router.navigate(['employees'])
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("EMPLOYEE DELETE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    }
  }

  uploadEmployeePicture() {
    if (this.addEmployeeForm.value.empId != "") {
      const formData = new FormData();
      if (this.addEmployeeForm.value.empPicture.length > 0) {
        formData.append('dp', this.addEmployeeForm.value.empPicture[0].file);
        formData.append('employeeId', this.addEmployeeForm.value.empId);
      }
      this.backendService.uploadEmployeeProfilePic(formData).subscribe(res =>{
        console.log(res);
      })
      // console.log(this.addEmployeeForm.value.empPicture);
    }else{
      this.errorMsgService.getInfoMessage("Please add the Employee First")
      this.openInfoDialog();
    }
  }

  /* ================ COMPARING DROPDOWN FUNCTIONS ======================= */

  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

  compareCompany(o1, o2): boolean {
    return o1.companyId === o2.companyId;
  }

  compareLocation(o1, o2): boolean {
    return o1.street1 === o2.street1;
  }

  /* ================ DROPDOWNS FUNCTIONS ======================= */

  getCompaniesDropdown() {
    this.backendService.getAdminCompanies().subscribe(res => {
      this.companiesDropDown = res.data;
      if (this.addEmployeeForm.value.empId != "") {
        this.addEmployeeForm.get('empCompany').setValue(this.companiesDropDown[0]);
        this.addEmployeeForm.get('empCompany').disable();
        this.getCompanyAddresses(this.companiesDropDown[0]);
        this.addEmployeeForm.get('businessAddress').setValue(this.employees.businessAddress);
      } else {
        this.addEmployeeForm.get('empCompany').setValue(this.companiesDropDown[0]);
        this.addEmployeeForm.get('empCompany').disable();
        this.getCompanyAddresses(this.companiesDropDown[0]);
      }
    })
  }

  getCompanyAddresses(data) {
    this.companyAddress = data.addresses;
  }

  getSupplierCompaniesDropdown() {
    this.backendService.getAllFilterDriverSupplier().subscribe(res => {
      if (res.success) {
        this.supplierCompanyDropDown = res.data;
        if (this.addEmployeeForm.value.empId != "") {
          if (this.employees.supplierId != null) {
            for (let i = 0; i < this.supplierCompanyDropDown.length; i++) {
              if (this.supplierCompanyDropDown[i].companyId == this.employees.supplierId) {
                this.addEmployeeForm.get('empSupplier').setValue(this.supplierCompanyDropDown[i]);
              }
            }
          }
        }
      }
    })
  }

  getContactChannelDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.channelTypeDropDown = res.data;
      if (this.addEmployeeForm.value.empId == "") {
        for (let i = 0; i < this.channelTypeDropDown.length; i++) {
          this.contacts = this.addEmployeeForm.get('empContact') as FormArray;
          this.contacts.push(this.formBuilder.group({
            contactId: [null],
            channel: [this.channelTypeDropDown[i]],
            data: ['']
          }));
        }
      }
    })
  }

  getDepartmentDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.departmentDropDown = res.data;
    })
  }

  getPositionDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.positionDropDown = res.data;
    })
  }

  getEmployeeTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.employeeTypeDropDown = res.data;
    })
  }

  getStateDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.stateDropDown = res.data;
    })
  }

  getCityDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.cityDropDown = res.data;
    })
  }

  /* ================ TRAINING PROFILE TAB FUNCTIONS ======================= */

  createTrainingProfile() {
    return this.formBuilder.group({
      trainingProfileId: [''],
      trainingFile: [''],
      trainingPicture: [''],
      trainingAttendance: [''],
      trainingType: [''],
      trainingDate: [''],
      trainingExpiry: [''],
      trainerName: [''],
      trainerStation: [''],
      trainingProfileURL: [null],
      attendanceSheetURL: [null],
      pictureWithTrainerURL: [null]
    });
  }

  addTrainingProfile() {
    this.trainingProfiles = this.addEmployeeForm.get('empTrainingProfile') as FormArray;
    this.trainingProfiles.push(this.createTrainingProfile());
  }

  deleteTrainingProfile(index, id) {
    this.backendService.deleteTrainingProfileService(id).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.trainingProfiles = this.addEmployeeForm.get('empTrainingProfile') as FormArray;
        this.trainingProfiles.removeAt(index);
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("TRAINING PROFILE DELETE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  submitTrainingProfile(profile) {
    //console.log("PROFILE", profile.value)
    const formData = new FormData();
    if (profile.value.trainingFile.length > 0) {
      formData.append('trainingCertificate', profile.value.trainingFile[0].file);
    }
    if (profile.value.trainingPicture.length > 0) {
      formData.append('pictureWithTrainer', profile.value.trainingPicture[0].file);
    }
    if (profile.value.trainingAttendance.length > 0) {
      formData.append('attendanceSheet', profile.value.trainingAttendance[0].file);
    }
    formData.append('trainingProfileId', profile.value.trainingProfileId);
    formData.append('trainingType', profile.value.trainingType);
    formData.append('issuedDate', this.datePipe.transform(profile.value.trainingDate, environment.SystemTypes_Date_Format));
    formData.append('expiryDate', this.datePipe.transform(profile.value.trainingExpiry, environment.SystemTypes_Date_Format));
    formData.append('trainerName', profile.value.trainerName);
    formData.append('trainingStation', profile.value.trainerStation);
    formData.append('trainingCertificateUrl', profile.value.trainingProfileURL);
    formData.append('attendanceSheetUrl', profile.value.attendanceSheetURL);
    formData.append('pictureWithTrainerUrl', profile.value.pictureWithTrainerURL);
    formData.append('employeeId', this.addEmployeeForm.value.empId);

    if (profile.value.trainingProfileId != "") {
      this.updateTrainingProfile(formData);
    } else {
      this.uploadTrainingProfile(formData);
    }
  }

  uploadTrainingProfile(data) {
    this.backendService.uploadTrainingProfile(data).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("TRAINING PROFILE ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateTrainingProfile(data) {
    this.backendService.updateTrainingProfile(data).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("TRAINING PROFILE UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  getTrainingProfiles(id) {
    this.backendService.getTrainingProfile(id).subscribe(res => {
      //console.log("TRAINING PROFILE DATA", res.data);
      if (res.success) {
        for (let i = 0; i < res.data.length; i++) {
          if (res.data[i].trainingCertificateUrl != null && res.data[i].trainingCertificateUrl != '') {
            res.data[i].trainingCertificateUrl = environment.docUrl + res.data[i].trainingCertificateUrl
          }
          if (res.data[i].attendanceSheetUrl != null && res.data[i].attendanceSheetUrl != '') {
            res.data[i].attendanceSheetUrl = environment.docUrl + res.data[i].attendanceSheetUrl
          }
          if (res.data[i].pictureWithTrainerUrl != null && res.data[i].pictureWithTrainerUrl != '') {
            res.data[i].pictureWithTrainerUrl = environment.docUrl + res.data[i].pictureWithTrainerUrl
          }
          this.trainingProfiles = this.addEmployeeForm.get('empTrainingProfile') as FormArray;
          this.trainingProfiles.push(this.formBuilder.group({
            trainingProfileId: [res.data[i].trainingProfileId],
            trainingFile: [''],
            trainingPicture: [''],
            trainingAttendance: [''],
            trainingType: [res.data[i].trainingType],
            trainingDate: [res.data[i].issuedDate],
            trainingExpiry: [res.data[i].expiryDate],
            trainerName: [res.data[i].trainerName],
            trainerStation: [res.data[i].trainingStation],
            trainingProfileURL: [res.data[i].trainingCertificateUrl ? res.data[i].trainingCertificateUrl : null],
            attendanceSheetURL: [res.data[i].attendanceSheetUrl ? res.data[i].attendanceSheetUrl : null],
            pictureWithTrainerURL: [res.data[i].pictureWithTrainerUrl ? res.data[i].pictureWithTrainerUrl : null]
          }));
        }
      }
    })
  }

  viewFile(url) {
    window.open(url, "_blank");
  }

  deleteTrainingProfUrl(id) {
    this.trainingProfiles = this.addEmployeeForm.get('empTrainingProfile') as FormArray;
    this.trainingProfiles.at(id).get('trainingProfileURL').setValue(null)
    // this.trainingProfiles.at(id).value.trainingProfileURL = null
    // this.cdRef.detectChanges();
    // this.backendService.deleteFileService(url).subscribe(res => {
    //   if (res.success) {
    //     this.errorMsgService.getSuccessMessage(res.message);
    //     this.openSuccessDialog();
    //     this.trainingProfiles = this.addEmployeeForm.get('empTrainingProfile') as FormArray;
    //     this.trainingProfiles.at(id).value.trainingProfileURL = ""
    //     this.cdRef.detectChanges();
    //   } else {
    //     this.errorMsgService.getErrorMessage(res.errorMessage);
    //     this.openErrorDialog();
    //   }
    //   //console.log("TRAINING PROFILE UPDATE RESPONSE", res);
    // }, (err: HttpErrorResponse) => {
    //   if (err.status == 500) {
    //     this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
    //     this.openInfoDialog();
    //   }
    // });
  }

  deleteTrainingAttendaceUrl(id) {
    this.trainingProfiles = this.addEmployeeForm.get('empTrainingProfile') as FormArray;
    this.trainingProfiles.at(id).get('attendanceSheetURL').setValue(null)
  }

  deleteTrainingPicUrl(id) {
    this.trainingProfiles = this.addEmployeeForm.get('empTrainingProfile') as FormArray;
    this.trainingProfiles.at(id).get('pictureWithTrainerURL').setValue(null)
  }
  /* ================ MEDICAL REPORT TAB FUNCTIONS ======================= */

  createMedicalReport() {
    return this.formBuilder.group({
      medicalId: [''],
      medFile: [''],
      medType: [''],
      medDate: [''],
      medResult: [''],
      medHospName: [''],
      medStation: [''],
      medicalUrl: ['']
    });
  }

  addMedicalReport() {
    this.medicalTestReports = this.addEmployeeForm.get('empMedicalReport') as FormArray;
    this.medicalTestReports.push(this.createMedicalReport());
  }

  deleteMedicalReport(index, id) {
    this.backendService.deleteMedicalReportService(id).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.medicalTestReports = this.addEmployeeForm.get('empMedicalReport') as FormArray;
        this.medicalTestReports.removeAt(index);
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("MEDICAL REPORT DELETE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  submitMedicalReport(reports) {
    //console.log("MEDICAL REPORTS", reports.value)
    const formData = new FormData();
    if (reports.value.medFile.length > 0) {
      formData.append('file', reports.value.medFile[0].file);
    }
    formData.append('mtrId', reports.value.medicalId);
    formData.append('mtrType', reports.value.medType);
    formData.append('mtrDate', this.datePipe.transform(reports.value.medDate, environment.SystemTypes_Date_Format));
    formData.append('mtrHospital', reports.value.medHospName);
    formData.append('mtrResult', reports.value.medResult);
    formData.append('mtrStation', reports.value.medStation);
    formData.append('employeeId', this.addEmployeeForm.value.empId);

    if (reports.value.medicalId != "") {
      this.updateMedicalReports(formData);
    } else {
      this.uploadMedicalReports(formData);
    }
  }

  uploadMedicalReports(meddata) {
    this.backendService.uploadMedicalReport(meddata).subscribe(res => {
      //console.log("MEDICAL REPORT ADD RESPONSE", res);
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateMedicalReports(meddata) {
    this.backendService.updateMedicalReport(meddata).subscribe(res => {
      //console.log("MEDICAL REPORT UPDATE RESPONSE", res);
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  getMedicalReports(id) {
    this.backendService.getMedicalReport(id).subscribe(res => {
      //console.log("MEDICAL REPORT GET DATA", res.data);
      if (res.success) {
        for (let i = 0; i < res.data.length; i++) {
          this.medicalTestReports = this.addEmployeeForm.get('empMedicalReport') as FormArray;
          this.medicalTestReports.push(this.formBuilder.group({
            medicalId: [res.data[i].mtrId],
            medFile: [''],
            medType: [res.data[i].mtrType],
            medDate: [res.data[i].mtrDate],
            medResult: [res.data[i].mtrResult],
            medHospName: [res.data[i].mtrHospital],
            medStation: [res.data[i].mtrStation],
            medicalUrl: [this.backendService.url + 'files/' + res.data[i].url ? res.data[i].url : '']
          }));
        }
      }
    })
  }

  deleteMedicalUrl() {

  }

  viewMedicalReportFile(data) {

  }

  /* ================ REFERENCES TAB FUNCTIONS ======================= */

  createEmployeeReference() {
    return this.formBuilder.group({
      refId: [''],
      refFile: [''],
      refForm: [''],
      refName: [''],
      refContact: [''],
      refRelation: [''],
      refAddress: [''],
      refCnic: [''],
      refFormUrl: [''],
      refCnicUrl: ['']
    });
  }

  addEmployeeReference() {
    this.employeeReferences = this.addEmployeeForm.get('empReferences') as FormArray;
    this.employeeReferences.push(this.createEmployeeReference());
  }

  deleteEmployeeReference(index, id) {
    this.backendService.deleteEmployeeReferenceService(id).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.employeeReferences = this.addEmployeeForm.get('empReferences') as FormArray;
        this.employeeReferences.removeAt(index);
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("EMPLOYEE REFERENCE DELETE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  submitEmployeeReference(ref) {
    //console.log("REFERENCE", ref.value)
    const formData = new FormData();
    if (ref.value.refFile.length > 0) {
      formData.append('cnic', ref.value.refFile[0].file);
    }
    if (ref.value.refForm.length > 0) {
      formData.append('referenceForm', ref.value.refForm[0].file);
    }
    formData.append('referenceId', ref.value.refId);
    formData.append('referenceName', ref.value.refName);
    formData.append('referenceCnic', ref.value.refCnic);
    formData.append('referenceRelation', ref.value.refRelation);
    formData.append('referenceContact', ref.value.refContact);
    formData.append('referenceAddress', ref.value.refAddress);
    formData.append('employeeId', this.addEmployeeForm.value.empId);

    if (ref.value.refId != "") {
      this.updateEmployeeReference(formData);
    } else {
      this.uploadEmployeeReference(formData);
    }
  }

  uploadEmployeeReference(data) {
    this.backendService.addEmployeeReferences(data).subscribe(res => {
      //console.log("EMPLOYEE REFERENCES UPLOAD RESPONSE", res);
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateEmployeeReference(data) {
    this.backendService.addEmployeeReferences(data).subscribe(res => {
      //console.log("EMPLOYEE REFERENCES UPDATE RESPONSE", res);
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  getEmployeeReference(id) {
    this.backendService.getEmployeeReferences(id).subscribe(res => {
      //console.log("EMPLOYEE REFERENCES GET DATA", res.data);
      if (res.success) {
        for (let i = 0; i < res.data.length; i++) {
          this.employeeReferences = this.addEmployeeForm.get('empReferences') as FormArray;
          this.employeeReferences.push(this.formBuilder.group({
            refId: [res.data.referenceId],
            refFile: [''],
            refForm: [''],
            refName: [res.data.referenceName],
            refContact: [res.data.referenceContact],
            refRelation: [res.data.referenceRelation],
            refAddress: [res.data.referenceAddress],
            refCnic: [res.data.referenceCnic],
            refFormUrl: [res.data.referenceFormUrl],
            refCnicUrl: [res.data.cnicUrl]
          }));
        }
      }
    })
  }

  deleteRefFormUrl() {

  }

  viewRefFormFile(data) {

  }

  deleteRefCnicUrl() {

  }

  /* ================ MEDICAL REPORT TAB FUNCTIONS ======================= */
  addHeavyVehicleExp() {
    let heavyVehData = {
      heavyVehicleExperienceId: "",
      experienceStartDate: this.datePipe.transform(this.addEmployeeForm.value.empHVstart, environment.SystemTypes_Date_Format),
      experienceEndDate: this.datePipe.transform(this.addEmployeeForm.value.empHVend, environment.SystemTypes_Date_Format),
      trucksOperated: this.addEmployeeForm.value.empHVtruck,
      experienceCompanyName: this.addEmployeeForm.value.empHVcompany,
      employeeId: this.addEmployeeForm.value.empId,
    }

    this.backendService.addHeavyVehiclesExperience(heavyVehData).subscribe(res => {
      if (res.success) {
        this.addEmployeeForm.get("empHVstart").reset();
        this.addEmployeeForm.get("empHVend").reset();
        this.addEmployeeForm.get("empHVtruck").reset();
        this.addEmployeeForm.get("empHVcompany").reset();
        this.getHeavyVehicle.push(res.data);
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("HEAVY VEHICLE EXP RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  getHeavyVehicleExperiences(id) {
    this.backendService.getEmpVehicleExperience(id).subscribe(res => {
      if (res.success) {
        this.getHeavyVehicle = res.data;
      }
    })
  }

  deleteHeavyVehicleExp(vehicle: any) {
    // this.heavyVehicles = <FormArray>this.addEmployeeForm.get('heavyVehicleExperience');
    // this.heavyVehicles.removeAt(id);
    //console.log("DELETE HEAVY VEHICLE DATA", vehicle)
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
