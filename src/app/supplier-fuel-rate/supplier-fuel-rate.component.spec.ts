import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierFuelRateComponent } from './supplier-fuel-rate.component';

describe('SupplierFuelRateComponent', () => {
  let component: SupplierFuelRateComponent;
  let fixture: ComponentFixture<SupplierFuelRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierFuelRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierFuelRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
