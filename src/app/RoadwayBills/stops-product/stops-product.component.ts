import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-stops-product',
  templateUrl: './stops-product.component.html',
  styleUrls: ['./stops-product.component.scss']
})
export class StopsProductComponent implements OnInit {

  categoryType = environment.CATEGORY_TYPE;
  stopTypeCode = environment.STOP_TYPE;
  cityType = environment.CITY_TYPE;
  stateTypeDropdownCode = environment.STATE_TYPE;


  stopForm: FormGroup;
  orderStop: FormArray;
  orderProduct: FormArray;
  roadwayBill: any = null;
  shipperDropDown: any;
  filteredShipper: any;
  cityDropDown: any;
  stateDropDown: any;
  stopTypeDropDown: any = [];
  filteredCities: any = [];
  categoryTypeDropDown: any;
  editForm = false;


  constructor(public backendService: BackEndService,
    private formBuilder: FormBuilder,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {}

  /* ======================= ONINIT COMPONENT FUNCTION ============================= */

  ngOnInit() {

    this.stopForm = this.formBuilder.group({
      rwbId: [null],
      loadStop: this.formBuilder.array([]),
      loadProduct: this.formBuilder.array([])
    })


    this.getStopTypeDropdown(this.stopTypeCode);
    this.getCategoryTypeDropdown(this.categoryType);
    this.getCityDropdown(this.cityType);
    this.getStateDropdown(this.stateTypeDropdownCode);
    this.roadwayBill = this.backendService.stopData;

    if (this.roadwayBill != null) {
      this.stopForm.get('rwbId').setValue(this.roadwayBill.rwbId);

      if(this.roadwayBill.customer != null){
        this.getShipperConsignee(this.roadwayBill.customer.companyId)
      }else{
        this.getShipperConsignee(this.roadwayBill.broker.companyId)
      }
      this.getAllLoadStops(this.roadwayBill.rwbId)

      if (this.roadwayBill.products != null) {
        for (let i = 0; i < this.roadwayBill.products.length; i++) {
          this.orderProduct = this.stopForm.get('loadProduct') as FormArray;
          this.orderProduct.push(this.formBuilder.group({
            productId: [this.roadwayBill.products[i].productId],
            category: [this.roadwayBill.products[i].category],
            commodity: [this.roadwayBill.products[i].commodity],
            weight: [this.roadwayBill.products[i].weight],
            weightUnit: [this.roadwayBill.products[i].weightUnit],
            packageCount: [this.roadwayBill.products[i].packageCount],
            temperature: [this.roadwayBill.products[i].temperature],
            roadwayBillId: [this.roadwayBill.products[i].roadwayBillId]
          }));
        }
      }
    }
  }

  onChanges(event) {
    if (event.target.value.length > 0) {
      const filterValue = event.target.value.toLowerCase();
      this.filteredShipper = this.shipperDropDown.filter(option => {
        return option.name.toLowerCase().includes(filterValue)
      })
    } else {
      this.filteredShipper = this.shipperDropDown;
    }
  }

  onSearchCity(id): void {
		this.orderStop = this.stopForm.get('loadStop') as FormArray;
		this.orderStop.at(id).get("city").valueChanges.subscribe(val => {
			if (val.length > 0) {
				const filterValue = val.toLowerCase();
				this.filteredCities = this.cityDropDown.filter(option => {
					return option.value.toLowerCase().includes(filterValue)
				})
			} else {
				this.filteredCities = this.cityDropDown;
			}
		})
	}

  showStopAddress(data, index) {
    let db = this.stopForm.get('loadStop') as FormArray;
    let dataGroup = db.at(index)
    dataGroup.get('warehouseId').setValue(data.warehouseId)
    dataGroup.get('company').setValue(data.name)
    dataGroup.get('country').setValue(data.address.country);
    dataGroup.get('city').setValue(data.address.city);
    dataGroup.get('street1').setValue(data.address.street1);
    dataGroup.get('street2').setValue(data.address.street2);
    dataGroup.get('state').setValue(data.address.state);
    dataGroup.get('zipCode').setValue(data.address.zipCode);
    dataGroup.get('contact1').setValue(data.phone);
    dataGroup.get('email').setValue(data.email);
  }

  resetStopField(item) {
    item.get('warehouseId').reset();
    item.get('company').reset();
    item.get('city').reset();
    item.get('street1').reset();
    item.get('street2').reset();
    item.get('state').reset();
    item.get('zipCode').reset();
    item.get('contact1').reset();
    item.get('email').reset();
  }

  //============================== DROPDOWN FUNCTIONS ===============================
  getStopTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        res.data.forEach(element => {
          if (element.value !== environment.SystemTypes_MAINTENANCE && element.value !== environment.SystemTypes_FUEL)
            this.stopTypeDropDown.push(element);
        });

      }
    })
  }

  getCategoryTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.categoryTypeDropDown = res.data;
      }
    })
  }

  getShipperConsignee(id) {
      this.backendService.getWarehousesService(id).subscribe(res => {
        if (res.success) {
          this.shipperDropDown = res.data;
          this.filteredShipper = res.data;
        }
      })
  }

  getCityDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.cityDropDown = res.data;
      }
    })
  }

  getStateDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.stateDropDown = res.data;
    })
  }

  getAllLoadStops(id){
    this.backendService.getAllLoadStopsById(id).subscribe(res =>{
      if(res.success){
        if(res.data.length > 0){
          for (let i = 0; i < res.data.length; i++) {
            if (res.data[i].appointmentStartTime != null) {
              res.data[i].appointmentStartTime = new Date(res.data[i].appointmentStartTime)
            }
            this.orderStop = this.stopForm.get('loadStop') as FormArray;
            this.orderStop.push(this.formBuilder.group({
              stopId: [res.data[i].stopId],
              sequenceNum: [res.data[i].stopSequenceNumber],
              stopType: [res.data.stopType],
              warehouseId: [res.data[i].warehouse.warehouseId],
              refNum: [res.data[i].referenceNumber],
              company: [res.data[i].warehouse.name],
              arrivalDate: [res.data[i].appointmentStartTime],
              street1: [res.data[i].warehouse.address.street1],
              street2: [res.data[i].warehouse.address.street2],
              city: [res.data[i].warehouse.address.city],
              country: [res.data[i].warehouse.address.country],
              zipCode: [null],
              state: [res.data[i].warehouse.address.state],
              latitude: [res.data[i].warehouse.address.latitude],
              longitude: [res.data[i].warehouse.address.longitude],
              contact1: [res.data[i].warehouse.phone],
              email: [res.data[i].warehouse.email]
            }));
          }
        }
      }
    })
  }

  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

   // ============================== ADD NEW STOP FUNCTIONS ===============================
   createStop() {
    return this.formBuilder.group({
      stopId: [null],
      sequenceNum: [null],
      stopType: [null],
      refNum: [null],
      warehouseId: [null],
      company: [null],
      arrivalDate: [null],
      street1: [null],
      street2: [null],
      city: [null],
      country: ['Pakistan'],
      zipCode: [null],
      longitude: [null],
      latitude: [null],
      state: [null],
      contact1: [null],
      email: [null]
    });
  }

  addNewStopInFormArray() {
    this.orderStop = this.stopForm.get('loadStop') as FormArray;
    this.orderStop.push(this.createStop());
  }

  submitLoadStop(data, index) {
    let stopData = {
      stopId: data.stopId,
      stopSequenceNumber: data.sequenceNum,
      stopType: data.stopType,
      warehouse: {
        warehouseId: data.warehouseId,
        name: data.company,
        address: {
          country: data.country,
          city: data.city,
          state: data.state,
          street1: data.street1,
          street2: data.street2,
          zipCode: data.zipCode,
          longitude: data.longitude,
          latitude: data.latitude,
        },
        phone: data.contact1,
        email: data.email,
      },
      appointmentStartTime: this.datePipe.transform(data.arrivalDate, "yyyy-MM-dd HH:mm:ss"),
      appointmentEndTime: null,
      referenceNumber: data.refNum,
      roadwayBillId: this.stopForm.value.rwbId
    }
    // console.log("SUBMIT LOADSTOP", stopData);
    if (stopData.stopId == null) {
      this.addLoadStop(stopData, index);
    } else {
      this.updateLoadStop(stopData);
    }
  }

  addLoadStop(data, id) {
    this.backendService.addLoadStopService(data).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.stopForm.get('loadStop').get([id]).get('stopId').setValue(res.data.stopId);
        this.stopForm.get('loadStop').get([id]).get('warehouseId').setValue(res.data.warehouse.warehouseId);
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("RWB STOPS ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateLoadStop(data) {
    this.backendService.updateLoadStopService(data).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("RWB STOPS UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteLoadStop(id, index) {
    if (id != null) {
      this.backendService.deleteLoadStopService(id).subscribe(res => {
        if (res.success) {
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
          this.orderStop = this.stopForm.get('loadStop') as FormArray;
          this.orderStop.removeAt(index)
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("RWB UPDATE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    } else {
      this.orderStop = this.stopForm.get('loadStop') as FormArray;
      this.orderStop.removeAt(index)
    }
  }

  //============================== ADD NEW PRODUCT FUNCTIONS ===============================
  createProduct() {
    return this.formBuilder.group({
      productId: [null],
      category: [null],
      commodity: [null],
      weight: [null],
      weightUnit: ['Ton'],
      packageCount: [null],
      temperature: [null],
      roadwayBillId: [this.stopForm.value.rwbId]
    });
  }

  addProductInFormArray() {
    this.orderProduct = this.stopForm.get('loadProduct') as FormArray;
    this.orderProduct.push(this.createProduct());
  }

  submitLoadProduct(data, index) {
    let prodData = data;
    // console.log("SUBMIT LOADPRODUCT", prodData);
    if (prodData.productId == null) {
      this.addLoadProduct(prodData, index);
    } else {
      this.updateLoadProduct(prodData);
    }
  }

  addLoadProduct(data, id) {
    this.backendService.addLoadProductService(data).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.stopForm.get('loadProduct').get([id]).get('productId').setValue(res.data);
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("RWB PRODUCT ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateLoadProduct(data) {
    this.backendService.updateLoadProductService(data).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("RWB PRODUCT UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteLoadProduct(id, index) {
    if (id != null) {
      this.backendService.deleteLoadProductService(id).subscribe(res => {
        if (res.success) {
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
          this.orderProduct = this.stopForm.get('loadProduct') as FormArray;
          this.orderProduct.removeAt(index)
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("RWB PRODUCT DELETE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    } else {
      this.orderProduct = this.stopForm.get('loadProduct') as FormArray;
      this.orderProduct.removeAt(index)
    }
  }

  displayCity(cit): string {
    return cit && cit.value;
  }

   /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
