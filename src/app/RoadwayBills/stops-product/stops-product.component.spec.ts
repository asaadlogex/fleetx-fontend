import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StopsProductComponent } from './stops-product.component';

describe('StopsProductComponent', () => {
  let component: StopsProductComponent;
  let fixture: ComponentFixture<StopsProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StopsProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StopsProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
