import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-load-progress',
  templateUrl: './load-progress.component.html',
  styleUrls: ['./load-progress.component.scss']
})
export class LoadProgressComponent implements OnInit {

  stopProgTypeDropDown: any;
  progressForm: FormGroup;
  orderProgress: FormArray;
  roadwayBill: any = null;
  cityDropDown: any;
  stateDropDown: any;
  stopTypeDropDown: any = [];
  equipmentDropDown: any;
  equipmentSizeDropDown: any;
  brokerDropDown: any;
  driverDropDown: any;
  containerDropDown: any;
  tractorDropDown: any;
  trailerDropDown: any;
  vehicleOwnerDropDown: any;
  editForm = false;
  addProgressDiv = false;
  showProgRentalField = false;

  constructor(public backendService: BackEndService,
    private formBuilder: FormBuilder,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) { }

  /* ======================= ONINIT COMPONENT FUNCTION ============================= */

  ngOnInit() {
    this.progressForm = this.formBuilder.group({
      rwbId: [null],
      loadProgress: this.formBuilder.array([]),
    })

    this.roadwayBill = this.backendService.progressData;

    if (this.roadwayBill != null) {
      this.progressForm.get('rwbId').setValue(this.roadwayBill.rwbId);
      this.getAllLoadProgress(this.roadwayBill.rwbId);
      this.stopProgTypeDropDown = this.roadwayBill.stopProgTypeDropDown;
      this.stateDropDown = this.roadwayBill.stateDropDown;
      this.cityDropDown = this.roadwayBill.cityDropDown;
      this.tractorDropDown = this.roadwayBill.tractorDropDown;
      this.trailerDropDown = this.roadwayBill.trailerDropDown;
      this.containerDropDown = this.roadwayBill.containerDropDown;
      this.driverDropDown = this.roadwayBill.driverDropDown;
      this.vehicleOwnerDropDown = this.roadwayBill.vehicleOwnerDropDown;
      this.equipmentDropDown = this.roadwayBill.equipmentDropDown;
      this.equipmentSizeDropDown = this.roadwayBill.equipmentSizeDropDown;
      this.brokerDropDown = this.roadwayBill.brokerDropDown;
    }

  }

  /* ======================= HIDE EDIT DISPLAY FUNCTION ============================= */

  displayProgRentalFields(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_RENTAL) {
        this.showProgRentalField = true;
      } else {
        this.showProgRentalField = false;
      }
    }
  }

  /*============================== ADD UPDATE DELETE LOAD PROGRESS FUNCTIONS ===============================*/
  newProgressAdded(event) {
    if (event.arrivalTime != null || event.departureTime != null) {
      event.arrivalTime = new Date(event.arrivalTime)
      event.departureTime = new Date(event.departureTime)
    }
    this.orderProgress = this.progressForm.get('loadProgress') as FormArray;
    this.orderProgress.push(this.formBuilder.group({
      progressId: [event.stopProgressId],
      progressType: [event.stopProgressType],
      progressTractor: [event.tractor],
      progressTrailer: [event.trailer],
      progressContainer: [event.container],
      progressDriver1: [event.driver1],
      progressDriver2: [event.driver2],
      progressVehicleOwn: [event.vehicleOwnership],
      progressEquipSize: [event.rentalVehicle ? event.rentalVehicle.equipmentSize : null],
      progressVehicleEquipment: [event.rentalVehicle ? event.rentalVehicle.equipmentType : null],
      progressRentalId: [event.rentalVehicle ? event.rentalVehicle.rentalVehicleId : null],
      progressRentalBroker: [event.rentalVehicle ? event.rentalVehicle.broker : null],
      progressVehicleNumber: [event.rentalVehicle ? event.rentalVehicle.vehicleNumber : null],
      progressCurrentKm: [event.currentKm],
      progressArrival: [event.arrivalTime],
      progressDeparture: [event.departureTime],
      progressDetention: [event.detentionInHrs],
      progressInstruction: [event.instruction],
      progressStopId: [event.stopId],
      progressRwbId: [event.roadwayBillId],
      progressWarehouseId: [event.warehouse.warehouseId],
      progressWarehouseName: [event.warehouse.name],
      progressAddressId: [event.warehouse.address.addressId],
      progressStreet1: [event.warehouse.address.street1],
      progressStreet2: [event.warehouse.address.street2],
      progressCity: [event.warehouse.address.city],
      progressState: [event.warehouse.address.state],
      progressZipCode: [null],
      progressCountry: [event.warehouse.address.country],
      progressLatitude: [event.warehouse.address.latitude],
      progressLongitude: [event.warehouse.address.longitude],
      progressPhone: [event.warehouse.phone],
      progressEmail: [event.warehouse.email],
    }));
    this.addProgress(false);
  }

  addProgress(state) {
    if(state == true){
      this.backendService.progressData = this.roadwayBill;
    }
    this.addProgressDiv = state;
  }

  makeEditable() {
    this.editForm = !this.editForm
  }

  updateLoadProgress(data) {
    let progData = {
      stopProgressId: data.progressId,
      stopProgressType: data.progressType,
      tractor: null,
      trailer: null,
      container: null,
      driver1: null,
      driver2: null,
      vehicleOwnership: data.progressVehicleOwn,
      rentalVehicle: null,
      currentKm: data.progressCurrentKm,
      arrivalTime: this.datePipe.transform(data.progressArrival, "yyyy-MM-dd HH:mm:ss"),
      departureTime: this.datePipe.transform(data.progressDeparture, "yyyy-MM-dd HH:mm:ss"),
      detentionInHrs: data.progressDetention,
      instruction: data.progressInstruction,
      stopId: data.progressStopId,
      roadwayBillId: data.progressRwbId,
      warehouse: {
        warehouseId: data.progressWarehouseId,
        name: data.progressWarehouseName,
        address: {
          addressId: data.progressAddressId,
          street1: data.progressStreet1,
          street2: data.progressStreet2,
          city: data.progressCity,
          state: data.progressState,
          zipCode: null,
          country: data.progressCountry,
          latitude: data.progressLatitude,
          longitude: data.progressLongitude,
        },
        phone: data.progressPhone,
        email: data.progressEmail
      }
    }
    if (progData.vehicleOwnership.value == environment.SystemTypes_RENTAL) {
      progData.rentalVehicle = {
        rentalVehicleId: null,
        equipmentType: data.progressVehicleEquipment,
        equipmentSize: data.progressEquipSize,
        broker: data.progressRentalBroker,
        vehicleNumber: data.progressVehicleNumber
      }
    } else {
      progData.tractor = data.progressTractor;
      progData.trailer = data.progressTrailer;
      progData.container = data.progressContainer;
      progData.driver1 = data.progressDriver1;
      progData.driver2 = data.progressDriver2;
    }
    this.backendService.updateLoadProgressService(progData).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.makeEditable();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("UPDATE PROGRESS RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  closeProgress() {
    this.addProgress(false);
  }

  deleteLoadProgress(id, index) {
    this.backendService.deleteLoadProgressService(id).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.orderProgress = this.progressForm.get('loadProgress') as FormArray;
        this.orderProgress.removeAt(index)
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("DELETE LOAD PROGRESS RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  getAllLoadProgress(id){
    this.backendService.getAllLoadProgressService(id).subscribe(res =>{
      if(res.success){
        if(res.data.length > 0){
          for (let i = 0; i < res.data.length; i++) {
            if (res.data[i].arrivalTime != null) {
              res.data[i].arrivalTime = new Date(res.data[i].arrivalTime)
            }
            if (res.data[i].departureTime != null) {
              res.data[i].departureTime = new Date(res.data[i].departureTime)
            }
            if (res.data[i].rentalVehicle != null) {
              this.showProgRentalField = true;
            }
            this.orderProgress = this.progressForm.get('loadProgress') as FormArray;
            this.orderProgress.push(this.formBuilder.group({
              progressId: [res.data[i].stopProgressId],
              progressType: [res.data[i].stopProgressType],
              progressTractor: [res.data[i].tractor],
              progressTrailer: [res.data[i].trailer],
              progressContainer: [res.data[i].container],
              progressDriver1: [res.data[i].driver1],
              progressDriver2: [res.data[i].driver2],
              progressVehicleOwn: [res.data[i].vehicleOwnership],
              progressEquipSize: [res.data[i].rentalVehicle ? res.data[i].rentalVehicle.equipmentSize : null],
              progressVehicleEquipment: [res.data[i].rentalVehicle ? res.data[i].rentalVehicle.equipmentType : null],
              progressRentalId: [res.data[i].rentalVehicle ? res.data[i].rentalVehicle.rentalVehicleId : null],
              progressRentalBroker: [res.data[i].rentalVehicle ? res.data[i].rentalVehicle.broker : null],
              progressVehicleNumber: [res.data[i].rentalVehicle ? res.data[i].rentalVehicle.vehicleNumber : null],
              progressCurrentKm: [res.data[i].currentKm],
              progressArrival: [res.data[i].arrivalTime],
              progressDeparture: [res.data[i].departureTime],
              progressDetention: [res.data[i].detentionInHrs],
              progressInstruction: [res.data[i].instruction],
              progressStopId: [res.data[i].stopId],
              progressRwbId: [res.data[i].roadwayBillId],
              progressWarehouseId: [res.data[i].warehouse.warehouseId],
              progressWarehouseName: [res.data[i].warehouse.name],
              progressAddressId: [res.data[i].warehouse.address.addressId],
              progressStreet1: [res.data[i].warehouse.address.street1],
              progressStreet2: [res.data[i].warehouse.address.street2],
              progressCity: [res.data[i].warehouse.address.city],
              progressState: [res.data[i].warehouse.address.state],
              progressZipCode: [null],
              progressCountry: [res.data[i].warehouse.address.country],
              progressLatitude: [res.data[i].warehouse.address.latitude],
              progressLongitude: [res.data[i].warehouse.address.longitude],
              progressPhone: [res.data[i].warehouse.phone],
              progressEmail: [res.data[i].warehouse.email],
            }));
          }
        }
      }
    })
  }

  //============================== COMPARE DROPDOWN FUNCTION ===============================
  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

  compareTractor(o1, o2): boolean {
    if (o2 != null)
      return o1.tractorId === o2.tractorId;
  }

  compareTrailer(o1, o2): boolean {
    if (o2 != null)
      return o1.trailerId === o2.trailerId;
  }

  compareContainer(o1, o2): boolean {
    if (o2 != null)
      return o1.containerId === o2.containerId;
  }

  compareDrivers(o1, o2): boolean {
    if (o2 != null)
      return o1.employeeId === o2.employeeId;
  }

  compareBroker(o1, o2): boolean {
    if (o2 != null)
      return o1.companyId === o2.companyId;
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
