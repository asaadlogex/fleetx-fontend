import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { BackEndService } from 'app/services/back-end.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { environment } from 'environments/environment';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-progress-form',
  templateUrl: './progress-form.component.html',
  styleUrls: ['./progress-form.component.scss']
})
export class ProgressFormComponent implements OnInit {

  stopTypeCode = environment.STOP_TYPE;
  cityType = environment.CITY_TYPE;
  stateTypeDropdownCode = environment.STATE_TYPE;
  vehicleOwnerType = environment.VEHICLE_OWNERSHIP_TYPE;
  equipmentTypeDropdown = environment.TRAILER_TYPE;
  equipmentSizeDropdown = environment.TRAILER_SIZE_TYPE;

  progressForm: FormGroup;
  roadwayBill : any = null;
  displayProgRentalFields: boolean = false;
  driverDropDown: any;
  containerDropDown: any;
  tractorDropDown: any;
  trailerDropDown: any;
  trackerDropDown: any;
  vehicleOwnerDropDown: any;
  equipmentDropDown: any;
  equipmentSizeDropDown: any;
  addProgressDiv = true;
  stopProgTypeDropDown: any;
  cityDropDown: any;
  stateDropDown: any;
  brokerDropDown: any;
  RoadWayBillID: any;

  @Output() progressAdded = new EventEmitter();
  @Output() progressClose = new EventEmitter();

  constructor(public backendService: BackEndService,
    private formBuilder: FormBuilder,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {}

  ngOnInit() {
    this.roadwayBill = this.backendService.progressData;
    if (this.roadwayBill != null) {
      this.progressForm = this.formBuilder.group({
        stopProgId: [null],
        stopType: [null],
        tractor: [null],
        tracker: [null],
        trailer: [null],
        container: [null],
        driver1: [null],
        driver2: [null],
        equipSize: [null],
        vehicleEquipment: [null],
        rentalId: [''],
        rentalBroker: [null],
        vehicleNum: [null],
        startKm: [null],
        arrivalDateTime: [null],
        departureDateTime: [null],
        detention: [null],
        instruction: [''],
        stopId: [null],
        rwbId: [this.roadwayBill.rwbId],
        vehicleOwn: [null],
        warehouseId: [null],
        warehouseName: [null],
        addressId: [null],
        street1: [null],
        street2: [null],
        city: [null],
        state: [null],
        zipCode: [null],
        country: [null],
        latitude: [null],
        longitude: [null],
        phone: [null],
        email: [null],
      });
      this.RoadWayBillID = this.roadwayBill.rwbId;
      this.stopProgTypeDropDown = this.roadwayBill.stopProgTypeDropDown
      this.cityDropDown = this.roadwayBill.cityDropDown;
      this.stateDropDown = this.roadwayBill.stateDropDown;
      this.brokerDropDown = this.roadwayBill.brokerDropDown;
      this.driverDropDown = this.roadwayBill.driverDropDown;
      this.trailerDropDown = this.roadwayBill.trailerDropDown;
      this.tractorDropDown = this.roadwayBill.tractorDropDown;
      this.containerDropDown = this.roadwayBill.containerDropDown;
      this.vehicleOwnerDropDown = this.roadwayBill.vehicleOwnerDropDown;
      this.equipmentSizeDropDown = this.roadwayBill.equipmentSizeDropDown;
      this.equipmentDropDown = this.roadwayBill.equipmentDropDown;
    }
  }

  /* ==================== DISPLAY FIELD ON THE BASIS OF DROPDOWN ============================ */
  displayRentalFields(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_RENTAL) {
        this.displayProgRentalFields = true;
      } else {
        this.displayProgRentalFields = false;
      }
    }
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  /* =============================== DROPDOWN FUNCTIONS ================================= */

  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

  /* ============================ ADD PROGRESS FUNCTIONS =========================== */
  submitProgress() {
    let data = {
      stopProgressId: this.progressForm.value.stopProgId,
      stopProgressType: this.progressForm.value.stopType,
      tractor: null,
      trailer: null,
      container: null,
      driver1: null,
      driver2: null,
      vehicleOwnership: this.progressForm.value.vehicleOwn,
      rentalVehicle: null,
      currentKm: this.progressForm.value.startKm,
      arrivalTime: this.datePipe.transform(this.progressForm.value.arrivalDateTime, "yyyy-MM-dd HH:mm:ss"),
      departureTime: this.datePipe.transform(this.progressForm.value.departureDateTime, "yyyy-MM-dd HH:mm:ss"),
      detentionInHrs: this.progressForm.value.detention,
      instruction: this.progressForm.value.instruction,
      stopId: this.progressForm.value.stopId,
      roadwayBillId: this.RoadWayBillID,
      warehouse: {
        warehouseId: this.progressForm.value.warehouseId,
        name: this.progressForm.value.warehouseName,
        address: {
          addressId: this.progressForm.value.addressId,
          street1: this.progressForm.value.street1,
          street2: this.progressForm.value.street2,
          city: this.progressForm.value.city,
          state: this.progressForm.value.state,
          zipCode: null,
          country: "Pakistan",
          latitude: this.progressForm.value.latitude,
          longitude: this.progressForm.value.longitude,
        },
        phone: this.progressForm.value.phone,
        email: this.progressForm.value.email
      }
    }
    if (data.vehicleOwnership.value == environment.SystemTypes_RENTAL) {
      data.rentalVehicle = {
        rentalVehicleId: null,
        equipmentType: this.progressForm.value.vehicleEquipment,
        equipmentSize: this.progressForm.value.equipSize,
        broker: this.progressForm.value.rentalBroker,
        vehicleNumber: this.progressForm.value.vehicleNum
      }
    } else {
      data.tractor = this.progressForm.value.tractor;
      data.trailer = this.progressForm.value.trailer;
      data.container = this.progressForm.value.container;
      data.driver1 = this.progressForm.value.driver1;
      data.driver2 = this.progressForm.value.driver2;
    }

    this.addProgress(data);
  }

  addProgress(data) {
    this.backendService.addLoadProgressService(data).subscribe(res =>{
    if (res.success) {
      this.progressForm.reset();
      this.errorMsgService.getSuccessMessage(res.message);
      this.openSuccessDialog();
      this.progressAdded.emit(res.data);
    } else {
      this.errorMsgService.getErrorMessage(res.errorMessage);
      this.openErrorDialog();
    }
    //console.log("ADD PROGRESS RESPONSE", res);
  }, (err: HttpErrorResponse) => {
    if (err.status == 500) {
      this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
      this.openInfoDialog();
    }
  });
  }

  closeProgressForm(){
    this.progressClose.emit()
  }
}
