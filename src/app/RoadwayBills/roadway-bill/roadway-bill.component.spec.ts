import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadwayBillComponent } from './roadway-bill.component';

describe('RoadwayBillComponent', () => {
  let component: RoadwayBillComponent;
  let fixture: ComponentFixture<RoadwayBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadwayBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadwayBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
