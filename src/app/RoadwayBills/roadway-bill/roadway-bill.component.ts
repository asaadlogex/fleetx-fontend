import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatDialog, MatRadioChange } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-roadway-bill',
  templateUrl: './roadway-bill.component.html',
  styleUrls: ['./roadway-bill.component.scss']
})
export class RoadwayBillComponent implements OnInit {

  // for company search
  filteredOptions: Observable<string[]>;

  //============================== DROPDOWN CODES ===============================
  RwbStatusType = environment.RWB_STATUS_TYPE;
  customerType = environment.CUSTOMER_TYPE;
  paymentModeType = environment.PAYMENT_MODE_TYPE;
  stopTypeCode = environment.STOP_TYPE;
  cityType = environment.CITY_TYPE;
  stateTypeDropdownCode = environment.STATE_TYPE;
  areaTypeCode = environment.AREA_TYPE;
  vehicleOwnerType = environment.VEHICLE_OWNERSHIP_TYPE;
  equipmentTypeDropdown = environment.TRAILER_TYPE;
  equipmentSizeDropdown = environment.TRAILER_SIZE_TYPE;

  //============================== VARIABLES ===============================
  documents = [];
  isArea = "Intercity";
  roadwayBillForm: FormGroup;
  roadwayBill: any;
  displayAsset: any;
  isEmpty: boolean = false;
  orderJobId: string;
  rwbStatusDropDown: any;
  customerDropDown: any;
  stopProgTypeDropDown: any;
  paymentModeDropDown: any;
  vehicleOwnerDropDown: any;
  equipmentDropDown: any;
  equipmentSizeDropDown: any;
  routeDropDown: any = [];
  tempRoute: any = [];
  cityDropDown: any;
  stateDropDown: any;
  brokerDropDown: any;
  customerTypeDropDown: any;
  assetDropDown: any;
  driverDropDown: any;
  tractorDropDown: any;
  trailerDropDown: any;
  trackerDropDown: any;
  containerDropDown: any;
  showCompanyCustomer = false;
  showRentalField = false;
  jobData: any;
  editAsset = false;
  dispatchBtn = false;
  completeBtn = false;
  showSaveBtn = true;

  constructor(public backendService: BackEndService,
    private formBuilder: FormBuilder,
    private router: Router,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) { }

  //============================== ON INIT COMPONENT FUNCTIONS ===============================

  ngOnInit() {
    this.roadwayBillForm = this.formBuilder.group({
      rwbId: [null],
      rwbStatus: [null, Validators.required],
      customerType: [null, Validators.required],
      jobID: [this.orderJobId],
      orderNum: [null],
      appWeight: [null, Validators.required],
      isEmptyTrip: [false],
      rwbDate: [null, Validators.required],
      loadNum: [null],
      bolNum: [null],
      payMode: [null, Validators.required],
      contractCustomer: [null],
      broker: [null],
      route: [null],
      routeCity: [null],
      taxProvince: [null],
      vehicleOwn: ['', Validators.required],
      instruction: [null],
      tmsKm: [null],
      trackerKm: [null],
      totalIncome: [null],
      totalExpense: [null],
      netIncome: [null],
      loadProgress: this.formBuilder.array([]),
      assets: [''],
      assetCombId: [''],
      equipType: [null],
      tractor: [false],
      tracker: [true],
      trailer: [false],
      container: [false],
      driver1: [false],
      driver2: [false],
      equipSize: [null],
      vehicleEquipment: [null],
      rentalId: [null],
      rentalBroker: [null],
      licNumber: [null],
    });

    this.roadwayBillForm.controls['jobID'].disable();
    this.jobData = this.backendService.jobData;
    this.orderJobId = sessionStorage.getItem("jobId");
    sessionStorage.removeItem('jobId')
    this.roadwayBillForm.get('jobID').setValue(this.orderJobId);

    let getRoadwayBill = JSON.parse(sessionStorage.getItem("getRoadwayBill"));
    sessionStorage.removeItem("getRoadwayBill")
    if (getRoadwayBill != null) {
      this.roadwayBill = getRoadwayBill;
      this.roadwayBillForm.get('rwbId').setValue(this.roadwayBill.rwbId);
      this.roadwayBillForm.get('rwbStatus').setValue(this.roadwayBill.rwbStatus);
      this.roadwayBillForm.get('customerType').setValue(this.roadwayBill.customerType);
      this.roadwayBillForm.get('isEmptyTrip').setValue(this.roadwayBill.isEmpty);
      this.roadwayBillForm.get('jobID').setValue(this.orderJobId);
      this.roadwayBillForm.get('orderNum').setValue(this.roadwayBill.orderNumber);
      this.roadwayBillForm.get('rwbDate').setValue(this.roadwayBill.rwbDate);
      this.roadwayBillForm.get('loadNum').setValue(this.roadwayBill.loadNumber);
      this.roadwayBillForm.get('bolNum').setValue(this.roadwayBill.bolNumber);
      this.roadwayBillForm.get('appWeight').setValue(this.roadwayBill.weightInTon);
      this.roadwayBillForm.get('payMode').setValue(this.roadwayBill.paymentMode);
      this.roadwayBillForm.get('contractCustomer').setValue(this.roadwayBill.customer);
      this.roadwayBillForm.get('broker').setValue(this.roadwayBill.broker);
      this.roadwayBillForm.get('route').setValue(this.roadwayBill.route);
      this.roadwayBillForm.get('vehicleOwn').setValue(this.roadwayBill.vehicleOwnership);
      this.roadwayBillForm.get('instruction').setValue(this.roadwayBill.instructions);
      this.roadwayBillForm.get('tmsKm').setValue(this.roadwayBill.tmsKm);
      this.roadwayBillForm.get('trackerKm').setValue(this.roadwayBill.trackerKm);
      this.roadwayBillForm.get('totalIncome').setValue(this.roadwayBill.totalIncome);
      this.roadwayBillForm.get('totalExpense').setValue(this.roadwayBill.totalExpense);
      this.roadwayBillForm.get('netIncome').setValue(this.roadwayBill.netIncome);
      this.roadwayBillForm.get('taxProvince').setValue(this.roadwayBill.taxState);

      this.displayContractual(this.roadwayBill.customerType);

      this.isEmpty = this.roadwayBill.isEmpty;

      if (this.roadwayBill.rwbStatus.value == environment.SystemTypes_EMPTY)
        this.completeBtn = true;
      if (this.roadwayBill.rwbStatus.value == environment.SystemTypes_BOOKED)
        this.dispatchBtn = true;

      if (this.roadwayBill.isArea) {
        this.isArea = "Intracity"
        this.roadwayBillForm.value.routeCity = this.roadwayBill.city;
      }

      if (this.roadwayBill.vehicleOwnership.value == environment.SystemTypes_RENTAL) {
        this.showRentalField = true;
        this.roadwayBillForm.get('rentalBroker').setValue(this.roadwayBill.rentalVehicle.broker)
        this.roadwayBillForm.get('rentalId').setValue(this.roadwayBill.rentalVehicle.rentalVehicleId)
        this.roadwayBillForm.get('equipType').setValue(this.roadwayBill.rentalVehicle.equipmentType)
        this.roadwayBillForm.get('equipSize').setValue(this.roadwayBill.rentalVehicle.equipmentSize)
        this.roadwayBillForm.get('licNumber').setValue(this.roadwayBill.rentalVehicle.vehicleNumber)
      }
    }
    this.getBrokerDropdown();
    this.getCustomerTypeDropdown(this.customerType);
    this.getPaymentModeDropdown(this.paymentModeType);
    this.getStopTypeDropdown(this.stopTypeCode);
    this.getRoadWayBillStatusDropdown(this.RwbStatusType);
    this.getCityDropdown(this.cityType);
    this.getStateDropdown(this.stateTypeDropdownCode);
    this.getCustomerDropdown();
    this.getAllCombinations();
    this.getVehicleOwnerDropdown(this.vehicleOwnerType);
    this.getEquipmentTypeDropdown(this.equipmentTypeDropdown);
    this.getEquipmentSizeDropdown(this.equipmentSizeDropdown);
    this.getAllFreeDrivers();
    this.getAllFreeTractors();
    this.getAllFreeTrailers();
    this.getAllFreeContainers();

  }

  // ============== MAKE ASSETS EDITABLE START =================

  makeAssetEditable() {
    this.editAsset = !this.editAsset
  }

  // ============== MAKE ASSETS EDITABLE END   =================

  //============================== RESET, HIDE AND SHOW FIELDS FUNCTIONS ===============================
  displayContractual(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_CONTRACTUAL) {
        this.showCompanyCustomer = true;
      } else {
        this.showCompanyCustomer = false;
      }
    }
  }

  tabIndex = 0;

  changeTab(event) {
    const data = this.roadwayBill;
    if (data != null) {
      data.stopProgTypeDropDown = this.stopProgTypeDropDown;
      data.equipmentDropDown = this.equipmentDropDown;
      data.equipmentSizeDropDown = this.equipmentSizeDropDown;
      data.vehicleOwnerDropDown = this.vehicleOwnerDropDown;
      data.brokerDropDown = this.brokerDropDown;
      data.tractorDropDown = this.tractorDropDown;
      data.trailerDropDown = this.trailerDropDown;
      data.driverDropDown = this.driverDropDown;
      data.containerDropDown = this.containerDropDown;
      data.cityDropDown = this.cityDropDown;
      data.stateDropDown = this.stateDropDown;
    }
    if (event.index == 0) {
      this.showSaveBtn = true;
    }
    if (event.index == 1) {
      this.backendService.stopData = this.roadwayBill;
      this.showSaveBtn = false;
    }
    if (event.index == 2) {
      this.backendService.progressData = data;
      this.showSaveBtn = false;
    }
    if (event.index == 3) {
      this.backendService.financeData = this.roadwayBill;
      this.showSaveBtn = false;
    }

    this.tabIndex = event.index;
  }

  displayRentalFields(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_RENTAL) {
        this.showRentalField = true;
      } else {
        this.showRentalField = false;
      }
    }
  }

  setTaxLocation(e) {
    if (e != undefined) {
      this.roadwayBillForm.get('taxProvince').setValue(e.routeFrom.state);
    }
  }

  checkedCombination(e) {
    if (e !== "" || e != null) {
      this.roadwayBillForm.get('tractor').setValue(true);
      this.roadwayBillForm.get('trailer').setValue(true);
      this.roadwayBillForm.get('container').setValue(true);
      this.roadwayBillForm.get('driver1').setValue(true);
      this.roadwayBillForm.get('driver2').setValue(true);
      this.displayAsset = e;
    }
  }

  changeAsset(data, val) {
    if (val == "tractor")
      this.displayAsset.tractor = data;
    if (val == "trailer")
      this.displayAsset.trailer = data;
    if (val == "container")
      this.displayAsset.container = data;
    if (val == "driver1")
      this.displayAsset.driver1 = data;
    if (val == "driver2")
      this.displayAsset.driver2 = data;
  }

  emptyTrip(data) {
    // console.log(data)
    if (data.checked) {
      this.isEmpty = true;
      this.getRouteDropdown();
    } else {
      this.isEmpty = false;
    }
  }

  selectionRoute($event: MatRadioChange) {
    // console.log($event.source.name, $event.value);
    if ($event.value === 'Intracity') {
      this.tempRoute = this.routeDropDown;
      this.routeDropDown = [];
    } else {
      this.routeDropDown = this.tempRoute;
    }

  }

  getRouteByCity(e) {
    if (e != undefined) {
      this.backendService.getRouteByCitiesService(e.code).subscribe(res => {
        this.routeDropDown = res.data;
      })
    }
  }

  getRouteForCustomer(data) {
    if (this.showCompanyCustomer) {
      this.getRouteByCustomerDropdown(data.companyId)
    } else {
      this.getRouteDropdown()
    }
  }

  //============================== ADD UPDATE DELETE ROADWAY BILL FUNCTIONS ===============================

  submitRoadwayBill() {
    if (!this.isEmpty) {
      if (this.roadwayBillForm.invalid) {
        return;
      }
    }
    this.roadwayBill = {
      rwbId: this.roadwayBillForm.value.rwbId,
      orderNumber: this.roadwayBillForm.value.orderNum,
      loadNumber: this.roadwayBillForm.value.loadNum,
      customerType: this.roadwayBillForm.value.customerType,
      isEmpty: this.isEmpty,
      rwbStatus: this.roadwayBillForm.value.rwbStatus,
      rwbDate: this.datePipe.transform(this.roadwayBillForm.value.rwbDate, environment.SystemTypes_Date_Format),
      bolNumber: this.roadwayBillForm.value.bolNum,
      weightInTon: this.roadwayBillForm.value.appWeight,
      tmsKm: this.roadwayBillForm.value.tmsKm,
      trackerKm: this.roadwayBillForm.value.trackerKm,
      paymentMode: this.roadwayBillForm.value.payMode,
      customer: null,
      broker: null,
      route: this.roadwayBillForm.value.route,
      city: null,
      isArea: false,
      taxState: this.roadwayBillForm.value.taxProvince,
      instructions: this.roadwayBillForm.value.instruction,
      tractor: null,
      trailer: null,
      container: null,
      tracker: null,
      driver1: null,
      driver2: null,
      assetCombinationId: null,
      vehicleOwnership: this.roadwayBillForm.value.vehicleOwn,
      rentalVehicle: null,
      jobId: this.roadwayBillForm.getRawValue().jobID,
      stops: null,
      products: null,
      incomes: null,
      expenses: null,
      stopProgresses: null
    }

    if (this.isArea != "Intercity") {
      this.roadwayBill.isArea = true;
      this.roadwayBill.city = this.roadwayBillForm.value.routeCity;
    }

    if (this.roadwayBillForm.value.customerType.value == environment.SystemTypes_CONTRACTUAL) {
      this.roadwayBill.customer = this.roadwayBillForm.value.contractCustomer
    } else {
      this.roadwayBill.broker = this.roadwayBillForm.value.broker
    }

    if (this.roadwayBillForm.value.vehicleOwn.value != environment.SystemTypes_RENTAL) {
      this.roadwayBill.assetCombinationId = this.displayAsset.assetCombinationId;
      if (this.roadwayBillForm.value.tractor == true) {
        this.roadwayBill.tractor = this.displayAsset.tractor;
      }
      if (this.roadwayBillForm.value.trailer == true) {
        this.roadwayBill.trailer = this.displayAsset.trailer;
      }
      if (this.roadwayBillForm.value.tracker == true) {
        this.roadwayBill.tracker = this.displayAsset.tracker;
      }
      if (this.roadwayBillForm.value.container == true) {
        this.roadwayBill.container = this.displayAsset.container;
      }
      if (this.roadwayBillForm.value.driver1 == true) {
        this.roadwayBill.driver1 = this.displayAsset.driver1;
      }
      if (this.roadwayBillForm.value.driver2 == true) {
        this.roadwayBill.driver2 = this.displayAsset.driver2;
      }
    } else {
      this.roadwayBill.rentalVehicle = {
        rentalVehicleId: this.roadwayBillForm.value.rentalId,
        broker: this.roadwayBillForm.value.rentalBroker,
        equipmentType: this.roadwayBillForm.value.equipType,
        equipmentSize: this.roadwayBillForm.value.equipSize,
        vehicleNumber: this.roadwayBillForm.value.licNumber
      }
    }

    // console.log("SUBMIT DATA", this.roadwayBill);
    if (this.roadwayBillForm.value.rwbId == null) {
      this.addRoadwayBill();
    } else {
      this.updateRoadwayBill();
    }
  }

  addRoadwayBill() {
    this.backendService.addRoadwayBillService(this.roadwayBill).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.roadwayBillForm.get("rwbId").setValue(res.data);
        this.roadwayBill.rwbId = res.data;
        this.dispatchBtn = true;
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("RWB ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateRoadwayBill() {
    this.backendService.updateRoadwayBillService(this.roadwayBill).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("RWB UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteRoadwayBill() {
    if (this.roadwayBill.rwbId != null || this.roadwayBill.rwbId != "") {
      this.backendService.deleteRoadwayBillService(this.roadwayBill.rwbId).subscribe(res => {
        if (res.success) {
          this.router.navigate(['job']);
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("RWB DELETE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    }
  }

  dispatchBill() {
    this.backendService.changeRoadwayBillStatus(this.roadwayBill.rwbId, environment.SystemTypes_DISPATCHED).subscribe(res => {
      if (res.success) {
        this.dispatchBtn = false;
        this.rwbStatusDropDown.forEach(element => {
          if (element.value == environment.SystemTypes_DISPATCHED) {
            this.roadwayBillForm.get('rwbStatus').setValue(element);
          }
        });

      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  completeBill() {
    this.backendService.changeRoadwayBillStatus(this.roadwayBill.rwbId, "Completed").subscribe(res => {
      if (res.success) {
        this.router.navigate(['job']);
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  //============================== GET DROPDOWN FUNCTIONS ===============================

  getAllCombinations() {
    this.backendService.getAllFilterAssetCombination('javail').subscribe(res => {
      if (res.success) {
        this.assetDropDown = res.data;
        if (this.roadwayBillForm.value.rwbId != "" && this.roadwayBillForm.value.vehicleOwn.value == environment.SystemTypes_OWNED) {
          if (this.roadwayBill.tractor != null) {
            // this.jobForm.value.tractor == true;
            this.roadwayBillForm.get("tractor").setValue(true)
          }
          if (this.roadwayBill.trailer != null) {
            // this.jobForm.value.trailer == true;
            this.roadwayBillForm.get("trailer").setValue(true)
          }
          if (this.roadwayBill.tracker != null) {
            // this.jobForm.value.trailer == true;
            this.roadwayBillForm.get("tracker").setValue(true)
          }
          if (this.roadwayBill.container != null) {
            // this.jobForm.value.container == true;
            this.roadwayBillForm.get("container").setValue(true)
          }
          if (this.roadwayBill.driver1 != null) {
            // this.jobForm.value.driver1 == true;
            this.roadwayBillForm.get("driver1").setValue(true)
          }
          if (this.roadwayBill.driver2 != null) {
            // this.jobForm.value.driver2 == true;
            this.roadwayBillForm.get("driver2").setValue(true)
          }
          this.displayAsset = {
            assetCombinationId: this.roadwayBill.assetCombinationId,
            tractor: this.roadwayBill.tractor,
            trailer: this.roadwayBill.trailer,
            tracker: this.roadwayBill.tracker,
            container: this.roadwayBill.container,
            driver1: this.roadwayBill.driver1,
            driver2: this.roadwayBill.driver2
          };
        }
      }
    })
  }

  getRoadWayBillStatusDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.rwbStatusDropDown = res.data;
        if (this.roadwayBillForm.value.rwbId == null) {
          for (let i = 0; i < this.rwbStatusDropDown.length; i++) {
            if (this.rwbStatusDropDown[i].value == environment.SystemTypes_BOOKED)
              this.roadwayBillForm.get('rwbStatus').setValue(this.rwbStatusDropDown[i]);
          }
        }
      }
    })
  }

  getCustomerTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.customerTypeDropDown = res.data;
        if (this.roadwayBillForm.value.rwbId == null) {
          for (let i = 0; i < this.customerTypeDropDown.length; i++) {
            if (this.customerTypeDropDown[i].value == environment.SystemTypes_CONTRACTUAL) {
              this.roadwayBillForm.get('customerType').setValue(this.customerTypeDropDown[i]);
              this.displayContractual(this.customerTypeDropDown[i]);
            }
          }
        }
      }
    })
  }

  getPaymentModeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.paymentModeDropDown = res.data;
      }
    })
  }

  getStopTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.stopProgTypeDropDown = res.data;
      }
    })
  }

  getRouteDropdown() {
    this.backendService.getRouteService().subscribe(res => {
      if (res.success) {
        this.routeDropDown = res.data;
      }
    })
  }

  getRouteByCustomerDropdown(id) {
    this.backendService.getRouteByCustomerService(id).subscribe(res => {
      if (res.success) {
        this.routeDropDown = res.data;
      }
    })
  }

  getCityDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.cityDropDown = res.data;
      }
    })
  }

  getStateDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.stateDropDown = res.data;
    })
  }

  getBrokerDropdown() {
    this.backendService.getAllFilterBroker().subscribe(res => {
      if (res.success) {
        this.brokerDropDown = res.data;
        if (this.roadwayBillForm.value.rwbId != null) {
          for (let i = 0; i < this.brokerDropDown.length; i++) {
            if (this.roadwayBillForm.value.broker != null) {
              if (this.brokerDropDown[i].companyId == this.roadwayBill.broker.companyId) {
                this.roadwayBillForm.get('broker').setValue(this.brokerDropDown[i])
                this.getRouteDropdown();
              }
            }
          }

          if (this.roadwayBill.rentalVehicle != null) {
            for (let j = 0; j < this.brokerDropDown.length; j++) {
              if (this.brokerDropDown[j].companyId == this.roadwayBill.rentalVehicle.broker.companyId) {
                this.roadwayBillForm.get('rentalBroker').setValue(this.brokerDropDown[j]);
              }
            }
          }
        }
      }
    })
  }

  getCustomerDropdown() {
    this.backendService.getAllFilterCustomerContract("true").subscribe(res => {
      if (res.success) {
        this.customerDropDown = res.data
        if (this.roadwayBillForm.value.rwbId != null) {
          if (this.roadwayBill.customer != null) {
            if (this.customerDropDown.length > 0) {
              for (let i = 0; i < this.customerDropDown.length; i++) {
                if (this.customerDropDown[i].companyId == this.roadwayBill.customer.companyId) {
                  this.roadwayBillForm.get('contractCustomer').setValue(this.customerDropDown[i])
                  this.getRouteForCustomer(this.customerDropDown[i]);
                  break;
                } else {
                  this.customerDropDown.push(this.roadwayBill.customer)
                  this.roadwayBillForm.get('contractCustomer').setValue(this.roadwayBill.customer)
                  this.getRouteForCustomer(this.roadwayBill.customer);
                  break;
                }
              }
            } else {
              this.customerDropDown.push(this.roadwayBill.customer)
              this.roadwayBillForm.get('contractCustomer').setValue(this.customerDropDown[0])
              this.getRouteForCustomer(this.customerDropDown[0]);
            }
          }
        }
      }
    })
  }

  getVehicleOwnerDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.vehicleOwnerDropDown = res.data;
      }
    })
  }

  getEquipmentTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.equipmentDropDown = res.data;
      }
    })
  }

  getEquipmentSizeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.equipmentSizeDropDown = res.data;
      }
    })
  }

  getAllFreeDrivers() {
    let data = {
      mov: "Free",
      pos: "Driver",
      rwbId: this.roadwayBillForm.value.rwbId
    }
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
    this.backendService.getAllRWBDrivers(queryString).subscribe(res => {
      if (res.success) {
        this.driverDropDown = res.data;
      }
    })
  }

  getAllFreeTractors() {
    let data = {
      mov: "Free",
      rwbId: this.roadwayBillForm.value.rwbId
    }
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
    this.backendService.getAllRWBTractor(queryString).subscribe(res => {
      if (res.success) {
        this.tractorDropDown = res.data;
      }
    })
  }

  getAllFreeTrailers() {
    let data = {
      mov: "Free",
      rwbId: this.roadwayBillForm.value.rwbId
    }
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
    this.backendService.getAllRWBTrailer(queryString).subscribe(res => {
      if (res.success) {
        this.trailerDropDown = res.data;
      }
    })
  }

  getAllFreeContainers() {
    let data = {
      mov: "Free",
      rwbId: this.roadwayBillForm.value.rwbId
    }
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
    this.backendService.getAllRWBContainer(queryString).subscribe(res => {
      if (res.success) {
        this.containerDropDown = res.data;
      }
    })
  }

  //============================== COMPARE DROPDOWN FUNCTION ===============================
  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

  compareTaxProvice(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2;
  }

  compareTractor(o1, o2): boolean {
    if (o2 != null)
      return o1.tractorId === o2.tractorId;
  }

  compareTrailer(o1, o2): boolean {
    if (o2 != null)
      return o1.trailerId === o2.trailerId;
  }

  compareContainer(o1, o2): boolean {
    if (o2 != null)
      return o1.containerId === o2.containerId;
  }

  compareDrivers(o1, o2): boolean {
    if (o2 != null)
      return o1.employeeId === o2.employeeId;
  }

  compareBroker(o1, o2): boolean {
    if (o2 != null)
      return o1.companyId === o2.companyId;
  }

  compareRouteType(o1, o2): boolean {
    if (o2 != null)
      return o1.routeId === o2.routeId;
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }


}
