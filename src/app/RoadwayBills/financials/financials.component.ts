import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-financials',
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.scss']
})
export class FinancialsComponent implements OnInit {

  incomeTypeCode = environment.INCOME_TYPE;
  areaTypeCode = environment.AREA_TYPE;
  expenseTypeCode = environment.EXPENSE_TYPE;

  financialForm: FormGroup;
  orderIncome: FormArray;
  orderExpense: FormArray;
  incomeTypeDropDown: any = [];
  expenseTypeDropDown: any;
  roadwayBill: any = null;
  editForm = false;

  constructor(public backendService: BackEndService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {}

   /* ======================= ONINIT COMPONENT FUNCTION ============================= */

  ngOnInit() {

    this.financialForm = this.formBuilder.group({
      rwbId: [null],
      totalIncome: [null],
      totalExpense: [null],
      netIncome: [null],
      expenseList: this.formBuilder.array([]),
      incomeList: this.formBuilder.array([]),
    })
    this.getExpenseTypeDropdown(this.expenseTypeCode);
    this.getIncomeTypeDropdown(this.incomeTypeCode);
    
    this.roadwayBill = this.backendService.financeData;

    if (this.roadwayBill != null) {
      this.financialForm.get('rwbId').setValue(this.roadwayBill.rwbId);
      this.financialForm.get('totalIncome').setValue(this.roadwayBill.totalIncome);
      this.financialForm.get('totalExpense').setValue(this.roadwayBill.totalExpense);
      this.financialForm.get('netIncome').setValue(this.roadwayBill.netIncome);

      if (this.roadwayBill.incomes != null) {
        for (let i = 0; i < this.roadwayBill.incomes.length; i++) {
          if (this.roadwayBill.incomes[i].incomeType.value != environment.SystemTypes_FREIGHTRATE) {
            this.orderIncome = this.financialForm.get('incomeList') as FormArray;
            this.orderIncome.push(this.formBuilder.group({
              incomeId: [this.roadwayBill.incomes[i].incomeId],
              description: [this.roadwayBill.incomes[i].description],
              amount: [this.roadwayBill.incomes[i].amount],
              incomeType: [this.roadwayBill.incomes[i].incomeType],
              roadwayBillId: [this.roadwayBill.incomes[i].roadwayBillId],
              editable: [false]
            }));
          }
        }
      }

      if (this.roadwayBill.expenses != null) {
        for (let i = 0; i < this.roadwayBill.expenses.length; i++) {
          this.orderExpense = this.financialForm.get('expenseList') as FormArray;
          this.orderExpense.push(this.formBuilder.group({
            expenseId: [this.roadwayBill.expenses[i].expenseId],
            description: [this.roadwayBill.expenses[i].description],
            amount: [this.roadwayBill.expenses[i].amount],
            expenseType: [this.roadwayBill.expenses[i].expenseType],
            roadwayBillId: [this.roadwayBill.expenses[i].roadwayBillId],
            editable: [false]
          }));
        }
      }

    }

  }

  /* ============================ DROPDOWN FUNCTION ========================================= */

  getIncomeTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        res.data.forEach(element => {
          if (element.value != environment.SystemTypes_FREIGHTRATE) {
            this.incomeTypeDropDown.push(element)
          }
        });
        // this.incomeTypeDropDown = res.data;
      }
    })
  }

  getExpenseTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.expenseTypeDropDown = res.data;
      }
    })
  }

    //============================== ADD NEW EXPENSE AND INCOME FUNCTIONS ===============================
    createExpense() {
      return this.formBuilder.group({
        expenseId: [null],
        description: [null],
        amount: [null],
        expenseType: [null],
        roadwayBillId: [this.roadwayBill.rwbId],
        editable: [true]
      });
    }
  
    addExpense() {
      this.orderExpense = this.financialForm.get('expenseList') as FormArray;
      this.orderExpense.push(this.createExpense());
    }
  
    createIncome() {
      return this.formBuilder.group({
        incomeId: [null],
        description: [null],
        amount: [null],
        incomeType: [null],
        roadwayBillId: [this.roadwayBill.rwbId],
        editable: [true]
      });
    }
  
    addIncome() {
      this.orderIncome = this.financialForm.get('incomeList') as FormArray;
      this.orderIncome.push(this.createIncome());
    }
  
    removeExpense(id: any, index) {
      if (id != null) {
        this.backendService.deleteLoadExpenseService(id).subscribe(res => {
          if (res.success) {
            this.errorMsgService.getSuccessMessage(res.message);
            this.openSuccessDialog();
            this.orderExpense = this.financialForm.get('expenseList') as FormArray;
            this.orderExpense.removeAt(index)
          } else {
            this.errorMsgService.getErrorMessage(res.errorMessage);
            this.openErrorDialog();
          }
          //console.log("RWB PRODUCT DELETE RESPONSE", res);
        }, (err: HttpErrorResponse) => {
          if (err.status == 500) {
            this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
            this.openInfoDialog();
          }
        });
      } else {
        this.orderExpense = this.financialForm.get('expenseList') as FormArray;
        this.orderExpense.removeAt(index)
      }
    }
  
    removeIncome(id: any, index) {
      if (id != null) {
        this.backendService.deleteLoadIncomeService(id).subscribe(res => {
          if (res.success) {
            this.errorMsgService.getSuccessMessage(res.message);
            this.openSuccessDialog();
            this.orderIncome = this.financialForm.get('incomeList') as FormArray;
            this.orderIncome.removeAt(index)
          } else {
            this.errorMsgService.getErrorMessage(res.errorMessage);
            this.openErrorDialog();
          }
          //console.log("RWB Income DELETE RESPONSE", res);
        }, (err: HttpErrorResponse) => {
          if (err.status == 500) {
            this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
            this.openInfoDialog();
          }
        });
      } else {
        this.orderIncome = this.financialForm.get('incomeList') as FormArray;
        this.orderIncome.removeAt(index)
      }
    }
  
    makeExpenseEditable(data, id) {
      this.financialForm.get('expenseList').get([id]).get('editable').setValue(true);
      this.financialForm.get('expenseList').get([id]).get('expenseId').setValue(data.value.expenseId);
      this.financialForm.get('expenseList').get([id]).get('description').setValue(data.value.description);
      this.financialForm.get('expenseList').get([id]).get('amount').setValue(data.value.amount);
      this.financialForm.get('expenseList').get([id]).get('expenseType').setValue(data.value.expenseType);
      this.financialForm.get('expenseList').get([id]).get('roadwayBillId').setValue(data.value.roadwayBillId);
    }
  
    makeIncomeEditable(data, id) {
      this.financialForm.get('incomeList').get([id]).get('editable').setValue(true);
      this.financialForm.get('incomeList').get([id]).get('incomeId').setValue(data.value.incomeId);
      this.financialForm.get('incomeList').get([id]).get('description').setValue(data.value.description);
      this.financialForm.get('incomeList').get([id]).get('amount').setValue(data.value.amount);
      this.financialForm.get('incomeList').get([id]).get('incomeType').setValue(data.value.incomeType);
      this.financialForm.get('incomeList').get([id]).get('roadwayBillId').setValue(data.value.roadwayBillId);
    }
  
    saveExpense(id: any) {
      this.orderExpense = <FormArray>this.financialForm.get('expenseList');
      if (this.orderExpense.value[id].expenseId == null) {
        this.backendService.addLoadExpenseService(this.orderExpense.value[id]).subscribe(res => {
          if (res.success) {
            this.errorMsgService.getSuccessMessage(res.message);
            this.openSuccessDialog();
            this.financialForm.get('expenseList').get([id]).get('editable').setValue(false);
            this.financialForm.get('expenseList').get([id]).get('expenseId').setValue(res.data);
          } else {
            this.errorMsgService.getErrorMessage(res.errorMessage);
            this.openErrorDialog();
          }
          //console.log("RWB EXPENSE ADD RESPONSE", res);
        }, (err: HttpErrorResponse) => {
          if (err.status == 500) {
            this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
            this.openInfoDialog();
          }
        });
      } else {
        this.backendService.updateLoadExpenseService(this.orderExpense.value[id]).subscribe(res => {
          if (res.success) {
            this.errorMsgService.getSuccessMessage(res.message);
            this.openSuccessDialog();
            this.orderExpense.value[id].editable = false;
          } else {
            this.errorMsgService.getErrorMessage(res.errorMessage);
            this.openErrorDialog();
          }
          //console.log("RWB EXPENSE Update RESPONSE", res);
        }, (err: HttpErrorResponse) => {
          if (err.status == 500) {
            this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
            this.openInfoDialog();
          }
        });
      }
    }
  
    saveIncome(id: any) {
      this.orderIncome = <FormArray>this.financialForm.get('incomeList');
      if (this.orderIncome.value[id].incomeId == null) {
        this.backendService.addLoadIncomeService(this.orderIncome.value[id]).subscribe(res => {
          if (res.success) {
            this.errorMsgService.getSuccessMessage(res.message);
            this.openSuccessDialog();
            this.financialForm.get('incomeList').get([id]).get('editable').setValue(false);
            this.financialForm.get('incomeList').get([id]).get('incomeId').setValue(res.data);
          } else {
            this.errorMsgService.getErrorMessage(res.errorMessage);
            this.openErrorDialog();
          }
          //console.log("RWB INCOME ADD RESPONSE", res);
        }, (err: HttpErrorResponse) => {
          if (err.status == 500) {
            this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
            this.openInfoDialog();
          }
        });
      } else {
        this.backendService.updateLoadIncomeService(this.orderIncome.value[id]).subscribe(res => {
          if (res.success) {
            this.errorMsgService.getSuccessMessage(res.message);
            this.openSuccessDialog();
            this.orderIncome.value[id].editable = false;
          } else {
            this.errorMsgService.getErrorMessage(res.errorMessage);
            this.openErrorDialog();
          }
          //console.log("RWB INCOME Update RESPONSE", res);
        }, (err: HttpErrorResponse) => {
          if (err.status == 500) {
            this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
            this.openInfoDialog();
          }
        });
      }
    }

    //============================== COMPARE DROPDOWN FUNCTION ===============================
    compareDropdownType(o1, o2): boolean {
      if (o2 != null)
        return o1.value === o2.value;
    }

   /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
