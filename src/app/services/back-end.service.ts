import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackEndService {

  //DATA TRANSFER VARIABLE START
  jobData;
  progressData;
  stateData;
  citiesData;
  reimbursedIds;
  reimbursedData;
  pendingData;
  invoiceData;
  stopData;
  financeData;
  //DATA TRASFER VARIABLE END

  token = sessionStorage.getItem('token');
  url = environment.baseUrl;

  constructor(private http: HttpClient) { }
 

  /*======================== LOGIN SERVICES ======================*/
  loginService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.post<{ data: any, errorMessage: string, success: boolean }>(this.url + 'authorize/login', data, { headers: headers })
  }

  verifyUser(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.post<{ data: any, errorMessage: string, success: boolean }>(this.url + 'authorize', data, { headers: headers })
  }

  isUserLoggedIn() {
    const getToken = sessionStorage.getItem('token')
    return !(getToken === null)
  }

  /*======================== COMPANY SERVICES ======================*/
  addCompanyService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies', data, { headers: headers })
  }

  updateCompanyService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies', data, { headers: headers })
  }

  deleteCompanyService(companyID: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/' + companyID, { headers: headers })
  }

  getAllCompaniesService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies', { headers: headers })
  }

  getAllDedicatedCompService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/fixed/filter?contract=Dedicated', { headers: headers })
  }

  getCompanyById(companyID: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/' + companyID, { headers: headers })
  }

  /*======================== ADMIN COMPANY SERVICES ======================*/
  addAdminCompanyService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/companies', data, { headers: headers })
  }

  updateAdminCompanyService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/companies', data, { headers: headers })
  }

  deleteAdminCompanyService(companyID: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/companies/' + companyID, { headers: headers })
  }

  getAdminCompanies() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/companies/', { headers: headers })
  }

  getAdminCompanyById(companyID: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/companies/' + companyID, { headers: headers })
  }


  /*======================== EMPLOYEE SERVICES ======================*/
  addEmployeeService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees', data, { headers: headers })
  }

  updateEmployeeService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees', data, { headers: headers })
  }

  deleteEmployeeService(empID: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/' + empID, { headers: headers })
  }

  getAllEmployeeService(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, totalRecords: any; message: string, errorMessage: string, success: boolean }>(this.url + 'employees', { headers: headers, params: param })
  }

  getSearchEmployeeService(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/filter?'+param, { headers: headers })
  }
  

  getEmployeeById(empID: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/' + empID, { headers: headers })
  }

  uploadEmployeeProfilePic(profileData) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/dp', profileData, { headers: headers })
  }

  /*======================== TRACTER SERVICES ======================*/
  addTractorService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/tractors', data, { headers: headers })
  }

  updateTractorService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/tractors', data, { headers: headers })
  }

  deleteTractorService(tractorId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/tractors/' + tractorId, { headers: headers })
  }

  getAllTractorService(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, totalRecords: any, errorMessage: string, success: boolean }>(this.url + 'assets/tractors', { headers: headers,  params: param })
  }

  getSearchTractorService(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/tractors/filter?'+param, { headers: headers })
  }

  getTractorById(tractorId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/tractors/' + tractorId, { headers: headers })
  }

  /*======================== TRAILER SERVICES ======================*/
  addTrailerService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trailers', data, { headers: headers })
  }

  updateTrailerService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trailers', data, { headers: headers })
  }

  deleteTrailerService(trailerId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trailers/' + trailerId, { headers: headers })
  }
  getSearchTrailerService(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trailers/filter?'+param, { headers: headers })
  }
  getAllTrailerService(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, totalRecords: any, errorMessage: string, success: boolean }>(this.url + 'assets/trailers', { headers: headers, params: param })
  }

  getTrailerById(trailerId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trailers/' + trailerId, { headers: headers })
  }

  /*======================== CONTAINER SERVICES ======================*/
  addContainerService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/containers', data, { headers: headers })
  }

  updateContainerService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/containers', data, { headers: headers })
  }

  deleteContainerService(containerId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/containers/' + containerId, { headers: headers })
  }

  getAllContainerService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/containers', { headers: headers })
  }

  getContainerById(containerId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/containers/' + containerId, { headers: headers })
  }

  /*======================== TRACKER SERVICES ======================*/
  addTrackerService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trackers', data, { headers: headers })
  }

  updateTrackerService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trackers', data, { headers: headers })
  }

  deleteTrackerService(trackerId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trackers/' + trackerId, { headers: headers })
  }

  getAllTrackerService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trackers', { headers: headers })
  }

  getTrackerById(trackerId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trackers/' + trackerId, { headers: headers })
  }

  /*======================== ASSET COMBINATION SERVICES ======================*/
  addCombinationsService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/assetCombinations', data, { headers: headers })
  }

  updateCombinationsService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/assetCombinations', data, { headers: headers })
  }

  deleteCombinationsService(combId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/assetCombinations/' + combId, { headers: headers })
  }

  getAllCombinationsService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/assetCombinations', { headers: headers })
  }

  getCombinationsById(combId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/assetCombinations/' + combId, { headers: headers })
  }

  /*======================== COMPANY CONTRACT SERVICES ======================*/

  addCompanyContractService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/companies', data, { headers: headers })
  }

  updateCompanyContractService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/companies', data, { headers: headers })
  }

  deleteCompanyContractService(combId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/companies/' + combId, { headers: headers })
  }

  getAllCompanyContractService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/companies', { headers: headers })
  }

  getCompanyContractById(combId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/companies/' + combId, { headers: headers })
  }

  /*======================== SUPPLIER FUEL RATE SERVICES ======================*/

  addFuelRateService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/fuelRates', data, { headers: headers })
  }

  getFuelRateService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/fuelRates', { headers: headers })
  }

  deleteFuelRateService(fuelRateID: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/fuelRates/' + fuelRateID, { headers: headers })
  }

  /*======================== ROUTES SERVICES ======================*/

  addRouteService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/routes', data, { headers: headers })
  }

  getRouteService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/routes', { headers: headers })
  }

  getRouteByIdService(id) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/routes/'+id, { headers: headers })
  }

  getRouteByCitiesService(cityCode) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/routes/intracity?city='+cityCode, { headers: headers })
  }

  getRouteByCustomerService(custID) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/companies/routes?customerId='+custID, { headers: headers })
  }

  deleteRouteService(routId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/routes/' + routId, { headers: headers })
  }

  /*======================== ROUTE RATES SERVICES ======================*/

  addRouteRateService(data: any, id) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/companies/'+id+'/rrc', data, { headers: headers })
  }

  deleteRouteRateService(rateId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/companies/rrc/rates/' +rateId, { headers: headers })
  }

  getRouteRateService(rateData) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    let params = new HttpParams().set('customerId', rateData.customerId)
      .set('routeId', rateData.route)
    if (rateData.startDate != null && rateData.startDate != '') {
      params = params.append("start", rateData.startDate)
    }
    if (rateData.endDate != null && rateData.endDate != '') {
      params = params.append("end", rateData.endDate)
    }
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'contracts/companies/routes/rates', { headers: headers, params: params })
  }

  /*======================== FUEL CARD SERVICES ======================*/

  addFuelCardService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/fuelCards', data, { headers: headers })
  }

  getFuelCardService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/fuelCards', { headers: headers })
  }

  getFuelCardByCustomerIdService(cardId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/fuelCards/filter?cId=' + cardId, { headers: headers })
  }

  getFuelCardByAdminIdService(cardId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/fuelCards/filter?acId=' + cardId, { headers: headers })
  }

  deleteFuelCardService(cardId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/fuelCards/' + cardId, { headers: headers })
  }

  /*======================== DROPDOWN SERVICES ======================*/
  getDropdowns(type: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'dropdowns?type=' + type, { headers: headers })
  }

  getAreaDropdownService(type: string, cityCode: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'dropdowns?type=' + type + '&city=' + cityCode, { headers: headers })
  }

  addCityDropdownService(data: any, param: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'dropdowns/cities', data, { headers: headers, params : param })
  }

  getAllDrivers() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/filter?pos=driver&avail=Free', { headers: headers })
  }

  getAllFilterTrailer() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trailers/filter?avail=Free', { headers: headers })
  }

  getAllFilterTractor() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/tractors/filter?avail=Free', { headers: headers })
  }

  getAllFilterContainer() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/containers/filter?avail=Free', { headers: headers })
  }

  getAllFilterTracker() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trackers/filter?avail=Free', { headers: headers })
  }

  getAllRWBDrivers(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/filter?'+param, { headers: headers })
  }

  getAllRWBTrailer(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trailers/filter?'+param, { headers: headers })
  }

  getAllRWBTractor(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/tractors/filter?'+param, { headers: headers })
  }

  getAllRWBContainer(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/containers/filter?'+param, { headers: headers })
  }

  getAllRWBTracker(rwbID) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/trackers/filter?mov=Free&rwbId=' + rwbID, { headers: headers })
  }

  getAllJobTractor(jobID) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/jobs/'+jobID+'/tractors', { headers: headers })
  }

  getAllJobTrailer(jobID) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/jobs/'+jobID+'/trailers', { headers: headers })
  }
  getAllFilterAssetCombination(apiUrl: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/assetCombinations/filter?' + apiUrl + '=Free', { headers: headers })
  }

  getAllFilterCustomer() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Customer&status=Active', { headers: headers })
  }

  getAllFilterSupplierCompany() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Supplier&status=Active', { headers: headers })
  }

  getAllFilterCustomerContract(haveContract) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Customer&status=Active&contract=' + haveContract, { headers: headers })
  }

  getAllFilterBroker() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Broker&status=Active', { headers: headers })
  }

  getAllFilterFuelSupplier() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Supplier&st=Fuel&status=Active', { headers: headers })
  }

  getAllFilterTruckSupplier() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Supplier&st=Truck&status=Active', { headers: headers })
  }

  getAllFilterDriverSupplier() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Supplier&st=Driver&status=Active', { headers: headers })
  }

  getAllFilterContainerSupplier() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Supplier&st=Container&status=Active', { headers: headers })
  }

  getAllFilterTrailerSupplier() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Supplier&st=Trailer&status=Active', { headers: headers })
  }

  getAllFilterTrackerSupplier() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Supplier&st=Tracker&status=Active', { headers: headers })
  }

  getAllFilterFuelCard() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'admin/fuelCards', { headers: headers })
  }
  /*======================== TRAINING PROFILE SERVICES ======================*/
  uploadTrainingProfile(profileData) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/tp', profileData, { headers: headers })
  }

  updateTrainingProfile(profileData) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/tp', profileData, { headers: headers })
  }

  getTrainingProfile(empId) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/tp?employeeId=' + empId, { headers: headers })
  }

  deleteTrainingProfileService(profileId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/tp/' + profileId, { headers: headers })
  }

  /*======================== MEDICAL REPORT SERVICES ======================*/
  uploadMedicalReport(reportData) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/mtr', reportData, { headers: headers })
  }

  updateMedicalReport(reportData) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/mtr', reportData, { headers: headers })
  }

  getMedicalReport(empId) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/mtr?employeeId=' + empId, { headers: headers })
  }

  deleteMedicalReportService(medId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/mtr/' + medId, { headers: headers })
  }

  /*======================== EMPLOYEE REFERENCES SERVICES ======================*/
  addEmployeeReferences(ref) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/references', ref, { headers: headers })
  }

  getEmployeeReferences(empId) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/references?employeeId=' + empId, { headers: headers })
  }

  deleteEmployeeReferenceService(refID: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/references/' + refID, { headers: headers })
  }
  /*======================== EMPLOYEE HEAVY VEHICLE SERVICES ======================*/
  addHeavyVehiclesExperience(heavyVehicle) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/hve', heavyVehicle, { headers: headers })
  }

  getEmpVehicleExperience(empId) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/documents/hve?employeeId=' + empId, { headers: headers })
  }

  /*======================== JOB ORDER SERVICES ======================*/

  addJobService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/jobs', data, { headers: headers })
  }

  updateJobService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/jobs', data, { headers: headers })
  }

  deleteJobService(jobId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/jobs/' + jobId, { headers: headers })
  }

  getAllJobService(param) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, totalRecords: any, errorMessage: string, success: boolean }>(this.url + 'orders/jobs', { headers: headers, params: param })
  }

  getJobById(jobId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/jobs/' + jobId, { headers: headers })
  }

  jobSearchService(params: any) {
    // let params = new HttpParams()
    // if (jobSearchData.jobID != null && jobSearchData.jobID != '')
    //   params = params.append("jobId", jobSearchData.jobID)
    // if (jobSearchData.rwbId != null && jobSearchData.rwbId != '')
    //   params = params.append("rwbId", jobSearchData.rwbId)
    // if (jobSearchData.lic != null && jobSearchData.lic != '')
    //   params = params.append("lic", jobSearchData.lic)
    // if (jobSearchData.startDate != null && jobSearchData.startDate != '')
    //   params = params.append("start", jobSearchData.startDate)
    // if (jobSearchData.endDate != null && jobSearchData.endDate != '')
    //   params = params.append("end", jobSearchData.endDate)
    // if (jobSearchData.jobStatus != null && jobSearchData.jobStatus != '')
    //   params = params.append("status", jobSearchData.jobStatus)
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/jobs/filter?' + params, { headers: headers})
  }

  addJobFuelService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/job/fuel', data, { headers: headers })
  }

  updateJobFuelService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/job/fuel', data, { headers: headers })
  }

  deleteJobFuelService(jobFuelId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/job/fuel/' + jobFuelId, { headers: headers })
  }

  /*======================== ROADWAY BILL SERVICES ======================*/

  addRoadwayBillService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb', data, { headers: headers })
  }

  updateRoadwayBillService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb', data, { headers: headers })
  }

  deleteRoadwayBillService(rwbId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/' + rwbId, { headers: headers })
  }

  getAllRoadwayBillService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb', { headers: headers })
  }

  getRoadwayBillById(rwbId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/' + rwbId, { headers: headers })
  }

  changeRoadwayBillStatus(rwbId: string, status: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/'+rwbId+'/status?status='+status, { headers: headers })
  }

  getAllLoadStopsById(rwbId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/stops/filter/?rwbId=' + rwbId, { headers: headers })
  }

  addLoadStopService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/stops', data, { headers: headers })
  }

  updateLoadStopService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/stops', data, { headers: headers })
  }

  deleteLoadStopService(loadId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/stops/' + loadId, { headers: headers })
  }

  getWarehousesService(refId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/warehouses/filter?companyId=' + refId, { headers: headers })
  }

  addLoadProductService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/products', data, { headers: headers })
  }

  updateLoadProductService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/products', data, { headers: headers })
  }

  deleteLoadProductService(prodId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/products/' + prodId, { headers: headers })
  }

  addLoadProgressService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/stopProgress', data, { headers: headers })
  }

  updateLoadProgressService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/stopProgress', data, { headers: headers })
  }

  getAllLoadProgressService(rwbId) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/stopProgress/filter?rwbId='+ rwbId, { headers: headers })
  }

  deleteLoadProgressService(progId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/stopProgress/' + progId, { headers: headers })
  }

  addLoadExpenseService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/expenses', data, { headers: headers })
  }

  updateLoadExpenseService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/expenses', data, { headers: headers })
  }

  deleteLoadExpenseService(expenseId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/expenses/' + expenseId, { headers: headers })
  }
  
  addLoadIncomeService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/incomes', data, { headers: headers })
  }

  updateLoadIncomeService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/incomes', data, { headers: headers })
  }

  deleteLoadIncomeService(incomeId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/incomes/' + incomeId, { headers: headers })
  }

  getRoadWayBill(params) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/filter?' + params, { headers: headers })
  }

  addIncome(data) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/incomes', data, { headers: headers })
  }

  updateIncome(data) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'orders/rwb/incomes', data, { headers: headers })
  }

  tripDetails(id) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'summary/trip?jobId=' + id, { headers: headers })
  }
  //for export excel
  tripSummaryExport(start, end) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get(this.url + 'summary/trip/export?start=' + start + '&end=' + end, { responseType: 'blob' as 'json', headers: headers })

  }

  /*=================================== DELETE FILES SERVICES ======================================== */
  deleteFileService(fileUrl) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ message: string, errorMessage: string, success: boolean }>(fileUrl, { headers: headers })
  }

  /*=================================== JOB REIMBURSEMENT SERVICES ======================================== */
  sendJobReimbursementService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'reimbursements/send', data, { headers: headers })
  }

  sendJobApprovalService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'reimbursements', data, { headers: headers })
  }

  getReimbursementDocService(data: any){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post(this.url + 'reimbursements/preview', data, { responseType: 'blob' as 'json', headers: headers })
  }

  getAllReimbursementService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'reimbursements', { headers: headers})
  }

  getReimbursementByIdService(id) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'reimbursements/'+id, { headers: headers})
  }

  /*=================================== Accounts Trial Balance SERVICES ======================================== */
  getTrialBalanceService(params) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/trialBalance', { headers: headers, params : params })
  }

  getAdjustedEntriesService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/adjustments', { headers: headers })
  }

  getFilterAdjustedEntriesService(data) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/adjustments/filter?'+data, { headers: headers })
  }

  addAdjustmentEntryService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/adjustments', data, { headers: headers })
  }

  deleteAdjustmentEntryService(id) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/adjustments/'+id, { headers: headers })
  }

  /*=================================== INVOICE SERVICES ======================================== */
  getAllInvoices(data: any){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/rwb/filter?'+data, { headers: headers })
  }

  getDHInvoicesByIdService(id) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/dh/'+id, { headers: headers})
  }

  getInvoicesByIdService(id) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/'+id, { headers: headers})
  }

  getAllFilterInvoices(data: any){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/filter?'+data, { headers: headers })
  }

  getAllGeneratedInvoices(){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices', { headers: headers })
  }

  getAllDetentionInvoices(){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/dh', { headers: headers })
  }

  getAllPendingInvoices(){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/stats', { headers: headers })
  }

  getInvoicesForPrint(invID){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{data: any, message: string, errorMessage: string, success: boolean}>(this.url + 'accounts/invoices/'+invID+'/pdf', { responseType: 'blob' as 'json', headers: headers })
  }

  getDHInvoicesForPrint(invID){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{data: any, message: string, errorMessage: string, success: boolean}>(this.url + 'accounts/invoices/dh/'+invID+'/pdf', { responseType: 'blob' as 'json', headers: headers })
  }

  generateInvoiceService(data: any, type: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices?type='+type, data, { headers: headers })
  }

  generateFixInvoiceService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/fixed?'+ data, { headers: headers })
  }

  InvoicePaymentService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/payment', data, { headers: headers })
  }

  DHInvoicePaymentService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/dh/payment', data, { headers: headers })
  }

  getDHInvoicesPaymentsService(invID){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{data: any, message: string, errorMessage: string, success: boolean}>(this.url + 'accounts/invoices/dh/'+invID+'/payments', { headers: headers })
  }

  getInvoicesPaymentsService(invID){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{data: any, message: string, errorMessage: string, success: boolean}>(this.url + 'accounts/invoices/'+invID+'/payments', { headers: headers })
  }

  deleteInvoiceService(invId) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'accounts/invoices/'+invId, { headers: headers })
  }

  /*=================================== BULK UPLOAD DOCUMENT SERVICES ======================================== */

  uploadBulkFileService(fileData, pathUrl) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'assets/'+pathUrl+'/file', fileData, { headers: headers })
  }

  uploadEmpBulkFileService(fileData) {
    const headers = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'employees/file', fileData, { headers: headers })
  }

  /*============================ DOWNLOAD DRIVER, ASSET LIST DOCUMENT SERVICES =============================== */

  getEmployeeExportFileService(){
    const headers = new HttpHeaders({ 'Authorization': this.token });
    return this.http.get(this.url + 'employees/export', { responseType: 'blob' as 'json', headers: headers })
  }

  getTrailerExportFileService(){
    const headers = new HttpHeaders({ 'Authorization': this.token });
    return this.http.get(this.url + 'assets/trailers/export', { responseType: 'blob' as 'json', headers: headers })
  }

  getTractorExportFileService(){
    const headers = new HttpHeaders({ 'Authorization': this.token });
    return this.http.get(this.url + 'assets/tractors/export', { responseType: 'blob' as 'json', headers: headers })
  }
}
