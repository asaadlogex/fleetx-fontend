import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorMessageService {

  private errorMessageSource = new BehaviorSubject('');
  errorMessage = this.errorMessageSource.asObservable();

  private infoMessageSource = new BehaviorSubject('');
  infoMessage = this.infoMessageSource.asObservable();

  private successMessageSource = new BehaviorSubject('');
  successMessage = this.successMessageSource.asObservable();
  
  constructor() { }

  getErrorMessage(message: string) {
    this.errorMessageSource.next(message)
  }

  getSuccessMessage(message: string) {
    this.successMessageSource.next(message)
  }

  getInfoMessage(message: string) {
    this.infoMessageSource.next(message)
  }
}
