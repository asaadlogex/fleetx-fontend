import { TestBed } from '@angular/core/testing';

import { BackEnd2Service } from './back-end2.service';

describe('BackEnd2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackEnd2Service = TestBed.get(BackEnd2Service);
    expect(service).toBeTruthy();
  });
});
