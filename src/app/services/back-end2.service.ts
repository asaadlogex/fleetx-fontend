import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackEnd2Service {

  token = sessionStorage.getItem('token');
  url = environment.baseUrl;

  constructor(private http: HttpClient) { }

  /*======================== MAINTENANCE SERVICES ======================*/
  addBrand(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'dropdowns/brands', data, { headers: headers })
  }

  addItemCategories(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'dropdowns/itemCategories', data, { headers: headers })
  }

  addItemService(data: any, qty, cost) {
    let params = new HttpParams();
    if (qty != null)
      params = params.append("qty", qty);
    if (cost != null)
      params = params.append("cost", cost);
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/items', data, { headers: headers, params: params })
  }

  updateItemService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/items', data, { headers: headers })
  }

  deleteItemService(itemId: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/items/' + itemId, { headers: headers })
  }

  getItemById(itemId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/items/' + itemId, { headers: headers })
  }

  getAllItemService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/items', { headers: headers })
  }

  getAllInventoryService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory', { headers: headers })
  }

  /*======================== WORK ORDER SERVICES ======================*/
  addWorkOrderService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/workOrder', data, { headers: headers })
  }

  updateWorkOrderService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/workOrder', data, { headers: headers })
  }

  deleteWorkOrderService(wrkId: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/workOrder/' + wrkId, { headers: headers })
  }

  getWorkOrderById(wrkId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/workOrder/' + wrkId, { headers: headers })
  }

  getAllWorkOrderService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/workOrder', { headers: headers })
  }

  changeWorkOrderStatusService(id, stat) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/workOrder/'+id+'/status?status='+stat,{ headers: headers })
  }

  /*======================== PURCHASE ORDER SERVICES ======================*/
  addPurchaseOrderService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/purchase', data, { headers: headers })
  }

  updatePurchaseOrderService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/purchase', data, { headers: headers })
  }

  deletePurchaseOrderService(purId: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/purchase/' + purId, { headers: headers })
  }

  getPurchaseOrderById(purId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/purchase/' + purId, { headers: headers })
  }

  getAllPurchaseOrderService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/purchase', { headers: headers })
  }

  receivePurchaseOrderService(data: any, id) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/purchase/'+id+'/receive', data, { headers: headers })
  }

  /*======================== DropDown SERVICES ======================*/

  getDropdowns(type: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'dropdowns?type=' + type, { headers: headers })
  }

  getAllFilterVendors() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Supplier&st=Parts&status=Active', { headers: headers })
  }

  getAllFilterWorkshop() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'companies/filter?bt=Supplier&st=Workshop&status=Active', { headers: headers })
  }

  /*======================== ADJUSTED INVENTORY SERVICES ======================*/
  addInventoryAdjustmentService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.post<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/adjustments', data, { headers: headers })
  }

  updateInventoryAdjustmentService(data: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json' });
    return this.http.put<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/adjustments', data, { headers: headers })
  }

  deleteInventoryAdjustmentService(adjId: any) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.delete<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/adjustments/' + adjId, { headers: headers })
  }

  getInventoryAdjustmentById(adjId: string) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/adjustments/' + adjId, { headers: headers })
  }

  getAllInventoryAdjustmentService() {
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'inventory/adjustments', { headers: headers })
  }

  /*============================ DOWNLOAD INVENTORY LIST DOCUMENT SERVICES =============================== */

  getInventoryExportFileService(){
    const headers = new HttpHeaders({ 'Authorization': this.token });
    return this.http.get(this.url + 'inventory/export', { responseType: 'blob' as 'json', headers: headers })
  }

  getWorkOrderExportFileService(){
    const headers = new HttpHeaders({ 'Authorization': this.token });
    return this.http.get(this.url + 'inventory/workOrder/export', { responseType: 'blob' as 'json', headers: headers })
  }

  /*============================ DASHBOARD SERVICES =============================== */

  getAllDashboardDataService(){
    const headers = new HttpHeaders({ 'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': this.token });
    return this.http.get<{ data: any, message: string, errorMessage: string, success: boolean }>(this.url + 'dashboard', { headers: headers })
  }
}
