import { TestBed } from '@angular/core/testing';

import { EncrDecryptService } from './encr-decrypt.service';

describe('EncrDecryptService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EncrDecryptService = TestBed.get(EncrDecryptService);
    expect(service).toBeTruthy();
  });
});
