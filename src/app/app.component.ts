import { Component} from '@angular/core';
import { BackEndService } from './services/back-end.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public backendService : BackEndService, private router: Router){

  }
  ngOnInit(){
    let user = sessionStorage.getItem("user");
      let token = sessionStorage.getItem("token");
      if(user != null && token != null){
          let userDetail = {
              username : user,
              token : token
          }
          this.backendService.verifyUser(userDetail).subscribe(res =>{
            if(res.success){
              sessionStorage.setItem("token", res.data)
              this.router.navigate(['dashboard'])
            }else{
              this.router.navigate(['login'])
            }
          })
      }else{
        this.router.navigate(['login'])
      }
  }

}
