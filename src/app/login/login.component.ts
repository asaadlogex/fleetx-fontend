import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  credentials: any;
  invalidUserMsg: string = null;

  constructor(private formBuilder: FormBuilder,
    public backendService: BackEndService,
    private router: Router) {

    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit() {
    
    // if (this.backendService.isUserLoggedIn()) {
    //   this.router.navigate(['dashboard']);
    // }
  }

  login() {
    this.invalidUserMsg = null;
    if(this.loginForm.invalid){
      return;
    }
    this.credentials = {
      username: this.loginForm.value.userName,
      password: this.loginForm.value.password
    }
    this.backendService.loginService(this.credentials).subscribe(res => {
      if (res.success) {
        sessionStorage.setItem("user", res.data.username);
        this.verifyUser(res.data);
      }else{
        this.invalidUserMsg = "Invalid Username or Password."
      }
    })
  }

  verifyUser(user){
    this.backendService.verifyUser(user).subscribe(res => {
      if (res.success) {
        sessionStorage.setItem("token", res.data);
        this.router.navigate(['dashboard']);
      }else{
        this.router.navigate(['login']);
      }
    })
  }

}
