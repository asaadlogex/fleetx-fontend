import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { BackEndService } from 'app/services/back-end.service';
import { environment } from 'environments/environment';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-super-admin',
  templateUrl: './super-admin.component.html',
  styleUrls: ['./super-admin.component.scss']
})
export class SuperAdminComponent implements OnInit {

  /* ====================== VARIABLE CODE FOR DROPDOWN  ============================ */

  businessTypeDropdownCode = environment.BUSINESS_TYPE;
  channelTypeDropdownCode = environment.CHANNEL_TYPE;
  formationTypeDropdownCode = environment.FORMATION_TYPE;
  cityDropdownCode = environment.CITY_TYPE;
  stateTypeDropdownCode = environment.STATE_TYPE;

  /* ====================== VARIABLES ============================ */
  companyForm: FormGroup;
  submitted = false;
  company: any;
  channelTypeDropDown: any;
  businessTypeDropDown: any;
  supplierDropDown: any;
  cities: any;
  stateDropDown : any;
  routes: any = [];
  fuelRates: any = [];
  formationDropDown: any;
  addresses: FormArray;
  contacts: FormArray;

  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;

  constructor(public backendService: BackEndService,
    private formBuilder: FormBuilder,
    private router: Router,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {

    this.companyForm = this.formBuilder.group({
      companyId: [''],
      companyStatus: ['Active'],
      legalName: [''],
      companyCode : [''],
      businessType: [null],
      strnNumber: [null],
      ntnNumber: [null],
      sraNumber: [null],
      praNumber: [null],
      kraNumber: [null],
      braNumber: [null],
      formation: [null],
      fuelStatus: [''],
      fuelSupplier: [''],
      fuelStartDate: [''],
      fuelEndDate: [''],
      fuelPerLitre: [null],
      address: this.formBuilder.array([this.createAddress()]),
      contact: this.formBuilder.array([]),
    });
  }

  /* =============================== ONINIT COMPONENT FUNCTIONS ======================================== */

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    this.getBusinessTypeDropdown(this.businessTypeDropdownCode);
    this.getContactChannelDropdown(this.channelTypeDropdownCode);
    this.getformationDropdown(this.formationTypeDropdownCode);
    this.getCitiesDropdown(this.cityDropdownCode);
    this.getStateDropdown(this.stateTypeDropdownCode)
    this.getAdminCompany();
    this.getFuelSupplier();
    this.getAllFuelRate();

  }

  /* ====================== LOOK UP SEARCH FILTER FUNCTIONS ============================ */

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  /* ====================== CREATE ADDRESS AND CONTACT FORM GROUP FUNCTIONS ============================ */

  createAddress() {
    return this.formBuilder.group({
      addressId: [null],
      city: ['', Validators.required],
      street1: ['', Validators.required],
      street2: [''],
      country: ['', Validators.required],
      zipCode: ['', Validators.required],
      state: ['', Validators.required]
    });
  }

  createContact() {
    return this.formBuilder.group({
      contactId: [null],
      channel: ['', Validators.required],
      data: ['', Validators.required]
    });
  }

  /* ====================== ADD ADDRESS AND CONTACT FUNCTIONS ============================ */

  addNewAddress() {
    this.addresses = this.companyForm.get('address') as FormArray;
    this.addresses.push(this.createAddress());
  }

  addNewContact() {
    this.contacts = this.companyForm.get('contact') as FormArray;
    this.contacts.push(this.createContact());
  }

  /* ====================== DELETE ADDRESS AND CONTACT FUNCTIONS ============================ */

  deleteContact(data, index) {
    if (index > 0) {
      this.contacts = this.companyForm.get('contact') as FormArray;
      this.contacts.removeAt(index);
    }

  }

  //Function to Delete a Contact
  deleteAddress(data, index) {
    // if (index > 0) {
      this.addresses = this.companyForm.get('address') as FormArray;
      this.addresses.removeAt(index);
    // }
  }

  /* =================== ADD UPDATE AND DELETE ADMIN COMPANY FUNCTIONS ============================= */

  submitCompany() {
    if (this.companyForm.invalid) {
      return;
    }

    // for (let i = 0; i < this.companyForm.getRawValue().address.length; i++) {
    //   if (this.companyForm.getRawValue().address[i].zipCode != null)
    //     this.companyForm.getRawValue().address[i].zipCode = this.companyForm.getRawValue().address[i].zipCode.toString();
    // }

    // for (let i = 0; i < this.companyForm.value.contact.length; i++) {
    //   if (this.companyForm.value.contact[i].data != null)
    //     this.companyForm.value.contact[i].data = this.companyForm.value.contact[i].data.toString();
    // }

    this.company = {
      companyId: this.companyForm.value.companyId,
      companyCode: this.companyForm.value.companyCode,
      companyName: this.companyForm.value.legalName,
      formation: this.companyForm.value.formation,
      ntnNumber: this.companyForm.value.ntnNumber,
      strnNumber: this.companyForm.value.strnNumber,
      status: this.companyForm.value.companyStatus,
      //isInternal: true,
      businessType: this.companyForm.value.businessType,
      sra: this.companyForm.value.sraNumber,
      pra: this.companyForm.value.praNumber,
      kra: this.companyForm.value.kraNumber,
      bra: this.companyForm.value.braNumber,
      addresses: this.companyForm.getRawValue().address,
      contacts: this.companyForm.value.contact
    }

    if (this.companyForm.value.companyId != null && this.companyForm.value.companyId != '') {
      this.updateCompany();
    } else {
      this.addCompany();
    }

  }

  addCompany() {

    this.backendService.addAdminCompanyService(this.company).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.companyForm.get("companyId").setValue(res.data);
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("COMPANY ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateCompany() {
    this.backendService.updateAdminCompanyService(this.company).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("COMPANY UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteCompany() {
    if (this.company.companyId != null || this.company.companyId != "") {
      this.backendService.deleteAdminCompanyService(this.company.companyId).subscribe(res => {
        if (res.success) {
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
          this.router.navigate(['dashboard'])
        }else{
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("COMPANY DELETE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    }
  }

  /* ==================== FETCH ADMIN COMPANY DATA FUNCTIONS ============================== */
  getAdminCompany() {
    this.backendService.getAdminCompanies().subscribe(res => {
      if (res.success) {
        this.company = res.data[0];
        this.companyForm = this.formBuilder.group({
          companyId: [this.company.companyId],
          companyStatus: [this.company.status],
          legalName: [this.company.companyName],
          companyCode: [this.company.companyCode],
          businessType: [this.company.businessType],
          strnNumber: [this.company.strnNumber],
          ntnNumber: [this.company.ntnNumber],
          sraNumber: [this.company.sra],
          praNumber: [this.company.pra],
          kraNumber: [this.company.kra],
          braNumber: [this.company.bra],
          formation: [this.company.formation],
          fuelStatus: ['Active'],
          fuelSupplier: [''],
          fuelStartDate: [''],
          fuelEndDate: [''],
          fuelPerLitre: [null],
          address: this.formBuilder.array([]),
          contact: this.formBuilder.array([])
        });

        for (let i = 0; i < this.company.addresses.length; i++) {
          this.addresses = this.companyForm.get('address') as FormArray;
          this.addresses.push(this.formBuilder.group({
            addressId: [this.company.addresses[i].addressId],
            city: [this.company.addresses[i].city],
            street1: [this.company.addresses[i].street1],
            street2: [this.company.addresses[i].street2],
            country: [this.company.addresses[i].country],
            zipCode: [this.company.addresses[i].zipCode],
            state: [this.company.addresses[i].state]
          }));
          // this.addresses.disable();
        }

        for (let i = 0; i < this.company.contacts.length; i++) {
          this.contacts = this.companyForm.get('contact') as FormArray;
          this.contacts.push(this.formBuilder.group({
            contactId: [this.company.contacts[i].contactId],
            channel: [this.company.contacts[i].channel],
            data: [this.company.contacts[i].data]
          }));
        }
      }else{
        if(this.companyForm.value.companyId != ""){
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
      }
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  /* ============================== DROPDOWNS FUNCTIONS ========================================== */

  getBusinessTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.businessTypeDropDown = res.data;
    })
  }

  getContactChannelDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.channelTypeDropDown = res.data;
      if(this.companyForm.value.companyId == ""){
        for(let i=0; i<this.channelTypeDropDown.length; i++){
          this.contacts = this.companyForm.get('contact') as FormArray;
          this.contacts.push(this.formBuilder.group({
          contactId: [null],
          channel: [this.channelTypeDropDown[i]],
          data: ['']
        }));
        }
      }
    })
  }

  getformationDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.formationDropDown = res.data;
    })
  }

  getCitiesDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.cities = res.data;
    })
  }

  getStateDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.stateDropDown = res.data;
    })
  }

  getFuelSupplier() {
    this.backendService.getAllFilterFuelSupplier().subscribe(res => {
      this.supplierDropDown = res.data;
    })
  }

  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }



  /* ====================== SUPPLIER FUEL RATE TAB FUNCTIONS ============================ */

  addFuelRate() {
    let fuelData = {
      fscId: null,
      adminCompany: this.company,
      supplier: this.companyForm.value.fuelSupplier,
      fscStatus: this.companyForm.value.fuelStatus,
      fuelRates: [{
        fuelRateId: null,
        startDate: this.datePipe.transform(this.companyForm.value.fuelStartDate, environment.SystemTypes_Date_Format),
        endDate: this.datePipe.transform(this.companyForm.value.fuelEndDate, environment.SystemTypes_Date_Format),
        ratePerLitre: this.companyForm.value.fuelPerLitre,
        fscId: null
      }]
    }
    this.backendService.addFuelRateService(fuelData).subscribe(res => {
      if (res.success) {
        this.fuelRates.push(res.data);
        this.companyForm.get("fuelSupplier").reset();
        this.companyForm.get("fuelStatus").reset();
        this.companyForm.get("fuelStartDate").reset();
        this.companyForm.get("fuelEndDate").reset();
        this.companyForm.get("fuelPerLitre").reset();
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("DUEL RATE ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  getAllFuelRate() {
    this.backendService.getFuelRateService().subscribe(res => {
      if (res.success) {
        this.fuelRates = res.data;
      }
    })
  }

  deleteFuelRate(data: any, id) {
    this.backendService.deleteFuelRateService(data.fscId).subscribe(res => {
      if (res.success) {
        this.fuelRates = this.fuelRates.filter(x => x.fscId !== data.fscId)
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("DUEL RATE ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

   /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
