import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { OrderBillComponent } from '../order-bill/order-bill.component';
import { RoadwayBillComponent } from 'app/RoadwayBills/roadway-bill/roadway-bill.component';
import { StopsProductComponent } from 'app/RoadwayBills/stops-product/stops-product.component';
import { LoadProgressComponent } from 'app/RoadwayBills/load-progress/load-progress.component';
import { FinancialsComponent } from 'app/RoadwayBills/financials/financials.component';
import { ProgressFormComponent } from 'app/RoadwayBills/progress-form/progress-form.component';

@NgModule({
  declarations: [RoadwayBillComponent,
    StopsProductComponent,
    LoadProgressComponent,
    FinancialsComponent,
    ProgressFormComponent,],
  imports: [
    CommonModule,
    OrderRoutingModule
  ]
})
export class OrderModule { }
