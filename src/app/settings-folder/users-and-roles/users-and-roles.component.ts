import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource, MatPaginator } from "@angular/material";

@Component({
	selector: "app-users-and-roles",
	templateUrl: "./users-and-roles.component.html",
	styleUrls: ["./users-and-roles.component.scss"],
})
export class UsersAndRolesComponent implements OnInit {
	usersColumns: string[] = ["role", "description", "action"];
	usersSource = new MatTableDataSource<UserElement>(USER_DATA);

	@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

	ngOnInit() {
		this.usersSource.paginator = this.paginator;
	}
}

export interface UserElement {
	role: string;
	description: string;
}

const USER_DATA: UserElement[] = [
	{ role: "Hydrogen", description: "lorem ipsum dolor sit amet" },
	{ role: "Helium", description: "lorem ipsum dolor sit amet" },
	{ role: "Lithium", description: "lorem ipsum dolor sit amet" },
];
