import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from "@angular/material";
import { AddOptionModalComponent } from 'app/modals/add-option-modal/add-option-modal.component';


@Component({
  selector: "app-add-option-table",
  templateUrl: "./add-option-table.component.html",
  styleUrls: ["./add-option-table.component.scss"]
})
export class AddOptionTableComponent implements OnInit {
  lookupForm: FormGroup;
  displayedColumns: string[] = ["optionName", "optionCode", "action"];
  dataSource = new MatTableDataSource<LookupTableData>(LOOKUP_DATA);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    public dialog: MatDialog
  ) {
    this.lookupForm = new FormGroup({
      category: new FormControl(null),
      dropdown: new FormControl(null)
    });
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  search() {
    console.log(this.lookupForm.value);
  }

  openDialog() {
    const dialogRef = this.dialog.open(AddOptionModalComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}

export interface LookupTableData {
  optionName: string;
  optionCode: string;
}

const LOOKUP_DATA: LookupTableData[] = [
  { optionName: "Hydrogen", optionCode: "H" },
  { optionName: "Helium", optionCode: "He" },
  { optionName: "Lithium", optionCode: "Li" },
  { optionName: "Beryllium", optionCode: "Be" },
  { optionName: "Boron", optionCode: "B" },
  { optionName: "Carbon", optionCode: "C" },
  { optionName: "Nitrogen", optionCode: "N" },
  { optionName: "Oxygen", optionCode: "O" },
  { optionName: "Fluorine", optionCode: "F" },
  { optionName: "Neon", optionCode: "Ne" },
  { optionName: "Sodium", optionCode: "Na" },
  { optionName: "Magnesium", optionCode: "Mg" },
  { optionName: "Aluminum", optionCode: "Al" },
  { optionName: "Silicon", optionCode: "Si" },
  { optionName: "Phosphorus", optionCode: "P" },
  { optionName: "Sulfur", optionCode: "S" },
  { optionName: "Chlorine", optionCode: "Cl" },
  { optionName: "Argon", optionCode: "Ar" },
  { optionName: "Potassium", optionCode: "K" },
  { optionName: "Calcium", optionCode: "Ca" }
];
