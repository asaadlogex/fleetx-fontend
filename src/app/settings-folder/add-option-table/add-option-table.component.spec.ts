import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOptionTableComponent } from './add-option-table.component';

describe('AddOptionTableComponent', () => {
  let component: AddOptionTableComponent;
  let fixture: ComponentFixture<AddOptionTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOptionTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOptionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
