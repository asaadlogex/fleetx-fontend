import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BackEndService } from 'app/services/back-end.service';
import { DatePipe } from '@angular/common';
import { ErrorMessageService } from 'app/services/error-message.service';
import { MatDialog } from '@angular/material';
import { environment } from 'environments/environment';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { HttpErrorResponse } from '@angular/common/http';

interface ExpensesType {
  id: number;
  code: string;
  value: string;
  description: string;
  status: string;
}

interface ExpensesGroup {
  disabled?: boolean;
  name: string;
  values: ExpensesType[];
}

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})

export class AccountsComponent implements OnInit {

  incomeTypeCode = environment.INCOME_TYPE;
  expenseTypeCode = environment.EXPENSE_TYPE;

  searchForm: FormGroup;
  adjustedSearchForm: FormGroup;
  adjustedEntrySearchForm: FormGroup;
  adjustedEntryForm: FormGroup;
  displayedColumns: string[] = ['expense', 'debit', 'credit'];
  dataSource: MatTableDataSource<any>;
  adjustedDataSource: MatTableDataSource<any>;
  AdjustedTrialDataSource: MatTableDataSource<any>;
  adjustedEntryColumns: string[] = ['date', 'expense', 'debit', 'credit', 'action'];
  trialBalData: any = [];
  adjBalData: any = [];
  adjEntryData: any = [];
  trialBalCreditTotal = 0;
  trialBalDebitTotal = 0;
  AdjBalCreditTotal = 0;
  AdjEntryDebitTotal = 0;
  AdjEntryCreditTotal = 0;
  AdjBalDebitTotal = 0;
  incomeDropDown: any;
  expenseDropDown: any;
  expenseHeadDropDown: ExpensesGroup[]

  // adjustedEntries = new MatTableDataSource<AdjustedEntries>(ADJUSTED_ENTRY_DATA)


  constructor(public backendService: BackEndService,
    public datePipe: DatePipe,
    public errorMsgService: ErrorMessageService,
    public dialog: MatDialog, ) {

    this.searchForm = new FormGroup({
      start: new FormControl(null, Validators.required),
      end: new FormControl(null, Validators.required),
    })

    this.adjustedSearchForm = new FormGroup({
      adjStart: new FormControl(null, Validators.required),
      adjEnd: new FormControl(null, Validators.required),
    })

    this.adjustedEntrySearchForm = new FormGroup({
      accountName: new FormControl(null),
      adjEntStart: new FormControl(null, Validators.required),
      adjEntEnd: new FormControl(null, Validators.required),
    })

    this.adjustedEntryForm = new FormGroup({
      date: new FormControl(null, Validators.required),
      expenseHead: new FormControl(null, Validators.required),
      expenseType: new FormControl(null, Validators.required),
      expenseAmount: new FormControl(null, Validators.required),
    })

  }

  ngOnInit() {
    this.getIncomeTypeDropdown(this.incomeTypeCode);
    this.getAllAdjustedEntries();
  }

  /****************** search form **************************/

  getTrialBalance() {
    if (this.searchForm.invalid) {
      return;
    }
    let data = {
      start: this.datePipe.transform(this.searchForm.value.start, environment.SystemTypes_Date_Format),
      end: this.datePipe.transform(this.searchForm.value.end, environment.SystemTypes_Date_Format),
      adjusted: false
    }
    this.backendService.getTrialBalanceService(data).subscribe(res => {
      if (res.success) {
        this.trialBalData = [];
        this.trialBalDebitTotal = 0;
        this.trialBalCreditTotal = 0;
        for (let i = 0; i < res.data.entries.length; i++) {
          let trialData = {
            date: res.data.entries[i].date,
            accountHead: res.data.entries[i].accountHead,
            debitAmount: null,
            creditAmount: null,
          }
          if (res.data.entries[i].accountType == "Debit") {
            trialData.debitAmount = res.data.entries[i].amount;
            this.trialBalDebitTotal += res.data.entries[i].amount;
          } else {
            trialData.creditAmount = res.data.entries[i].amount;
            this.trialBalCreditTotal += res.data.entries[i].amount;
          }
          this.trialBalData.push(trialData);
        }
        this.dataSource = new MatTableDataSource(this.trialBalData);
      }
    })
  }

  getAdjustedTrialBalance() {
    if (this.adjustedSearchForm.invalid) {
      return;
    }
    let data = {
      start: this.datePipe.transform(this.adjustedSearchForm.value.adjStart, environment.SystemTypes_Date_Format),
      end: this.datePipe.transform(this.adjustedSearchForm.value.adjEnd, environment.SystemTypes_Date_Format),
      adjusted: true
    }
    this.backendService.getTrialBalanceService(data).subscribe(res => {
      if (res.success) {
        this.adjBalData = [];
        this.AdjBalDebitTotal = 0;
        this.AdjBalCreditTotal = 0;
        for (let i = 0; i < res.data.entries.length; i++) {
          let trialData = {
            date: res.data.entries[i].date,
            accountHead: res.data.entries[i].accountHead,
            debitAmount: null,
            creditAmount: null,
          }
          if (res.data.entries[i].accountType == "Debit") {
            trialData.debitAmount = res.data.entries[i].amount;
            this.AdjBalDebitTotal += res.data.entries[i].amount;
          } else {
            trialData.creditAmount = res.data.entries[i].amount;
            this.AdjBalCreditTotal += res.data.entries[i].amount;
          }
          this.adjBalData.push(trialData);
        }
        this.AdjustedTrialDataSource = new MatTableDataSource(this.adjBalData);
      }
    })
  }

  getAllAdjustedEntries() {
    this.backendService.getAdjustedEntriesService().subscribe(res => {
      if (res.success) {
        this.adjEntryData = [];
        this.AdjEntryDebitTotal = 0;
        this.AdjEntryCreditTotal = 0;
        for (let i = 0; i < res.data.length; i++) {
          let trialData = {
            id: res.data[i].id,
            date: res.data[i].date,
            accountHead: res.data[i].accountHead,
            debitAmount: null,
            creditAmount: null,
          }
          if (res.data.accountType == "Debit") {
            trialData.debitAmount = res.data[i].amount;
            this.AdjEntryDebitTotal += res.data[i].amount;
          } else {
            trialData.creditAmount = res.data[i].amount;
            this.AdjEntryCreditTotal += res.data[i].amount;
          }
          this.adjEntryData.push(trialData);
        }
        this.adjustedDataSource = new MatTableDataSource(this.adjEntryData);
      }
    })
  }

  getFilteredAdjustedEntries() {
    if (this.adjustedEntrySearchForm.invalid) {
      return;
    }
    let data = {
      head: this.adjustedEntrySearchForm.value.accountName,
      start: this.datePipe.transform(this.adjustedEntrySearchForm.value.adjEntStart, environment.SystemTypes_Date_Format),
      end: this.datePipe.transform(this.adjustedEntrySearchForm.value.adjEntEnd, environment.SystemTypes_Date_Format),
    }
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
    this.backendService.getFilterAdjustedEntriesService(queryString).subscribe(res => {
      if (res.success) {
        if (res.data.length > 0) {
          this.adjEntryData = [];
          this.AdjEntryDebitTotal = 0;
          this.AdjEntryCreditTotal = 0;
          for (let i = 0; i < res.data.length; i++) {
            let trialData = {
              id: res.data[i].id,
              date: res.data[i].date,
              accountHead: res.data[i].accountHead,
              debitAmount: null,
              creditAmount: null,
            }
            if (res.data.accountType == "Debit") {
              trialData.debitAmount = res.data[i].amount;
              this.AdjEntryDebitTotal += res.data[i].amount;
            } else {
              trialData.creditAmount = res.data[i].amount;
              this.AdjEntryCreditTotal += res.data[i].amount;
            }
            this.adjEntryData.push(trialData);
          }
          this.adjustedDataSource = new MatTableDataSource(this.adjEntryData);
        }else{
          this.errorMsgService.getInfoMessage(res.message)
          this.openInfoDialog();
        }
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();

      }
      //console.log("ADJUSTMENT ENTRY ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteAdjustedEntry(data, id){
    this.backendService.deleteAdjustmentEntryService(data.id).subscribe(res => {
			if (res.success) {
				const index = this.adjustedDataSource.data.indexOf(id);
				this.adjustedDataSource.data.splice(index, 1);
        this.adjustedDataSource._updateChangeSubscription(); // <-- Refresh the datasource
        if (data.debitAmount != null) {
          this.AdjEntryDebitTotal = this.AdjEntryDebitTotal - data.debitAmount;
        }
        if (data.creditAmount != null) {
          this.AdjEntryCreditTotal = this.AdjEntryCreditTotal - data.creditAmount;
        }
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
		},
			(err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
					this.openInfoDialog();
				}
			});
  }

  /****************** clear search form **************************/

  clearSearchForm() {
    this.searchForm.reset();
  }

  clearAdjustedSearchForm() {
    this.adjustedSearchForm.reset();
  }

  clearAdjEntSearchForm() {
    this.adjustedEntrySearchForm.reset();
  }
  /******************* add adjusted entry form *****************/
  addAdjustedEntry() {
    if (this.adjustedEntryForm.invalid) {
      return;
    }
    let data = {
      date: this.datePipe.transform(this.adjustedEntryForm.value.date, environment.SystemTypes_Date_Format),
      accountHead: this.adjustedEntryForm.value.expenseHead,
      accountType: this.adjustedEntryForm.value.expenseType,
      amount: this.adjustedEntryForm.value.expenseAmount,
    }

    this.backendService.addAdjustmentEntryService(data).subscribe(res => {
      if (res.success) {
        let trialData = {
          id: res.data.id,
          date: res.data.date,
          accountHead: res.data.accountHead,
          debitAmount: null,
          creditAmount: null,
        }
        if (res.data.accountType == "Debit") {
          trialData.debitAmount = res.data.amount;
          this.AdjEntryDebitTotal += res.data.amount;
        } else {
          trialData.creditAmount = res.data.amount;
          this.AdjEntryCreditTotal += res.data.amount;
        }
        this.adjEntryData.push(trialData);
        this.adjustedDataSource = new MatTableDataSource(this.adjEntryData);
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        // this.adjustedEntryForm.reset();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();

      }
      //console.log("ADJUSTMENT ENTRY ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });

  }

  /******************* DropDowns Functions *****************/

  getIncomeTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.incomeDropDown = res.data;
        this.getExpenseTypeDropdown(this.expenseTypeCode)
      }
    })
  }

  getExpenseTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.expenseDropDown = res.data
        this.expenseHeadDropDown = [
          {
            name: 'Income Type',
            values: this.incomeDropDown
          },
          {
            name: 'Expense Type',
            values: this.expenseDropDown
          }
        ]
      }
    })
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
