import { Component, OnInit } from "@angular/core";

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  // { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  { path: "/superAdmin", title: "Super Admin", icon: "dashboard", class: "" },
  { path: "/job", title: "Job", icon: "timeline", class: "" },
  { path: "/trips", title: "Trips", icon: "compare_arrows", class: "" },
  {
    path: "/reimbursement",
    title: "Send Reimbursement",
    icon: "rotate_90_degrees_ccw",
    class: "",
  },
  {
    path: "/reimbursement/approve",
    title: "Approve Reimbursement",
    icon: "rotate_90_degrees_ccw",
    class: "",
  },
  { path: "/accounts", title: "Accounts", icon: "functions", class: "" },
  { path: "/invoice", title: "Invoice", icon: "receipt", class: "" },
  { path: "/freightRates", title: "Rates", icon: "attach_money", class: "" },
  { path: "/companies", title: "Companies", icon: "house", class: "" },
  { path: "/employees", title: "Employees", icon: "person", class: "" },
  { path: "/tractors", title: "Tractors", icon: "local_shipping", class: "" },
  { path: "/tracker", title: "Tracker", icon: "explore", class: "" },
  { path: "/trailer", title: "Trailer", icon: "insert_link", class: "" },
  { path: "/container", title: "Container", icon: "note", class: "" },
  {
    path: "/assetsCombination",
    title: "Assets Combination",
    icon: "grid_on",
    class: "",
  },
  {
    path: "/companyContract",
    title: "Company Contract",
    icon: "assignment",
    class: "",
  },

  { path: "/addOptions", title: "Add Options", icon: "settings", class: "" },
  { path: "/routes", title: "Routes", icon: "settings", class: "" },
  {
    path: "/users&roles",
    title: "Users and Roles",
    icon: "settings",
    class: "",
  },

  { path: "/items", title: "Items", icon: "maintenance", class: "" },
  { path: "/inventories", title: "Inventory", icon: "note", class: "" },
  {
    path: "/inventoryAdj",
    title: "Inventory Adjustement",
    icon: "maintenance",
    class: "",
  },
  { path: "/vendors", title: "Vendors", icon: "maintenance", class: "" },
  {
    path: "/purchaseOrder",
    title: "Purchase Order",
    icon: "maintenance",
    class: "",
  },
  { path: "/workOrder", title: "Work Order", icon: "maintenance", class: "" },

  // supplier fuel rate added in super admin tab
  // { path: '/fuelRate', title: 'Supplier Feul Rate', icon: 'attach_money', class: '' },
  // { path: '/user-profile', title: 'User Profile', icon: 'person', class: '' },
  // { path: '/table-list', title: 'Table List', icon: 'content_paste', class: '' },
  // { path: '/typography', title: 'Typography', icon: 'library_books', class: '' },
  // { path: '/icons', title: 'Icons', icon: 'bubble_chart', class: '' },
  // { path: '/maps', title: 'Maps', icon: 'location_on', class: '' },
  // { path: '/notifications', title: 'Notifications', icon: 'notifications', class: '' },
];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"],
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  settingsOpen = false;
  maintenanceOpen = false;
  assetsOpen = false;
  reimbursementOpen = false;

  constructor() {}

  ngOnInit() {
    this.menuItems = ROUTES.filter((menuItem) => menuItem);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }
}
