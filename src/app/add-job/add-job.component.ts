import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.scss']
})
export class AddJobComponent implements OnInit {

  /* ========================= DROPDOWN CODES VARIABLE  ==================================== */
  jobStatusTypeDropdown = environment.JOB_STATUS_TYPE;
  vehicleOwnerType = environment.VEHICLE_OWNERSHIP_TYPE;
  equipmentTypeDropdown = environment.TRAILER_TYPE;
  equipmentSizeDropdown = environment.TRAILER_SIZE_TYPE;
  baseStationDropdown = environment.BASE_STATION_TYPE;
  tankDropdown = environment.FUEL_TANK_TYPE;

  /* ================================== VARIABLES ============================================= */
  jobForm: FormGroup;
  job: any;
  // displayAsset: any;
  // showRentalField: boolean = false;
  showJobOpenField: boolean = true;
  // assetDropDown: any;
  jobStatusDropDown: any;
  fuelTankDropDown: any
  // equipmentDropDown: any;
  // equipmentSizeDropDown: any;
  supplierDropDown: any;
  fuelCardDropDown: any;
  brokerDropDown: any;
  baseStationDropDown: any;
  driverDropDown: any;
  tractorDropDown: any;
  trailerDropDown: any;
  containerDropDown: any;
  // vehicleOwnerDropDown: any;
  fuelDetails: FormArray;
  addFuelDiv = false;
  editForm = false;

  constructor(public backendService: BackEndService,
    private formBuilder: FormBuilder,
    private router: Router,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {

    this.jobForm = this.formBuilder.group({
      jobID: [''],
      jobStat: ['', Validators.required],
      advance: [''],
      jobStartDate: ['', Validators.required],
      jobCompDate: [''],
      baseStart: [null, Validators.required],
      baseEnd: [null],
      totalKm: [null],
      startKm: [null],
      endKm: [null],
      trackerKm: [null],
      tmsKm: [null],
      odometerKm: [null],
      tractor: [null],
      trailer: [null],
      container: [null],
      driver1: [null],
      driver2: [null],
      rental: [null],
      fuelId: [null],
      fuelSupplier: [null],
      fuelTank: [null],
      fuelSlip: [null],
      slipDate: [null],
      readingKm: [null],
      fuelCard: [null],
      fuelAmount: [null],
      fuelLitre: [null],
      fuelRate: [null],
      fuelTractor: [null],
      // fuelDriver1: [null],
      // fuelDriver2: [null],
      fuelTrailer: [null],
      // fuelContainer: [null],
      fuelList: this.formBuilder.array([]),
      roadWayBill: this.formBuilder.array([])
    });

  }

  /* ========================= ONINT COMPONENT FUNCTION  ==================================== */
  ngOnInit() {

    // this.getAllCombinations();
    this.getJobStatusDropdown(this.jobStatusTypeDropdown);
    this.getFuelCardDropdown();
    // this.getVehicleOwnerDropdown(this.vehicleOwnerType);
    // this.getEquipmentTypeDropdown(this.equipmentTypeDropdown);
    // this.getEquipmentSizeDropdown(this.equipmentSizeDropdown);
    this.getBaseStationDropdown(this.baseStationDropdown);
    // this.getBrokerDropdown();
    this.getFuelSupplierDropdown();
    // this.getAllFreeDrivers();
    // this.getAllFreeTractors();
    // this.getAllFreeTrailers();
    // this.getAllFreeContainers();
    this.getFuelTankDropdown(this.tankDropdown);

    let getJob = JSON.parse(sessionStorage.getItem("getJob"));
    sessionStorage.removeItem("getJob")
    if (getJob != null) {
      this.job = getJob;
      this.jobForm = this.formBuilder.group({
        jobID: [this.job.jobId],
        jobStat: [this.job.jobStatus, Validators.required],
        advance: [this.job.jobAdvance],
        jobStartDate: [this.job.startDate, Validators.required],
        jobCompDate: [this.job.endDate],
        baseStart: [this.job.startBaseStation, Validators.required],
        baseEnd: [this.job.endBaseStation],
        totalKm: [this.job.totalKm],
        startKm: [this.job.startKm],
        endKm: [this.job.endKm],
        trackerKm: [this.job.trackerKm],
        odometerKm: [this.job.odometerKm],
        tmsKm: [this.job.tmsKm],
        tractor: [this.job.tractor],
        trailer: [this.job.trailer],
        container: [this.job.container],
        driver1: [this.job.driver1],
        driver2: [this.job.driver2],
        rental: [this.job.rentalVehicle],
        fuelId: [null],
        fuelSupplier: [null],
        fuelTank: [null],
        fuelSlip: [null],
        slipDate: [null],
        readingKm: [null],
        fuelCard: [null],
        fuelAmount: [null],
        fuelLitre: [null],
        fuelRate: [null],
        fuelTractor: [null],
        // fuelDriver1: [null],
        // fuelDriver2: [null],
        fuelTrailer: [null],
        // fuelContainer: [null],
        fuelList: this.formBuilder.array([]),
        roadWayBill: this.formBuilder.array([this.job.roadwayBills])
      });

      this.getAllFreeTractors();
      this.getAllFreeTrailers();

      if (this.job.jobStatus.value == environment.SystemTypes_CLOSE) {
        this.showJobOpenField = false;
      }

      if (this.job.jobFuelDetails != null) {
        for (let i = 0; i < this.job.jobFuelDetails.length; i++) {
          this.fuelDetails = this.jobForm.get('fuelList') as FormArray;
          this.fuelDetails.push(this.formBuilder.group({
            fuelIdArrayItem: [this.job.jobFuelDetails[i].jobFuelDetailId],
            fuelSupplierArrayItem: [this.job.jobFuelDetails[i].supplier],
            fuelTractorArrayItem: [this.job.jobFuelDetails[i].tractor],
            fuelTrailerArrayItem: [this.job.jobFuelDetails[i].trailer],
            // fuelContainerArrayItem: [this.job.jobFuelDetails[i].container],
            // fuelDriver1ArrayItem: [this.job.jobFuelDetails[i].driver1],
            // fuelDriver2ArrayItem: [this.job.jobFuelDetails[i].driver2],
            slipDateArrayItem: [this.job.jobFuelDetails[i].fuelSlipDate],
            fuelSlipArrayItem: [this.job.jobFuelDetails[i].fuelSlipNumber],
            fuelCardArrayItem: [this.job.jobFuelDetails[i].fuelCard],
            readingKmArrayItem: [this.job.jobFuelDetails[i].currentKm],
            fuelLitreArrayItem: [this.job.jobFuelDetails[i].fuelInLitre],
            fuelRateArrayItem: [this.job.jobFuelDetails[i].ratePerLitre],
            fuelAmountArrayItem: [this.job.jobFuelDetails[i].totalAmount],
            fuelTankArrayItem: [this.job.jobFuelDetails[i].fuelTankType],
          }));
        }
      }
    }
  }
  // ============== MAKE FUEL EXPANSION PANEL EDITABLE START  =================

  makeEditable() {
    this.editForm = !this.editForm
  }

  // ============== MAKE FUEL EXPANSION PANEL EDITABLE END   =================

  /* ========================= DISPLAY RENTAL JOB OPEN FIELDS AND COMBINATION TRUE ========================= */
  // displayRentalFields(e) {
  //   if (e != undefined) {
  //     if (e.value == environment.SystemTypes_RENTAL) {
  //       this.showRentalField = true;
  //     } else {
  //       this.showRentalField = false;
  //     }
  //   }
  // }

  displayOpenJobFields(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_CLOSE) {
        this.showJobOpenField = false;
      } else {
        this.showJobOpenField = true;
      }
    }
  }

  // checkedCombination(e) {
  //   if (e !== "" || e != null) {
  //     this.jobForm.get('tractor').setValue(true);
  //     this.jobForm.get('trailer').setValue(true);
  //     this.jobForm.get('container').setValue(true);
  //     this.jobForm.get('driver1').setValue(true);
  //     this.jobForm.get('driver2').setValue(true);
  //     this.displayAsset = e;
  //   }
  // }

  /* ================================= ADD UPDATE DELETE JOB FUNCTIONS  ============================================ */
  submitJob() {
    if (this.jobForm.invalid) {
      return;
    }
    this.job = {
      jobId: this.jobForm.value.jobID,
      jobStatus: this.jobForm.value.jobStat,
      jobAdvance: this.jobForm.value.advance,
      totalKm: this.jobForm.value.totalKm,
      startKm: this.jobForm.value.startKm,
      endKm: this.jobForm.value.endKm,
      tmsKm: this.jobForm.value.tmsKm,
      trackerKm: this.jobForm.value.trackerKm,
      startDate: this.datePipe.transform(this.jobForm.value.jobStartDate, environment.SystemTypes_Date_Format),
      endDate: this.datePipe.transform(this.jobForm.value.jobCompDate, environment.SystemTypes_Date_Format),
      odometerKm: this.jobForm.value.odometerKm,
      startBaseStation: this.jobForm.value.baseStart,
      endBaseStation: this.jobForm.value.baseEnd,
      jobFuelDetails: this.jobForm.value.fuelList,
      roadwayBills: this.jobForm.value.roadWayBill
    }

    // if (this.jobForm.value.tractor == true) {
    //   this.job.tractor = this.jobForm.value.assets.tractor;
    // }
    // if (this.jobForm.value.trailer == true) {
    //   this.job.trailer = this.jobForm.value.assets.trailer;
    // }
    // if (this.jobForm.value.tracker == true) {
    //   this.job.tracker = this.jobForm.value.assets.tracker;
    // }
    // if (this.jobForm.value.container == true) {
    //   this.job.container = this.jobForm.value.assets.container;
    // }
    // if (this.jobForm.value.driver1 == true) {
    //   this.job.driver1 = this.jobForm.value.assets.driver1;
    // }
    // if (this.jobForm.value.driver2 == true) {
    //   this.job.driver2 = this.jobForm.value.assets.driver2;
    // }
    // if (this.jobForm.value.vehicleOwn.value == environment.SystemTypes_RENTAL) {
    //   this.job.rentalVehicle = {
    //     rentalVehicleId: this.jobForm.value.rentalId,
    //     broker: this.jobForm.value.broker,
    //     equipmentType: this.jobForm.value.equipType,
    //     equipmentSize: this.jobForm.value.equipSize,
    //     vehicleNumber: this.jobForm.value.licNumber
    //   }
    // }
    //console.log("JOB DATA SUBMIT", this.job)
    if (this.jobForm.value.jobID != null && this.jobForm.value.jobID != '') {
      this.updateJob();
    } else {
      this.addJob();
    }

  }

  addJob() {
    this.backendService.addJobService(this.job).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.jobForm.get("jobID").setValue(res.data)
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("JOB ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  updateJob() {
    this.backendService.updateJobService(this.job).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("JOB UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteJob() {
    if (this.jobForm.value.jobID != null || this.jobForm.value.jobID != "") {
      this.backendService.deleteJobService(this.job.jobId).subscribe(res => {
        if (res.success) {
          this.router.navigate(['job']);
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("JOB DELETE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    }
  }

  /* ============================== DROPDOWNS FUNCTIONS  ========================================== */
  // getAllCombinations() {
  //   this.backendService.getAllFilterAssetCombination('javail').subscribe(res => {
  //     if (res.success) {
  //       this.assetDropDown = res.data;
  //       if (this.jobForm.value.jobID != "" && this.jobForm.value.vehicleOwn.value == environment.SystemTypes_OWNED) {
  //         if (this.job.tractor != null) {
  //           // this.jobForm.value.tractor == true;
  //           this.jobForm.get("tractor").setValue(true)
  //         }
  //         if (this.job.trailer != null) {
  //           // this.jobForm.value.trailer == true;
  //           this.jobForm.get("trailer").setValue(true)
  //         }
  //         if (this.job.container != null) {
  //           // this.jobForm.value.container == true;
  //           this.jobForm.get("container").setValue(true)
  //         }
  //         if (this.job.driver1 != null) {
  //           // this.jobForm.value.driver1 == true;
  //           this.jobForm.get("driver1").setValue(true)
  //         }
  //         if (this.job.driver2 != null) {
  //           // this.jobForm.value.driver2 == true;
  //           this.jobForm.get("driver2").setValue(true)
  //         }
  //         this.displayAsset = {
  //           tractor: this.job.tractor,
  //           trailer: this.job.trailer,
  //           container: this.job.container,
  //           driver1: this.job.driver1,
  //           driver2: this.job.driver2
  //         };
  //       }
  //     }
  //   })
  // }

  getJobStatusDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.jobStatusDropDown = res.data;
        if (this.jobForm.value.jobID == '') {
          for (let i = 0; i < this.jobStatusDropDown.length; i++) {
            if (this.jobStatusDropDown[i].value == environment.SystemTypes_OPEN) {
              this.jobForm.get('jobStat').setValue(this.jobStatusDropDown[i]);
            }
          }
        }
      }
    })
  }

  getFuelTankDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.fuelTankDropDown = res.data;
      }
    })
  }

  // getAllFreeDrivers() {
  //   this.backendService.getAllDrivers().subscribe(res => {
  //     if (res.success) {
  //       this.driverDropDown = res.data;
  //     }
  //   })
  // }

  getAllFreeTractors() {
    this.backendService.getAllJobTractor(this.job.jobId).subscribe(res => {
      if (res.success) {
        this.tractorDropDown = res.data;
      }
    })
  }

  getAllFreeTrailers() {
    this.backendService.getAllJobTrailer(this.job.jobId).subscribe(res => {
      if (res.success) {
        this.trailerDropDown = res.data;
      }
    })
  }

  // getAllFreeContainers() {
  //   this.backendService.getAllFilterContainer().subscribe(res => {
  //     if (res.success) {
  //       this.containerDropDown = res.data;
  //     }
  //   })
  // }

  // getVehicleOwnerDropdown(type: string) {
  //   this.backendService.getDropdowns(type).subscribe(res => {
  //     if (res.success) {
  //       this.vehicleOwnerDropDown = res.data;
  //     }
  //   })
  // }

  // getEquipmentTypeDropdown(type: string) {
  //   this.backendService.getDropdowns(type).subscribe(res => {
  //     if (res.success) {
  //       this.equipmentDropDown = res.data;
  //     }
  //   })
  // }

  // getEquipmentSizeDropdown(type: string) {
  //   this.backendService.getDropdowns(type).subscribe(res => {
  //     if (res.success) {
  //       this.equipmentSizeDropDown = res.data;
  //     }
  //   })
  // }

  getBaseStationDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      if (res.success) {
        this.baseStationDropDown = res.data;
      }
    })
  }

  // getBrokerDropdown() {
  //   this.backendService.getAllFilterBroker().subscribe(res => {
  //     if (res.success) {
  //       this.brokerDropDown = res.data;
  //       if (this.jobForm.value.jobID != "") {
  //         for (let i = 0; i < this.brokerDropDown.length; i++) {
  //           if (this.job.vehicleOwnership.value == environment.SystemTypes_RENTAL) {
  //             if (this.brokerDropDown[i].companyId == this.job.rentalVehicle.broker.companyId) {
  //               this.jobForm.get('broker').setValue(this.brokerDropDown[i])
  //             }
  //           }
  //         }
  //       }
  //     }
  //   })
  // }

  getFuelSupplierDropdown() {
    this.backendService.getAllFilterFuelSupplier().subscribe(res => {
      if (res.success) {
        this.supplierDropDown = res.data;
      }
    })
  }

  getFuelCardDropdown() {
    this.backendService.getAllFilterFuelCard().subscribe(res => {
      if (res.success) {
        this.fuelCardDropDown = res.data;
      }
    })
  }

  /* ========================= GET ROADWAY BILL BY JOB ID FUNCTION ========================= */
  findRoadwayBillByID(data) {
    this.backendService.getRoadwayBillById(data.rwbId).subscribe(res => {
      if (res.success) {
        sessionStorage.setItem("getRoadwayBill", JSON.stringify(res.data));
        this.router.navigate(['job/editRWB']);
      }
    })
  }

  setJobId() {
    sessionStorage.setItem("jobId", this.jobForm.value.jobID);
  }

  /* ============================== COMPARE DROPDOWN FUNCTIONS ======================================== */
  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

  compareTractor(o1, o2): boolean {
    if (o2 != null)
      return o1.trailerId === o2.trailerId;
  }

  compareTrailer(o1, o2): boolean {
    if (o2 != null)
      return o1.trailerId === o2.trailerId;
  }

  compareContainer(o1, o2): boolean {
    if (o2 != null)
      return o1.containerId === o2.containerId;
  }

  compareDrivers(o1, o2): boolean {
    if (o2 != null)
      return o1.employeeId === o2.employeeId;
  }

  compareSupplier(o1, o2): boolean {
    if (o2 != null)
      return o1.companyId === o2.companyId;
  }

  compareFuelCard(o1, o2): boolean {
    if (o2 != null)
      return o1.fuelCardId === o2.fuelCardId;
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  /* ========================= ADD REMOVE FUEL DETAILS IN JOB FUNTIONS ========================= */

  addFuel(state) {
    this.addFuelDiv = state;
  }

  submitJobFuel() {
    let FuelData = {
      jobFuelDetailId: this.jobForm.value.fuelId,
      supplier: this.jobForm.value.fuelSupplier,
      tractor: this.jobForm.value.fuelTractor,
      trailer: this.jobForm.value.fuelTrailer,
      // container: this.jobForm.value.fuelContainer,
      // driver1: this.jobForm.value.fuelDriver1,
      // driver2: this.jobForm.value.fuelDriver2,
      fuelSlipDate: this.datePipe.transform(this.jobForm.value.slipDate, environment.SystemTypes_Date_Format),
      fuelSlipNumber: this.jobForm.value.fuelSlip,
      fuelCard: this.jobForm.value.fuelCard,
      currentKm: this.jobForm.value.readingKm,
      fuelInLitre: this.jobForm.value.fuelLitre,
      ratePerLitre: this.jobForm.value.fuelRate,
      totalAmount: this.jobForm.value.fuelAmount,
      fuelTankType: this.jobForm.value.fuelTank,
      jobId: this.jobForm.value.jobID,
    }

    this.backendService.addJobFuelService(FuelData).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.addFuel(false);
        this.resetFuelFields();
        this.addJobFuelInArray(res.data);
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("JOB ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  addJobFuelInArray(data) {
    this.fuelDetails = this.jobForm.get('fuelList') as FormArray;
    this.fuelDetails.push(this.formBuilder.group({
      fuelIdArrayItem: [data.jobFuelDetailId],
      fuelSupplierArrayItem: [data.supplier],
      fuelTractorArrayItem: [data.tractor],
      fuelTrailerArrayItem: [data.trailer],
      // fuelContainerArrayItem: [data.container],
      // fuelDriver1ArrayItem: [data.driver1],
      // fuelDriver2ArrayItem: [data.driver2],
      slipDateArrayItem: [data.fuelSlipDate],
      fuelSlipArrayItem: [data.fuelSlipNumber],
      fuelCardArrayItem: [data.fuelCard],
      readingKmArrayItem: [data.currentKm],
      fuelLitreArrayItem: [data.fuelInLitre],
      fuelRateArrayItem: [data.ratePerLitre],
      fuelAmountArrayItem: [data.totalAmount],
      fuelTankArrayItem: [data.fuelTankType],
    }));
  }

  updateFuelDetail(data) {
    let fuelData = {
      jobFuelDetailId: data.fuelIdArrayItem,
      supplier: data.fuelSupplierArrayItem,
      tractor: data.fuelTractorArrayItem,
      trailer: data.fuelTrailerArrayItem,
      // container: data.fuelContainerArrayItem,
      // driver1: data.fuelDriver1ArrayItem,
      // driver2: data.fuelDriver2ArrayItem,
      fuelSlipDate: this.datePipe.transform(data.slipDateArrayItem, environment.SystemTypes_Date_Format),
      fuelSlipNumber: data.fuelSlipArrayItem,
      fuelCard: data.fuelCardArrayItem,
      currentKm: data.readingKmArrayItem,
      fuelInLitre: data.fuelLitreArrayItem,
      ratePerLitre: data.fuelRateArrayItem,
      totalAmount: data.fuelAmountArrayItem,
      fuelTankType: data.fuelTankArrayItem,
      jobId: this.jobForm.value.jobID,
    }
    this.backendService.updateJobFuelService(fuelData).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.makeEditable();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("FUEL UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteFuel(id, index) {
    this.backendService.deleteJobFuelService(id).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.fuelDetails = this.jobForm.get('fuelList') as FormArray;
        this.fuelDetails.removeAt(index)
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("FUEL DELETE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  resetFuelFields(){
    this.jobForm.get('fuelSupplier').setValue(null);
    this.jobForm.get('fuelTank').setValue(null);
    this.jobForm.get('fuelSlip').setValue(null);
    this.jobForm.get('readingKm').setValue(null);
    this.jobForm.get('fuelAmount').setValue(null);
    this.jobForm.get('fuelLitre').setValue(null);
    this.jobForm.get('fuelRate').setValue(null);
    this.jobForm.get('fuelTractor').setValue(null);
    this.jobForm.get('fuelTrailer').setValue(null);
    this.jobForm.get('slipDate').setValue(null);
  }

}
