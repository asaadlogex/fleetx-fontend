import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { DatePipe } from '@angular/common';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { MatDialog, PageEvent, MatPaginator } from '@angular/material';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss']
})
export class JobComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  
  jobData: any;
  jobDataResult: any;
  roadwayBills: any;
  checkOwned = environment.SystemTypes_OWNED;
  checkRental = environment.SystemTypes_RENTAL;
  form: FormGroup;
  pageEvent: PageEvent;
  page = 1;
  pageSize = 10;
  length: number;
  pageSizeOptions = [10, 20, 50];

  dataSource: MatTableDataSource<any>;

  applyFilter(filterValue: string) {
    let filter = filterValue.trim().toLowerCase();
    if (filter.length > 0) {
      let result = this.jobData.filter(x => {
        return JSON.stringify(x).toLowerCase().includes(filter)
      })
      this.jobData = result;
    } else {
      this.jobData = this.jobDataResult;
    }
  }



  constructor(public backendService: BackEndService,
    public datePipe: DatePipe,
    public errorMsgService: ErrorMessageService,
    public dialog: MatDialog,
    private router: Router) {
    this.form = new FormGroup({
      jobId: new FormControl(null),
      rwbId: new FormControl(null),
      lic: new FormControl(null),
      start: new FormControl(null),
      end: new FormControl(null),
      jobStatus: new FormControl(null),
      ownerShipStatus: new FormControl(null)
    })
  }

  ngOnInit() {
    this.getAllJobs(this.page, this.pageSize);
  }

  getAllJobs(pg, pgSz) {
    let params = {
      page: pg,
      size: pgSz
    }
    this.backendService.getAllJobService(params).subscribe(res => {
      if (res.success) {
        this.jobData = res.data;
        this.jobDataResult = res.data;
        this.length = res.totalRecords
      }
    })
  }

  findJobByID(data) {
    this.backendService.getJobById(data.jobId).subscribe(res => {
      if (res.success) {
        sessionStorage.setItem("getJob", JSON.stringify(res.data));
        this.router.navigate(['job/edit']);
      }
    })
  }

  getServerData(val) {
    this.getAllJobs(val.pageIndex + 1, val.pageSize)
  }

  setJobId(job) {
    sessionStorage.setItem("jobId", job.jobId)
    this.backendService.jobData = job;
  }

  findRoadwayBillByID(data) {
    this.backendService.getRoadwayBillById(data.rwbId).subscribe(res => {
      if (res.success) {
        sessionStorage.setItem("getRoadwayBill", JSON.stringify(res.data));
        this.router.navigate(['job/editRWB']);
      }
    })
  }

  search() {
    let data = {
      jobId: this.form.value.jobId,
      rwbId: this.form.value.rwbId,
      lic: this.form.value.lic,
      start: this.datePipe.transform(this.form.value.start, environment.SystemTypes_Date_Format),
      end: this.datePipe.transform(this.form.value.end, environment.SystemTypes_Date_Format),
      status: this.form.value.jobStatus,
      rental: this.form.value.ownerShipStatus
    }
    
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');

    this.backendService.jobSearchService(queryString).subscribe(res => {
      if (res.success) {
        // console.log("JOB SEARCH RESPONSE", res)
        if (res.data.length > 0) {
          this.jobData = res.data;
        } else {
          this.jobData = res.data;
          this.errorMsgService.getInfoMessage(res.message)
          this.openInfoDialog();
        }
      }
    })
  }

  ClearForm() {
    this.form.reset();
    this.getAllJobs(this.page, this.pageSize);
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
