import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
	selector: "app-add-role",
	templateUrl: "./add-role.component.html",
	styleUrls: ["./add-role.component.scss"],
})
export class AddRoleComponent implements OnInit {
	addRoleForm: FormGroup;
	constructor() {
		this.addRoleForm = new FormGroup({
			roleName: new FormControl(null),
			desc: new FormControl(null),
		});
	}

	ngOnInit() {}

	add() {
		console.log(this.addRoleForm.value);
	}
}
