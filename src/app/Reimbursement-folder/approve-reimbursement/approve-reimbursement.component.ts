import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { environment } from "../../../environments/environment.prod";
import { BackEndService } from "app/services/back-end.service";
import {
  MatTableDataSource,
  MatPaginator,
  MatTabChangeEvent,
  MatDialog,
} from "@angular/material";
import { ErrorMessageService } from "app/services/error-message.service";
import { HttpErrorResponse } from "@angular/common/http";
import { PrintPreviewModalComponent } from "app/modals/print-preview-modal/print-preview-modal.component";
import { InfoModalComponent } from "app/modals/info-modal/info-modal.component";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";
import { ReimburseAmountModalComponent } from "app/modals/reimburse-amount-modal/reimburse-amount-modal.component";

@Component({
  selector: "app-approve-reimbursement",
  templateUrl: "./approve-reimbursement.component.html",
  styleUrls: ["./approve-reimbursement.component.scss"],
})
export class ApproveReimbursementComponent implements OnInit {
 
  // dataSource = new MatTableDataSource<ReimbursementElement>(REIMBURSEMENT_DATA);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  showFooterReimburse = false;
  searchForm: FormGroup;
  jobData: any;
  closedJobsData: any;
  baseStationDropDown: any;
  reimbursementDropDown: any;
  sendJobs: any = [];
  sendApprove: any = [];
  totalBill = 0;
  jobApprovalData: any;

  baseStationCode = environment.BASE_STATION_TYPE;
  reimbursementCode = environment.REIMBURSEMENT_STATUS_TYPE;

  constructor(
    public dialog: MatDialog,
    public datePipe: DatePipe,
    public backendService: BackEndService,
    public errorMsgService: ErrorMessageService
  ) {
    this.searchForm = new FormGroup({
      jobId: new FormControl(null),
      baseStation: new FormControl(null),
      start: new FormControl(null),
      end: new FormControl(null),
      reimbStatus: new FormControl(null),
    });
  }

  ngOnInit() {
    // this.dataSource.paginator = this.paginator;
    this.getBaseStationDropDown(this.baseStationCode);
    this.getReimbursementDropDown(this.reimbursementCode);
    this.search();
  }

  search() {
    let data = {
      jobId: this.searchForm.value.jobId,
      base: this.searchForm.value.baseStation,
      start: this.datePipe.transform(this.searchForm.value.start, environment.SystemTypes_Date_Format),
      end: this.datePipe.transform(this.searchForm.value.end, environment.SystemTypes_Date_Format),
      rstatus: environment.SystemTypes_REIMBURSEMENT_SENT,
    };

    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data)
      .map((key) => key + "=" + data[key])
      .join("&");

    this.backendService.jobSearchService(queryString).subscribe((res) => {
      if (res.success) {
        // console.log("JOB SEARCH RESPONSE", res)
        if (res.data.length > 0) {
          this.jobApprovalData = res.data;
        } else {
          this.jobData = res.data;
          this.errorMsgService.getInfoMessage(res.message);
          this.openInfoDialog();
        }
      }
    });
  }

  // clear search form
  clearForm() {
    this.searchForm.reset();
    this.search();
  }

  /******************************** keep track of current tab index ******************************/
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    if (tabChangeEvent.index === 0) {
      this.showFooterReimburse = false;
    } else {
      this.showFooterReimburse = true;
    }
  }

  /****************************** To Send JOBS FOR APPROVED REIMBURSED Function****************************/

  selectAll(event){
		if(event){
			if(this.jobApprovalData.length > 0){
        this.sendApprove = [];
        this.totalBill = 0;
				this.jobApprovalData.forEach(element => {
          element.selected = true;
          this.totalBill += element.reimbursementExpense
					this.sendApprove.push(element.jobId);
				});
			}
		}else{
      this.sendApprove = [];
      this.totalBill = 0;
			this.jobApprovalData.forEach(element => {
				element.selected = false;
			});
		}
	}

  addJobForApproval(check, data) {
    if (check) {
      this.sendApprove.push(data.jobId);
      this.totalBill += data.reimbursementExpense;
    } else {
      if (this.sendApprove.length > 0) {
        let removeJob = this.sendApprove.filter(
          (res) => res.jobId === data.jobId
        );
        this.totalBill -= data.reimbursementExpense;
        this.sendApprove.pop(removeJob.jobId);
      }
    }
  }

  sendJobForApproval() {
    if (this.sendApprove.length > 0) {
      this.backendService.reimbursedIds = this.sendApprove;
      this.backendService.reimbursedData = this.totalBill;
      this.openReimburseAmountDialog();
    } else {
      this.errorMsgService.getInfoMessage(
        "Please Select any Job For Approval."
      );
      this.openInfoDialog();
    }
  }

  showReimbursmentStatus(listData) {
    for (let i = 0; i < this.jobApprovalData.length; i++) {
      for (let j = 0; j < listData.length; j++) {
        if (this.jobApprovalData[i].jobId == listData[j]) {
          for (let k = 0; k < this.reimbursementDropDown.length; k++) {
            if (
              this.reimbursementDropDown[k].value ==
              environment.SystemTypes_REIMBURSEMENT_REIMBURSED
            ) {
              this.jobApprovalData[
                i
              ].reimbursementStatus = this.reimbursementDropDown[k];
            }
          }
        }
      }
    }
  }

  /****************************** for printing the reimbursement document ****************************/
  print(doc) {
    var objFra = document.createElement("iframe"); // Create an IFrame.
    objFra.setAttribute("class", "print-preview");
    objFra.src = doc; // Set source.
    document.getElementById("print-preview").appendChild(objFra); // Add the frame to the web page.
    objFra.contentWindow.focus(); // Set focus.
    // objFra.contentWindow.print();      // Print it.
  }

  /************************* for opening print preview modal ***************************/
  getPreviewDocument() {
    if (this.sendApprove.length > 0) {
      this.backendService
        .getReimbursementDocService(this.sendApprove)
        .subscribe((res) => {
          const newBlob = new Blob([res.toString()], {
            type: "application/pdf, application/excel",
          });
          var downloadURL = window.URL.createObjectURL(res);
          this.openPrintPreviewModal(downloadURL);
        });
    } else {
      this.errorMsgService.getInfoMessage("Select any Job To Preview.");
      this.openInfoDialog();
    }
  }

  openPrintPreviewModal(url): void {
    const dialogRef = this.dialog.open(PrintPreviewModalComponent, {
      width: "56vw",
      height: "80vh",
    });

    this.print(url);

    dialogRef.afterClosed().subscribe((result) => {});
  }

  /************************ function to trigger info dialog ****************************/
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  // function to trigger success dialog
  openReimburseAmountDialog(): void {
    const dialogRef = this.dialog.open(ReimburseAmountModalComponent,{
      width: "600px",
      height: "300px"
    });

    dialogRef.afterClosed().subscribe((result) => {
      let chck = sessionStorage.getItem("amount");
      sessionStorage.removeItem("amount");
      if (chck == "paid") {
        let data = this.backendService.reimbursedIds;
        if (data.length > 0) this.showReimbursmentStatus(data);
      }
    });
  }

  /************************* Dropdown functions  **************************************/

  getBaseStationDropDown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      this.baseStationDropDown = res.data;
    });
  }

  getReimbursementDropDown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      this.reimbursementDropDown = res.data;
    });
  }
}

export interface ReimbursementElement {
  reimbId: string;
  jobStatus: string;
  reimbStatus: string;
  closeDate: string;
  expenses: number;
}

const REIMBURSEMENT_DATA: ReimbursementElement[] = [
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    reimbId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
];
