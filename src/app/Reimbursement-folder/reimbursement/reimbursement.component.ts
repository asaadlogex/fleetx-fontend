import { environment } from "../../../environments/environment.prod";
import { InfoModalComponent } from "../../modals/info-modal/info-modal.component";
import { ErrorMessageService } from "../../services/error-message.service";
import { BackEndService } from "app/services/back-end.service";
import { DatePipe } from "@angular/common";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { MatTabChangeEvent, MatDialog } from "@angular/material";
import { HttpErrorResponse } from "@angular/common/http";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";

@Component({
	selector: "app-reimbursement",
	templateUrl: "./reimbursement.component.html",
	styleUrls: ["./reimbursement.component.scss"],
})
export class ReimbursementComponent implements OnInit {
	showFooterReimburse = false;
	searchForm: FormGroup;
	jobData: any;
	closedJobsData: any;
	baseStationDropDown: any;
	reimbursementDropDown: any;
	sendJobs: any = [];
	sendApprove: any = [];
	totalBill = 0;
	jobApprovalData: any;

	baseStationCode = environment.BASE_STATION_TYPE;
	reimbursementCode = environment.REIMBURSEMENT_STATUS_TYPE;

	constructor(
		public dialog: MatDialog,
		public datePipe: DatePipe,
		public backendService: BackEndService,
		public errorMsgService: ErrorMessageService
	) {
		this.searchForm = new FormGroup({
			jobId: new FormControl(null),
			baseStation: new FormControl(null),
			start: new FormControl(null),
			end: new FormControl(null),
			reimbStatus: new FormControl(null),
		});
	}

	ngOnInit() {
		this.getBaseStationDropDown(this.baseStationCode);
		this.getReimbursementDropDown(this.reimbursementCode);
		this.search();
	}

	search() {
		let data = {
			jobId: this.searchForm.value.jobId,
			base: this.searchForm.value.baseStation,
			start: this.datePipe.transform(this.searchForm.value.start, environment.SystemTypes_Date_Format),
			end: this.datePipe.transform(this.searchForm.value.end, environment.SystemTypes_Date_Format),
			rstatus: environment.SystemTypes_REIMBURSEMENT_PENDING,
		};

		for (let prop in data) {
			if (!data[prop]) {
				delete data[prop];
			}
		}
		var queryString = Object.keys(data)
			.map((key) => key + "=" + data[key])
			.join("&");

		this.backendService.jobSearchService(queryString).subscribe((res) => {
			if (res.success) {
				// console.log("JOB SEARCH RESPONSE", res)
				if (res.data.length > 0) {
					this.jobData = res.data.filter(
						(data) => data.jobStatus.value === environment.SystemTypes_CLOSE
					);
				} else {
					this.jobData = res.data;
					this.errorMsgService.getInfoMessage(res.message);
					this.openInfoDialog();
				}
			}
		});
	}

	// clear search form
	clearForm() {
		this.searchForm.reset();
		this.search();
	}

	/******************************** keep track of current tab index ******************************/
	tabChanged(tabChangeEvent: MatTabChangeEvent): void {
		if (tabChangeEvent.index === 0) {
			this.showFooterReimburse = false;
		} else {
			this.showFooterReimburse = true;
		}
	}

	/****************************** To Send JOBS FOR APPROVED REIMBURSED Function****************************/

	selectAll(event){
		if(event){
			if(this.jobData.length > 0){
				this.sendJobs = [];
				this.totalBill = 0;
				this.jobData.forEach(element => {
					element.selected = true;
					this.totalBill += element.reimbursementExpense;
					this.sendJobs.push(element.jobId);
				});
			}
		}else{
			this.sendJobs = [];
			this.totalBill = 0;
			this.jobData.forEach(element => {
				element.selected = false;
			});
		}
	}

	addJobForReimbursement(check, data) {
		if (check) {
			if (
				data.reimbursementStatus.value ==
				environment.SystemTypes_REIMBURSEMENT_SENT
			) {
				this.errorMsgService.getInfoMessage(
					"This Job Is Already Sent For Approval.!"
				);
				this.openInfoDialog();
			} else {
				this.totalBill += data.reimbursementExpense;
				this.sendJobs.push(data.jobId);
			}
		} else {
			if (this.sendJobs.length > 0) {
				let removeJob = this.sendJobs.filter((res) => res.jobId === data.jobId);
				this.totalBill -= data.reimbursementExpense;
				this.sendJobs.pop(removeJob.jobId);
			}
		}
	}

	sendJobForReimbursement() {
		if (this.sendJobs.length > 0) {
			this.backendService.sendJobReimbursementService(this.sendJobs).subscribe(
				(res) => {
					if (res.success) {
						for (let i = 0; i < this.jobData.length; i++) {
							for (let j = 0; j < this.sendJobs.length; j++) {
								if (this.jobData[i].jobId == this.sendJobs[j]) {
									this.jobData.pop(this.jobData[i])
								}
							}
						}
						this.errorMsgService.getSuccessMessage(res.message);
						this.openSuccessDialog();
						this.sendJobs = [];
					} else {
						this.errorMsgService.getErrorMessage(res.errorMessage);
						this.openErrorDialog();
					}
					//console.log("SEND REIMBURSEMNT RESPONSE", res);
				},
				(err: HttpErrorResponse) => {
					if (err.status == 500) {
						this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
						this.openInfoDialog();
					}
				}
			);
		} else {
			this.errorMsgService.getInfoMessage(
				"Please Select any Job to Send For Reimbursement."
			);
			this.openInfoDialog();
		}
	}


	/************************ function to trigger info dialog ****************************/
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}


	/************************* Dropdown functions  **************************************/

	getBaseStationDropDown(type: string) {
		this.backendService.getDropdowns(type).subscribe((res) => {
			this.baseStationDropDown = res.data;
		});
	}

	getReimbursementDropDown(type: string) {
		this.backendService.getDropdowns(type).subscribe((res) => {
			this.reimbursementDropDown = res.data;
		});
	}
}
