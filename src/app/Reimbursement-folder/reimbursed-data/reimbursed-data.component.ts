import { Component, OnInit } from '@angular/core';
import {
  MatTableDataSource,
} from "@angular/material";
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reimbursed-data',
  templateUrl: './reimbursed-data.component.html',
  styleUrls: ['./reimbursed-data.component.scss']
})
export class ReimbursedDataComponent implements OnInit {

  dataSource : MatTableDataSource<any>;
  displayedColumns: string[] = [
    "reimbId",
    "jobStatus",
    "reimbStatus",
    "closeDate",
    "expenses",
    "view",
  ];

  constructor(public backendService: BackEndService,
    private router : Router) { }

  ngOnInit() {
    this.getAllReimbursement();
  }

  getAllReimbursement(){
    this.backendService.getAllReimbursementService().subscribe(res =>{
      if(res.success){
        this.dataSource = res.data;
      }
    })
  }

  getReimbursementById(data){
    this.backendService.getReimbursementByIdService(data.reimbursementInfoId).subscribe(res =>{
      if(res.success){
        this.backendService.reimbursedData = res.data;
        this.router.navigate(['/reimbursement/view']);
      }
    })
  }
}
