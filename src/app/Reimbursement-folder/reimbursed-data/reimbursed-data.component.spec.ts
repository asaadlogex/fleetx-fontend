import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReimbursedDataComponent } from './reimbursed-data.component';

describe('ReimbursedDataComponent', () => {
  let component: ReimbursedDataComponent;
  let fixture: ComponentFixture<ReimbursedDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReimbursedDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReimbursedDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
