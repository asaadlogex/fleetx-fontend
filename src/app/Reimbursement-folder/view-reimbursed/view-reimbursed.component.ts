import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource, MatPaginator } from "@angular/material";
import { BackEndService } from "app/services/back-end.service";

@Component({
  selector: "app-view-reimbursed",
  templateUrl: "./view-reimbursed.component.html",
  styleUrls: ["./view-reimbursed.component.scss"],
})
export class ViewReimbursedComponent implements OnInit {
  displayedColumns: string[] = [
    "jobId",
    "jobStatus",
    "reimbStatus",
    "closeDate",
    "expenses",
  ];
  dataSource = new MatTableDataSource<ReimbursementElement>(REIMBURSEMENT_DATA);
  reimbursedData: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(public backendService: BackEndService) {}

  ngOnInit() {
    
    this.reimbursedData  = this.backendService.reimbursedData;
    this.dataSource = this.reimbursedData.jobs;
    this.dataSource.paginator = this.paginator;
    // console.log(this.reimbursedData);
  }

  goBack(){
    window.history.back();
  }


}
export interface ReimbursementElement {
  jobId: string;
  jobStatus: string;
  reimbStatus: string;
  closeDate: string;
  expenses: number;
}

const REIMBURSEMENT_DATA: ReimbursementElement[] = [
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
  {
    jobId: "1",
    jobStatus: "close",
    reimbStatus: "paid",
    closeDate: "01-01-2020",
    expenses: 100,
  },
];
