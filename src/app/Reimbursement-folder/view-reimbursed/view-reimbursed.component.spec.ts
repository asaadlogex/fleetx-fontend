import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewReimbursedComponent } from './view-reimbursed.component';

describe('ViewReimbursedComponent', () => {
  let component: ViewReimbursedComponent;
  let fixture: ComponentFixture<ViewReimbursedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewReimbursedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewReimbursedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
