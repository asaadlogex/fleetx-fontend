import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BackEndService } from 'app/services/back-end.service';
import { BackEnd2Service } from 'app/services/back-end2.service';
import { ErrorMessageService } from 'app/services/error-message.service';


export interface Inventory {
  name: string;
  sku: string;
  category: string;
  type: string;
  qtyOut: string;
  stockOnhand: string;
  committedStock: string;
}

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss']
})
export class InventoryListComponent implements OnInit {

  displayedColumns: string[] = ["name", "sku", "category", "itemType", "qtyOut", "stockOnhand", "committedStock"];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;

  search() {}

  ClearForm() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dialog: MatDialog,
		private router: Router,
		public backendService: BackEndService,
		public backend2Service: BackEnd2Service,
		public errorMsgService: ErrorMessageService) {

    // this.form = new FormGroup({
    //   jobId: new FormControl(null),
    //   rwbId: new FormControl(null),
    //   lic: new FormControl(null),
    //   start: new FormControl(null),
    //   end: new FormControl(null),
    //   jobStatus: new FormControl(null),
    //   ownerShipStatus: new FormControl(null),
    // });
  }


  ngOnInit() {
    this.getAllInvetory();
  }

  getAllInvetory() {
    this.backend2Service.getAllInventoryService().subscribe((res) => {
      if(res.success){
        const ELEMENT_DATA: Inventory[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      }
    });
  }

  exportInventoryList() {
    this.backend2Service.getInventoryExportFileService().subscribe(res => {
      const newBlob = new Blob([res.toString()], {
        type: "application/excel",
      });
      var downloadURL = window.URL.createObjectURL(res);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = "Inventory_List" + ".xlsx";
      link.click();
    });
  }

  // findItemById(data: any) {
  //   this.backend2Service.getItemById(data.itemId).subscribe((res) => {
  //     if (res.data != null || res.data != "") {
  //       sessionStorage.setItem("getItem", JSON.stringify(res.data));
  //       this.router.navigate(["items/edit"]);
  //     }
  //   });
  // }

}
