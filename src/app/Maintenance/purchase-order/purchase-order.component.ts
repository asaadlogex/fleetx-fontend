import { Component, OnInit } from "@angular/core";
import { MatTableDataSource, MatDialog } from "@angular/material";
import { FormGroup, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { BackEndService } from "app/services/back-end.service";
import { BackEnd2Service } from "app/services/back-end2.service";
import { ErrorMessageService } from "app/services/error-message.service";

export interface Orders {
  date: string;
  orderNo: string;
  vendorName: string;
  refNo: string;
  received: string;
  amount: number;
  status: string;
}

@Component({
  selector: "app-purchase-order",
  templateUrl: "./purchase-order.component.html",
  styleUrls: ["./purchase-order.component.scss"],
})
export class PurchaseOrderComponent implements OnInit {
  displayedColumns: string[] = [
    "date",
    "orderNo",
    "vendorName",
    "refNo",
    "received",
    "amount",
    "status",
    // "deliveryDate",
  ];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;

  search() {}

  ClearForm() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dialog: MatDialog,
		private router: Router,
		public backendService: BackEndService,
		public backend2Service: BackEnd2Service,
		public errorMsgService: ErrorMessageService) {
      
    // this.form = new FormGroup({
    //   jobId: new FormControl(null),
    //   rwbId: new FormControl(null),
    //   lic: new FormControl(null),
    //   start: new FormControl(null),
    //   end: new FormControl(null),
    //   jobStatus: new FormControl(null),
    //   ownerShipStatus: new FormControl(null),
    // });
  }

  ngOnInit() {
    this.getAllOrders();
  }

  getAllOrders() {
    this.backend2Service.getAllPurchaseOrderService().subscribe((res) => {
      if(res.success){
        const ELEMENT_DATA: Orders[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      }
    });
  }

  findOrderById(data: any) {
    this.backend2Service.getPurchaseOrderById(data.purchaseOrderId).subscribe((res) => {
      if (res.data != null || res.data != "") {
        sessionStorage.setItem("getOrder", JSON.stringify(res.data));
        this.router.navigate(["purchaseOrder/edit"]);
      }
    });
  }
}