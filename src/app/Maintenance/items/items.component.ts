import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { FormGroup, FormControl } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { BackEndService } from "app/services/back-end.service";
import { BackEnd2Service } from "app/services/back-end2.service";
import { ErrorMessageService } from "app/services/error-message.service";

export interface Items {
  name: string;
  sku: string;
  category: string;
  brand: string;
  type: string;
}

@Component({
  selector: "app-items",
  templateUrl: "./items.component.html",
  styleUrls: ["./items.component.scss"],
})

export class ItemsComponent implements OnInit {
  displayedColumns: string[] = ["name", "sku", "category", "brand", "itemType"];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;

  search() {}

  ClearForm() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dialog: MatDialog,
		private router: Router,
		public backendService: BackEndService,
		public backend2Service: BackEnd2Service,
		public errorMsgService: ErrorMessageService) {

    // this.form = new FormGroup({
    //   jobId: new FormControl(null),
    //   rwbId: new FormControl(null),
    //   lic: new FormControl(null),
    //   start: new FormControl(null),
    //   end: new FormControl(null),
    //   jobStatus: new FormControl(null),
    //   ownerShipStatus: new FormControl(null),
    // });
  }


  ngOnInit() {
    this.getAllItems();
  }

  getAllItems() {
    this.backend2Service.getAllItemService().subscribe((res) => {
      if(res.success){
        const ELEMENT_DATA: Items[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      }
    });
  }

  findItemById(data: any) {
    this.backend2Service.getItemById(data.itemId).subscribe((res) => {
      if (res.data != null || res.data != "") {
        sessionStorage.setItem("getItem", JSON.stringify(res.data));
        this.router.navigate(["items/edit"]);
      }
    });
  }
}
