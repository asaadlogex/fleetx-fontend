import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AddCategoryBrandComponent } from "app/modals/add-category-brand/add-category-brand.component";
import { MatDialog } from "@angular/material";
import { BackEndService } from "app/services/back-end.service";
import { ErrorMessageService } from "app/services/error-message.service";
import { BackEnd2Service } from "app/services/back-end2.service";
import { HttpErrorResponse } from "@angular/common/http";
import { environment } from "environments/environment";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";
import { InfoModalComponent } from "app/modals/info-modal/info-modal.component";
import { Router } from "@angular/router";

@Component({
	selector: "app-add-item",
	templateUrl: "./add-item.component.html",
	styleUrls: ["./add-item.component.scss"],
})
export class AddItemComponent implements OnInit {

	categoryCode = environment.ITEM_CATEGORY;
	brandCode = environment.BRAND;
	itemTypeCode = environment.ITEM_TYPE;
	itemUnitCode = environment.ITEM_UNIT;

	addItemForm: FormGroup;
	filteredCategory: any = [];
	filteredBrand: any = [];
	items: any;
	itemCategoryDropDown: any;
	itemBrandDropDown: any;
	itemUnitDropDown: any;
	itemTypeDropDown: any;
	amount = null;
	quant = null

	constructor(public dialog: MatDialog,
		private router: Router,
		public backendService: BackEndService,
		public backend2Service: BackEnd2Service,
		public errorMsgService: ErrorMessageService) {
	}

	/* =============================== COMPONENT ONINIT FUNCTION ================================= */
	ngOnInit() {
		this.getItemCategoryDropdown(this.categoryCode);
		this.getItemBrandDropdown(this.brandCode);
		this.getItemTypeDropdown(this.itemTypeCode);
		this.getItemUnitDropdown(this.itemUnitCode);

		this.addItemForm = new FormGroup({
			itemId: new FormControl(null),
			itemType: new FormControl(null, Validators.required),
			name: new FormControl(null, Validators.required),
			sku: new FormControl(null),
			unit: new FormControl(null),
			dimensions: new FormControl(null),
			length: new FormControl(null),
			breadth: new FormControl(null),
			height: new FormControl(null),
			weight: new FormControl(null),
			brand: new FormControl(null),
			category: new FormControl(null),
			manufacturer: new FormControl(null),
			upc: new FormControl(null),
			mpn: new FormControl(null),
			ean: new FormControl(null),
			isbn: new FormControl(null),
			cost: new FormControl(null, Validators.required),
			quantity: new FormControl(null, Validators.required),
			refundable: new FormControl(true),
			itemPic: new FormControl(null)
		});

		let getItem = JSON.parse(sessionStorage.getItem("getItem"));
		sessionStorage.removeItem("getItem");
		if (getItem != null) {
			this.items = getItem;
			this.addItemForm.get('itemId').setValue(this.items.itemId);
			this.addItemForm.get('itemType').setValue(this.items.itemType);
			this.addItemForm.get('name').setValue(this.items.itemName);
			this.addItemForm.get('dimensions').setValue(this.items.itemDimension);
			this.addItemForm.get('length').setValue(this.items.itemDimension.length);
			this.addItemForm.get('breadth').setValue(this.items.itemDimension.breadth);
			this.addItemForm.get('height').setValue(this.items.itemDimension.height);
			this.addItemForm.get('sku').setValue(this.items.itemSku);
			this.addItemForm.get('unit').setValue(this.items.itemUnit);
			this.addItemForm.get('weight').setValue(this.items.weightInKg);
			this.addItemForm.get('brand').setValue(this.items.itemBrand);
			this.addItemForm.get('category').setValue(this.items.itemCategory);
			this.addItemForm.get('upc').setValue(this.items.itemUpc);
			this.addItemForm.get('mpn').setValue(this.items.itemMpn);
			this.addItemForm.get('ean').setValue(this.items.itemEan);
			this.addItemForm.get('isbn').setValue(this.items.itemIsbn);
			this.addItemForm.get('cost').setValue(this.items.itemPrice);
			this.addItemForm.get('quantity').setValue(this.items.inventory.qtyOnHand);
			this.addItemForm.get('refundable').setValue(this.items.isRefundable);
			this.addItemForm.get('itemPic').setValue(this.items.itemPicUrl);
		}
		this.onChanges();
	}

	onChanges() {
		this.addItemForm.controls['category'].valueChanges.subscribe(val => {
			if (val.length > 0) {
				const filterValue = val.toLowerCase();
				this.filteredCategory = this.itemCategoryDropDown.filter(option => {
					return option.value.toLowerCase().includes(filterValue)

				})
			} else {
				this.filteredCategory = this.itemCategoryDropDown;
			}
		})

		this.addItemForm.controls['brand'].valueChanges.subscribe(val => {
			if (val.length > 0) {
				const filterValue = val.toLowerCase();
				this.filteredBrand = this.itemBrandDropDown.filter(option => {
					return option.value.toLowerCase().includes(filterValue)

				})
			} else {
				this.filteredBrand = this.itemBrandDropDown;
			}
		})
	}

	changeValidity(){
		if(this.addItemForm.value.itemType.value == "Goods"){
			this.addItemForm.get('cost').enable();
			this.addItemForm.get('quantity').enable();
		}else{
			this.addItemForm.get('cost').disable();
			this.addItemForm.get('quantity').disable();
		}
	}

	/* =============================== ADD UPDATE DELETE ITEM FUNCTIONS ================================= */

	submit() {
		if (this.addItemForm.invalid) {
			return;
		}
		let itemData = {
			itemId: this.addItemForm.value.itemId,
			itemName: this.addItemForm.value.name,
			itemSku: this.addItemForm.value.sku,
			itemType: this.addItemForm.value.itemType,
			itemUnit: this.addItemForm.value.unit,
			itemCategory: this.addItemForm.value.category,
			itemBrand: this.addItemForm.value.brand,
			itemDimension: {
				length: this.addItemForm.value.length,
				breadth: this.addItemForm.value.breadth,
				height: this.addItemForm.value.height,
			},
			weightInKg: this.addItemForm.value.weight,
			itemUpc: this.addItemForm.value.upc,
			itemEan: this.addItemForm.value.ean,
			itemMpn: this.addItemForm.value.mpn,
			itemIsbn: this.addItemForm.value.isbn,
			isRefundable: this.addItemForm.value.refundable,
			itemPrice: this.addItemForm.value.cost,
			itemPicUrl: null
		}

		if(itemData.itemType.value == "Goods"){
			this.amount = this.addItemForm.value.cost;
			this.quant = this.addItemForm.value.quantity;
		}else{
			this.amount = null;
			this.quant = null;
		}

		if (this.addItemForm.value.itemId == null) {
			this.addItem(itemData, this.quant, this.amount);
		} else {
			this.updateItem(itemData);
		}
	}

	addItem(data, qty, cost) {
		this.backend2Service.addItemService(data, qty, cost).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
				this.addItemForm.get("itemId").setValue(res.data.itemId);
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("ITEM ADD RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	updateItem(data) {
		this.backend2Service.updateItemService(data).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("ITEM UPDATE RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	//FUNCTION TO DELETE A ITEM
	deleteItem() {
		if (this.addItemForm.value.itemId != null) {
			this.backend2Service.deleteItemService(this.items.itemId).subscribe(res => {
				if (res.success) {
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
					this.router.navigate(['items'])
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("ITEM DELETE RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});
		}
	}

	/* =============================== DROPDOWN FUNCTION ================================= */

	getItemCategoryDropdown(type: string) {
		this.backendService.getDropdowns(type).subscribe(res => {
			this.itemCategoryDropDown = res.data;
			this.filteredCategory = res.data;
		})
	}

	getItemBrandDropdown(type: string) {
		this.backendService.getDropdowns(type).subscribe(res => {
			this.itemBrandDropDown = res.data;
			this.filteredBrand = res.data;
		})
	}

	getItemTypeDropdown(type: string) {
		this.backendService.getDropdowns(type).subscribe(res => {
			this.itemTypeDropDown = res.data;
		})
	}

	getItemUnitDropdown(type: string) {
		this.backendService.getDropdowns(type).subscribe(res => {
			this.itemUnitDropDown = res.data;
		})
	}

	/* =============================== COMPARE DROPDOWN FUNCTION ================================= */
	compareDropdownType(o1, o2): boolean {
		if (o2 != null)
			return o1.value === o2.value;
	}

	displayBrand(brd): string {
		return brd && brd.value;
	}

	displayCategory(cat): string {
		return cat && cat.value;
	}

	/* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger info dialog
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	openCategoryModal(val) {
		const dialogRef = this.dialog.open(AddCategoryBrandComponent, {
			data: val,
			width: '400px',
		});

		dialogRef.afterClosed().subscribe((result) => {
			this.getItemCategoryDropdown(this.categoryCode);
		 });
	}

	openBrandModal(val) {
		const dialogRef = this.dialog.open(AddCategoryBrandComponent, {
			data: val,
			width: '400px',

		});

		dialogRef.afterClosed().subscribe((result) => { 
			this.getItemBrandDropdown(this.brandCode)
		});
	}
}
