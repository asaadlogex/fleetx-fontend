import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { BackEndService } from "app/services/back-end.service";
import { BackEnd2Service } from "app/services/back-end2.service";
import { ErrorMessageService } from "app/services/error-message.service";
import { environment } from "environments/environment";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";
import { InfoModalComponent } from "app/modals/info-modal/info-modal.component";
import { HttpErrorResponse } from "@angular/common/http";
import { DatePipe } from "@angular/common";
import { ReceivePurchaseComponent } from "app/modals/receive-purchase/receive-purchase.component";

@Component({
	selector: "app-add-purchase-order",
	templateUrl: "./add-purchase-order.component.html",
	styleUrls: ["./add-purchase-order.component.scss"],
})
export class AddPurchaseOrderComponent implements OnInit {

	orderStatusCode = environment.PURCHASE_ORDER_STATUS;

	addPurchaseOrder: FormGroup;
	purchaseOrder: any;
	filteredItem: any = [];
	filteredVendor: any = [];
	VendorDropDown: any;
	orderStatusDropDown: any;
	ItemDropDown: any;
	Items: FormArray;
	SumOfRates = 0;
	showRecvBtn = false;

	constructor(private formBuilder: FormBuilder,
		public dialog: MatDialog,
		private router: Router,
		public datePipe: DatePipe,
		public backendService: BackEndService,
		public backend2Service: BackEnd2Service,
		public errorMsgService: ErrorMessageService) { }

	/* =============================== COMPONENT ONINIT FUNCTION ================================= */

	ngOnInit() {

		this.addPurchaseOrder = new FormGroup({
			purchaseId: new FormControl(null),
			vendorName: new FormControl(null, Validators.required),
			orderStatus: new FormControl(null, Validators.required),
			refNo: new FormControl(null),
			date: new FormControl(null, Validators.required),
			expectedDate: new FormControl(null),
			discountAmount: new FormControl(0),
			grossAmount: new FormControl(null),
			totalAmount: new FormControl(null),
			itemList: new FormArray([]),
		});

		this.getOrderStatusDropdown(this.orderStatusCode);
		this.getAllItems();
		this.getAllVendors();
		this.addPurchaseOrder.controls['orderStatus'].disable();

		let getOrder = JSON.parse(sessionStorage.getItem("getOrder"));
		sessionStorage.removeItem("getOrder");
		if (getOrder != null) {
			this.purchaseOrder = getOrder;
			this.addPurchaseOrder.get('purchaseId').setValue(this.purchaseOrder.purchaseOrderId);
			this.addPurchaseOrder.get('vendorName').setValue(this.purchaseOrder.vendor);
			this.addPurchaseOrder.get('orderStatus').setValue(this.purchaseOrder.purchaseOrderStatus);
			this.addPurchaseOrder.get('refNo').setValue(this.purchaseOrder.referenceNumber);
			this.addPurchaseOrder.get('date').setValue(this.purchaseOrder.orderedOn);
			this.addPurchaseOrder.get('expectedDate').setValue(this.purchaseOrder.expectedOn);
			this.addPurchaseOrder.get('discountAmount').setValue(this.purchaseOrder.discountInAmount);
			this.addPurchaseOrder.get('totalAmount').setValue(this.purchaseOrder.totalAmount);
			this.addPurchaseOrder.get('grossAmount').setValue(this.purchaseOrder.grossAmount);

			if(this.purchaseOrder.purchaseOrderStatus !== environment.SystemTypes_PURCHASE_RECEIVED){
				this.showRecvBtn = true;
			}

			if (this.purchaseOrder.itemPurchaseDetail !== null) {
				for (let i = 0; i < this.purchaseOrder.itemPurchaseDetail.length; i++) {
					this.Items = this.addPurchaseOrder.get('itemList') as FormArray;
					this.Items.push(this.formBuilder.group({
						itemPurchaseDetailId: [this.purchaseOrder.itemPurchaseDetail[i].itemPurchaseDetailId],
						item: [this.purchaseOrder.itemPurchaseDetail[i].item],
						qtyReceived: [this.purchaseOrder.itemPurchaseDetail[i].qtyReceived],
						qtyRemaining: [this.purchaseOrder.itemPurchaseDetail[i].qtyRemaining],
						qtyOrderedTotal: [this.purchaseOrder.itemPurchaseDetail[i].qtyOrderedTotal],
						qtyReceivedTotal: [this.purchaseOrder.itemPurchaseDetail[i].qtyReceivedTotal],
						rate: [this.purchaseOrder.itemPurchaseDetail[i].rate],
						amount: [this.purchaseOrder.itemPurchaseDetail[i].amount]
					}));
				}
			}
			
		}
		if(this.addPurchaseOrder.value.purchaseId == null){
			this.addNewItem();
		}
		this.onChanges();
	}

	onChanges() {
		this.addPurchaseOrder.controls['vendorName'].valueChanges.subscribe(val => {
			if (val.length > 0) {
				const filterValue = val.toLowerCase();
				this.filteredVendor = this.VendorDropDown.filter(option => {
					return option.companyName.toLowerCase().includes(filterValue)

				})
			} else {
				this.filteredVendor = this.VendorDropDown;
			}
		})
	}

	/* =============================== ADD UPDATE DELETE PURCHASE ORDER FUNCTIONS ================================= */
	submit() {
		if (this.addPurchaseOrder.invalid) {
			return;
		}
		let orderData = {
			purchaseOrderId: this.addPurchaseOrder.value.purchaseId,
			referenceNumber: this.addPurchaseOrder.value.refNo,
			vendor: this.addPurchaseOrder.value.vendorName,
			deliveredTo: null,
			orderedOn: this.datePipe.transform(this.addPurchaseOrder.value.date, environment.SystemTypes_Date_Format),
			expectedOn: this.datePipe.transform(this.addPurchaseOrder.value.expectedDate, environment.SystemTypes_Date_Format),
			receivedOn: null,
			purchaseOrderStatus: this.addPurchaseOrder.getRawValue().orderStatus,
			itemPurchaseDetail: this.addPurchaseOrder.value.itemList,
			grossAmount: this.addPurchaseOrder.value.grossAmount,
			discountInAmount: this.addPurchaseOrder.value.discountAmount,
			totalAmount: this.addPurchaseOrder.value.totalAmount,
		}

		if (this.addPurchaseOrder.value.purchaseId == null) {
			this.addOrder(orderData);
		} else {
			this.updateOrder(orderData);
		}
	}

	addOrder(data) {
		this.backend2Service.addPurchaseOrderService(data).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
				this.addPurchaseOrder.get("purchaseId").setValue(res.data.purchaseOrderId);
				this.addPurchaseOrder.get("grossAmount").setValue(res.data.grossAmount);
				this.addPurchaseOrder.get("totalAmount").setValue(res.data.totalAmount);
				this.showRecvBtn = true;
				this.purchaseOrder = res.data;
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("ORDER ADD RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	updateOrder(data) {
		this.backend2Service.updatePurchaseOrderService(data).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
				this.addPurchaseOrder.get("grossAmount").setValue(res.data.grossAmount);
				this.addPurchaseOrder.get("totalAmount").setValue(res.data.totalAmount);
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("ORDER UPDATE RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	deletePurchaseOrder() {
		if (this.purchaseOrder.purchaseOrderId != null) {
			this.backend2Service.deletePurchaseOrderService(this.purchaseOrder.purchaseOrderId).subscribe(res => {
			  if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
				this.router.navigate(['purchaseOrder'])
			  }else{
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			  }
			  //console.log("ORDER DELETE RESPONSE", res);
			}, (err: HttpErrorResponse) => {
			  if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			  }
			});
		  }
	}

	/* =============================== ADD UPDATE DELETE ITEM LIST FUNCTIONS ================================= */
	createItem() {
		return this.formBuilder.group({
			itemPurchaseDetailId: [null],
			item: [null],
			qtyOrderedTotal: [null],
			qtyReceived: [null],
			qtyReceivedTotal: [null],
			qtyRemaining: [null],
			rate: [null],
			amount: [null]
		});
	}

	addNewItem() {
		this.Items = this.addPurchaseOrder.get('itemList') as FormArray;
		this.Items.push(this.createItem());
	}

	//Function to Delete a ITem
	deleteItem(index) {
		this.Items = this.addPurchaseOrder.get('itemList') as FormArray;
		this.Items.removeAt(index);
	}

	onSearchItem(id): void {
		this.Items = this.addPurchaseOrder.get('itemList') as FormArray;
		this.Items.at(id).get("item").valueChanges.subscribe(val => {
			if (val.length > 0) {
				const filterValue = val.toLowerCase();
				this.filteredItem = this.ItemDropDown.filter(option => {
					return option.itemName.toLowerCase().includes(filterValue)
				})
			} else {
				this.filteredItem = this.ItemDropDown;
			}
		})
	}

	getRate(event, id){
		const data = event.source.value;
		this.Items = this.addPurchaseOrder.get('itemList') as FormArray;
		this.Items.at(id).get("rate").setValue(data.inventory.avgCost);
	}

	getTotalAmount(id){
		this.Items = this.addPurchaseOrder.get('itemList') as FormArray;
		let rates = this.Items.at(id).get('rate').value;
		this.Items.at(id).get("qtyOrderedTotal").valueChanges.subscribe(val => {
			this.SumOfRates = rates * val;
			this.Items.at(id).get("amount").setValue(this.SumOfRates);
		})
	}

	getTotalAmount2(id){
		this.Items = this.addPurchaseOrder.get('itemList') as FormArray;
		let rates = this.Items.at(id).get('qtyOrderedTotal').value;
		this.Items.at(id).get("rate").valueChanges.subscribe(val => {
			this.SumOfRates = rates * val;
			this.Items.at(id).get("amount").setValue(this.SumOfRates);
		})
	}

	/* =============================== DROPDOWNS FUNCTIONS ================================= */
	getOrderStatusDropdown(type: string) {
		this.backend2Service.getDropdowns(type).subscribe(res => {
			this.orderStatusDropDown = res.data;
			if(this.addPurchaseOrder.value.purchaseId == null){
				this.orderStatusDropDown.forEach(element => {
					if(element.value == environment.SystemTypes_REIMBURSEMENT_PENDING){
						this.addPurchaseOrder.get('orderStatus').setValue(element);
					}
				});
			}
		})
	}

	getAllItems() {
		this.backend2Service.getAllItemService().subscribe(res => {
			if (res.success) {
				this.ItemDropDown = res.data;
				this.filteredItem = res.data;
			}
		})
	}

	getAllVendors() {
		this.backend2Service.getAllFilterVendors().subscribe(res => {
			if (res.success) {
				this.VendorDropDown = res.data;
				this.filteredVendor = res.data;
			}
		})
	}

	/* =============================== COMPARE DROPDOWN FUNCTION ================================= */
	compareDropdownType(o1, o2): boolean {
		if (o2 != null)
			return o1.value === o2.value;
	}

	displayVendor(ven): string {
		return ven && ven.companyName;
	}

	displayItem(item): string {
		return item && item.itemName;
	}

	/* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger info dialog
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger Receive Order Dialog
	openReceiveOrderDialog(): void {
		const dialogRef = this.dialog.open(ReceivePurchaseComponent, {
			width: '450px',
			data: this.purchaseOrder
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}
}
