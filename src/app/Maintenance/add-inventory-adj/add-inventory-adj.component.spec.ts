import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInventoryAdjComponent } from './add-inventory-adj.component';

describe('AddInventoryAdjComponent', () => {
  let component: AddInventoryAdjComponent;
  let fixture: ComponentFixture<AddInventoryAdjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInventoryAdjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInventoryAdjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
