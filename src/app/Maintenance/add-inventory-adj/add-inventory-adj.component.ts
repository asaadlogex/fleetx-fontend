import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, FormBuilder } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { BackEndService } from "app/services/back-end.service";
import { BackEnd2Service } from "app/services/back-end2.service";
import { ErrorMessageService } from "app/services/error-message.service";
import { HttpErrorResponse } from "@angular/common/http";
import { environment } from "environments/environment";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";
import { InfoModalComponent } from "app/modals/info-modal/info-modal.component";

@Component({
	selector: "app-add-inventory-adj",
	templateUrl: "./add-inventory-adj.component.html",
	styleUrls: ["./add-inventory-adj.component.scss"],
})
export class AddInventoryAdjComponent implements OnInit {

	addInventoryAdjForm: FormGroup;
	ItemDropDown: any;
	filteredItem: any = [];
	adjustedInventory: any;

	constructor(private formBuilder: FormBuilder,
		public dialog: MatDialog,
		private router: Router,
		public datePipe: DatePipe,
		public backendService: BackEndService,
		public backend2Service: BackEnd2Service,
		public errorMsgService: ErrorMessageService) { }

	ngOnInit() {
		this.addInventoryAdjForm = new FormGroup({
			adjId: new FormControl(null),
			refNo: new FormControl(null),
			date: new FormControl(null),
			reason: new FormControl(null),
			itemName: new FormControl(null),
			adjQty: new FormControl(null),
		});
		
		this.getAllItems();

		let getadjustedInventory = JSON.parse(sessionStorage.getItem("getadjustedInventory"));
		sessionStorage.removeItem("getadjustedInventory");
		if (getadjustedInventory != null) {
			this.adjustedInventory = getadjustedInventory;
			this.addInventoryAdjForm.get('adjId').setValue(this.adjustedInventory.inventoryAdjustmentId);
			this.addInventoryAdjForm.get('refNo').setValue(this.adjustedInventory.referenceNumber);
			this.addInventoryAdjForm.get('date').setValue(this.adjustedInventory.inventoryAdjustmentDate);
			this.addInventoryAdjForm.get('reason').setValue(this.adjustedInventory.adjustmentReason);
			this.addInventoryAdjForm.get('itemName').setValue(this.adjustedInventory.item);
			this.addInventoryAdjForm.get('adjQty').setValue(this.adjustedInventory.adjustmentQty);
		}
		this.onChanges()
	}

	onChanges() {
		this.addInventoryAdjForm.controls['itemName'].valueChanges.subscribe(val => {
			if (val.length > 0) {
				const filterValue = val.toLowerCase();
				this.filteredItem = this.ItemDropDown.filter(option => {
					return option.numberPlate.toLowerCase().includes(filterValue)

				})
			} else {
				this.filteredItem = this.ItemDropDown;
			}
		})
	}

	/* =============================== ADD UPDATE DELETE ITEM FUNCTIONS ================================= */

	submit() {
		if (this.addInventoryAdjForm.invalid) {
			return;
		}
		let adjData = {
			inventoryAdjustmentId: this.addInventoryAdjForm.value.adjId,
			item: this.addInventoryAdjForm.value.itemName,
			referenceNumber: this.addInventoryAdjForm.value.refNo,
			adjustmentReason: this.addInventoryAdjForm.value.reason,
			inventoryAdjustmentDate: this.datePipe.transform(this.addInventoryAdjForm.value.date, environment.SystemTypes_Date_Format),
			adjustmentQty: this.addInventoryAdjForm.value.adjQty
		}

		if (this.addInventoryAdjForm.value.adjId == null) {
			this.addInventoryAdjusted(adjData);
		} else {
			this.updateInventoryAdjusted(adjData);
		}
	}

	addInventoryAdjusted(data) {
		this.backend2Service.addInventoryAdjustmentService(data).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
				this.addInventoryAdjForm.get("adjId").setValue(res.data.inventoryAdjustmentId);
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("ADJUSTMENT ADD RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	updateInventoryAdjusted(data) {
		this.backend2Service.updateInventoryAdjustmentService(data).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("ADJUSTMENT UPDATE RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	//FUNCTION TO DELETE
	deleteInventoryAdj() {
		if (this.addInventoryAdjForm.value.adjId != null) {
			this.backend2Service.deleteInventoryAdjustmentService(this.adjustedInventory.inventoryAdjustmentId).subscribe(res => {
				if (res.success) {
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
					this.router.navigate(['items'])
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("DELETE RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});
		}
	}

	/* =============================== DROPDOWN FUNCTION ================================= */
	getAllItems() {
		this.backend2Service.getAllItemService().subscribe(res => {
			if (res.success) {
				this.ItemDropDown = res.data;
				this.filteredItem = res.data;
			}
		})
	}

	/* =============================== COMPARE DROPDOWN FUNCTION ================================= */

	displayItem(item): string {
		return item && item.itemName;
	}

	/* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger info dialog
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}
}
