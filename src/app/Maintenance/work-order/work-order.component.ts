import { Component, OnInit } from "@angular/core";
import { MatTableDataSource, MatDialog } from "@angular/material";
import { FormGroup, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { BackEndService } from "app/services/back-end.service";
import { BackEnd2Service } from "app/services/back-end2.service";
import { ErrorMessageService } from "app/services/error-message.service";

export interface Orders {
  orderNo: string;
  date: string;
  truck: string;
  orderCat: string;
  compDate: string;
  workshop: string;
  orderStatus: string;
}

@Component({
  selector: "app-work-order",
  templateUrl: "./work-order.component.html",
  styleUrls: ["./work-order.component.scss"],
})
export class WorkOrderComponent implements OnInit {
  displayedColumns: string[] = [
    "date",
    "orderNo",
    "truck",
    "orderCat",
    "compDate",
    "workshop",
    "status",
  ];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;

  search() {}

  ClearForm() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dialog: MatDialog,
		private router: Router,
		public backendService: BackEndService,
		public backend2Service: BackEnd2Service,
		public errorMsgService: ErrorMessageService) {
    // this.form = new FormGroup({
    //   jobId: new FormControl(null),
    //   rwbId: new FormControl(null),
    //   lic: new FormControl(null),
    //   start: new FormControl(null),
    //   end: new FormControl(null),
    //   jobStatus: new FormControl(null),
    //   ownerShipStatus: new FormControl(null),
    // });
  }

  ngOnInit() {
    this.getAllOrders();
  }

  getAllOrders() {
    this.backend2Service.getAllWorkOrderService().subscribe((res) => {
      if(res.success){
        const ELEMENT_DATA: Orders[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      }
    });
  }

  findOrderById(data: any) {
    this.backend2Service.getWorkOrderById(data.workOrderId).subscribe((res) => {
      if (res.data != null || res.data != "") {
        sessionStorage.setItem("getWorkOrder", JSON.stringify(res.data));
        this.router.navigate(["workOrder/edit"]);
      }
    });
  }

  exportWorkOrderList() {
    this.backend2Service.getWorkOrderExportFileService().subscribe(res => {
      const newBlob = new Blob([res.toString()], {
        type: "application/excel",
      });
      var downloadURL = window.URL.createObjectURL(res);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = "WorkOrder_List" + ".xlsx";
      link.click();
    });
  }
}
