import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
	selector: "app-add-vendors",
	templateUrl: "./add-vendors.component.html",
	styleUrls: ["./add-vendors.component.scss"],
})
export class AddVendorsComponent implements OnInit {
	addVendorForm: FormGroup;

	constructor() {
		this.addVendorForm = new FormGroup({
			vendorStatus: new FormControl(null),
			salutation: new FormControl(null),
			firstName: new FormControl(null),
			lastName: new FormControl(null),
			companyName: new FormControl(null),
			vendorName: new FormControl(null),
			vendorWeb: new FormControl(null),
			currency: new FormControl(null),
			paymentTerms: new FormControl(null),
		});
	}

	ngOnInit() {}

	submitVendor() {
		console.log(this.addVendorForm.value);
	}

	deleteVendor() {
		console.log("delete vendor");
	}

	addNewContact() {
		console.log("add new contact");
	}
}
