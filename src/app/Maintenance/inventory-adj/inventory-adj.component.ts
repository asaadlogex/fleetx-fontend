import { Component, OnInit } from "@angular/core";
import { MatTableDataSource, MatDialog } from "@angular/material";
import { FormGroup, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { BackEndService } from "app/services/back-end.service";
import { BackEnd2Service } from "app/services/back-end2.service";
import { ErrorMessageService } from "app/services/error-message.service";

export interface Adjustment {
  date: string;
  item: string;
  refNo: string;
  adjQty: number;
}

@Component({
  selector: "app-inventory-adj",
  templateUrl: "./inventory-adj.component.html",
  styleUrls: ["./inventory-adj.component.scss"],
})
export class InventoryAdjComponent implements OnInit {
  displayedColumns: string[] = [
    "date",
    "reason",
    "description",
    "status",
  ];
  dataSource: MatTableDataSource<any>;
  form: FormGroup;

  search() {}

  ClearForm() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dialog: MatDialog,
		private router: Router,
		public backendService: BackEndService,
		public backend2Service: BackEnd2Service,
		public errorMsgService: ErrorMessageService) {
    // this.form = new FormGroup({
    //   jobId: new FormControl(null),
    //   rwbId: new FormControl(null),
    //   lic: new FormControl(null),
    //   start: new FormControl(null),
    //   end: new FormControl(null),
    //   jobStatus: new FormControl(null),
    //   ownerShipStatus: new FormControl(null),
    // });
  }

  ngOnInit() {
    this.getAllAdjustments();
  }

  getAllAdjustments() {
    this.backend2Service.getAllInventoryAdjustmentService().subscribe((res) => {
      if(res.success){
        const ELEMENT_DATA: Adjustment[] = res.data;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      }
    });
  }

  findAdjustmentById(data: any) {
    this.backend2Service.getInventoryAdjustmentById(data.inventoryAdjustmentId).subscribe((res) => {
      if (res.data != null || res.data != "") {
        sessionStorage.setItem("getadjustedInventory", JSON.stringify(res.data));
        this.router.navigate(["inventoryAdj/edit"]);
      }
    });
  }
}
