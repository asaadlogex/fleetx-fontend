import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryAdjComponent } from './inventory-adj.component';

describe('InventoryAdjComponent', () => {
  let component: InventoryAdjComponent;
  let fixture: ComponentFixture<InventoryAdjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryAdjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryAdjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
