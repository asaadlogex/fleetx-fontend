import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from "@angular/forms";
import { environment } from "environments/environment";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";
import { InfoModalComponent } from "app/modals/info-modal/info-modal.component";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { BackEndService } from "app/services/back-end.service";
import { BackEnd2Service } from "app/services/back-end2.service";
import { ErrorMessageService } from "app/services/error-message.service";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
	selector: "app-add-work-order",
	templateUrl: "./add-work-order.component.html",
	styleUrls: ["./add-work-order.component.scss"],
})
export class AddWorkOrderComponent implements OnInit {

	workorderStatusCode = environment.WORK_ORDER_STATUS;
	baseStationCode = environment.BASE_STATION_TYPE;
	priorityCode = environment.PRIORITY;
	workOrderCatCode = environment.WORK_ORDER_CATEGORY;
	workOrderSubCode = environment.WORK_ORDER_SUB_CATEGORY;

	addWorkOrder: FormGroup;
	workOrder: any;
	// filter fields
	truckDropDown: any;
	orderStatusDropDown: any;
	orderCategoryDropDown: any;
	baseStationDropDown: any;
	priorityDropDown: any;
	orderSubCatDropDown: any;
	ItemDropDown: any;
	filteredItem: any = [];
	filteredTruck: any = [];
	workshopDropDown: any;
	Items: FormArray;
	showProgress = false;
	showClose = false;
	SumOfRates = 0;

	constructor(private formBuilder: FormBuilder,
		public dialog: MatDialog,
		private router: Router,
		public datePipe: DatePipe,
		public backendService: BackEndService,
		public backend2Service: BackEnd2Service,
		public errorMsgService: ErrorMessageService) { }

	ngOnInit() {
		this.addWorkOrder = new FormGroup({
			orderId: new FormControl(null),
			truck: new FormControl(null, Validators.required),
			truckKm: new FormControl(null),
			refNo: new FormControl(null),
			baseStation: new FormControl(null, Validators.required),
			workOrderDate: new FormControl(null, Validators.required),
			completionDate: new FormControl(null),
			orderStatus: new FormControl(null, Validators.required),
			orderCategory: new FormControl(null),
			orderSubCategory: new FormControl(null),
			workShop: new FormControl(null, Validators.required),
			priority: new FormControl(null),
			activity: new FormControl(null),
			senderName: new FormControl(null, Validators.required),
			totalAmount: new FormControl(null),
			itemList: new FormArray([])
		});

		this.getAllItems();
		this.getAllTractors();
		this.getAllWorkShops();
		this.getOrderStatusDropdown(this.workorderStatusCode);
		this.getBaseStations(this.baseStationCode);
		this.getWorkOrderCategory(this.workOrderCatCode);
		this.getOrderSubCategory(this.workOrderSubCode);
		this.getPriorityDropdown(this.priorityCode);
		this.addWorkOrder.controls['orderStatus'].disable();

		let getWorkOrder = JSON.parse(sessionStorage.getItem("getWorkOrder"));
		sessionStorage.removeItem("getWorkOrder");
		if (getWorkOrder != null) {
			this.workOrder = getWorkOrder;
			this.addWorkOrder.get('orderId').setValue(this.workOrder.workOrderId);
			this.addWorkOrder.get('truck').setValue(this.workOrder.tractor);
			this.addWorkOrder.get('truckKm').setValue(this.workOrder.tractorKm);
			this.addWorkOrder.get('refNo').setValue(this.workOrder.referenceNumber);
			this.addWorkOrder.get('workOrderDate').setValue(this.workOrder.workOrderDate);
			this.addWorkOrder.get('completionDate').setValue(this.workOrder.workOrderCompletionDate);
			this.addWorkOrder.get('baseStation').setValue(this.workOrder.baseStation);
			this.addWorkOrder.get('senderName').setValue(this.workOrder.senderName);
			this.addWorkOrder.get('activity').setValue(this.workOrder.activityDetail);
			this.addWorkOrder.get('orderStatus').setValue(this.workOrder.workOrderStatus);
			this.addWorkOrder.get('orderCategory').setValue(this.workOrder.workOrderCategory);
			this.addWorkOrder.get('orderSubCategory').setValue(this.workOrder.workOrderSubCategory);
			this.addWorkOrder.get('priority').setValue(this.workOrder.priority);
			this.addWorkOrder.get('workShop').setValue(this.workOrder.workshop);
			this.addWorkOrder.get('totalAmount').setValue(this.workOrder.totalAmount);

			if(this.workOrder.workOrderStatus.value == environment.SystemTypes_INPROGRESS){
				this.showClose = true;
			}
			if(this.workOrder.workOrderStatus.value == environment.SystemTypes_OPEN){
				this.showProgress = true;
			}

			if (this.workOrder.workOrderItemDetails !== null) {
				for (let i = 0; i < this.workOrder.workOrderItemDetails.length; i++) {
					this.Items = this.addWorkOrder.get('itemList') as FormArray;
					this.Items.push(this.formBuilder.group({
						workOrderItemDetailId: [this.workOrder.workOrderItemDetails[i].workOrderItemDetailId],
						item: [this.workOrder.workOrderItemDetails[i].item],
						qtyUsed: [this.workOrder.workOrderItemDetails[i].qtyUsed],
						rate: [this.workOrder.workOrderItemDetails[i].rate],
						amount: [this.workOrder.workOrderItemDetails[i].amount]
					}));
				}
			}
		}
		if (this.addWorkOrder.value.orderId == null) {
			this.addNewItem();
		}
		this.onChanges();
	}

	onChanges() {
		this.addWorkOrder.controls['truck'].valueChanges.subscribe(val => {
			if (val.length > 0) {
				const filterValue = val.toLowerCase();
				this.filteredTruck = this.truckDropDown.filter(option => {
					return option.numberPlate.toLowerCase().includes(filterValue)

				})
			} else {
				this.filteredTruck = this.truckDropDown;
			}
		})
	}

	/* =============================== ADD UPDATE DELETE WORK ORDER FUNCTIONS ================================= */
	submit() {
		if (this.addWorkOrder.invalid) {
			return;
		}
		let orderData = {
			workOrderId: this.addWorkOrder.value.orderId,
			tractor: this.addWorkOrder.value.truck,
			tractorKm: this.addWorkOrder.value.truckKm,
			baseStation: this.addWorkOrder.value.baseStation,
			priority: this.addWorkOrder.value.priority,
			workOrderDate: this.datePipe.transform(this.addWorkOrder.value.workOrderDate, environment.SystemTypes_Date_Format),
			workOrderCompletionDate: this.datePipe.transform(this.addWorkOrder.value.completionDate, environment.SystemTypes_Date_Format),
			workOrderStatus: this.addWorkOrder.getRawValue().orderStatus,
			workOrderCategory: this.addWorkOrder.value.orderCategory,
			workOrderSubCategory: this.addWorkOrder.value.orderSubCategory,
			workOrderItemDetails: this.addWorkOrder.value.itemList,
			referenceNumber: this.addWorkOrder.value.refNo,
			senderName: this.addWorkOrder.value.senderName,
			activityDetail: this.addWorkOrder.value.activity,
			workshop: this.addWorkOrder.value.workShop,
			totalAmount: this.addWorkOrder.value.totalAmount
		}

		if (this.addWorkOrder.value.orderId == null) {
			this.addOrder(orderData);
		} else {
			this.updateWorkOrder(orderData);
		}
	}

	addOrder(data) {
		this.backend2Service.addWorkOrderService(data).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
				this.addWorkOrder.get("orderId").setValue(res.data.workOrderId);
				this.addWorkOrder.get("totalAmount").setValue(res.data.totalAmount);
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("ORDER ADD RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	updateWorkOrder(data) {
		this.backend2Service.updateWorkOrderService(data).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
				this.addWorkOrder.get("totalAmount").setValue(res.data.totalAmount);
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("ORDER UPDATE RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	deleteWorkOrder() {
		if (this.workOrder.workOrderId != null) {
			this.backend2Service.deleteWorkOrderService(this.workOrder.workOrderId).subscribe(res => {
				if (res.success) {
					this.errorMsgService.getSuccessMessage(res.message);
					this.openSuccessDialog();
					this.router.navigate(['workOrder'])
				} else {
					this.errorMsgService.getErrorMessage(res.errorMessage);
					this.openErrorDialog();
				}
				//console.log("ORDER DELETE RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});
		}
	}

	onSearchItem(id): void {
		this.Items = this.addWorkOrder.get('itemList') as FormArray;
		this.Items.at(id).get("item").valueChanges.subscribe(val => {
			if (val.length > 0) {
				const filterValue = val.toLowerCase();
				this.filteredItem = this.ItemDropDown.filter(option => {
					return option.itemName.toLowerCase().includes(filterValue)
				})
			} else {
				this.filteredItem = this.ItemDropDown;
			}
		})
	}


	/* =============================== ADD UPDATE DELETE ITEM LIST FUNCTIONS ================================= */
	createItem() {
		return this.formBuilder.group({
			workOrderItemDetailId: [null],
			item: [null],
			qtyUsed: [null],
			rate: [null],
			amount: [null]
		});
	}

	addNewItem() {
		this.Items = this.addWorkOrder.get('itemList') as FormArray;
		this.Items.push(this.createItem());
	}

	//Function to Delete a ITem
	deleteItem(index) {
		this.Items = this.addWorkOrder.get('itemList') as FormArray;
		this.Items.removeAt(index);
	}

	changeStatus(val) {
		this.backend2Service.changeWorkOrderStatusService(this.workOrder.workOrderId, val).subscribe(res => {
			if (res.success) {
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
				this.router.navigate(['workOrder'])
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
			//console.log("ORDER DELETE RESPONSE", res);
		}, (err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
				this.openInfoDialog();
			}
		});
	}

	getRate(event, id){
		const data = event.source.value;
		this.Items = this.addWorkOrder.get('itemList') as FormArray;
		this.Items.at(id).get("rate").setValue(data.inventory.avgCost);
	}

	getTotalAmount(id){
		this.Items = this.addWorkOrder.get('itemList') as FormArray;
		let rates = this.Items.at(id).get('rate').value;
		this.Items.at(id).get("qtyUsed").valueChanges.subscribe(val => {
			this.SumOfRates = rates * val;
			this.Items.at(id).get("amount").setValue(this.SumOfRates);
		})
	}

	getTotalAmount2(id){
		this.Items = this.addWorkOrder.get('itemList') as FormArray;
		let rates = this.Items.at(id).get('qtyUsed').value;
		this.Items.at(id).get("rate").valueChanges.subscribe(val => {
			this.SumOfRates = rates * val;
			this.Items.at(id).get("amount").setValue(this.SumOfRates);
		})
	}

	/* =============================== DROPDOWNS FUNCTIONS ================================= */
	getOrderStatusDropdown(type: string) {
		this.backend2Service.getDropdowns(type).subscribe(res => {
			this.orderStatusDropDown = res.data;
			if (this.addWorkOrder.value.orderId == null) {
				this.orderStatusDropDown.forEach(element => {
					if (element.value == environment.SystemTypes_OPEN) {
						this.addWorkOrder.get('orderStatus').setValue(element);
					}
				});
			}
		})
	}

	getAllItems() {
		this.backend2Service.getAllItemService().subscribe(res => {
			if (res.success) {
				this.ItemDropDown = res.data;
				this.filteredItem = res.data;
			}
		})
	}

	getAllTractors() {
		let params = {
			page: 0,
		  }
		this.backendService.getAllTractorService(params).subscribe(res => {
			if (res.success) {
				this.truckDropDown = res.data;
				this.filteredTruck = res.data;
			}
		})
	}

	getAllWorkShops() {
		this.backend2Service.getAllFilterWorkshop().subscribe(res => {
			if (res.success) {
				this.workshopDropDown = res.data;
			}
		})
	}

	getBaseStations(type: string) {
		this.backend2Service.getDropdowns(type).subscribe(res => {
			this.baseStationDropDown = res.data;
		})
	}

	getWorkOrderCategory(type: string) {
		this.backend2Service.getDropdowns(type).subscribe(res => {
			this.orderCategoryDropDown = res.data;
		})
	}

	getOrderSubCategory(type: string) {
		this.backend2Service.getDropdowns(type).subscribe(res => {
			this.orderSubCatDropDown = res.data;
		})
	}

	getPriorityDropdown(type: string) {
		this.backend2Service.getDropdowns(type).subscribe(res => {
			this.priorityDropDown = res.data;
		})
	}

	/* =============================== COMPARE DROPDOWN FUNCTION ================================= */
	compareDropdownType(o1, o2): boolean {
		if (o2 != null)
			return o1.value === o2.value;
	}

	compareWorkshop(o1, o2): boolean {
		if (o2 != null)
			return o1.companyId === o2.companyId;
	}

	displayTruck(trucks): string {
		return trucks && trucks.numberPlate;
	}

	displayItem(item): string {
		return item && item.itemName;
	}

	/* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	// function to trigger info dialog
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}
}
