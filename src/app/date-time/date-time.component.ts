import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-date-time',
  templateUrl: './date-time.component.html',
  styleUrls: ['./date-time.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class DateTimeComponent implements OnInit {
  @Input() type: string;
  @Input() stopDateTime : string;
  @Input() rowIndex: string;
  @Input() dateTimeForm: FormGroup;
  @Output() myOutput: EventEmitter<string> = new EventEmitter();

  selectedDate;

  dateType;


  constructor() {
    this.dateTimeForm = new FormGroup({
      arrivalTime: new FormControl(),
      arrivalDate: new FormControl(),
      departureTime: new FormControl()
    });
    // this.dateTimeForm.addControl('arrivalDate', new FormControl())
    // this.dateTimeForm.addControl('arrivalTime', new FormControl())
    // this.dateTimeForm.addControl('departureTime', new FormControl())
   }


  ngOnInit() {
     //console.log(this.stopDateTime)
    // this.dateType = 'Hello';
    // this.dateTimeForm.get('dateTime').valueChanges.subscribe(res => {
    //   console.log(res)
    //   this.myOutput.emit(res);
    // })
  }

  sendValues() {
    // this.myOutput.emit(this.outputMessage);
  }

}
