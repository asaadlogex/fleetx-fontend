import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetentionInvoiceComponent } from './detention-invoice.component';

describe('DetentionInvoiceComponent', () => {
  let component: DetentionInvoiceComponent;
  let fixture: ComponentFixture<DetentionInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetentionInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetentionInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
