import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { DatePipe } from '@angular/common';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detention-invoice',
  templateUrl: './detention-invoice.component.html',
  styleUrls: ['./detention-invoice.component.scss']
})
export class DetentionInvoiceComponent implements OnInit { 

  searchForm: FormGroup;
  customerDropDown: any = [];
  paymentDropDown: any;
  invoiceData: any;

  payTypeCode = environment.PAYMENT_STATUS_TYPE;

  displayedColumns: string[] = [
    "invoiceNum",
    "invoiceDate",
    "customer",
    "taxProvince",
    "amount",
    "status",
    "view",
  ];

  dataSource: MatTableDataSource<any>;

  selection = new SelectionModel<rwbs>(true, []);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    public backendService: BackEndService,
    public datePipe: DatePipe,
    public errorMsgService: ErrorMessageService,
    public dialog: MatDialog,
    private router: Router) {}

  ngOnInit() {

    this.searchForm = new FormGroup({
      startDate: new FormControl(null),
      endDate: new FormControl(null),
      customer: new FormControl(null),
      paymentStatus: new FormControl(null)
    })

    this.getAllCustomers();
    this.getPaymentStatusDropdown(this.payTypeCode);
    this.getAllInvoices();
  }

  getAllInvoices() {
    this.backendService.getAllDetentionInvoices().subscribe((res) => {
      if (res.success) {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  search() {
    // console.log(this.searchForm.value);
    if (this.searchForm.invalid) {
      return;
    }
    let data = {
      customerId: this.searchForm.value.customer,
      type: "Fixed",
      fixed: false,
      payment: this.searchForm.value.paymentStatus,
      start: this.datePipe.transform(this.searchForm.value.startDate, environment.SystemTypes_Date_Format),
      end: this.datePipe.transform(this.searchForm.value.endDate, environment.SystemTypes_Date_Format),
    };
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data)
      .map((key) => key + "=" + data[key])
      .join("&");
    this.backendService.getAllFilterInvoices(queryString).subscribe(
      (res) => {
        if (res.success) {
          if (res.data.length > 0) {
            this.dataSource = new MatTableDataSource(res.data);
            this.dataSource.paginator = this.paginator;
          } else {
            this.errorMsgService.getInfoMessage(res.message);
            this.openInfoDialog();
          }
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("SEARCH INVOICE RESPONSE RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  clearForm() {
    this.searchForm.reset();
  }

  getAllCustomers() {
    this.backendService.getAllFilterCustomer().subscribe((res) => {
      if (res.success) {
        this.customerDropDown = res.data;
        this.getAllBrokers();
      }
    });
  }

  getAllBrokers() {
    this.backendService.getAllFilterBroker().subscribe((res) => {
      if (res.success) {
        if (res.data.length > 0) {
          res.data.forEach((element) => {
            this.customerDropDown.push(element);
          });
        }
      }
    });
  }

  getPaymentStatusDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      if (res.success) {
        this.paymentDropDown = res.data;
      }
    });
  }

  deleteInvoice(data, id){
		this.backendService.deleteInvoiceService(data.invoiceId).subscribe(res=>{
			if(res.success){
        const index = this.dataSource.data.indexOf(id);
        this.dataSource.data.splice(index, 1);
        this.dataSource._updateChangeSubscription(); // <-- Refresh the datasource
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
		},
		(err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
				this.openInfoDialog();
			}
		});
	}

  viewInvoice(data) {
    this.backendService.getDHInvoicesByIdService(data.invoiceId).subscribe(res =>{
      if(res.success){
        this.backendService.invoiceData = res.data;
        this.router.navigate(['viewDetentionInvoice'])
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
      }
    },(err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
				this.openInfoDialog();
			}
		});
	}

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }
}

export interface rwbs {
  rwbId: number;
  customer: string;
  route: string;
  taxProvince: string;
  amount: number;
  status: string;
}