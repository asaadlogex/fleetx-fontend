import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { BackEndService } from 'app/services/back-end.service';
import { ErrorMessageService } from 'app/services/error-message.service';
import { EmailInvoiceModalComponent } from 'app/modals/email-invoice-modal/email-invoice-modal.component';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ReceivePaymentModalComponent } from 'app/modals/receive-payment-modal/receive-payment-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { PaymentInfoModalComponent } from 'app/modals/payment-info-modal/payment-info-modal.component';

@Component({
  selector: 'app-view-detn-invoice',
  templateUrl: './view-detn-invoice.component.html',
  styleUrls: ['./view-detn-invoice.component.scss']
})
export class ViewDetnInvoiceComponent implements OnInit {
  displayedColumns: string[] = [
    "sNo",
    "rwbId",
    "route",
    "departure",
    "vehicleNo",
    "tripCharges",
    "loadingCharges",
    "offloadCharges",
    "detentionCharges",
    "otherCharges",
    "amount",
  ];

  dataSource: MatTableDataSource<any>;
  invData: any;
  showPerTrip: boolean = false;
  showPerTon: boolean = false;
  showDedicated: boolean = false;

  constructor(
    public dialog: MatDialog,
    public backendService: BackEndService,
    public errorMsgService: ErrorMessageService
  ) {}

  ngOnInit() {
    let data = this.backendService.invoiceData;
    if (data != null) {
      this.invData = data;

      let i = 1;
      data.dhLineItems.forEach((element) => {
        element.sNo = i++;
      });
      this.dataSource = data.dhLineItems;
    }
  }

  // function to trigger email invoice dialog
  openEmailInvoiceDialog() {
    const dialogRef = this.dialog.open(EmailInvoiceModalComponent);

    dialogRef.afterClosed().subscribe((result) => {});
  }

  printInvoice() {
    this.backendService.getDHInvoicesForPrint(this.invData.invoiceId).subscribe(
      (res) => {
        const newBlob = new Blob([res.toString()], {
          type: "application/pdf",
        });
        var downloadURL = window.URL.createObjectURL(res);
        window.open(downloadURL, "_blank");

        //console.log("COMPANY ADD RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  // function to open receive payment dialog
  openReceiveDialogDialog(invDATA) {
    invDATA.type = "DH";
    const dialogRef = this.dialog.open(ReceivePaymentModalComponent, {
      data: invDATA,
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  getPaymentInfo(data){
    this.backendService.getDHInvoicesPaymentsService(data.invoiceId).subscribe(res =>{
      if(res.success){
        this.openPaymentInfoDialog(res.data);
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
    },
    (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
        this.openInfoDialog();
      }
    });
  }

  /************************ function to trigger info dialog ****************************/
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  // function to open check pending dialog
  openPaymentInfoDialog(invDATA) {
    const dialogRef = this.dialog.open(PaymentInfoModalComponent, {
      width: "800px",
      data: invDATA
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }
}

