import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDetnInvoiceComponent } from './view-detn-invoice.component';

describe('ViewDetnInvoiceComponent', () => {
  let component: ViewDetnInvoiceComponent;
  let fixture: ComponentFixture<ViewDetnInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDetnInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDetnInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
