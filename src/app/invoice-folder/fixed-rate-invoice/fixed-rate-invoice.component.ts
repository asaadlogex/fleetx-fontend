import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { MatPaginator, MatTableDataSource, MatDatepicker, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment, Moment } from 'moment';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { DatePipe } from '@angular/common';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { Router } from '@angular/router';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-fixed-rate-invoice',
  templateUrl: './fixed-rate-invoice.component.html',
  styleUrls: ['./fixed-rate-invoice.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class FixedRateInvoiceComponent implements OnInit {

  searchForm: FormGroup;
  addInvForm: FormGroup
  customerDropDown: any;
  paymentDropDown: any;
  year: any;
  month: any;
  searchInvData: any = [];
  billIds: any = [];
  addInvDiv = false;
  editForm = false;

  payTypeCode = environment.PAYMENT_STATUS_TYPE;

  displayedColumns: string[] = [
    "invoiceNum",
    "invoiceDate",
    "customer",
    "taxProvince",
    "amount",
    "status",
    "view",
  ];

  dataSource: MatTableDataSource<any>;

  selection = new SelectionModel<rwbs>(true, []);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    public backendService: BackEndService,
    public datePipe: DatePipe,
    public errorMsgService: ErrorMessageService,
    public dialog: MatDialog,
    private router: Router) {}

  ngOnInit() {

    this.searchForm = new FormGroup({
      month: new FormControl(moment()),
      startDate: new FormControl(null),
      endDate: new FormControl(null),
      customer: new FormControl(null),
      paymentStatus: new FormControl(null)
    })

    this.addInvForm = new FormGroup({
      period: new FormControl(moment(), Validators.required),
      customer: new FormControl(null, Validators.required)
    })
    
    this.getDedicatedCompanyDropdown();
    this.getPaymentStatusDropdown(this.payTypeCode);
    this.getAllInvoices();
  }

  getAllInvoices() {
    let data = {
      fixed: true
    }
    var queryString = Object.keys(data)
      .map((key) => key + "=" + data[key])
      .join("&");
    this.backendService.getAllFilterInvoices(queryString).subscribe((res) => {
      if (res.success) {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  search() {
    // console.log(this.searchForm.value);
    if (this.searchForm.invalid) {
      return;
    }
    if (this.searchForm.value.month != null) {
      let convertDate = this.datePipe.transform(this.searchForm.value.month, environment.SystemTypes_Date_Format);
      this.year = convertDate.substring(0, 4);
      this.month = convertDate.substring(5, 7);
    }
    let data = {
      customerId: this.searchForm.value.customer,
      type: "Fixed",
      fixed: true,
      payment: this.searchForm.value.paymentStatus,
      start: this.datePipe.transform(this.searchForm.value.startDate, environment.SystemTypes_Date_Format),
      end: this.datePipe.transform(this.searchForm.value.endDate, environment.SystemTypes_Date_Format),
      month: this.month,
      year: this.year,
    };
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data)
      .map((key) => key + "=" + data[key])
      .join("&");
    this.backendService.getAllFilterInvoices(queryString).subscribe(
      (res) => {
        if (res.success) {
          if (res.data.length > 0) {
            this.searchInvData = res.data;
            this.dataSource = new MatTableDataSource(res.data);
            this.dataSource.paginator = this.paginator;
            this.billIds = [];
          } else {
            this.errorMsgService.getInfoMessage(res.message);
            this.openInfoDialog();
          }
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("SEARCH INVOICE RESPONSE RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  clearForm() {
    this.searchForm.reset();
  }

  getDedicatedCompanyDropdown() {
    this.backendService.getAllDedicatedCompService().subscribe((res) => {
      this.customerDropDown = res.data;
    });
  }

  getPaymentStatusDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      if (res.success) {
        this.paymentDropDown = res.data;
      }
    });
  }

  onChangeSelectAll(event) {
    // console.log('changeSelected')
    if (this.searchInvData.length > 0) {
      if (event.checked) {
        this.dataSource.data.forEach((obj) => {
          obj.selected = true;
          this.billIds.push(obj.rwbId);
        });
      } else {
        this.dataSource.data.forEach((obj) => {
          obj.selected = false;
          this.billIds.pop(obj.rwbId);
        });
      }
    }
  }

  pushBill(event, data) {
    // console.log('bill pushed')
    if (event.checked) {
      this.billIds.push(data.rwbId);
    } else {
      this.billIds.pop(data.rwbId);
    }
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.searchForm.value.month
    ctrlValue.year(normalizedYear.year());
    this.searchForm.get("month").setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.searchForm.value.month;
    ctrlValue.month(normalizedMonth.month());
    this.searchForm.get("month").setValue(ctrlValue);
    datepicker.close();
    // console.log(this.searchForm.value)
  }

  chosenYearHandler2(normalizedYear: Moment) {
    const ctrlValue = this.addInvForm.value.period
    ctrlValue.year(normalizedYear.year());
    this.addInvForm.get("period").setValue(ctrlValue);
  }

  chosenMonthHandler2(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.addInvForm.value.period;
    ctrlValue.month(normalizedMonth.month());
    this.addInvForm.get("period").setValue(ctrlValue);
    datepicker.close();
    // console.log(this.searchForm.value)
  }

  addInvoice() {
    if (this.addInvForm.invalid) {
      return;
    }

    let data = {
      customer: this.addInvForm.value.customer,
      date: this.datePipe.transform(this.addInvForm.value.period, environment.SystemTypes_Date_Format)
    }
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data)
      .map((key) => key + "=" + data[key])
      .join("&");
    this.backendService.generateFixInvoiceService(queryString).subscribe(res => {
      if (res.success) {
        // console.log(res);
        this.addInvDiv = false;
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("RWB UPDATE RESPONSE", res);
    },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      });
  }

  showInvDiv(state) {
    this.addInvDiv = state;
  }

  deleteInvoice(data, id){
		this.backendService.deleteInvoiceService(data.invoiceId).subscribe(res=>{
			if(res.success){
        const index = this.dataSource.data.indexOf(id);
        this.dataSource.data.splice(index, 1);
        this.dataSource._updateChangeSubscription(); // <-- Refresh the datasource
				this.errorMsgService.getSuccessMessage(res.message);
				this.openSuccessDialog();
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
		},
		(err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
				this.openInfoDialog();
			}
		});
	}

  viewInvoice(data) {
    this.backendService.getInvoicesByIdService(data.invoiceId).subscribe(res =>{
      if(res.success){
        this.backendService.invoiceData = res.data;
        this.router.navigate(['viewFixedInvoice'])
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
      }
    },(err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
				this.openInfoDialog();
			}
		});
	}

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }
}

export interface rwbs {
  rwbId: number;
  customer: string;
  route: string;
  taxProvince: string;
  amount: number;
  status: string;
}