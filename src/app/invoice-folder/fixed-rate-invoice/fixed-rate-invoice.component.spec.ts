import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedRateInvoiceComponent } from './fixed-rate-invoice.component';

describe('FixedRateInvoiceComponent', () => {
  let component: FixedRateInvoiceComponent;
  let fixture: ComponentFixture<FixedRateInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedRateInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedRateInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
