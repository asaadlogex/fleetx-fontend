import { MatTableDataSource } from "@angular/material/table";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import { SelectionModel } from "@angular/cdk/collections";
import { MatPaginator } from "@angular/material/paginator";
import { BackEndService } from "app/services/back-end.service";
import { DatePipe } from "@angular/common";
import { MatDialog } from "@angular/material";
import { ErrorMessageService } from "app/services/error-message.service";
import { environment } from "environments/environment";
import { HttpErrorResponse } from "@angular/common/http";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";
import { InfoModalComponent } from "app/modals/info-modal/info-modal.component";
import { CheckPendingModalComponent } from "app/modals/check-pending-modal/check-pending-modal.component";
import { Router } from "@angular/router";
import { PaymentInfoModalComponent } from "app/modals/payment-info-modal/payment-info-modal.component";

@Component({
  selector: "app-invoice",
  templateUrl: "./invoice.component.html",
  styleUrls: ["./invoice.component.scss"],
})
export class InvoiceComponent implements OnInit {
  invoiceStatusCode = environment.INVOICE_STATUS_TYPE;
  invoiceTypeCode = environment.CONTRACT_TYPE;
  stateTypeCode = environment.STATE_TYPE;
  payTypeCode = environment.PAYMENT_STATUS_TYPE;

  selection = new SelectionModel<rwbs>(true, []);

  searchForm: FormGroup;
  searchInvoiceForm: FormGroup;
  customerDropDown: any = [];
  tempCustomer: any;
  invoiceDropDown: any;
  paymentDropDown: any;
  getType = null;
  invoiceTypeDropDown: any;
  stateDropDown: any;
  searchInvData: any = [];
  billIds: any = [];
  tempData: any = [];
  year: any;
  month: any;
  showPendingBtn = true;

  displayedColumns: string[] = [
    "select",
    "rwbId",
    "customer",
    "route",
    "taxProvince",
    "amount",
    "status",
  ];
  invoiceColumn: string[] = [
    "invoiceNum",
    "invoiceDate",
    "customer",
    "taxProvince",
    "amount",
    "status",
    "view",
  ];

  dataSource: MatTableDataSource<any>;
  invoicesSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    public backendService: BackEndService,
    public datePipe: DatePipe,
    public errorMsgService: ErrorMessageService,
    public dialog: MatDialog,
    private router: Router
  ) {
    // this.dataSource.data = [];
  }

  ngOnInit() {
    // this.invoicesSource.paginator = this.paginator;

    this.searchForm = new FormGroup({
      customer: new FormControl(null, Validators.required),
      start: new FormControl(null),
      end: new FormControl(null),
      state: new FormControl(null, Validators.required),
      // invoiceStatus: new FormControl(null),
      invoiceType: new FormControl(null, Validators.required),
    });

    this.searchInvoiceForm = new FormGroup({
      customer: new FormControl(null),
      start: new FormControl(null),
      end: new FormControl(null),
      invoiceNo: new FormControl(null),
      // invoiceStatus: new FormControl(null),
      invoiceType: new FormControl(null),
      paymentStatus: new FormControl(null),
    });

    this.getAllCustomers();
    this.getInvoiceStatusDropdown(this.invoiceStatusCode);
    this.getInvoiceTypeDropdown(this.invoiceTypeCode);
    this.getPaymentStatusDropdown(this.payTypeCode);
    this.getStateDropdown(this.stateTypeCode);
    this.getAllInvoices();
  }

  /*=============================== DROPDOWNS FUNCTIONS ================================= */

  getAllCustomers() {
    this.backendService.getAllFilterCustomer().subscribe((res) => {
      if (res.success) {
        this.customerDropDown = res.data;
        this.getAllBrokers();
      }
    });
  }

  getAllBrokers() {
    this.backendService.getAllFilterBroker().subscribe((res) => {
      if (res.success) {
        if (res.data.length > 0) {
          res.data.forEach((element) => {
            this.customerDropDown.push(element);
          });
        }
      }
    });
  }

  getInvoiceStatusDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      if (res.success) {
        this.invoiceDropDown = res.data;
      }
    });
  }

  getPaymentStatusDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      if (res.success) {
        this.paymentDropDown = res.data;
      }
    });
  }

  getInvoiceTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      if (res.success) {
        this.invoiceTypeDropDown = res.data;
      }
    });
  }

  getStateDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      this.stateDropDown = res.data;
    });
  }

  /*======================== FETCH ALL INVOICES =========================== */

  getAllInvoices() {
    let data = {
      fixed: false,
    };
    var queryString = Object.keys(data)
      .map((key) => key + "=" + data[key])
      .join("&");
    this.backendService.getAllFilterInvoices(queryString).subscribe((res) => {
      if (res.success) {
        this.invoicesSource = res.data;
        this.invoicesSource.paginator = this.paginator;
      }
    });
  }

  checkPendingInvoices() {
    this.backendService.getAllPendingInvoices().subscribe((res) => {
      // console.log(res);
      if (res.success) {
        this.backendService.pendingData = res.data;
        this.openCheckPendingDialog();
      }
    });
  }

  onChanges(event) {
    if (event.target.value.length > 0) {
      const filterValue = event.target.value.toLowerCase();
      this.customerDropDown = this.tempCustomer.filter((option) => {
        return option.name.toLowerCase().includes(filterValue);
      });
    } else {
      this.customerDropDown = this.tempCustomer;
    }
  }

  tabIndex = 0;

  changeTab(event) {
    if(event.index == 0){
    this.showPendingBtn = true;
    }else{
      this.showPendingBtn = false;
    }

    this.tabIndex = event.index;
  }

  viewInvoice(data) {
    this.backendService.getInvoicesByIdService(data.invoiceId).subscribe(res =>{
      if(res.success){
        this.backendService.invoiceData = res.data;
        this.router.navigate(['viewInvoice'])
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
      }
    },(err: HttpErrorResponse) => {
			if (err.status == 500) {
				this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
				this.openInfoDialog();
			}
		});
  }

  deleteInvoice(data, id) {
    this.backendService.deleteInvoiceService(data.invoiceId).subscribe(
      (res) => {
        if (res.success) {
          const index = this.invoicesSource.data.indexOf(id);
          this.invoicesSource.data.splice(index, 1);
          this.invoicesSource._updateChangeSubscription(); // <-- Refresh the datasource
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  /*======================== SEARCH INVOICE AND CLEAR SEARCH CHECKBOX FUNCTIONS =========================== */
  search() {
    if (this.searchForm.invalid) {
      return;
    }
    let data = {
      customer: this.searchForm.value.customer,
      start: this.datePipe.transform(this.searchForm.value.start, environment.SystemTypes_Date_Format),
      end: this.datePipe.transform(this.searchForm.value.end, environment.SystemTypes_Date_Format),
      tax: this.searchForm.value.state,
      invoice: "Pending",
      type: this.searchForm.value.invoiceType,
    };
    this.getType = data.type;

    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    var queryString = Object.keys(data)
      .map((key) => key + "=" + data[key])
      .join("&");
    this.backendService.getAllInvoices(queryString).subscribe(
      (res) => {
        if (res.success) {
          if (res.data.length > 0) {
            res.data.forEach((element) => {
              if (element.customer != null) {
                element.customerName = element.customer;
              } else if (element.broker != null) {
                element.customerName = element.broker;
              }
            });
            this.searchInvData = res.data;
            this.dataSource = new MatTableDataSource(res.data);
            this.dataSource.paginator = this.paginator;
            this.billIds = [];
          } else {
            this.dataSource = res.data;
            this.errorMsgService.getInfoMessage(res.message);
            this.openInfoDialog();
          }
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("SEARCH INVOICE RESPONSE RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  clearForm() {
    this.searchForm.reset();
  }

  searchInvoice() {
    if (this.searchInvoiceForm.invalid) {
      return;
    }
    let data = {
      customerId: this.searchInvoiceForm.value.customer,
      start: this.datePipe.transform(
        this.searchInvoiceForm.value.start,
        environment.SystemTypes_Date_Format
      ),
      end: this.datePipe.transform(
        this.searchInvoiceForm.value.end,
        environment.SystemTypes_Date_Format
      ),
      // invoiceNumber: this.searchInvoiceForm.value.invoiceNo,
      invoice: "Generated",
      type: this.searchInvoiceForm.value.invoiceType,
      payment: this.searchInvoiceForm.value.paymentStatus,
      fixed: false,
    };
    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }
    data.fixed = false;
    var queryString = Object.keys(data)
      .map((key) => key + "=" + data[key])
      .join("&");
    this.backendService.getAllFilterInvoices(queryString).subscribe(
      (res) => {
        if (res.success) {
          if (res.data.length > 0) {
            this.invoicesSource = new MatTableDataSource(res.data);
            this.invoicesSource.paginator = this.paginator;
          } else {
            this.errorMsgService.getInfoMessage(res.message);
            this.openInfoDialog();
          }
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("SEARCH Generated INVOICE RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  clearInvoiceForm() {
    this.searchInvoiceForm.reset();
  }

  onChangeSelectAll(event) {
    if (this.searchInvData.length > 0) {
      if (event.checked) {
        this.dataSource.data.forEach((obj) => {
          obj.selected = true;
          this.billIds.push(obj.rwbId);
        });
      } else {
        this.dataSource.data.forEach((obj) => {
          obj.selected = false;
          this.billIds.pop(obj.rwbId);
        });
      }
    }
  }

  pushBill(event, data) {
    if (event.checked) {
      this.billIds.push(data.rwbId);
    } else {
      this.billIds.pop(data.rwbId);
    }
  }

  /*=============================== GENERATE INVOICE FUNCTIONS ================================= */

  generateInvoice() {
    if (this.billIds.length > 0) {
      this.backendService
        .generateInvoiceService(this.billIds, this.getType)
        .subscribe(
          (res) => {
            if (res.success) {
              // console.log(res);
              this.tempData = this.invoicesSource.data;
              this.tempData.push(res.data);
              this.invoicesSource = new MatTableDataSource(this.tempData);
              this.errorMsgService.getSuccessMessage(res.message);
              this.openSuccessDialog();
              let data = this.dataSource.data;
              for (let i = 0; i < data.length; i++) {
                for (let j = 0; j < this.billIds.length; i++) {
                  if (this.billIds[j] == data[i].rwbId) {
                    const index = this.dataSource.data.indexOf(i);
                    this.dataSource.data.splice(index, 1);
                    this.dataSource._updateChangeSubscription(); // <-- Refresh the datasource
                  }
                }
              }
            } else {
              this.errorMsgService.getErrorMessage(res.errorMessage);
              this.openErrorDialog();
            }
          },
          (err: HttpErrorResponse) => {
            if (err.status == 500) {
              this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
              this.openInfoDialog();
            }
          }
        );
    } else {
      this.errorMsgService.getInfoMessage(
        "Please Select any Bill to Generate Invoice"
      );
      this.openInfoDialog();
    }
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to open check pending dialog
  openCheckPendingDialog() {
    const dialogRef = this.dialog.open(CheckPendingModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to open check pending dialog
  openPaymentInfoDialog() {
    const dialogRef = this.dialog.open(PaymentInfoModalComponent, {
      width: "800px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }
}

export interface rwbs {
  rwbId: number;
  customer: string;
  route: string;
  taxProvince: string;
  amount: number;
  status: string;
}
