import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFixedRateInvoiceComponent } from './view-fixed-rate-invoice.component';

describe('ViewFixedRateInvoiceComponent', () => {
  let component: ViewFixedRateInvoiceComponent;
  let fixture: ComponentFixture<ViewFixedRateInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFixedRateInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFixedRateInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
