import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { BackEndService } from 'app/services/back-end.service';
import { EmailInvoiceModalComponent } from 'app/modals/email-invoice-modal/email-invoice-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ErrorMessageService } from 'app/services/error-message.service';
import { DatePipe } from '@angular/common';
import { ReceivePaymentModalComponent } from 'app/modals/receive-payment-modal/receive-payment-modal.component';
import { PaymentInfoModalComponent } from 'app/modals/payment-info-modal/payment-info-modal.component';

@Component({
	selector: 'app-view-fixed-rate-invoice',
	templateUrl: './view-fixed-rate-invoice.component.html',
	styleUrls: ['./view-fixed-rate-invoice.component.scss']
})
export class ViewFixedRateInvoiceComponent implements OnInit {

	displayedColumnD: string[] = [
		"departure",
		"vehicleNo",
		"StdKm",
		"variableRate",
		"amount",
	];

	dataSource: MatTableDataSource<any>;
	invData: any

	constructor(public dialog: MatDialog,
		public backendService: BackEndService,
		public errorMsgService: ErrorMessageService,
		public datePipe: DatePipe) { }

	ngOnInit() {
		let data = this.backendService.invoiceData;
		if (data != null) {
			this.invData = data;
			let i = 1;
			let date = this.invData.year + '/' + this.invData.month + '/01';
			let temp = this.datePipe.transform(date, "MMM-yyyy");
			data.lineItems.forEach(element => {
				element.period = temp;
				element.sNo = i++;
			});
			this.dataSource = data.lineItems;

		}
	}

	// function to trigger email invoice dialog
	openEmailInvoiceDialog() {
		const dialogRef = this.dialog.open(EmailInvoiceModalComponent);

		dialogRef.afterClosed().subscribe((result) => {
		});
	}

	printInvoice() {
		this.backendService
			.getInvoicesForPrint(this.invData.invoiceId)
			.subscribe((res) => {
				const newBlob = new Blob([res.toString()], {
					type: "application/pdf",
				});
				var downloadURL = window.URL.createObjectURL(res);
				window.open(downloadURL, "_blank");

				//console.log("COMPANY ADD RESPONSE", res);
			}, (err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
					this.openInfoDialog();
				}
			});
	}

	getPaymentInfo(data) {
		this.backendService.getInvoicesPaymentsService(data.invoiceId).subscribe(res => {
			if (res.success) {
				this.openPaymentInfoDialog(res.data);
			} else {
				this.errorMsgService.getErrorMessage(res.errorMessage);
				this.openErrorDialog();
			}
		},
			(err: HttpErrorResponse) => {
				if (err.status == 500) {
					this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
					this.openInfoDialog();
				}
			});
	}

	openReceiveDialogDialog(invDATA) {
		invDATA.type = "Normal";
		const dialogRef = this.dialog.open(ReceivePaymentModalComponent, {
			data: invDATA
		});


		dialogRef.afterClosed().subscribe((result) => {
		});
	}

	/************************ function to trigger info dialog ****************************/
	openInfoDialog(): void {
		const dialogRef = this.dialog.open(InfoModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to trigger error dialog
	openErrorDialog(): void {
		const dialogRef = this.dialog.open(ErrorModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to trigger success dialog
	openSuccessDialog(): void {
		const dialogRef = this.dialog.open(SuccessModalComponent, {
			width: "450px",
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}

	// function to open Payment Info dialog
	openPaymentInfoDialog(IData) {
		const dialogRef = this.dialog.open(PaymentInfoModalComponent, {
			width: "800px",
			data: IData
		});

		dialogRef.afterClosed().subscribe((result) => { });
	}
}