import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatDialog, MatTableDataSource, MatPaginator } from "@angular/material";
import { AddCityModalComponent } from "app/modals/add-city-modal/add-city-modal.component";
import { AddAreaModalComponent } from "app/modals/add-area-modal/add-area-modal.component";
import { environment } from "environments/environment";
import { BackEndService } from "app/services/back-end.service";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";
import { InfoModalComponent } from "app/modals/info-modal/info-modal.component";
import { ErrorMessageService } from "app/services/error-message.service";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "app-routes",
  templateUrl: "./routes.component.html",
  styleUrls: ["./routes.component.scss"]
})
export class RoutesComponent implements OnInit {

  stateTypeDropdownCode = environment.STATE_TYPE;
  cityTypeDropdownCode = environment.CITY_TYPE;
  areaTypeDropdownCode = environment.AREA_TYPE;

  stateDropDown: any;
  cityDropDown: any;
  areaDropDown: any;

  addIntercityRouteForm: FormGroup;
  addIntracityRouteForm: FormGroup;
  intercityColumns: string[] = ['routeId', 'from', 'to', 'distance', 'action'];
  interCityRoutes: MatTableDataSource<any>;
  intracityColumns: string[] = ['routeId', 'from', 'to', 'distance', 'action'];
  intraCityRoutes: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatPaginator, { static: true }) paginators: MatPaginator;

  constructor(public dialog: MatDialog,
    public backendService: BackEndService,
    public errorMsgService: ErrorMessageService) {

    this.addIntercityRouteForm = new FormGroup({
      routeFrom: new FormControl(null, Validators.required),
      routeTo: new FormControl(null, Validators.required),
      routeDistance: new FormControl(null, Validators.required)
    });
    this.addIntracityRouteForm = new FormGroup({
      routeFrom: new FormControl(null, Validators.required),
      routeTo: new FormControl(null, Validators.required),
      routeCity: new FormControl(null, Validators.required),
      routeDistance: new FormControl(null, Validators.required)
    });
  }

  ngOnInit() {
    this.getStateDropdown(this.stateTypeDropdownCode);
    this.getCityDropdown(this.cityTypeDropdownCode);
    this.getAllRoutes();
  }

  addIntercityRoute() {
    let routeData = {
      routeId: null,
      routeTo: this.addIntercityRouteForm.value.routeTo,
      routeFrom: this.addIntercityRouteForm.value.routeFrom,
      avgDistanceInKM: this.addIntercityRouteForm.value.routeDistance
    }
    this.backendService.addRouteService(routeData).subscribe(res => {
      if (res.success) {
        this.interCityRoutes.data.push(res.data);
        this.interCityRoutes.data.length + 1;
        this.interCityRoutes.filter = "";
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("ROUTE INTERCITY ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  addIntracityRoute() {
    let routeData = {
      routeId: null,
      routeTo: this.addIntracityRouteForm.value.routeTo,
      routeFrom: this.addIntracityRouteForm.value.routeFrom,
      avgDistanceInKM: this.addIntracityRouteForm.value.routeDistance
    }
    this.backendService.addRouteService(routeData).subscribe(res => {
      if (res.success) {
        this.intraCityRoutes.data.push(res.data)
        this.intraCityRoutes.data.length + 1;
        this.intraCityRoutes.filter = "";
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("ROUTE INTRACITY ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  getAllRoutes() {
    this.backendService.getRouteService().subscribe(res => {
      if (res.success) {
        if (res.data.length > 0) {
          let cityRoute = res.data.filter(x => x.routeFrom.description === "City")
          let areaRoute = res.data.filter(x => x.routeFrom.description !== "City")
          this.interCityRoutes = new MatTableDataSource(cityRoute)
          this.interCityRoutes.paginator = this.paginator;
          this.intraCityRoutes = new MatTableDataSource(areaRoute)
          this.intraCityRoutes.paginator = this.paginators;
        }
      }
    })
  }

  deleteInterCityRoute(data: any, id) {
    this.backendService.deleteRouteService(data.routeId).subscribe(res => {
      if (res.success) {
        const index = this.interCityRoutes.data.indexOf(id);
        this.interCityRoutes.data.splice(index, 1);
        this.interCityRoutes._updateChangeSubscription(); // <-- Refresh the datasource
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("DELETE ROUTE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  deleteIntraCityRoute(data: any, id) {
    this.backendService.deleteRouteService(data.routeId).subscribe(res => {
      if (res.success) {
        const index = this.intraCityRoutes.data.indexOf(id);
        this.intraCityRoutes.data.splice(index, 1);
        this.intraCityRoutes._updateChangeSubscription(); // <-- Refresh the datasource
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      } else {
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("DELETE ROUTE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.interCityRoutes.filter = filterValue;
  }

  applyAreaFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.interCityRoutes.filter = filterValue;
  }

  /* ============================== DROPDOWNS FUNCTIONS ========================================== */

  getAreaByCity(cityCode : string){
    this.getAreaDropdown(this.areaTypeDropdownCode, cityCode);
  }

  getStateDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.stateDropDown = res.data;
      this.backendService.stateData = this.stateDropDown;
    })
  }

  getCityDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.cityDropDown = res.data;
      this.backendService.citiesData = this.cityDropDown;
    })
  }

  getAreaDropdown(type: string, code: string) {
    this.backendService.getAreaDropdownService(type, code).subscribe(res => {
      this.areaDropDown = res.data;
    })
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  openAddCityDialog() {
    const dialogRef = this.dialog.open(AddCityModalComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getCityDropdown(this.cityTypeDropdownCode);
    });
  }

  openAddAreaDialog() {
    const dialogRef = this.dialog.open(AddAreaModalComponent);

    dialogRef.afterClosed().subscribe(result => {
      let code = sessionStorage.getItem("code");
      sessionStorage.removeItem("code");
      if(code != null)
      this.getAreaDropdown(this.areaTypeDropdownCode, code)
    });
  }

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}

