import { Component, OnInit, ViewChild } from '@angular/core';
import { BackEndService } from 'app/services/back-end.service';
import { FormGroup, FormControl } from '@angular/forms';
import { environment } from 'environments/environment';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-freight-rates',
  templateUrl: './freight-rates.component.html',
  styleUrls: ['./freight-rates.component.scss']
})
export class FreightRatesComponent implements OnInit {

  form: FormGroup;
  roadWayBills;
  environment = environment;

  constructor(private backEndService: BackEndService, public datePipe: DatePipe) {
    this.backEndService.getRoadWayBill('rateStatus=true').subscribe((res: any) => {
      this.roadWayBills = res.data;
    })
    this.form = new FormGroup({
      jobId: new FormControl(''),
      rwbId: new FormControl(''),
      start: new FormControl(''),
      end: new FormControl(''),
      rateStatus: new FormControl(false)
    })
  }

  ngOnInit() {
  }

  rate(incomes, type) {
    if (incomes) {
      let rwBill = incomes.filter(x => {
        return x.incomeType.value == environment.SystemTypes_FREIGHTRATE
      })
      if (rwBill.length > 0) {
        if (type == 'amount') {
          return rwBill[0].amount;
        } else {
          return rwBill[0].description;
        }
      } else {
        return ''
      }
    } else {
      return ''
    }
  }

  saveRate(rateValue, description, rwbId, incomes) {
    let income = {
      amount: rateValue.value,
      description: description.value,
      incomeId: null,
      incomeType: {
        code: "ITD",
        description: "Income Type",
        id: 1,
        status: "Active",
        value: "Freight rate"
      },
      roadwayBillId: rwbId
    }

    if (incomes) {
      let rwBill = incomes.filter(x => {
        return x.incomeType.value == environment.SystemTypes_FREIGHTRATE
      })
      if (rwBill.length > 0) {
        // Update
        income.incomeId = rwBill[0].incomeId;
        this.backEndService.updateIncome(income).subscribe((res: any) => {
          // Income Added
        })
      }
    } else {
      this.backEndService.addIncome(income).subscribe((res: any) => {
        // Income Added
        this.backEndService.getRoadWayBill('rateStatus=true').subscribe((res: any) => {
          this.roadWayBills = res.data;
        })
      })
    }

  }

  search() {
    this.form.get('start').patchValue(this.datePipe.transform(this.form.get('start').value, environment.SystemTypes_Date_Format))
    this.form.get('end').patchValue(this.datePipe.transform(this.form.get('end').value, environment.SystemTypes_Date_Format))
    let finalObject = { ...this.form.value }
    for (let prop in finalObject) {
      if (prop == 'rateStatus') {

      } else {
        if (!finalObject[prop]) {
          delete finalObject[prop];
        }
      }
    }
    var queryString = Object.keys(finalObject).map(key => key + '=' + finalObject[key]).join('&');
    this.backEndService.getRoadWayBill(queryString).subscribe((res: any) => {
      this.roadWayBills = res.data;
    })
  }

}