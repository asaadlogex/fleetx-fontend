import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreightRatesComponent } from './freight-rates.component';

describe('FreightRatesComponent', () => {
  let component: FreightRatesComponent;
  let fixture: ComponentFixture<FreightRatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreightRatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreightRatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
