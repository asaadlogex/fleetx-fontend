import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { BackEndService } from 'app/services/back-end.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { SuccessModalComponent } from 'app/modals/success-modal/success-modal.component';
import { MatDialog } from '@angular/material';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';
import { ErrorMessageService } from 'app/services/error-message.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-company-form',
  templateUrl: './create-company-form.component.html',
  styleUrls: ['./create-company-form.component.scss']
})


export class CreateCompanyFormComponent implements OnInit {

  businessTypeDropdownCode = environment.BUSINESS_TYPE;
  channelTypeDropdownCode = environment.CHANNEL_TYPE;
  formationTypeDropdownCode = environment.FORMATION_TYPE;
  supplierTypeDropdownCode = environment.SUPPLIER_TYPE;
  cityTypeDropdownCode = environment.CITY_TYPE;
  stateTypeDropdownCode = environment.STATE_TYPE;
  companyTypeDropdownCode = environment.COMPANY_TYPE;


  addCompanyForm: FormGroup;
  submitted = false;
  companyType = environment.SystemTypes_COMPANY;
  showSupplierType: boolean = false;
  company: any;
  channelTypeDropDown: any;
  businessTypeDropDown: any;
  supplierDropDown: any
  formationDropDown: any;
  stateDropDown : any;
  cityDropDown : any;
  companyTypeDropDown: any;
  documents = [
  ];
  addresses: FormArray;
  contacts: FormArray;

  constructor(private formBuilder: FormBuilder,
    public backendService: BackEndService,
    private router: Router,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService) {

    this.addCompanyForm = this.formBuilder.group({
      companyId: [''],
      companyStatus: ['Active', Validators.required],
      companyCode: [''],
      legalName: ['', Validators.required],
      fullName: ['', Validators.required],
      businessType: ['', Validators.required],
      strnNumber: [null],
      ntnNumber: [null],
      sraNumber: [null],
      praNumber: [null],
      kraNumber: [null],
      braNumber: [null],
      formation: [null],
      supplyType: [null],
      compContracts: this.formBuilder.array([]),
      address: this.formBuilder.array([this.createAddress()]),
      contact: this.formBuilder.array([]),
    });
  }

  /* =============================== ADD ADDRESS AND CONTACT IN COMPANY ARRAY FUNCTIONS ================================= */
  createAddress() {
    return this.formBuilder.group({
      addressId: [null],
      city: [null, Validators.required],
      street1: ['', Validators.required],
      street2: [''],
      country: ['Pakistan', Validators.required],
      zipCode: ['', Validators.required],
      state: [null, Validators.required]
    });
  }

  createContact() {
    return this.formBuilder.group({
      contactId: [null],
      channel: ['', Validators.required],
      data: ['', Validators.required]
    });
  }

  addNewAddress() {
    this.addresses = this.addCompanyForm.get('address') as FormArray;
    this.addresses.push(this.createAddress());
  }

  addNewContact() {
    this.contacts = this.addCompanyForm.get('contact') as FormArray;
    this.contacts.push(this.createContact());
  }

  /* ======================================= ONINIT COMPONENT FUNCTIONS ============================================ */
  ngOnInit() {

    //FETCH COMPANY DATA FOR UPDATING PROCESS
    let getCompany = JSON.parse(sessionStorage.getItem("getCompany"));
    sessionStorage.removeItem("getCompany");
    if (getCompany != null) {
      this.company = getCompany;
      this.addCompanyForm = this.formBuilder.group({
        companyId: [this.company.companyId],
        companyStatus: [this.company.status, Validators.required],
        legalName : ['', Validators.required],
        fullName : ['', Validators.required],
        companyCode: [this.company.companyCode],
        businessType: [this.company.businessType, Validators.required],
        strnNumber: [this.company.strnNumber],
        ntnNumber: [this.company.ntnNumber],
        sraNumber: [this.company.sra],
        praNumber: [this.company.pra],
        kraNumber: [this.company.kra],
        braNumber: [this.company.bra],
        formation: [this.company.formation],
        supplyType: [this.company.supplierType],
        compContracts: this.formBuilder.array([this.company.contracts]),
        address: this.formBuilder.array([]),
        contact: this.formBuilder.array([])
      });

      if (this.company.companyType.value == environment.SystemTypes_COMPANY) {
        this.companyType = environment.SystemTypes_COMPANY
        this.addCompanyForm.get('legalName').setValue(this.company.companyName)
      } else {
        this.companyType = environment.SystemTypes_INDIVIDUAL
        this.addCompanyForm.get('fullName').setValue(this.company.companyName)
      }

      if (this.company.businessType.value == environment.SystemTypes_SUPPLIER) {
        this.showSupplierType = true;
      }

      for (let i = 0; i < this.company.addresses.length; i++) {
        this.addresses = this.addCompanyForm.get('address') as FormArray;
        this.addresses.push(this.formBuilder.group({
          addressId: [this.company.addresses[i].addressId],
          city: [this.company.addresses[i].city, Validators.required],
          street1: [this.company.addresses[i].street1, Validators.required],
          street2: [this.company.addresses[i].street2],
          country: [this.company.addresses[i].country, Validators.required],
          zipCode: [this.company.addresses[i].zipCode, Validators.required],
          state: [this.company.addresses[i].state, Validators.required]
        }));
        // this.addresses.disable();
      }

      for (let i = 0; i < this.company.contacts.length; i++) {
        this.contacts = this.addCompanyForm.get('contact') as FormArray;
        this.contacts.push(this.formBuilder.group({
          contactId: [this.company.contacts[i].contactId],
          channel: [this.company.contacts[i].channel, Validators.required],
          data: [this.company.contacts[i].data]
        }));
      }
    }

    this.getBusinessTypeDropdown(this.businessTypeDropdownCode);
    this.getContactChannelDropdown(this.channelTypeDropdownCode);
    this.getformationDropdown(this.formationTypeDropdownCode);
    this.getSupplierDropdown(this.supplierTypeDropdownCode)
    this.getCityDropdown(this.cityTypeDropdownCode);
    this.getStateDropdown(this.stateTypeDropdownCode);
    this.getCompanyTypeDropdown(this.companyTypeDropdownCode);

  }

  //FUNCTION TO SHOW SUPPLIER DROPDOWN
  showSupplierTypes(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_SUPPLIER) {
        this.showSupplierType = true;
      } else {
        this.showSupplierType = false;
      }
    }
  }

  //Function to Delete a Contact
  deleteContact(data, index) {
    if (index > 0) {
      this.contacts = this.addCompanyForm.get('contact') as FormArray;
      this.contacts.removeAt(index);
    }
  }

  //Function to Delete a Address
  deleteAddress(data, index) {
    // if (index > 0) {
      this.addresses = this.addCompanyForm.get('address') as FormArray;
      this.addresses.removeAt(index);
    // }

  }

  /* =============================== ADD UPDATE DELETE COMPANY FUNCTIONS ================================= */
  submitCompany() {
    if (this.companyType == environment.SystemTypes_COMPANY) {
      this.addCompanyForm.get("fullName").disable();
    }
    if (this.companyType == environment.SystemTypes_INDIVIDUAL) {
      this.addCompanyForm.get("legalName").disable();
    }

    if (this.addCompanyForm.invalid) {
      return;
    }

    // for (let i = 0; i < this.addCompanyForm.value.address.length; i++) {
    //   if (this.addCompanyForm.value.address[i].zipCode != null)
    //     this.addCompanyForm.value.address[i].zipCode = this.addCompanyForm.value.address[i].zipCode.toString();
    // }

    // for (let i = 0; i < this.addCompanyForm.value.contact.length; i++) {
    //   if (this.addCompanyForm.value.contact[i].data != null)
    //     this.addCompanyForm.value.contact[i].data = this.addCompanyForm.value.contact[i].data.toString();
    // }

    this.company = {
      companyId: this.addCompanyForm.value.companyId,
      companyName : '',
      companyCode : '',
      formation: this.addCompanyForm.value.formation,
      ntnNumber: this.addCompanyForm.value.ntnNumber,
      strnNumber: this.addCompanyForm.value.strnNumber,
      status: this.addCompanyForm.value.companyStatus,
      isInternal: true,
      businessType: this.addCompanyForm.value.businessType,
      supplierType : null,
      companyType : null,
      sra: this.addCompanyForm.value.sraNumber,
      pra: this.addCompanyForm.value.praNumber,
      kra: this.addCompanyForm.value.kraNumber,
      bra: this.addCompanyForm.value.braNumber,
      addresses: this.addCompanyForm.getRawValue().address,
      contacts: this.addCompanyForm.value.contact,
      contracts: this.addCompanyForm.value.compContracts
    }

    if (this.companyType == environment.SystemTypes_COMPANY) {
      this.company.companyName = this.addCompanyForm.value.legalName;
      for(let i=0; i<this.companyTypeDropDown.length; i++){
        if(this.companyTypeDropDown[i].value == environment.SystemTypes_COMPANY){
          this.company.companyType = this.companyTypeDropDown[i];
        }
      }
    }else{
      this.company.companyName = this.addCompanyForm.value.fullName;
      for(let i=0; i<this.companyTypeDropDown.length; i++){
        if(this.companyTypeDropDown[i].value == environment.SystemTypes_INDIVIDUAL){
          this.company.companyType = this.companyTypeDropDown[i];
        }
      }
    }
    
    if (this.company.businessType.value == environment.SystemTypes_SUPPLIER) {
      if (this.addCompanyForm.value.supplyType != null)
        this.company.supplierType = this.addCompanyForm.value.supplyType;
    }
    //console.log(this.company);
    if (this.addCompanyForm.value.companyId != null && this.addCompanyForm.value.companyId != '') {
      this.updateCompany();
    } else {
      this.addCompany();
    }
  }

  //FUNCTION TO ADD NEW COMPANY
  addCompany() {
    this.backendService.addCompanyService(this.company).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
        this.addCompanyForm.get("companyId").setValue(res.data);
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("COMPANY ADD RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  //FUNCTION TO UPDATE A COMPANY
  updateCompany() {
    this.backendService.updateCompanyService(this.company).subscribe(res => {
      if (res.success) {
        this.errorMsgService.getSuccessMessage(res.message);
        this.openSuccessDialog();
      }else{
        this.errorMsgService.getErrorMessage(res.errorMessage);
        this.openErrorDialog();
      }
      //console.log("COMPANY UPDATE RESPONSE", res);
    }, (err: HttpErrorResponse) => {
      if (err.status == 500) {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
        this.openInfoDialog();
      }
    });
  }

  //FUNCTION TO DELETE A COMPANY
  deleteCompany() {
    if (this.company.companyId != null || this.company.companyId != "") {
      this.backendService.deleteCompanyService(this.company.companyId).subscribe(res => {
        if (res.success) {
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
          this.router.navigate(['companies'])
        }else{
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("COMPANY DELETE RESPONSE", res);
      }, (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG)
          this.openInfoDialog();
        }
      });
    }
  }

  /* =============================== DROPDOWNS FUNCTIONS ================================= */
  getBusinessTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.businessTypeDropDown = res.data;
    })
  }

  getContactChannelDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.channelTypeDropDown = res.data;
      if(this.addCompanyForm.value.companyId == ""){
        for(let i=0; i<this.channelTypeDropDown.length; i++){
          this.contacts = this.addCompanyForm.get('contact') as FormArray;
          this.contacts.push(this.formBuilder.group({
          contactId: [null],
          channel: [this.channelTypeDropDown[i], Validators.required],
          data: ['']
        }));
        }
      }
    })
  }

  getformationDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.formationDropDown = res.data;
    })
  }

  getSupplierDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.supplierDropDown = res.data;
    })
  }

  getStateDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.stateDropDown = res.data;
    })
  }

  getCityDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.cityDropDown = res.data;
    })
  }

  getCompanyTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe(res => {
      this.companyTypeDropDown = res.data;
    })
  }

  /* =============================== COMPARE DROPDOWN FUNCTION ================================= */
  compareDropdownType(o1, o2): boolean {
    if (o2 != null)
      return o1.value === o2.value;
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
