import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { LoaderService } from 'app/services/loader.service';

@Component({
  selector: 'app-loading-bar',
  templateUrl: './loading-bar.component.html',
  styleUrls: ['./loading-bar.component.scss']
})
export class LoadingBarComponent implements OnInit {
  
  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  isLoading: Subject<boolean> = this.loaderService.isLoading;
  
  constructor(public loaderService : LoaderService) { }

  ngOnInit() {
  }

}
