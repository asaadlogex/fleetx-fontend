import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyContractListComponent } from './company-contract-list.component';

describe('CompanyContractListComponent', () => {
  let component: CompanyContractListComponent;
  let fixture: ComponentFixture<CompanyContractListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyContractListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyContractListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
