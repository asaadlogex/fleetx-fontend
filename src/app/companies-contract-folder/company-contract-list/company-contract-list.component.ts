import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { BackEndService } from "app/services/back-end.service";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormControl } from "@angular/forms";

export interface CompanyContract {
  name: string;
  businessType: string;
  address: string;
  verified: string;
}

@Component({
  selector: "app-company-contract-list",
  templateUrl: "./company-contract-list.component.html",
  styleUrls: ["./company-contract-list.component.scss"],
})
export class CompanyContractListComponent implements OnInit {
  displayedColumns: string[] = ["name", "businessType", "address", "status"];
  dataSource: MatTableDataSource<any>;
  companyContracts: any = [];

  form: FormGroup;

  search() {}

  ClearForm() {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public backEnd: BackEndService, private router: Router) {
    this.form = new FormGroup({
      jobId: new FormControl(null),
      rwbId: new FormControl(null),
      lic: new FormControl(null),
      start: new FormControl(null),
      end: new FormControl(null),
      jobStatus: new FormControl(null),
      ownerShipStatus: new FormControl(null),
    });
  }

  ngOnInit() {
    this.getAllContracts();
  }

  getAllContracts() {
    this.backEnd.getAllCompanyContractService().subscribe((res) => {
      //console.log(res);
      if (res.success) {
        this.companyContracts = res.data;
        const ELEMENT_DATA: CompanyContract[] = this.companyContracts;
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      }
    });
  }

  findContractById(data: any) {
    this.backEnd
      .getCompanyContractById(data.companyContractId)
      .subscribe((res) => {
        if (res.data != null || res.data != "") {
          sessionStorage.setItem(
            "getCompanyContract",
            JSON.stringify(res.data)
          );
          this.router.navigate(["companyContract/edit"]);
        }
      });
  }
}
