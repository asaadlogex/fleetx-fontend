import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, FormArray, Validators } from "@angular/forms";
import { environment } from "environments/environment";
import { BackEndService } from "app/services/back-end.service";
import { Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { MatDialog, MatRadioChange } from "@angular/material";
import { ErrorMessageService } from "app/services/error-message.service";
import { HttpErrorResponse } from "@angular/common/http";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";
import { InfoModalComponent } from "app/modals/info-modal/info-modal.component";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource, MatTable } from "@angular/material/table";
import { filter } from "rxjs-compat/operator/filter";

export interface RouteTable {
  status: string;
  startDate: string;
  endDate: string;
  amount: string;
  delete: any;
}

@Component({
  selector: "app-company-contract",
  templateUrl: "./company-contract.component.html",
  styleUrls: ["./company-contract.component.scss"],
})
export class CompanyContractComponent implements OnInit {
  // ViewChild Decorator for Pagination
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  /* =============================== ENVIRONMENT DROPDOWNS VARIABLE ================================= */

  cityTypeDropdownCode = environment.CITY_TYPE;
  contractTypeDropdownCode = environment.CONTRACT_TYPE;
  tailerSizeCode = environment.TRAILER_SIZE_TYPE;
  tailerTypeCode = environment.TRAILER_TYPE;

  /* =============================== VARIABLES ================================= */

  companyContractForm: FormGroup;
  companiesDropDown: any;
  isArea = "Intercity";
  contracts: any;
  cityDropDown: any;
  customerDropDown: any;
  contractTypeDropDown: any;
  customerRouteDropDown: any;
  assetDropDown: any;
  trailerSizeDropDown: any;
  trailerTypeDropDown: any;
  supplierDropDown: any;
  routeDropDown: any;
  routes: any = [];
  fuelCards: any = [];
  combinations: any = [];
  combinationList: any = [];
  showDedicated = false;
  showPerTon = false;
  showAddRouteForm = false;
  showRouteName: string = null;

  // *********** Table Filter Options - Start *************************//

  displayedColumns: string[] = [
    "startDate",
    "endDate",
    "trailerType",
    "trailerSize",
    "amount",
    "status",
    "delete",
  ];
  dataSource: MatTableDataSource<any>;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    public backendService: BackEndService,
    private formBuilder: FormBuilder,
    private router: Router,
    public datePipe: DatePipe,
    public dialog: MatDialog,
    public errorMsgService: ErrorMessageService
  ) {
    this.companyContractForm = formBuilder.group({
      contractId: [""],
      owner: [""],
      customer: ["", Validators.required],
      contStartDate: ["", Validators.required],
      contEndDate: [""],
      contType: [null, Validators.required],
      fixRate: [null, Validators.required],
      detCharges: [null, Validators.required],
      haltCharges: [null, Validators.required],
      route: [""],
      routeCity: [null],
      routeDistance: [""],
      routeRate: [null],
      routeTotalRate: [null],
      routePerTon: [null],
      rateID: [null],
      rateContractId: [null],
      rateStart: [""],
      rateEnd: [""],
      routeRateStatus: ["Active"],
      trailerSize: [null],
      trailerType: [null],
      minWeight: [null],
      maxWeight: [null],
      fuelCard: [""],
      fuelLimit: [""],
      fuelSupp: [""],
      fuelStatus: [""],
      fuelExpiry: [""],
      assets: [""],
      routeList: this.formBuilder.array([]),
      assetCombinationList: this.formBuilder.array([]),
      showCombination: this.formBuilder.array([]),
      routeDedicatedList: this.formBuilder.array([]),
      routePerTonList: this.formBuilder.array([]),
      status: ["Active"],
      searchRoute: ["", Validators.required],
      searchStart: [""],
      searchEnd: [""],
    });
  }

  /* =============================== ONINIT COMPONENT FUNCTIONS ================================= */

  ngOnInit() {
    this.getCityTypeDropdown(this.cityTypeDropdownCode);
    this.getContractTypeDropdown(this.contractTypeDropdownCode);
    this.getTrailerSizeDropdown(this.tailerSizeCode);
    this.getTrailerTypeDropdown(this.tailerTypeCode);
    this.getCompaniesDropdown();
    this.getCustomerDropdown();
    this.getRouteDropdown();
    this.getAssetCombinationDropdown();
    this.getFuelSupplierDropdown();

    let getCompanyContract = JSON.parse(
      sessionStorage.getItem("getCompanyContract")
    );
    sessionStorage.removeItem("getCompanyContract");
    if (getCompanyContract != null) {
      this.contracts = getCompanyContract;
      this.companyContractForm = this.formBuilder.group({
        contractId: [this.contracts.companyContractId],
        owner: [""],
        customer: [this.contracts.customerId, Validators.required],
        contStartDate: [this.contracts.contractStart, Validators.required],
        contEndDate: [this.contracts.contractEnd],
        contType: [this.contracts.contractType, Validators.required],
        fixRate: [this.contracts.fixedRate],
        detCharges: [this.contracts.detentionRate],
        haltCharges: [this.contracts.haltingRate],
        route: [""],
        routeCity: [null],
        routeDistance: [""],
        routeRate: [null],
        routeTotalRate: [null],
        routePerTon: [null],
        rateID: [null],
        rateContractId: [null],
        rateStart: [""],
        rateEnd: [""],
        trailerSize: [null],
        trailerType: [null],
        minWeight: [null],
        maxWeight: [null],
        routeRateStatus: ["Active"],
        fuelCard: [""],
        fuelLimit: [""],
        fuelSupp: [""],
        fuelStatus: [""],
        fuelExpiry: [""],
        assets: [""],
        routeList: this.formBuilder.array([]),
        assetCombinationList: this.formBuilder.array([]),
        showCombination: this.formBuilder.array([]),
        routeDedicatedList: this.formBuilder.array([]),
        routePerTonList: this.formBuilder.array([]),
        status: [this.contracts.contractStatus],
        searchRoute: ["", Validators.required],
        searchStart: [""],
        searchEnd: [""],
      });

      if (
        this.contracts.contractType.value == environment.SystemTypes_DEDICATED
      ) {
        this.showDedicated = true;
        for (let i = 0; i < this.contracts.routeRateContracts.length; i++) {
          this.routes = this.companyContractForm.get(
            "routeDedicatedList"
          ) as FormArray;
          this.routes.push(
            this.formBuilder.group({
              routeRateContractId: [
                this.contracts.routeRateContracts[i].routeRateContractId,
              ],
              route: [this.contracts.routeRateContracts[i].route],
              routeRates: [this.contracts.routeRateContracts[i].routeRates],
              status: [this.contracts.routeRateContracts[i].status],
            })
          );
        }

        if (this.contracts.assetCombinationId != null) {
          for (let i = 0; i < this.contracts.assetCombinationId.length; i++) {
            this.combinations = this.companyContractForm.get(
              "assetCombinationList"
            ) as FormArray;
            this.combinations.push(
              this.formBuilder.control(this.contracts.assetCombinationId[i])
            );
            this.getAssetCombinationById(this.contracts.assetCombinationId[i]);
          }
        }
      }
      if (this.contracts.contractType.value == environment.SystemTypes_PERTON) {
        this.showPerTon = true;
        for (let i = 0; i < this.contracts.routeRateContracts.length; i++) {
          this.routes = this.companyContractForm.get(
            "routePerTonList"
          ) as FormArray;
          this.routes.push(
            this.formBuilder.group({
              routeRateContractId: [
                this.contracts.routeRateContracts[i].routeRateContractId,
              ],
              route: [this.contracts.routeRateContracts[i].route],
              routeRates: [this.contracts.routeRateContracts[i].routeRates],
              status: [this.contracts.routeRateContracts[i].status],
            })
          );
        }
      } else {
        for (let i = 0; i < this.contracts.routeRateContracts.length; i++) {
          this.routes = this.companyContractForm.get("routeList") as FormArray;
          this.routes.push(
            this.formBuilder.group({
              routeRateContractId: [
                this.contracts.routeRateContracts[i].routeRateContractId,
              ],
              route: [this.contracts.routeRateContracts[i].route],
              routeRates: [this.contracts.routeRateContracts[i].routeRates],
              status: [this.contracts.routeRateContracts[i].status],
            })
          );
        }
      }
    }
  }

  /* =============================== ADD UPDATE DELETE COMPANY CONTRACTS FUNCTIONS ================================= */

  submitContract() {
    this.companyContractForm.get("searchRoute").disable();
    if (this.companyContractForm.invalid) {
      return;
    }
    this.contracts = {
      companyContractId: this.companyContractForm.value.contractId,
      adminCompanyId: this.companyContractForm.getRawValue().owner.companyId,
      customerId: this.companyContractForm.value.customer.companyId,
      contractStart: this.datePipe.transform(
        this.companyContractForm.value.contStartDate,
        environment.SystemTypes_Date_Format
      ),
      contractEnd: this.datePipe.transform(
        this.companyContractForm.value.contEndDate,
        environment.SystemTypes_Date_Format
      ),
      contractType: this.companyContractForm.value.contType,
      fixedRate: this.companyContractForm.value.fixRate,
      detentionRate: this.companyContractForm.value.detCharges,
      haltingRate: this.companyContractForm.value.haltCharges,
      assetCombinationId: this.companyContractForm.value.assetCombinationList,
      routeRateContracts: [],
      contractStatus: this.companyContractForm.value.status,
    };

    // if(this.contracts.contractType.value == environment.SystemTypes_DEDICATED){
    //     this.companyContractForm.get('detCharges').disable;
    //     this.companyContractForm.get('haltCharges').disable;
    // }else{
    //     this.companyContractForm.get('fixRate').disable;
    // }

    if (this.contracts.contractType.value == environment.SystemTypes_DEDICATED) {
      this.contracts.routeRateContracts = this.companyContractForm.value.routeDedicatedList;
    } else {
      this.contracts.routeRateContracts = this.companyContractForm.value.routeList;
    }

    if (this.companyContractForm.value.contractId == "") {
      this.addContract();
    } else {
      this.updateContract();
    }
  }

  addContract() {
    this.backendService.addCompanyContractService(this.contracts).subscribe(
      (res) => {
        if (res.success) {
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
          this.companyContractForm
            .get("contractId")
            .setValue(res.data.companyContractId);
          this.companyContractForm.get("searchRoute").enable();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        // console.log("ADD CONTRACT RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  updateContract() {
    this.backendService.updateCompanyContractService(this.contracts).subscribe(
      (res) => {
        if (res.success) {
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
          this.companyContractForm.get("searchRoute").enable();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("CONTRACT UPDATE RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  deleteContract() {
    if (
      this.contracts.companyContractId != null ||
      this.contracts.companyContractId != ""
    ) {
      this.backendService
        .deleteCompanyContractService(this.contracts.companyContractId)
        .subscribe(
          (res) => {
            if (res.success) {
              this.router.navigate(["companyContract"]);
              this.errorMsgService.getSuccessMessage(res.message);
              this.openSuccessDialog();
            } else {
              this.errorMsgService.getErrorMessage(res.errorMessage);
              this.openErrorDialog();
            }
            //console.log("contract delete RESPONSE", res);
          },
          (err: HttpErrorResponse) => {
            if (err.status == 500) {
              this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
              this.openInfoDialog();
            }
          }
        );
    }
  }

  /* ==================== HIDE AND SHOW DEDICATED AND DISTANCE FIELDS FUNCTIONS ========================= */

  showDedicationFields(e) {
    if (e != undefined) {
      if (e.value == environment.SystemTypes_DEDICATED) {
        this.showDedicated = true;
        this.companyContractForm.get('fixRate').enable();
        this.companyContractForm.get('detCharges').disable();
        this.companyContractForm.get('haltCharges').disable();
      } else {
        this.showDedicated = false;
        this.companyContractForm.get('fixRate').disable();
        this.companyContractForm.get('detCharges').enable();
        this.companyContractForm.get('haltCharges').enable();
      }

      if (e.value == environment.SystemTypes_PERTON) {
        this.showPerTon = true;
      } else {
        this.showPerTon = false;
      }
    }
    // this.companyContractForm.get("route").reset();
    // this.companyContractForm.get("routeDistance").reset();
    // this.companyContractForm.get("routeRate").reset();
    // this.companyContractForm.get("routeTotalRate").reset();
  }

  showDistance(e) {
    if (e != undefined) {
      this.companyContractForm.get("routeDistance").setValue(e.avgDistanceInKM);
    } else {
      this.companyContractForm.get("routeDistance").setValue(null);
    }
  }

  assignTotalRate(val) {
    if (this.companyContractForm.getRawValue().routeDistance != null) {
      this.companyContractForm
        .get("routeTotalRate")
        .setValue(val * this.companyContractForm.getRawValue().routeDistance);
    }
  }

  getRouteByCity(e) {
    if (e != undefined) {
      this.backendService.getRouteByCitiesService(e.code).subscribe((res) => {
        this.routeDropDown = res.data;
      });
    }
  }

  /* =============================== ADD REMOVE ROUTES FUNCTIONS ================================= */

  addRoute() {
    return this.formBuilder.group({
      route: [this.companyContractForm.value.route],
      ratePerKm: [this.companyContractForm.value.routeRate],
      totalAmount: [this.companyContractForm.value.routeTotalRate],
    });
  }

  deleteRouteRate(index: number, rateDAta) {
    this.backendService.deleteRouteRateService(rateDAta.routeRateId).subscribe(
      (res) => {
        if (res.success) {
          const rates = this.dataSource.data;
          rates.splice(
            this.paginator.pageIndex * this.paginator.pageSize + index,
            1
          );
          this.dataSource.data = rates;
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("Route Rate Delete RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  pushRouteInArray() {
    let cnDate = new Date(this.companyContractForm.value.contStartDate);
    cnDate.setHours(0, 0, 0, 0);
    if (this.companyContractForm.value.rateStart < cnDate) {
      this.errorMsgService.getErrorMessage(
        "Rate Effective Date must be greater then or equals to Contract Start Date"
      );
      this.openErrorDialog();
    } else {
      let data = {
        routeRateContractId: this.companyContractForm.value.rateContractId,
        route: this.companyContractForm.value.route,
        routeRates: [
          {
            routeRateId: this.companyContractForm.value.rateID,
            ratePerKm: this.companyContractForm.value.routeRate,
            effectiveFrom: this.datePipe.transform(
              this.companyContractForm.value.rateStart,
              environment.SystemTypes_Date_Format
            ),
            effectiveTill: this.datePipe.transform(
              this.companyContractForm.value.rateEnd,
              environment.SystemTypes_Date_Format
            ),
            trailerSize: this.companyContractForm.value.trailerSize,
            trailerType: this.companyContractForm.value.trailerType,
            weightLow: this.companyContractForm.value.minWeight,
            weightHigh: this.companyContractForm.value.maxWeight,
            totalAmount: this.companyContractForm.value.routeTotalRate,
            ratePerTon: this.companyContractForm.value.routePerTon,
            status: this.companyContractForm.value.routeRateStatus,
          },
        ],
        status: "Active",
      };

      this.backendService
        .addRouteRateService(data, this.companyContractForm.value.contractId)
        .subscribe((res) => {
          if (res.success) {
            this.companyContractForm.get("searchRoute").setValue(data.route);
            this.searchRates();
            this.errorMsgService.getSuccessMessage(res.message);
            this.openSuccessDialog();
            this.showAddRouteForm = false;
            this.companyContractForm.get("route").reset();
            this.companyContractForm.get("rateStart").reset();
            this.companyContractForm.get("rateEnd").reset();
            this.companyContractForm.get("routeRate").reset();
            this.companyContractForm.get("trailerSize").reset();
            this.companyContractForm.get("trailerType").reset();
            this.companyContractForm.get("minWeight").reset();
            this.companyContractForm.get("maxWeight").reset();
          } else {
            this.errorMsgService.getErrorMessage(res.errorMessage);
            this.openErrorDialog();
          }
        });
    }
    // else {
    //   if (this.companyContractForm.value.routeList.length > 0) {
    //     let index = -1;
    //     let routeExists;
    //     routeExists = this.companyContractForm.value.routeList.some(x => {
    //       index++;
    //       return x.route.routeId == this.companyContractForm.value.route.routeId
    //     })
    //     if (routeExists) {
    //       this.companyContractForm.value.routeList[index].routeRates.push({
    //         routeRateId: this.companyContractForm.value.rateID,
    //         ratePerKm: this.companyContractForm.value.routeRate,
    //         effectiveFrom: this.datePipe.transform(this.companyContractForm.value.rateStart, environment.SystemTypes_Date_Format),
    //         effectiveTill: this.datePipe.transform(this.companyContractForm.value.rateEnd, environment.SystemTypes_Date_Format),
    //         trailerSize: this.companyContractForm.value.trailerSize,
    //         trailerType: this.companyContractForm.value.trailerType,
    //         weightLow: this.companyContractForm.value.minWeight,
    //         weightHigh: this.companyContractForm.value.maxWeight,
    //         totalAmount: this.companyContractForm.value.routeTotalRate,
    //         status: this.companyContractForm.value.routeRateStatus
    //       })
    //     } else {
    //       this.routes = <FormArray>this.companyContractForm.get('routeList');
    //       this.routes.push(this.formBuilder.group({
    //         routeRateContractId: [this.companyContractForm.value.rateContractId],
    //         route: [this.companyContractForm.value.route],
    //         routeRates: [[{
    //           routeRateId: this.companyContractForm.value.rateID,
    //           ratePerKm: this.companyContractForm.value.routeRate,
    //           effectiveFrom: this.datePipe.transform(this.companyContractForm.value.rateStart, environment.SystemTypes_Date_Format),
    //           effectiveTill: this.datePipe.transform(this.companyContractForm.value.rateEnd, environment.SystemTypes_Date_Format),
    //           trailerSize: this.companyContractForm.value.trailerSize,
    //           trailerType: this.companyContractForm.value.trailerType,
    //           weightLow: this.companyContractForm.value.minWeight,
    //           weightHigh: this.companyContractForm.value.maxWeight,
    //           totalAmount: this.companyContractForm.value.routeTotalRate,
    //           status: this.companyContractForm.value.routeRateStatus
    //         }]],
    //         status: ['Active']
    //       }));
    //     }
    //     index = -1;
    //   } else {
    //     this.routes = <FormArray>this.companyContractForm.get('routeList');
    //     this.routes.push(this.formBuilder.group({
    //       routeRateContractId: [this.companyContractForm.value.rateContractId],
    //       route: [this.companyContractForm.value.route],
    //       routeRates: [[{
    //         routeRateId: this.companyContractForm.value.rateID,
    //         ratePerKm: this.companyContractForm.value.routeRate,
    //         effectiveFrom: this.datePipe.transform(this.companyContractForm.value.rateStart, environment.SystemTypes_Date_Format),
    //         effectiveTill: this.datePipe.transform(this.companyContractForm.value.rateEnd, environment.SystemTypes_Date_Format),
    //         totalAmount: this.companyContractForm.value.routeTotalRate,
    //         status: this.companyContractForm.value.routeRateStatus
    //       }]],
    //       status: ['Active']
    //     }));
    //   }
    //   this.companyContractForm.get("route").reset();
    //   this.companyContractForm.get("rateStart").reset();
    //   this.companyContractForm.get("rateEnd").reset();
    //   this.companyContractForm.get("routeDistance").reset();
    //   this.companyContractForm.get("routeRate").reset();
    //   this.companyContractForm.get("routeTotalRate").reset();
    // }
  }

  pushDedicatedRouteInArray() {
    let cnDate = new Date(this.companyContractForm.value.contStartDate);
    cnDate.setHours(0, 0, 0, 0);
    if (this.companyContractForm.value.rateStart < cnDate) {
      this.errorMsgService.getErrorMessage(
        "Rate Effective Date must be greater then or equals to Contract Start Date"
      );
      this.openErrorDialog();
    } else {
      if (this.companyContractForm.value.routeDedicatedList.length > 0) {
        let index = -1;
        let routeExists;
        routeExists = this.companyContractForm.value.routeDedicatedList.some(
          (x) => {
            index++;
            return (
              x.route.routeId == this.companyContractForm.value.route.routeId
            );
          }
        );
        if (routeExists) {
          this.companyContractForm.value.routeDedicatedList[
            index
          ].routeRates.push({
            routeRateId: this.companyContractForm.value.rateID,
            ratePerKm: this.companyContractForm.value.routeRate,
            effectiveFrom: this.datePipe.transform(
              this.companyContractForm.value.rateStart,
              environment.SystemTypes_Date_Format
            ),
            effectiveTill: this.datePipe.transform(
              this.companyContractForm.value.rateEnd,
              environment.SystemTypes_Date_Format
            ),
            totalAmount: this.companyContractForm.value.routeTotalRate,
            status: this.companyContractForm.value.routeRateStatus,
          });
        } else {
          this.routes = <FormArray>(
            this.companyContractForm.get("routeDedicatedList")
          );
          this.routes.push(
            this.formBuilder.group({
              route: [this.companyContractForm.value.route],
              routeRates: [
                [
                  {
                    routeRateId: this.companyContractForm.value.rateID,
                    ratePerKm: this.companyContractForm.value.routeRate,
                    effectiveFrom: this.datePipe.transform(
                      this.companyContractForm.value.rateStart,
                      environment.SystemTypes_Date_Format
                    ),
                    effectiveTill: this.datePipe.transform(
                      this.companyContractForm.value.rateEnd,
                      environment.SystemTypes_Date_Format
                    ),
                    totalAmount: this.companyContractForm.value.routeTotalRate,
                    status: this.companyContractForm.value.routeRateStatus,
                  },
                ],
              ],
              status: ["Active"],
            })
          );
        }
        index = -1;
      } else {
        this.routes = <FormArray>(
          this.companyContractForm.get("routeDedicatedList")
        );
        this.routes.push(
          this.formBuilder.group({
            route: [this.companyContractForm.value.route],
            routeRates: [
              [
                {
                  routeRateId: this.companyContractForm.value.rateID,
                  ratePerKm: this.companyContractForm.value.routeRate,
                  effectiveFrom: this.datePipe.transform(
                    this.companyContractForm.value.rateStart,
                    environment.SystemTypes_Date_Format
                  ),
                  effectiveTill: this.datePipe.transform(
                    this.companyContractForm.value.rateEnd,
                    environment.SystemTypes_Date_Format
                  ),
                  totalAmount: this.companyContractForm.value.routeTotalRate,
                  status: this.companyContractForm.value.routeRateStatus,
                },
              ],
            ],
            status: ["Active"],
          })
        );
      }
      this.companyContractForm.get("route").reset();
      this.companyContractForm.get("rateStart").reset();
      this.companyContractForm.get("rateEnd").reset();
      this.companyContractForm.get("routeDistance").reset();
      this.companyContractForm.get("routeRate").reset();
      this.companyContractForm.get("routeTotalRate").reset();
    }
  }

  removeRoute(id: any) {
    this.routes = <FormArray>this.companyContractForm.get("routeList");
    this.routes.removeAt(id);
  }

  removeDedicatedRoute(id: any) {
    this.routes = <FormArray>this.companyContractForm.get("routeDedicatedList");
    this.routes.removeAt(id);
  }

  removeRates(id: any, data) {
    this.routes = <FormArray>this.companyContractForm.get("routeList");
    this.routes = this.routes.at(id).value.routeRates;
    this.routes.forEach((item, index) => {
      if (item === data) this.routes.splice(index, 1);
    });
  }

  removeDedicatedRates(id: any, data) {
    this.routes = <FormArray>this.companyContractForm.get("routeDedicatedList");
    this.routes = this.routes.at(id).value.routeRates;
    this.routes.forEach((item, index) => {
      if (item === data) this.routes.splice(index, 1);
    });
  }

  searchRates() {
    let rateData = {
      customerId: this.companyContractForm.value.customer.companyId,
      route: this.companyContractForm.value.searchRoute.routeId,
      startDate: this.datePipe.transform(
        this.companyContractForm.value.searchStart,
        environment.SystemTypes_Date_Format
      ),
      endDate: this.datePipe.transform(
        this.companyContractForm.value.searchEnd,
        environment.SystemTypes_Date_Format
      ),
    };
    this.backendService.getRouteRateService(rateData).subscribe((res) => {
      if (res.success) {
        this.dataSource = new MatTableDataSource<RouteTable>(res.data);
        this.dataSource.paginator = this.paginator;
        this.showRouteName =
          this.companyContractForm.value.searchRoute.routeFrom.value +
          "-" +
          this.companyContractForm.value.searchRoute.routeTo.value;
      } else {
        this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
        this.openInfoDialog();
      }
      if (res.data.length == 0) {
        this.errorMsgService.getInfoMessage(res.message);
        this.openInfoDialog();
      }
    });
  }

  pushCombinationInArray() {
    this.combinations = <FormArray>(
      this.companyContractForm.get("assetCombinationList")
    );
    this.combinations.push(
      this.formBuilder.control(
        this.companyContractForm.get("assets").value.assetCombinationId
      )
    );
    this.combinationList = <FormArray>(
      this.companyContractForm.get("showCombination")
    );
    this.combinationList.push(
      this.formBuilder.control(this.companyContractForm.get("assets").value)
    );
    this.companyContractForm.get("assets").reset();
  }

  removeCombination(id: any) {
    this.combinations = <FormArray>(
      this.companyContractForm.get("assetCombinationList")
    );
    this.combinationList = <FormArray>(
      this.companyContractForm.get("showCombination")
    );
    this.combinations.removeAt(id);
    this.combinationList.removeAt(id);
  }

  /* =============================== ADD UPDATE GET FUEL CARDS FUNCTIONS ================================= */

  addFuelCard() {
    let fuelCardData = {
      fuelCardId: null,
      fuelCardNumber: this.companyContractForm.value.fuelCard,
      expiry: this.datePipe.transform(
        this.companyContractForm.value.fuelExpiry,
        environment.SystemTypes_Date_Format
      ),
      limit: this.companyContractForm.value.fuelLimit,
      supplierId: this.companyContractForm.value.fuelSupp.companyId,
      customerId: this.contracts.customerId,
      status: "",
      adminCompanyId: this.contracts.adminCompanyId,
    };
    if (this.companyContractForm.value.fuelStatus == true) {
      fuelCardData.status = "Active";
    } else {
      fuelCardData.status = "Inactive";
    }
    this.backendService.addFuelCardService(fuelCardData).subscribe(
      (res) => {
        if (res.success) {
          this.fuelCards.push(res.data);
          this.companyContractForm.get("fuelCard").reset();
          this.companyContractForm.get("fuelLimit").reset();
          this.companyContractForm.get("fuelSupp").reset();
          this.companyContractForm.get("fuelExpiry").reset();
          this.companyContractForm.get("fuelStatus").setValue(false);
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("ADD FUEL CARD RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  getFuelCards() {
    this.backendService.getFuelCardService().subscribe((res) => {
      if (res.success) {
        //this.fuelCards = res.data;
      }
    });
  }

  getFuelCardsById(id) {
    this.backendService.getFuelCardByCustomerIdService(id).subscribe((res) => {
      this.fuelCards = res.data;
    });
  }

  deleteFuelCard(data: any, id) {
    this.backendService.deleteFuelCardService(data.fuelCardId).subscribe(
      (res) => {
        if (res.success) {
          this.fuelCards = this.fuelCards.filter(
            (x) => x.fuelCardId !== data.fuelCardId
          );
          this.errorMsgService.getSuccessMessage(res.message);
          this.openSuccessDialog();
        } else {
          this.errorMsgService.getErrorMessage(res.errorMessage);
          this.openErrorDialog();
        }
        //console.log("FUEL CARD delete RESPONSE", res);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 500) {
          this.errorMsgService.getInfoMessage(environment.SERVER_ERROR_MSG);
          this.openInfoDialog();
        }
      }
    );
  }

  /* =============================== DROPDOWNS FUNCTIONS ================================= */

  getContractTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      this.contractTypeDropDown = res.data;
    });
  }

  getCustomerRouteDropdown(id: string) {
    this.backendService.getRouteByCustomerService(id).subscribe((res) => {
      this.customerRouteDropDown = res.data;
    });
  }

  getRouteDropdown() {
    this.backendService.getRouteService().subscribe((res) => {
      this.routeDropDown = res.data;
    });
  }

  getCityTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      this.cityDropDown = res.data;
    });
  }

  getFuelSupplierDropdown() {
    this.backendService.getAllFilterFuelSupplier().subscribe((res) => {
      this.supplierDropDown = res.data;
    });
  }

  getCustomerDropdown() {
    this.backendService.getAllFilterCustomer().subscribe((res) => {
      this.customerDropDown = res.data;
      if (this.companyContractForm.value.contractId != "") {
        for (let i = 0; i < this.customerDropDown.length; i++) {
          if (this.customerDropDown[i].companyId == this.contracts.customerId) {
            this.companyContractForm
              .get("customer")
              .setValue(this.customerDropDown[i]);
            this.getFuelCardsById(this.contracts.customerId);
            this.getCustomerRouteDropdown(this.contracts.customerId);
          }
        }
      }
    });
  }

  getCompaniesDropdown() {
    this.backendService.getAdminCompanies().subscribe((res) => {
      this.companiesDropDown = res.data;
      this.companyContractForm.get("owner").setValue(this.companiesDropDown[0]);
      this.companyContractForm.get("owner").disable();
    });
  }

  getAssetCombinationDropdown() {
    this.backendService
      .getAllFilterAssetCombination("avail")
      .subscribe((res) => {
        this.assetDropDown = res.data;
      });
  }

  getTrailerSizeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      this.trailerSizeDropDown = res.data;
    });
  }

  selectionRoute($event: MatRadioChange) {
    // console.log($event.source.name, $event.value);
    if ($event.value === "Intercity") {
      this.getRouteDropdown();
    } else {
      this.routeDropDown = [];
    }
  }

  getTrailerTypeDropdown(type: string) {
    this.backendService.getDropdowns(type).subscribe((res) => {
      this.trailerTypeDropDown = res.data;
    });
  }

  getAssetCombinationById(id: string) {
    this.backendService.getCombinationsById(id).subscribe((res) => {
      if (res.success) {
        this.combinationList = <FormArray>(
          this.companyContractForm.get("showCombination")
        );
        this.combinationList.push(this.formBuilder.control(res.data));
      }
    });
  }

  /* =============================== COMPARE DROPDOWNS FUNCTIONS ================================= */

  compareDropdownType(o1, o2): boolean {
    if (o2 != null) return o1.value === o2.value;
  }

  compareCompany(o1, o2): boolean {
    return o1.companyId === o2.companyId;
  }

  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to trigger success dialog
  openSuccessDialog(): void {
    const dialogRef = this.dialog.open(SuccessModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: "450px",
    });

    dialogRef.afterClosed().subscribe((result) => { });
  }
}
