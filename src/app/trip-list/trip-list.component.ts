import { Component, OnInit, ViewChild } from '@angular/core';
import { BackEndService } from 'app/services/back-end.service';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from 'environments/environment';
import { MatPaginator, PageEvent, MatDialog } from '@angular/material';
import { ErrorMessageService } from 'app/services/error-message.service';
import { ErrorModalComponent } from 'app/modals/error-modal/error-modal.component';
import { InfoModalComponent } from 'app/modals/info-modal/info-modal.component';

@Component({
  selector: 'app-trip-list',
  templateUrl: './trip-list.component.html',
  styleUrls: ['./trip-list.component.scss']
})
export class TripListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  form: FormGroup;
  searchForm: FormGroup;
  jobData;
  pageEvent: PageEvent;
  page = 1;
  pageSize = 10;
  length: number;
  pageSizeOptions = [10, 20, 50];

  constructor(private backEndService: BackEndService, public datePipe: DatePipe , 
    public errorMsgService: ErrorMessageService,
    public dialog: MatDialog,) {

    this.form = new FormGroup({
      start: new FormControl('', Validators.required),
      end: new FormControl('', Validators.required)

    })

    this.searchForm = new FormGroup({
      jobId: new FormControl(null),
      rwbId: new FormControl(null),
      lic: new FormControl(null),
      start: new FormControl(null),
      end: new FormControl(null),
      jobStatus: new FormControl(null),
      // ownerShipStatus: new FormControl(null)
    })

  }

  ngOnInit() {
    this.getAllTrips(this.page, this.pageSize);
  }

  getAllTrips(pg, pgSz) {
    let params = {
      page: pg,
      size: pgSz
    }
    this.backEndService.getAllJobService(params).subscribe((res: any) => {
      this.jobData = res.data;
      this.length = res.totalRecords
    })
  }

  getServerData(val) {
    this.getAllTrips(val.pageIndex + 1, val.pageSize)
  }

  exportExcel() {
    if (this.form.valid) {
      this.form.get('start').patchValue(this.datePipe.transform(this.form.get('start').value, environment.SystemTypes_Date_Format))
      this.form.get('end').patchValue(this.datePipe.transform(this.form.get('end').value, environment.SystemTypes_Date_Format))

      this.backEndService.tripSummaryExport(this.form.get('start').value, this.form.get('end').value).subscribe((res: any) => {

        const newBlob = new Blob([(res.toString())], { type: 'application/excel' });
        var downloadURL = window.URL.createObjectURL(res);
        var link = document.createElement('a');
        link.href = downloadURL;
        var startDate = this.form.value.start;
        var endDate = this.form.value.end;
        link.download = "Trip Sheet (" + startDate + "—" + endDate + ")";
        link.click();
      })
    }
  }

  search() {
    let data = {
      jobId: this.searchForm.value.jobId,
      rwbId: this.searchForm.value.rwbId,
      lic: this.searchForm.value.lic,
      start: this.datePipe.transform(this.searchForm.value.start, environment.SystemTypes_Date_Format),
      end: this.datePipe.transform(this.searchForm.value.end, environment.SystemTypes_Date_Format),
      status: this.searchForm.value.jobStatus,
      // rental: this.form.value.ownerShipStatus
    }

    for (let prop in data) {
      if (!data[prop]) {
        delete data[prop];
      }
    }

    var queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');

    this.backEndService.jobSearchService(queryString).subscribe(res => {
      if (res.success) {
        // console.log("JOB SEARCH RESPONSE", res)
        if (res.data.length > 0) {
          this.jobData = res.data;
        } else {
          this.jobData = res.data;
          this.errorMsgService.getInfoMessage(res.message)
          this.openInfoDialog();
        }
      }
    })
  }

  ClearForm() {
    this.searchForm.reset();
    this.getAllTrips(this.page, this.pageSize);
  }
  /* =============================== DIALOG FOR ERROR INFO AND SUCCESS FUNCTIONS ================================= */

  // function to trigger error dialog
  openErrorDialog(): void {
    const dialogRef = this.dialog.open(ErrorModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // function to trigger info dialog
  openInfoDialog(): void {
    const dialogRef = this.dialog.open(InfoModalComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
