import { ViewInvoiceComponent } from "../../invoice-folder/view-invoice/view-invoice.component";
import { InvoiceComponent } from "../../invoice-folder/invoice/invoice.component";
import { AccountsComponent } from "./../../accounts/accounts.component";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "../../dashboard/dashboard.component";
import { CompaniesComponent } from "app/companies-folder/companies/companies.component";
import { CreateCompanyFormComponent } from "app/companies-folder/create-company-form/create-company-form.component";
import { EmployeesComponent } from "app/employee-folder/employees/employees.component";
import { AddEmployeeComponent } from "app/employee-folder/add-employee/add-employee.component";
import { TractorsComponent } from "app/asset-folder/vehicles/vehicles.component";
import { AddTractorComponent } from "app/asset-folder/add-vehicle/add-vehicle.component";
import { TrackerComponent } from "app/asset-folder/tracker/tracker.component";
import { ContainerComponent } from "app/asset-folder/container/container.component";
import { AddTrackerComponent } from "app/asset-folder/add-tracker/add-tracker.component";
import { AddContainerComponent } from "app/asset-folder/add-container/add-container.component";
import { AssetsCombinationComponent } from "app/asset-folder/assets-combination/assets-combination.component";
import { CompanyContractComponent } from "app/companies-contract-folder/company-contract/company-contract.component";
import { TrailerComponent } from "app/asset-folder/trailer/trailer.component";
import { AddTrailerComponent } from "app/asset-folder/add-trailer/add-trailer.component";
import { SuperAdminComponent } from "app/super-admin/super-admin.component";
import { JobComponent } from "app/job/job.component";
import { CombinationListComponent } from "app/asset-folder/combination-list/combination-list.component";
import { CompanyContractListComponent } from "app/companies-contract-folder/company-contract-list/company-contract-list.component";
import { InfoModalComponent } from "app/modals/info-modal/info-modal.component";
import { SuccessModalComponent } from "app/modals/success-modal/success-modal.component";
import { ErrorModalComponent } from "app/modals/error-modal/error-modal.component";
import { PrintPreviewModalComponent } from "./../../modals/print-preview-modal/print-preview-modal.component";
import { AddJobComponent } from "app/add-job/add-job.component";
// import { AddRwbComponent } from "app/add-rwb/add-rwb.component";
import { InputFileConfig, InputFileModule } from "ngx-input-file";
import { MaterialFileInputModule } from "ngx-material-file-input";
import { DatePipe } from "@angular/common";
// import { UpgradeComponent } from '../../upgrade/upgrade.component';
// import { MatModule } from '../../../../node_modules/ng'

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatRadioModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MAT_DATE_LOCALE,
  MatNativeDateModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MatDialogModule,
  MatPaginatorModule,
} from "@angular/material";
import { from } from "rxjs";
import { LoginComponent } from "./../../login/login.component";
import { SupplierFuelRateComponent } from "app/supplier-fuel-rate/supplier-fuel-rate.component";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { DateTimeComponent } from "app/date-time/date-time.component";
import { TripListComponent } from "app/trip-list/trip-list.component";
import { TripDetailsComponent } from "app/trip-details/trip-details.component";
import { EncrDecryptService } from "app/services/encr-decrypt.service";
import { FreightRatesComponent } from "app/freight-rates/freight-rates.component";
import { BackEndService } from "app/services/back-end.service";
import { ReimbursementComponent } from "app/Reimbursement-folder/reimbursement/reimbursement.component";
import { AddOptionTableComponent } from "app/settings-folder/add-option-table/add-option-table.component";
import { AddOptionModalComponent } from "app/modals/add-option-modal/add-option-modal.component";
import { RoutesComponent } from "app/routes/routes.component";
import { AddAreaModalComponent } from "app/modals/add-area-modal/add-area-modal.component";
import { AddCityModalComponent } from "app/modals/add-city-modal/add-city-modal.component";
import { FixedRateInvoiceComponent } from "app/invoice-folder/fixed-rate-invoice/fixed-rate-invoice.component";
import { ViewFixedRateInvoiceComponent } from "app/invoice-folder/view-fixed-rate-invoice/view-fixed-rate-invoice.component";
import { UsersAndRolesComponent } from "app/settings-folder/users-and-roles/users-and-roles.component";
import { AddRoleComponent } from "app/add-role/add-role.component";
import { ReceivePaymentModalComponent } from "app/modals/receive-payment-modal/receive-payment-modal.component";
import { ReimburseAmountModalComponent } from "app/modals/reimburse-amount-modal/reimburse-amount-modal.component";
import { ItemsComponent } from "app/Maintenance/items/items.component";
import { InventoryAdjComponent } from "app/Maintenance/inventory-adj/inventory-adj.component";
import { VendorsComponent } from "app/Maintenance/vendors/vendors.component";
import { PurchaseOrderComponent } from "app/Maintenance/purchase-order/purchase-order.component";
import { WorkOrderComponent } from "app/Maintenance/work-order/work-order.component";
import { AddWorkOrderComponent } from "app/Maintenance/add-work-order/add-work-order.component";
import { AddPurchaseOrderComponent } from "app/Maintenance/add-purchase-order/add-purchase-order.component";
import { AddVendorsComponent } from "app/Maintenance/add-vendors/add-vendors.component";
import { AddInventoryAdjComponent } from "app/Maintenance/add-inventory-adj/add-inventory-adj.component";
import { AddItemComponent } from "app/Maintenance/add-item/add-item.component";
import { UploadBulkModalComponent } from "app/modals/upload-bulk-modal/upload-bulk-modal.component";
import { CheckPendingModalComponent } from "app/modals/check-pending-modal/check-pending-modal.component";
import { EmailInvoiceModalComponent } from "app/modals/email-invoice-modal/email-invoice-modal.component";
import { PaymentInfoModalComponent } from "app/modals/payment-info-modal/payment-info-modal.component";
import { ApproveReimbursementComponent } from "app/Reimbursement-folder/approve-reimbursement/approve-reimbursement.component";
import { ViewReimbursedComponent } from "app/Reimbursement-folder/view-reimbursed/view-reimbursed.component";
import { StopsProductComponent } from "app/RoadwayBills/stops-product/stops-product.component";
import { LoadProgressComponent } from "app/RoadwayBills/load-progress/load-progress.component";
import { FinancialsComponent } from "app/RoadwayBills/financials/financials.component";
import { ProgressFormComponent } from "app/RoadwayBills/progress-form/progress-form.component";
import { RoadwayBillComponent } from "app/RoadwayBills/roadway-bill/roadway-bill.component";
import { ReimbursedDataComponent } from "app/Reimbursement-folder/reimbursed-data/reimbursed-data.component";
import { DetentionInvoiceComponent } from "app/invoice-folder/detention-invoice/detention-invoice.component";
import { ViewDetnInvoiceComponent } from "app/invoice-folder/view-detn-invoice/view-detn-invoice.component";
import { AddCategoryBrandComponent } from "app/modals/add-category-brand/add-category-brand.component";
import { InventoryListComponent } from "app/Maintenance/inventory-list/inventory-list.component";
import { BackEnd2Service } from "app/services/back-end2.service";
import { ReceivePurchaseComponent } from "app/modals/receive-purchase/receive-purchase.component";

const config: InputFileConfig = {};

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatTableModule,
    MatTabsModule,
    MatRadioModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    InputFileModule.forRoot(config),
    MaterialFileInputModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatDialogModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatPaginatorModule,
  ],
  declarations: [
    DashboardComponent,
    CompaniesComponent,
    CreateCompanyFormComponent,
    EmployeesComponent,
    AddEmployeeComponent,
    AddTractorComponent,
    TractorsComponent,
    TrackerComponent,
    ContainerComponent,
    AddContainerComponent,
    AddTrackerComponent,
    AssetsCombinationComponent,
    CompanyContractComponent,
    TrailerComponent,
    AddTrailerComponent,
    SuperAdminComponent,
    JobComponent,
    ErrorModalComponent,
    SuccessModalComponent,
    InfoModalComponent,
    AddJobComponent,
    RoadwayBillComponent,
    CombinationListComponent,
    CompanyContractListComponent,
    SupplierFuelRateComponent,
    DateTimeComponent,
    TripListComponent,
    TripDetailsComponent,
    ProgressFormComponent,
    FreightRatesComponent,
    ReimbursementComponent,
    PrintPreviewModalComponent,
    AccountsComponent,
    InvoiceComponent,
    ViewInvoiceComponent,
    AddOptionTableComponent,
    AddOptionModalComponent,
    RoutesComponent,
    AddCityModalComponent,
    AddAreaModalComponent,
    FixedRateInvoiceComponent,
    ViewFixedRateInvoiceComponent,
    ViewDetnInvoiceComponent,
    UsersAndRolesComponent,
    AddRoleComponent,
    ReceivePaymentModalComponent,
    ReceivePurchaseComponent,
    ReimburseAmountModalComponent,
    ItemsComponent,
    InventoryAdjComponent,
    VendorsComponent,
    PurchaseOrderComponent,
    WorkOrderComponent,
    AddItemComponent,
    AddInventoryAdjComponent,
    AddVendorsComponent,
    AddPurchaseOrderComponent,
    AddWorkOrderComponent,
    UploadBulkModalComponent,
    CheckPendingModalComponent,
    EmailInvoiceModalComponent,
    PaymentInfoModalComponent,
    AddCategoryBrandComponent,
    ApproveReimbursementComponent,
    ViewReimbursedComponent,
    StopsProductComponent,
    LoadProgressComponent,
    FinancialsComponent,
    ReimbursedDataComponent,
    DetentionInvoiceComponent,
    InventoryListComponent,
    // OrderBillComponent
  ],
  entryComponents: [
    ErrorModalComponent,
    SuccessModalComponent,
    InfoModalComponent,
    PrintPreviewModalComponent,
    AddOptionModalComponent,
    AddCityModalComponent,
    AddAreaModalComponent,
    ReceivePaymentModalComponent,
    ReceivePurchaseComponent,
    ReimburseAmountModalComponent,
    UploadBulkModalComponent,
    CheckPendingModalComponent,
    EmailInvoiceModalComponent,
    PaymentInfoModalComponent,
    AddCategoryBrandComponent,
  ],
  providers: [
    EncrDecryptService,
    DatePipe,
    BackEndService,
    BackEnd2Service,
    { provide: MAT_DATE_LOCALE, useValue: "en-GB" },
  ],
  bootstrap: [RoadwayBillComponent],
})
export class AdminLayoutModule {}
