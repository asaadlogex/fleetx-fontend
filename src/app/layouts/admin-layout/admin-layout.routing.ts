import { ViewInvoiceComponent } from "../../invoice-folder/view-invoice/view-invoice.component";
import { InvoiceComponent } from "../../invoice-folder/invoice/invoice.component";
import { AccountsComponent } from "./../../accounts/accounts.component";
import { Routes } from "@angular/router";

import { DashboardComponent } from "../../dashboard/dashboard.component";
import { CompaniesComponent } from "app/companies-folder/companies/companies.component";
import { CreateCompanyFormComponent } from "app/companies-folder/create-company-form/create-company-form.component";
import { EmployeesComponent } from "app/employee-folder/employees/employees.component";
import { AddEmployeeComponent } from "app/employee-folder/add-employee/add-employee.component";
import { AddTractorComponent } from "app/asset-folder/add-vehicle/add-vehicle.component";
import { TractorsComponent } from "app/asset-folder/vehicles/vehicles.component";
import { TrackerComponent } from "app/asset-folder/tracker/tracker.component";
import { ContainerComponent } from "app/asset-folder/container/container.component";
import { AddTrackerComponent } from "app/asset-folder/add-tracker/add-tracker.component";
import { AddContainerComponent } from "app/asset-folder/add-container/add-container.component";
import { AssetsCombinationComponent } from "app/asset-folder/assets-combination/assets-combination.component";
import { AddTrailerComponent } from "app/asset-folder/add-trailer/add-trailer.component";
import { TrailerComponent } from "app/asset-folder/trailer/trailer.component";
import { CompanyContractComponent } from "app/companies-contract-folder/company-contract/company-contract.component";
import { SuperAdminComponent } from "app/super-admin/super-admin.component";
import { JobComponent } from "app/job/job.component";
import { AddJobComponent } from "app/add-job/add-job.component";
import { CombinationListComponent } from "app/asset-folder/combination-list/combination-list.component";
import { CompanyContractListComponent } from "app/companies-contract-folder/company-contract-list/company-contract-list.component";
import { SupplierFuelRateComponent } from "app/supplier-fuel-rate/supplier-fuel-rate.component";
import { TripListComponent } from "app/trip-list/trip-list.component";
import { TripDetailsComponent } from "app/trip-details/trip-details.component";
import { AuthGuard } from "app/services/auth.guard";
import { FreightRatesComponent } from "app/freight-rates/freight-rates.component";
import { ReimbursementComponent } from "app/Reimbursement-folder/reimbursement/reimbursement.component";
import { AddOptionTableComponent } from "app/settings-folder/add-option-table/add-option-table.component";
import { RoutesComponent } from "app/routes/routes.component";
import { ViewFixedRateInvoiceComponent } from "app/invoice-folder/view-fixed-rate-invoice/view-fixed-rate-invoice.component";
import { UsersAndRolesComponent } from "app/settings-folder/users-and-roles/users-and-roles.component";
import { AddRoleComponent } from "app/add-role/add-role.component";
import { ItemsComponent } from "app/Maintenance/items/items.component";
import { InventoryAdjComponent } from "app/Maintenance/inventory-adj/inventory-adj.component";
import { VendorsComponent } from "app/Maintenance/vendors/vendors.component";
import { PurchaseOrderComponent } from "app/Maintenance/purchase-order/purchase-order.component";
import { WorkOrderComponent } from "app/Maintenance/work-order/work-order.component";
import { AddItemComponent } from "app/Maintenance/add-item/add-item.component";
import { AddInventoryAdjComponent } from "app/Maintenance/add-inventory-adj/add-inventory-adj.component";
import { AddVendorsComponent } from "app/Maintenance/add-vendors/add-vendors.component";
import { AddPurchaseOrderComponent } from "app/Maintenance/add-purchase-order/add-purchase-order.component";
import { AddWorkOrderComponent } from "app/Maintenance/add-work-order/add-work-order.component";
import { ApproveReimbursementComponent } from "app/Reimbursement-folder/approve-reimbursement/approve-reimbursement.component";
import { ViewReimbursedComponent } from "app/Reimbursement-folder/view-reimbursed/view-reimbursed.component";
import { RoadwayBillComponent } from "app/RoadwayBills/roadway-bill/roadway-bill.component";
import { ViewDetnInvoiceComponent } from "app/invoice-folder/view-detn-invoice/view-detn-invoice.component";
import { InventoryListComponent } from "app/Maintenance/inventory-list/inventory-list.component";
import { OrderBillComponent } from "app/Orders/order-bill/order-bill.component";

export const AdminLayoutRoutes: Routes = [
  {
    path: "job/addRWB",
    component: RoadwayBillComponent,
    children: [
      {
        path: "job/addRWB",
        loadChildren: () =>
          import("app/Orders/order/order.module").then(
            (m) => m.OrderModule
          ),
      },
    ],
  },
  {
    path: "job/editRWB",
    component: RoadwayBillComponent,
    children: [
      {
        path: "job/editRWB",
        loadChildren: () =>
          import("app/Orders/order/order.module").then(
            (m) => m.OrderModule
          ),
      },
    ],
  },
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "companies",
    component: CompaniesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "companies/create",
    component: CreateCompanyFormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "companies/edit",
    component: CreateCompanyFormComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "employees",
    component: EmployeesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "employees/add",
    component: AddEmployeeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "employees/edit",
    component: AddEmployeeComponent,
    canActivate: [AuthGuard],
  },

  { path: "tractors", component: TractorsComponent, canActivate: [AuthGuard] },
  {
    path: "tractors/add",
    component: AddTractorComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "tractors/edit",
    component: AddTractorComponent,
    canActivate: [AuthGuard],
  },

  { path: "tracker", component: TrackerComponent, canActivate: [AuthGuard] },
  {
    path: "tracker/add",
    component: AddTrackerComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "tracker/edit",
    component: AddTrackerComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "container",
    component: ContainerComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "container/add",
    component: AddContainerComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "container/edit",
    component: AddContainerComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "assetsCombination",
    component: CombinationListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "assetsCombination/add",
    component: AssetsCombinationComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "assetsCombination/edit",
    component: AssetsCombinationComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "companyContract",
    component: CompanyContractListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "companyContract/add",
    component: CompanyContractComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "companyContract/edit",
    component: CompanyContractComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "superAdmin",
    component: SuperAdminComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "fuelRate",
    component: SupplierFuelRateComponent,
    canActivate: [AuthGuard],
  },

  { path: "trailer", component: TrailerComponent, canActivate: [AuthGuard] },
  {
    path: "trailer/add",
    component: AddTrailerComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "trailer/edit",
    component: AddTrailerComponent,
    canActivate: [AuthGuard],
  },

  { path: "job", component: JobComponent, canActivate: [AuthGuard] },
  { path: "job/add", component: AddJobComponent, canActivate: [AuthGuard] },
  { path: "job/edit", component: AddJobComponent, canActivate: [AuthGuard] },
  // {
  //   path: "job/addRWB",
  //   component: RoadwayBillComponent,
  //   canActivate: [AuthGuard],
  // },
  // {
  //   path: "job/editRWB",
  //   component: RoadwayBillComponent,
  //   canActivate: [AuthGuard],
  // },

  { path: "trips", component: TripListComponent, canActivate: [AuthGuard] },
  {
    path: "trips/details/:id",
    component: TripDetailsComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "freightRates",
    component: FreightRatesComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "reimbursement",
    component: ReimbursementComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "reimbursement/approve",
    component: ApproveReimbursementComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "reimbursement/view",
    component: ViewReimbursedComponent,
    canActivate: [AuthGuard],
  },

  { path: "accounts", component: AccountsComponent, canActivate: [AuthGuard] },

  { path: "invoice", component: InvoiceComponent, canActivate: [AuthGuard] },
  {
    path: "viewInvoice",
    component: ViewInvoiceComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "viewDetentionInvoice",
    component: ViewDetnInvoiceComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "viewFixedInvoice",
    component: ViewFixedRateInvoiceComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "addOptions",
    component: AddOptionTableComponent,
    canActivate: [AuthGuard],
  },
  { path: "routes", component: RoutesComponent, canActivate: [AuthGuard] },
  {
    path: "users&roles",
    component: UsersAndRolesComponent,
    canActivate: [AuthGuard],
  },
  { path: "addRole", component: AddRoleComponent, canActivate: [AuthGuard] },

  { path: "items", component: ItemsComponent, canActivate: [AuthGuard] },
  { path: "items/add", component: AddItemComponent, canActivate: [AuthGuard] },
  { path: "items/edit", component: AddItemComponent, canActivate: [AuthGuard] },
  {
    path: "inventories",
    component: InventoryListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "inventoryAdj",
    component: InventoryAdjComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "inventoryAdj/add",
    component: AddInventoryAdjComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "inventoryAdj/edit",
    component: AddInventoryAdjComponent,
    canActivate: [AuthGuard],
  },
  { path: "vendors", component: VendorsComponent, canActivate: [AuthGuard] },
  {
    path: "vendors/add",
    component: AddVendorsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "purchaseOrder",
    component: PurchaseOrderComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "purchaseOrder/add",
    component: AddPurchaseOrderComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "purchaseOrder/edit",
    component: AddPurchaseOrderComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "workOrder",
    component: WorkOrderComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "workOrder/add",
    component: AddWorkOrderComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "workOrder/edit",
    component: AddWorkOrderComponent,
    canActivate: [AuthGuard],
  },
  // { path: 'login', component: LoginComponent },
];
