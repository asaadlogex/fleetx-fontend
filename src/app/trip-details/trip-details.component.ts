import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { BackEndService } from 'app/services/back-end.service';

@Component({
  selector: 'app-trip-details',
  templateUrl: './trip-details.component.html',
  styleUrls: ['./trip-details.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TripDetailsComponent implements OnInit {

  objectKeys = Object.keys;
  columnsToDisplay = ['stopProgressType.value', 'roadwayBillId'];
  columnsHeader = ['stop', 'rwbId', 'location', 'address', 'ATA', 'ATD', 'detention', 'distance']



  expandedElement: ProgressTable | null;
  ownershipStatus = 'rental'
  jSummary;
  sub;

  constructor(private route: ActivatedRoute, private backEndService: BackEndService) {
    //console.log(this.route.snapshot.params['id'])
    let id = this.route.snapshot.params['id'];
   this.sub = this.backEndService.tripDetails(id).subscribe((res: any) => {
      // console.log(res)
      this.jSummary = res.data;
      // console.log("job summary", this.jSummary)
    })
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}

export interface ProgressTable {
  stop: number;
  rwbId: string;
  location: string;
  address: string;
  ATA: string;
  ATD: string;
  detention: string;
  distance: number;
  tractor: string;
  trailer: string;
  driver1: string;
  driver2: string;
};
